<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Demographic Question Set Key
    |--------------------------------------------------------------------------
    |
    | The demographics form is built using questions and translations
    | from the question set database. This setting controls which
    | question set to use for the demographics form.
    |
    */

    'demographic_question_set_key' => 'rtw-demographics',


    /*
    |--------------------------------------------------------------------------
    | Question Form name prefix
    |--------------------------------------------------------------------------
    |
    | On the question forms each form field name is the ID of the question
    | prefixed with the below
    |
    */

    'form_name_prefix' => 'question-',


];
