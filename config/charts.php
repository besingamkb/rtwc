<?php

return [

    'phantomjs' => storage_path('charts/phantomjs/phantomjs'),

    'highcharts_convert' => storage_path('charts/phantomjs/highcharts-convert.js'),

    'constr' => 'Chart',

];
