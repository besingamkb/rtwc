<?php

use App\Models\Account;
use Illuminate\Database\Seeder;

class AccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $accounts = array(
            [
                'name' => 'So Technology Ltd',
            ],
        );

        // Loop through each user above and create the record for them in the database.
        foreach ($accounts as $account) {
            Account::create($account);
        }
    }
}
