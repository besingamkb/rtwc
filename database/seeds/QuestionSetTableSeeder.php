<?php

use Illuminate\Database\Seeder;
use App\Models\QuestionSet;

class QuestionSetTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //seed your question sets here
        $questionSets = array(
            [
                'key' => 'rtw-demographics',
                'name' => 'Responsible Trade Worldwide Demographics',
                'welcome_text' => '',
                'intro_text' => '',
                'type' => 'rtw-demographics',
                'status' => 2
            ],
            [
                'key' => 'rtw-jl',
                'name' => 'Responsible Trade Worldwide (John Lewis)',
                'welcome_text' => 'Hello and Welcome to Responsible Trade Worldwide - John Lewis',
                'intro_text' => '<p>We are delighted to inform you that we are carrying out an independent survey on behalf of your organisation.</p>

    <p>Our questionnaire should take no more than 10 minutes to complete. Any information you provide will remain both confidential and anonymous.</p>

    <p>Please read each statement and indicate the extent to which you agree or disagree by ticking one of the choices.</p>',
                'finished_text' => '<h2>Thank you</h2>
<p>Thank you for completing the assessment. Your view and account of working for your organisation has been helpful in the assessment.</p>',
                'type' => 'rtw',
                'status' => 2
            ],
            [
                'key' => 'rtw-employer',
                'name' => 'Responsible Trade Worldwide Employer',
                'welcome_text' => '',
                'intro_text' => '<p>Welcome to the Responsible Trade Worldwide Employer Metrics Assessment. Please complete the following fields, providing demographic information about your organisation. If you have any technical issues while completing the survey, please email rtwassess@reddyco.com</p>',
                'finished_text' => '',
                'type' => 'rtw-employer',
                'status' => 2
            ],
            [
                'key' => 'rtw',
                'name' => 'Responsible Trade Worldwide',
                'welcome_text' => 'Hello and Welcome to Responsible Trade Worldwide',
                'intro_text' => '<p>We are delighted to inform you that we are carrying out an independent survey on behalf of your organisation.</p>

    <p>Our questionnaire should take no more than 10 minutes to complete. Any information you provide will remain both confidential and anonymous.</p>

    <p>Please read each statement and indicate the extent to which you agree or disagree by ticking one of the choices.</p>',
                'finished_text' => '<h2>Thank you</h2>
<p>Thank you for completing the assessment. Your view and account of working for your organisation has been helpful in the assessment.</p>',
                'type' => 'rtw',
                'status' => 1
            ],
        );

        foreach ($questionSets as $questionSet) {
            QuestionSet::create($questionSet);
        }
    }
}
