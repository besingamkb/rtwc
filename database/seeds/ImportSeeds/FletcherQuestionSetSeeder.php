<?php

use Illuminate\Database\Seeder;

class FletcherQuestionSetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Delete the original baseline question set.
        \App\Models\QuestionSet::where(['key' => 'rtw-fletcher'])->delete();

        // Store the csvs.
        $csv = \League\Csv\Reader::createFromPath(storage_path('imports/fletchers_question_set.csv'));
        $csvQuestions = $csv->setOffset(0)->fetchAll(); // Set offset to skip header.

        $csv = \League\Csv\Reader::createFromPath(storage_path('imports/baseline_question_set_text.csv'));
        $csvQuestionSetText = $csv->setOffset(0)->fetchAll(); // Set offset to skip header.

        // Add the new question set.
        $questionSet = \App\Models\QuestionSet::forceCreate([
            'key' => 'rtw-fletcher',
            'name' => 'Responsible Trade Worldwide (Fletcher)',
            'type' => 'rtw',
            'status' => \App\Models\QuestionSet\Status::DISABLED,
            'max_score' => 29
        ]);

        // Add the translations for the question set.
        foreach (array_keys($csvQuestionSetText[0]) as $csvColumnIndex) {
            $locale = '';

            switch ($csvColumnIndex) {
                case 1:
                    $locale = 'en';
                    break;
                case 2:
                    $locale = 'ar';
                    break;
                case 3:
                    $locale = 'zh';
                    break;
                case 4:
                    $locale = 'fa';
                    break;
                case 5:
                    $locale = 'de';
                    break;
                case 6:
                    $locale = 'gu';
                    break;
                case 7:
                    $locale = 'hi';
                    break;
                case 8:
                    $locale = 'lt';
                    break;
                case 9:
                    $locale = 'pl';
                    break;
                case 10:
                    $locale = 'pt';
                    break;
                case 11:
                    $locale = 'pa';
                    break;
                case 12:
                    $locale = 'ro';
                    break;
                case 13:
                    $locale = 'ru';
                    break;
                case 14:
                    $locale = 'es';
                    break;
                case 15:
                    $locale = 'ta';
                    break;
                default:
            }

            if (!$locale) {
                continue;
            }

            $questionSetTranslation = $questionSet->getNewTranslation($locale);
            $questionSetTranslation->question_set_id = $questionSet->id;
            $questionSetTranslation->welcome_text = $csvQuestionSetText[13][$csvColumnIndex];
            $questionSetTranslation->intro_text = $csvQuestionSetText[1][$csvColumnIndex];
            $questionSetTranslation->finished_text = $csvQuestionSetText[2][$csvColumnIndex];
            $questionSetTranslation->save();
        }

        /// Add the questions to the question set.
        foreach ($csvQuestions as $csvQuestionKey => $csvQuestionRow) {
            if ($csvQuestionKey == 0) {
                continue;
            }

            $question = \App\Models\Question::forceCreate([
                'question_set_id' => $questionSet->id,
                'category_id' => \App\Models\QuestionCategory::where(['key' => $csvQuestionRow[0]])->first()->id,
                'order' => $csvQuestionKey + 1,
                'key' => $csvQuestionRow[1],
                'max_score' => 1,
                'type_id' => 5
            ]);

            // Iterate over all translations in csv.
            // We go over the spreadsheet in a horizontal fashion to work out the text for each language.
            // See http://www.science.co.il/Language/Locale-codes.php for all language codes.
            foreach (array_keys($csvQuestionRow) as $csvColumnIndex) {
                $locale = '';

                switch ($csvColumnIndex) {
                    case 2:
                        $locale = 'en';
                        break;
                    case 3:
                        $locale = 'ar';
                        break;
                    case 4:
                        $locale = 'zh';
                        break;
                    case 5:
                        $locale = 'fa';
                        break;
                    case 6:
                        $locale = 'de';
                        break;
                    case 7:
                        $locale = 'gu';
                        break;
                    case 8:
                        $locale = 'hi';
                        break;
                    case 9:
                        $locale = 'lt';
                        break;
                    case 10:
                        $locale = 'pl';
                        break;
                    case 11:
                        $locale = 'pt';
                        break;
                    case 12:
                        $locale = 'pa';
                        break;
                    case 13:
                        $locale = 'ro';
                        break;
                    case 14:
                        $locale = 'ru';
                        break;
                    case 15:
                        $locale = 'es';
                        break;
                    case 16:
                        $locale = 'ta';
                        break;
                    default:
                        break;
                }

                if (!$locale) {
                    continue;
                }

                // Add question translation text.
                $questionTranslation = $question->getNewTranslation($locale);
                $questionTranslation->question_id = $question->id;
                $questionTranslation->text = $csvQuestionRow[$csvColumnIndex];
                $questionTranslation->save();
            }

            // Add question options for the questions.
            foreach ($csvQuestionSetText as $csvQuestionSetTextKey => $csvQuestionSetTextRow) {
                $option = null;
                $count = 0;

                switch ($csvQuestionSetTextKey) {
                    case 8:
                    case 9:
                    case 10:
                    case 11:
                    case 12:
                        $count++;
                        $option = \App\Models\QuestionOption::forceCreate([
                            'question_id' => $question->id,
                            'order' => $csvQuestionSetTextKey - 7
                        ]);
                        break;
                    default:
                }

                if (!$option) {
                    continue;
                }

                // Create the translation for the question option that has been created.
                foreach (array_keys($csvQuestionSetTextRow) as $csvColumnIndex) {
                    $locale = '';

                    switch ($csvColumnIndex) {
                        case 1:
                            $locale = 'en';
                            break;
                        case 2:
                            $locale = 'ar';
                            break;
                        case 3:
                            $locale = 'zh';
                            break;
                        case 4:
                            $locale = 'fa';
                            break;
                        case 5:
                            $locale = 'de';
                            break;
                        case 6:
                            $locale = 'gu';
                            break;
                        case 7:
                            $locale = 'hi';
                            break;
                        case 8:
                            $locale = 'lt';
                            break;
                        case 9:
                            $locale = 'pl';
                            break;
                        case 10:
                            $locale = 'pt';
                            break;
                        case 11:
                            $locale = 'pa';
                            break;
                        case 12:
                            $locale = 'ro';
                            break;
                        case 13:
                            $locale = 'ru';
                            break;
                        case 14:
                            $locale = 'es';
                            break;
                        case 15:
                            $locale = 'ta';
                            break;
                        default:
                    }

                    if (!$locale) {
                        continue;
                    }

                    $optionTranslation = $option->getNewTranslation($locale);
                    $optionTranslation->question_option_id = $option->id;
                    $optionTranslation->text = $csvQuestionSetTextRow[$csvColumnIndex];
                    $optionTranslation->save();
                }
            }
        }
    }
}
