<?php

use Illuminate\Database\Seeder;

class BaselineQuestionSetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Delete the original baseline question set.
        \App\Models\QuestionSet::where(['key' => 'rtw'])->delete();

        // Store the csvs.
        $csv = \League\Csv\Reader::createFromPath(storage_path('imports/baseline_question_set.csv'));
        $csvQuestions = $csv->setOffset(0)->fetchAll(); // Set offset to skip header.

        $csv = \League\Csv\Reader::createFromPath(storage_path('imports/baseline_question_set_text.csv'));
        $csvQuestionSetText = $csv->setOffset(0)->fetchAll(); // Set offset to skip header.

        $csv = \League\Csv\Reader::createFromPath(storage_path('imports/baseline_demographics.csv'));
        $csvDemographics = $csv->setOffset(0)->fetchAll(); // Set offset to skip header.

        $csv = \League\Csv\Reader::createFromPath(storage_path('imports/baseline_demographic_options.csv'));
        $csvDemographicOptions = $csv->setOffset(0)->fetchAll(); // Set offset to skip header.

        // Add the new question set.
        $questionSet = \App\Models\QuestionSet::forceCreate([
            'key' => 'rtw',
            'name' => 'Responsible Trade Worldwide',
            'type' => 'rtw',
            'status' => \App\Models\QuestionSet\Status::DISABLED,
            'max_score' => 29
        ]);

        // Add the translations for the question set.
        foreach (array_keys($csvQuestionSetText[0]) as $csvColumnIndex) {
            $locale = '';

            switch ($csvColumnIndex) {
                case 1:
                    $locale = 'en';
                    break;
                case 2:
                    $locale = 'ar';
                    break;
                case 3:
                    $locale = 'zh';
                    break;
                case 4:
                    $locale = 'fa';
                    break;
                case 5:
                    $locale = 'de';
                    break;
                case 6:
                    $locale = 'gu';
                    break;
                case 7:
                    $locale = 'hi';
                    break;
                case 8:
                    $locale = 'lt';
                    break;
                case 9:
                    $locale = 'pl';
                    break;
                case 10:
                    $locale = 'pt';
                    break;
                case 11:
                    $locale = 'pa';
                    break;
                case 12:
                    $locale = 'ro';
                    break;
                case 13:
                    $locale = 'ru';
                    break;
                case 14:
                    $locale = 'es';
                    break;
                case 15:
                    $locale = 'ta';
                    break;
                default:
            }

            if (!$locale) {
                continue;
            }

            $questionSetTranslation = $questionSet->getNewTranslation($locale);
            $questionSetTranslation->question_set_id = $questionSet->id;
            $questionSetTranslation->welcome_text = $csvQuestionSetText[13][$csvColumnIndex];
            $questionSetTranslation->intro_text = $csvQuestionSetText[1][$csvColumnIndex];
            $questionSetTranslation->finished_text = $csvQuestionSetText[2][$csvColumnIndex];
            $questionSetTranslation->save();
        }

        // Add new translations for demographic questions.
        $demographicOptions = [
            'gender' => \App\Models\Question::where(['key' => 'gender'])->first(),
            'age' => \App\Models\Question::where(['key' => 'age'])->first(),
            'employsyou' => \App\Models\Question::where(['key' => 'employsyou'])->first(),
            'level' => \App\Models\Question::where(['key' => 'level'])->first(),
            'role' => \App\Models\Question::where(['key' => 'role'])->first(),
            'shift' => \App\Models\Question::where(['key' => 'shift'])->first(),
            'howlong' => \App\Models\Question::where(['key' => 'howlong'])->first(),
            'ethnicity' => \App\Models\Question::where(['key' => 'ethnicity'])->first()
        ];

        foreach ($csvDemographics as $csvDemographicKey => $csvDemographicRow) {
            foreach (array_keys($csvDemographicRow) as $csvColumnIndex) {
                $locale = '';

                switch ($csvColumnIndex) {
                    case 0:
                        $locale = 'en';
                        break;
                    case 1:
                        $locale = 'ar';
                        break;
                    case 2:
                        $locale = 'zh';
                        break;
                    case 3:
                        $locale = 'fa';
                        break;
                    case 4:
                        $locale = 'de';
                        break;
                    case 5:
                        $locale = 'gu';
                        break;
                    case 6:
                        $locale = 'hi';
                        break;
                    case 7:
                        $locale = 'lt';
                        break;
                    case 8:
                        $locale = 'pl';
                        break;
                    case 9:
                        $locale = 'pt';
                        break;
                    case 10:
                        $locale = 'pa';
                        break;
                    case 11:
                        $locale = 'ro';
                        break;
                    case 12:
                        $locale = 'ru';
                        break;
                    case 13:
                        $locale = 'es';
                        break;
                    case 14:
                        $locale = 'ta';
                        break;
                    default:
                }

                if (!$locale) {
                    continue;
                }

                switch ($csvDemographicKey) {
                    case 0:
                        if ($demographicOptions['gender']->hasTranslation($locale)) {
                            continue;
                        }

                        $genderTranslation = $demographicOptions['gender']->getNewTranslation($locale);
                        $genderTranslation->question_id = $demographicOptions['gender']->id;
                        $genderTranslation->text = $csvDemographicRow[$csvColumnIndex];
                        $genderTranslation->save();
                        break;
                    case 1:
                        if ($demographicOptions['age']->hasTranslation($locale)) {
                            continue;
                        }

                        $ageTranslation = $demographicOptions['age']->getNewTranslation($locale);
                        $ageTranslation->question_id = $demographicOptions['age']->id;
                        $ageTranslation->text = $csvDemographicRow[$csvColumnIndex];
                        $ageTranslation->save();
                        break;
                    case 2:
                        if ($demographicOptions['employsyou']->hasTranslation($locale)) {
                            continue;
                        }

                        $employsyouTranslation = $demographicOptions['employsyou']->getNewTranslation($locale);
                        $employsyouTranslation->question_id = $demographicOptions['employsyou']->id;
                        $employsyouTranslation->text = $csvDemographicRow[$csvColumnIndex];
                        $employsyouTranslation->save();
                        break;
                    case 3:
                        if ($demographicOptions['level']->hasTranslation($locale)) {
                            continue;
                        }

                        $levelTranslation = $demographicOptions['level']->getNewTranslation($locale);
                        $levelTranslation->question_id = $demographicOptions['level']->id;
                        $levelTranslation->text = $csvDemographicRow[$csvColumnIndex];
                        $levelTranslation->save();
                        break;
                    case 4:
                        if ($demographicOptions['role']->hasTranslation($locale)) {
                            continue;
                        }

                        $roleTranslation = $demographicOptions['role']->getNewTranslation($locale);
                        $roleTranslation->question_id = $demographicOptions['role']->id;
                        $roleTranslation->text = $csvDemographicRow[$csvColumnIndex];
                        $roleTranslation->save();
                        break;
                    case 5:
                        if ($demographicOptions['shift']->hasTranslation($locale)) {
                            continue;
                        }

                        $shiftTranslation = $demographicOptions['shift']->getNewTranslation($locale);
                        $shiftTranslation->question_id = $demographicOptions['shift']->id;
                        $shiftTranslation->text = $csvDemographicRow[$csvColumnIndex];
                        $shiftTranslation->save();
                        break;
                    case 6:
                        if ($demographicOptions['howlong']->hasTranslation($locale)) {
                            continue;
                        }

                        $howlongTranslation = $demographicOptions['howlong']->getNewTranslation($locale);
                        $howlongTranslation->question_id = $demographicOptions['howlong']->id;
                        $howlongTranslation->text = $csvDemographicRow[$csvColumnIndex];
                        $howlongTranslation->save();
                        break;
                    case 7:
                        if ($demographicOptions['ethnicity']->hasTranslation($locale)) {
                            continue;
                        }

                        $ethnicityTranslation = $demographicOptions['ethnicity']->getNewTranslation($locale);
                        $ethnicityTranslation->question_id = $demographicOptions['ethnicity']->id;
                        $ethnicityTranslation->text = $csvDemographicRow[$csvColumnIndex];
                        $ethnicityTranslation->save();
                        break;
                    default:
                }
            }
        }

        foreach ($demographicOptions as $demographicOption) {
            foreach ($demographicOption->options_ as $option) {
                foreach ($csvDemographicOptions as $csvDemographicOptionKey => $csvDemographicOptionRow) {
                    foreach (array_keys($csvDemographicOptionRow) as $csvColumnIndex) {
                        $locale = '';

                        switch ($csvColumnIndex) {
                            case 0:
                                $locale = 'en';
                                break;
                            case 1:
                                $locale = 'ar';
                                break;
                            case 2:
                                $locale = 'zh';
                                break;
                            case 3:
                                $locale = 'fa';
                                break;
                            case 4:
                                $locale = 'de';
                                break;
                            case 5:
                                $locale = 'gu';
                                break;
                            case 6:
                                $locale = 'hi';
                                break;
                            case 7:
                                $locale = 'lt';
                                break;
                            case 8:
                                $locale = 'pl';
                                break;
                            case 9:
                                $locale = 'pt';
                                break;
                            case 10:
                                $locale = 'pa';
                                break;
                            case 11:
                                $locale = 'ro';
                                break;
                            case 12:
                                $locale = 'ru';
                                break;
                            case 13:
                                $locale = 'es';
                                break;
                            case 14:
                                $locale = 'ta';
                                break;
                            default:
                        }

                        if (!$locale) {
                            continue;
                        }

                        if ($option->hasTranslation($locale)) {
                            continue;
                        }

                        $optionTranslation = $option->getNewTranslation($locale);
                        $optionTranslation->question_option_id = $option->id;
                        $optionTranslation->text = $csvDemographicOptionRow[$csvColumnIndex];
                        $optionTranslation->save();
                    }
                }
            }
        }

        /// Add the questions to the question set.
        foreach ($csvQuestions as $csvQuestionKey => $csvQuestionRow) {
            if ($csvQuestionKey == 0) {
                continue;
            }

            $question = \App\Models\Question::forceCreate([
                'question_set_id' => $questionSet->id,
                'category_id' => \App\Models\QuestionCategory::where(['key' => $csvQuestionRow[0]])->first()->id,
                'order' => $csvQuestionKey + 1,
                'key' => $csvQuestionRow[1],
                'max_score' => 1,
                'type_id' => 5
            ]);

            // Iterate over all translations in csv.
            // We go over the spreadsheet in a horizontal fashion to work out the text for each language.
            // See http://www.science.co.il/Language/Locale-codes.php for all language codes.
            foreach (array_keys($csvQuestionRow) as $csvColumnIndex) {
                $locale = '';

                switch ($csvColumnIndex) {
                    case 2:
                        $locale = 'en';
                        break;
                    case 3:
                        $locale = 'ar';
                        break;
                    case 4:
                        $locale = 'zh';
                        break;
                    case 5:
                        $locale = 'fa';
                        break;
                    case 6:
                        $locale = 'de';
                        break;
                    case 7:
                        $locale = 'gu';
                        break;
                    case 8:
                        $locale = 'hi';
                        break;
                    case 9:
                        $locale = 'lt';
                        break;
                    case 10:
                        $locale = 'pl';
                        break;
                    case 11:
                        $locale = 'pt';
                        break;
                    case 12:
                        $locale = 'pa';
                        break;
                    case 13:
                        $locale = 'ro';
                        break;
                    case 14:
                        $locale = 'ru';
                        break;
                    case 15:
                        $locale = 'es';
                        break;
                    case 16:
                        $locale = 'ta';
                        break;
                    default:
                        break;
                }

                if (!$locale) {
                    continue;
                }

                // Add question translation text.
                $questionTranslation = $question->getNewTranslation($locale);
                $questionTranslation->question_id = $question->id;
                $questionTranslation->text = $csvQuestionRow[$csvColumnIndex];
                $questionTranslation->save();
            }

            // Add question options for the questions.
            foreach ($csvQuestionSetText as $csvQuestionSetTextKey => $csvQuestionSetTextRow) {
                $option = null;
                $count = 0;

                switch ($csvQuestionSetTextKey) {
                    case 8:
                    case 9:
                    case 10:
                    case 11:
                    case 12:
                        $count++;
                        $option = \App\Models\QuestionOption::forceCreate([
                            'question_id' => $question->id,
                            'order' => $csvQuestionSetTextKey - 7
                        ]);
                        break;
                    default:
                }

                if (!$option) {
                    continue;
                }

                // Create the translation for the question option that has been created.
                foreach (array_keys($csvQuestionSetTextRow) as $csvColumnIndex) {
                    $locale = '';

                    switch ($csvColumnIndex) {
                        case 1:
                            $locale = 'en';
                            break;
                        case 2:
                            $locale = 'ar';
                            break;
                        case 3:
                            $locale = 'zh';
                            break;
                        case 4:
                            $locale = 'fa';
                            break;
                        case 5:
                            $locale = 'de';
                            break;
                        case 6:
                            $locale = 'gu';
                            break;
                        case 7:
                            $locale = 'hi';
                            break;
                        case 8:
                            $locale = 'lt';
                            break;
                        case 9:
                            $locale = 'pl';
                            break;
                        case 10:
                            $locale = 'pt';
                            break;
                        case 11:
                            $locale = 'pa';
                            break;
                        case 12:
                            $locale = 'ro';
                            break;
                        case 13:
                            $locale = 'ru';
                            break;
                        case 14:
                            $locale = 'es';
                            break;
                        case 15:
                            $locale = 'ta';
                            break;
                        default:
                    }

                    if (!$locale) {
                        continue;
                    }

                    $optionTranslation = $option->getNewTranslation($locale);
                    $optionTranslation->question_option_id = $option->id;
                    $optionTranslation->text = $csvQuestionSetTextRow[$csvColumnIndex];
                    $optionTranslation->save();
                }
            }
        }
    }
}
