<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'Steven Oddy',
                'username' => 'sotech',
                'email' => 'steven.oddy@sotechnology.co.uk',
                'password' => bcrypt('abcd1234')
            ],
            [
                'name' => 'Brad Bird',
                'username' => 'brad.bird',
                'email' => 'brad.bird@sotechnology.co.uk',
                'password' => bcrypt('abcd1234')
            ],
            [
                'name' => 'Test User',
                'username' => 'test.user.1',
                'email' => 'test.user.1@example.com',
                'password' => bcrypt('abcd1234')
            ],
            [
                'name' => 'Test User',
                'username' => 'test.user.2',
                'email' => 'test.user.2@example.com',
                'password' => bcrypt('abcd1234')
            ]
        ];

        // Loop through each user above and create the record for them in the database.
        foreach ($users as $user) {
            \App\Models\User::create($user);
        }

        // Add users to roles.
        $adminRole = \App\Models\Entrust\Role::where('name', 'admin')->first();
        $userRole = \App\Models\Entrust\Role::where('name', 'user')->first();

        $adminUser1 = \App\Models\User::where('username', 'sotech')->first();
        $adminUser2 = \App\Models\User::where('username', 'brad.bird')->first();

        $adminUser1->attachRole($adminRole);
        $adminUser2->attachRole($adminRole);

        $testUser1 = \App\Models\User::where('username', 'test.user.1')->first();
        $testUser2 = \App\Models\User::where('username', 'test.user.2')->first();

        $testUser1->attachRole($userRole);
        $testUser2->attachRole($userRole);

        // Attach the users to an account
        $account = \App\Models\Account::first();

        $adminUser1->account()->associate($account)->save();
        $adminUser2->account()->associate($account)->save();
        $testUser1->account()->associate($account)->save();
        $testUser2->account()->associate($account)->save();
    }
}
