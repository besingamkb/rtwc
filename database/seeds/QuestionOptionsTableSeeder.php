<?php

use Illuminate\Database\Seeder;

class QuestionOptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // demographics option
        

        $questionOptions = array(
            [
                'question-key' => 'gender', 
                'text' => 'Male',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'ذكر'],
                    ['locale' => 'es', 'text' => 'Hombre'],
                    ['locale' => 'pt', 'text' => 'Masculino'],
                    ['locale' => 'pl', 'text' => 'Mężczyzna'],
                    ['locale' => 'lt', 'text' => 'Vyras'],
                    ['locale' => 'pa', 'text' => 'ਮਰਦ'],
                ]
            ],
            [
                'question-key' => 'gender', 
                'text' => 'Female',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'أنثى'],
                    ['locale' => 'es', 'text' => 'Mujer'],
                    ['locale' => 'pt', 'text' => 'Feminino'],
                    ['locale' => 'pl', 'text' => 'Kobieta'],
                    ['locale' => 'lt', 'text' => 'Moteris'],
                    ['locale' => 'pa', 'text' => 'ਔਰਤ'],
                ]
            ],
            [
                'question-key' => 'age', 
                'text' => '15 years or younger',
                'translations' => [
                    ['locale' => 'ar', 'text' => '15 عامًا أو أصغر'],
                    ['locale' => 'es', 'text' => '15 años o menos'],
                    ['locale' => 'pt', 'text' => '15 anos ou menos'],
                    ['locale' => 'pl', 'text' => '15 lat lub poniżej'],
                    ['locale' => 'lt', 'text' => '15 metų ar mažiau'],
                    ['locale' => 'pa', 'text' => '15 ਸਾਲ ਜਾਂ ਛੋਟਾ'],
                ]
            ],
            [
                'question-key' => 'age', 
                'text' => '16-18 years',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'من 16 إلى 18 عامًا'],
                    ['locale' => 'es', 'text' => '16-18 años'],
                    ['locale' => 'pt', 'text' => '16-18 anos'],
                    ['locale' => 'pl', 'text' => '16-18 lat'],
                    ['locale' => 'lt', 'text' => '16-18 metų'],
                    ['locale' => 'pa', 'text' => '16-18 ਸਾਲ'],
                ]
            ],
            [
                'question-key' => 'age', 
                'text' => '19-25 years',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'من 19 إلى 25 عامًا'],
                    ['locale' => 'es', 'text' => '19-25 años'],
                    ['locale' => 'pt', 'text' => '19-25 anos'],
                    ['locale' => 'pl', 'text' => '19-25 lat'],
                    ['locale' => 'lt', 'text' => '19-25 metų'],
                    ['locale' => 'pa', 'text' => '19-25 ਸਾਲ'],
                ]
            ],
            [
                'question-key' => 'age', 
                'text' => '26-35 years',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'من 26 إلى 35 عامًا'],
                    ['locale' => 'es', 'text' => '26-35 años'],
                    ['locale' => 'pt', 'text' => '26-35 anos'],
                    ['locale' => 'pl', 'text' => '26-35 lat'],
                    ['locale' => 'lt', 'text' => '26-35 metų'],
                    ['locale' => 'pa', 'text' => '26-35 ਸਾਲ'],
                ]
            ],
            [
                'question-key' => 'age', 
                'text' => '36-45 years',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'من 36 إلى 45 عامًا'],
                    ['locale' => 'es', 'text' => '36-45 años'],
                    ['locale' => 'pt', 'text' => '36-45 anos'],
                    ['locale' => 'pl', 'text' => '36-45 lat'],
                    ['locale' => 'lt', 'text' => '36-45 metų'],
                    ['locale' => 'pa', 'text' => '36-45 ਸਾਲ'],
                ]
            ],
            [
                'question-key' => 'age', 
                'text' => '46-55 years',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'من 46 إلى 55 عامًا'],
                    ['locale' => 'es', 'text' => '46-55 años'],
                    ['locale' => 'pt', 'text' => '46-55 anos'],
                    ['locale' => 'pl', 'text' => '46-55 lat'],
                    ['locale' => 'lt', 'text' => '46-55 metų'],
                    ['locale' => 'pa', 'text' => '46-55 ਸਾਲ'],
                ]
            ],
            [
                'question-key' => 'age', 
                'text' => '56-65 years',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'من 56 إلى 65 عامًا'],
                    ['locale' => 'es', 'text' => '56-65 años'],
                    ['locale' => 'pt', 'text' => '56-65 anos'],
                    ['locale' => 'pl', 'text' => '56-65 lat'],
                    ['locale' => 'lt', 'text' => '56-65 metų'],
                    ['locale' => 'pa', 'text' => '56-65 ਸਾਲ'],
                ]
            ],
            [
                'question-key' => 'age', 
                'text' => '66 years plus',
                'translations' => [
                    ['locale' => 'ar', 'text' => '66 عامًا أو اكثر'],
                    ['locale' => 'es', 'text' => '66 años o más'],
                    ['locale' => 'pt', 'text' => '66 anos ou mais'],
                    ['locale' => 'pl', 'text' => '66 lat i powyżej'],
                    ['locale' => 'lt', 'text' => '66 metai ar daugiau'],
                    ['locale' => 'pa', 'text' => '66 ਸਾਲ ਤੋਂ ਉਪਰ'],
                ]
            ],
            [
                'question-key' => 'employsyou', 
                'text' => 'Employed by the organisation on a part-time or full time basis',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'موظف من الشركة على أساس دوام جزئي أو دوام كامل'],
                    ['locale' => 'es', 'text' => 'Trabajo en la empresa a jornada completa o parcial'],
                    ['locale' => 'pt', 'text' => 'Trabalho para a organização a tempo parcial ou completo'],
                    ['locale' => 'pl', 'text' => 'Zatrudnienie przez organizację na pół etatu lub na cały etat'],
                    ['locale' => 'lt', 'text' => 'Dirbu įmonėje nepilną ar pilną darbo laiką'],
                    ['locale' => 'pa', 'text' => 'ਜਥੇਬੰਦੀ ਵਲੋਂ ਅੰਸ਼-ਕਾਲੀ ਜਾਂ ਪੂਰੇ ਸਮੇਂ ਦੇ ਅਧਾਰ ਤੇ ਨੌਕਰੀ ਤੇ ਰਖਿਆ ਗਿਆ'],
                ]
            ],
            [
                'question-key' => 'employsyou', 
                'text' => 'Work for a labour provider/agency',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'أعمل لدى موفر عمل'],
                    ['locale' => 'es', 'text' => 'Trabajo para una agencia de empleo'],
                    ['locale' => 'pt', 'text' => 'Trabalho para uma agência de trabalho'],
                    ['locale' => 'pl', 'text' => 'Zatrudnienie przez dostawcę kadry pracowniczej/agencję pośrednictwa pracy'],
                    ['locale' => 'lt', 'text' => 'Dirbu per įdarbinimo įstaigą / agentūrą'],
                    ['locale' => 'pa', 'text' => 'ਕਿਰਤੀ ਉਪਲਬਧ ਕਰਾਉਣ ਵਾਲਾ/ਏਜੰਸੀ ਲਈ ਕੰਮ ਕਰਨਾ'],
                ]
            ],
            [
                'question-key' => 'employsyou', 
                'text' => 'Work for the organisation on a fixed term contract',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'أعمل لدى الشركة بعقد ثابت المدة'],
                    ['locale' => 'es', 'text' => 'Trabajo para la empresa con un contrato de duración determinada'],
                    ['locale' => 'pt', 'text' => 'Trabalho para a organização com contrato a termo'],
                    ['locale' => 'pl', 'text' => 'Praca dla organizacji na podstawie umowy na czas określony'],
                    ['locale' => 'lt', 'text' => 'Dirbu įmonėje pagal terminuotą darbo sutartį'],
                    ['locale' => 'pa', 'text' => 'ਪੱਕੀ ਮਿਆਦ ਦੇ ਇਕਰਾਰ ਤੇ ਜਥੇਬੰਦੀ ਲਈ ਕੰਮ ਕਰਨਾ'],
                ]
            ],
            [
                'question-key' => 'employsyou', 
                'text' => 'Other e.g. self-employed',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'أخرى'],
                    ['locale' => 'es', 'text' => 'Otro, p. ej., autónomo'],
                    ['locale' => 'pt', 'text' => 'Outra opção, por exemplo, trabalhador por conta própria'],
                    ['locale' => 'pl', 'text' => 'Inne, np. samozatrudnienie'],
                    ['locale' => 'lt', 'text' => 'Kita, pvz., verčiuosi privačia praktika'],
                    ['locale' => 'pa', 'text' => 'ਹੋਰ ਜਿਵੇਂ ਸਵੈ-ਰੋਜ਼ਗਾਰ'],
                ]
            ],
            [
                'question-key' => 'level', 
                'text' => 'Manager/Supervisor',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'مدير'],
                    ['locale' => 'es', 'text' => 'Director/supervisor'],
                    ['locale' => 'pt', 'text' => 'Gestor/supervisor'],
                    ['locale' => 'pl', 'text' => 'Menedżer/Kierownik'],
                    ['locale' => 'lt', 'text' => 'Vadovas'],
                    ['locale' => 'pa', 'text' => 'ਮੈਨੇਜਰ/ਸੁਪਰਵਾਈਜ਼ਰ'],
                ]
            ],
            [
                'question-key' => 'level', 
                'text' => 'Non Manager',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'غير مدير'],
                    ['locale' => 'es', 'text' => 'Cargo no directivo'],
                    ['locale' => 'pt', 'text' => 'Não gestor'],
                    ['locale' => 'pl', 'text' => 'Stanowisko niekierownicze'],
                    ['locale' => 'lt', 'text' => 'Nevadovaujančios pareigos'],
                    ['locale' => 'pa', 'text' => 'ਮੈਨੇਜਰ ਤੋਂ ਇਲਾਵਾ'],
                ]
            ],
            [
                'question-key' => 'role', 
                'text' => 'Operational Role',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'دور التشغيلي'],
                    ['locale' => 'es', 'text' => 'Papel Operacional'],
                    ['locale' => 'pt', 'text' => 'papel operacional'],
                    ['locale' => 'pl', 'text' => 'Rola Operacyjny'],
                    ['locale' => 'lt', 'text' => 'Veiklos vaidmuo'],
                    ['locale' => 'pa', 'text' => 'ਅਪਰੇਸ਼ਨਲ ਭੂਮਿਕਾ'],
                ]
            ],
            [
                'question-key' => 'role', 
                'text' => 'Production Role',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'دور الإنتاج'],
                    ['locale' => 'es', 'text' => 'Papel Producción'],
                    ['locale' => 'pt', 'text' => 'produção Papel'],
                    ['locale' => 'pl', 'text' => 'Rola produkcji'],
                    ['locale' => 'lt', 'text' => 'gamybos vaidmuo'],
                    ['locale' => 'pa', 'text' => 'ਉਤਪਾਦਨ ਭੂਮਿਕਾ'],
                ]
            ],
            [
                'question-key' => 'role', 
                'text' => 'Seasonal Role',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'دور الموسمية'],
                    ['locale' => 'es', 'text' => 'Papel Estacional'],
                    ['locale' => 'pt', 'text' => 'Papel sazonal'],
                    ['locale' => 'pl', 'text' => 'Sezonowe Rola'],
                    ['locale' => 'lt', 'text' => 'Sezoninis vaidmuo'],
                    ['locale' => 'pa', 'text' => 'ਮੌਸਮੀ ਭੂਮਿਕਾ'],
                ]
            ],
            [
                'question-key' => 'shift', 
                'text' => 'Day Shift',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'وردية نهارية'],
                    ['locale' => 'es', 'text' => 'Turno De Dia'],
                    ['locale' => 'pt', 'text' => 'turno do dia'],
                    ['locale' => 'pl', 'text' => 'Dzienna Zmiana'],
                    ['locale' => 'lt', 'text' => 'dienos pamainos metu'],
                    ['locale' => 'pa', 'text' => 'ਦਿਵਸ Shift'],
                ]
            ],
            [
                'question-key' => 'shift', 
                'text' => 'Night Shift',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'مناوبة ليلية'],
                    ['locale' => 'es', 'text' => 'Turno Nocturno'],
                    ['locale' => 'pt', 'text' => 'Turno Da Noite'],
                    ['locale' => 'pl', 'text' => 'Nocna Zmiana'],
                    ['locale' => 'lt', 'text' => 'Naktinė Pamaina'],
                    ['locale' => 'pa', 'text' => 'ਰਾਤ Shift'],
                ]
            ],
            [
                'question-key' => 'shift', 
                'text' => 'Rotating Shift',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'تناوب التحول'],
                    ['locale' => 'es', 'text' => 'turno rotativo'],
                    ['locale' => 'pt', 'text' => 'girando Turno'],
                    ['locale' => 'pl', 'text' => 'obracanie Przesunięcie'],
                    ['locale' => 'lt', 'text' => 'Besisukantis Shift'],
                    ['locale' => 'pa', 'text' => 'ਘੁੰਮੇ Shift'],
                ]
            ],
            [
                'question-key' => 'howlong', 
                'text' => '0-5 years',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'من 0 إلى 5 سنوات'],
                    ['locale' => 'es', 'text' => '0-5 años'],
                    ['locale' => 'pt', 'text' => '0-5 anos'],
                    ['locale' => 'pl', 'text' => '0-5 lat'],
                    ['locale' => 'lt', 'text' => '0–5 metus'],
                    ['locale' => 'pa', 'text' => '0-5 ਸਾਲ'],
                ]
            ],
            [
                'question-key' => 'howlong', 
                'text' => '6-10 years',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'من 6 إلى 10 سنوات'],
                    ['locale' => 'es', 'text' => '6-10 años'],
                    ['locale' => 'pt', 'text' => '6-10 anos'],
                    ['locale' => 'pl', 'text' => '6-10 lat'],
                    ['locale' => 'lt', 'text' => '6–10 metų'],
                    ['locale' => 'pa', 'text' => '6-10 ਸਾਲ'],
                ]
            ],
            [
                'question-key' => 'howlong', 
                'text' => '11-15 years',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'من 11 إلى 15 سنة'],
                    ['locale' => 'es', 'text' => '11-15 años'],
                    ['locale' => 'pt', 'text' => '11-15 anos'],
                    ['locale' => 'pl', 'text' => '11-15 lat'],
                    ['locale' => 'lt', 'text' => '11–15 metų'],
                    ['locale' => 'pa', 'text' => '11-15 ਸਾਲ'],
                ]
            ],
            [
                'question-key' => 'howlong', 
                'text' => '16-20 years',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'من 16 إلى 20 سنة'],
                    ['locale' => 'es', 'text' => '16-20 años'],
                    ['locale' => 'pt', 'text' => '16-20 anos'],
                    ['locale' => 'pl', 'text' => '16-20 lat'],
                    ['locale' => 'lt', 'text' => '16–20 metų'],
                    ['locale' => 'pa', 'text' => '16-20 ਸਾਲ'],
                ]
            ],
            [
                'question-key' => 'howlong', 
                'text' => '21-25 years',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'من 21 إلى 25 سنة'],
                    ['locale' => 'es', 'text' => '21-25 años'],
                    ['locale' => 'pt', 'text' => '21-25 anos'],
                    ['locale' => 'pl', 'text' => '21-25 lat'],
                    ['locale' => 'lt', 'text' => '21–25 metus'],
                    ['locale' => 'pa', 'text' => '21-25 ਸਾਲ'],
                ]
            ],
            [
                'question-key' => 'howlong', 
                'text' => '26 plus years',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'أكثر من 26 سنة'],
                    ['locale' => 'es', 'text' => '26 años o más'],
                    ['locale' => 'pt', 'text' => '26 anos ou mais'],
                    ['locale' => 'pl', 'text' => '26 lat i powyżej'],
                    ['locale' => 'lt', 'text' => 'daugiau nei 25 metus'],
                    ['locale' => 'pa', 'text' => '26 ਸਾਲ ਤੋਂ ਉਪਰ'],
                ]
            ],
            [
                'question-key' => 'ethnicity', 
                'text' => 'White British',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'بريطاني أبيض'],
                    ['locale' => 'es', 'text' => 'Británico blanco'],
                    ['locale' => 'pt', 'text' => 'Britânico branco'],
                    ['locale' => 'pl', 'text' => 'Biały Brytyjczyk'],
                    ['locale' => 'lt', 'text' => 'Baltaodžių britų'],
                    ['locale' => 'pa', 'text' => 'ਗੋਰਾ ਬ੍ਰਿਟਿਸ਼'],
                ]
            ],
            [
                'question-key' => 'ethnicity', 
                'text' => 'White Irish',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'أيرلندي أبيض'],
                    ['locale' => 'es', 'text' => 'Irlandés blanco'],
                    ['locale' => 'pt', 'text' => 'Irlandês branco'],
                    ['locale' => 'pl', 'text' => 'Biały Irlandczyk'],
                    ['locale' => 'lt', 'text' => 'Baltaodžių airių'],
                    ['locale' => 'pa', 'text' => 'ਗੋਰਾ ਆਇਰਿਸ਼'],
                ]
            ],
            [
                'question-key' => 'ethnicity', 
                'text' => 'Other white background',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'خلفية بيضاء أخرى'],
                    ['locale' => 'es', 'text' => 'Blanco de otro origen'],
                    ['locale' => 'pt', 'text' => 'Outra origem branca'],
                    ['locale' => 'pl', 'text' => 'Inne pochodzenie białe'],
                    ['locale' => 'lt', 'text' => 'Kitų baltaodžių'],
                    ['locale' => 'pa', 'text' => 'ਗੋਰੇ ਪਿਛੋਕਡ਼ ਵਾਲੇ ਹੋਰ '],
                ]
            ],
            [
                'question-key' => 'ethnicity', 
                'text' => 'Mixed: White and Black Caribbean',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'مختلط: أبيض وأسود'],
                    ['locale' => 'es', 'text' => 'Mixto: blanco y negro del Caribe'],
                    ['locale' => 'pt', 'text' => 'Mestiço: branco e negro caribenho'],
                    ['locale' => 'pl', 'text' => 'Mieszana: biała i czarna karaibska'],
                    ['locale' => 'lt', 'text' => 'Mišri: baltaodžių ir juodaodžių karibų'],
                    ['locale' => 'pa', 'text' => 'ਮਿਸ਼੍ਰਤ: ਗੋਰਾ ਅਤੇ ਕਾਲਾ ਕੈਰੇਬੀਅਨ'],
                ]
            ],
            [
                'question-key' => 'ethnicity', 
                'text' => 'Mixed: White and Black African',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'مختلط: أبيض وأسود'],
                    ['locale' => 'es', 'text' => 'Mixto: blanco y negro africano'],
                    ['locale' => 'pt', 'text' => 'Mestiço: branco e negro africano'],
                    ['locale' => 'pl', 'text' => 'Mieszana: biała i czarna afrykańska'],
                    ['locale' => 'lt', 'text' => 'Mišri: baltaodžių ir juodaodžių afrikiečių'],
                    ['locale' => 'pa', 'text' => 'ਮਿਸ਼੍ਰਤ: ਗੋਰਾ ਅਤੇ ਕਾਲਾ ਅਫ਼ਰੀਕੀ'],
                ]
            ],
            [
                'question-key' => 'ethnicity', 
                'text' => 'Mixed: White and Asian',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'مختلط: أبيض وآسيوي'],
                    ['locale' => 'es', 'text' => 'Mixto: blanco y asiático'],
                    ['locale' => 'pt', 'text' => 'Mestiço: branco e asiático'],
                    ['locale' => 'pl', 'text' => 'Mieszana: biała i azjatycka'],
                    ['locale' => 'lt', 'text' => 'Azijiečių'],
                    ['locale' => 'pa', 'text' => 'ਮਿਸ਼੍ਰਤ: ਗੋਰਾ ਅਤੇ ਏਸ਼ਿਆਈ'],
                ]
            ],
            [
                'question-key' => 'ethnicity', 
                'text' => 'Mixed: Other mixed',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'مختلط: أخرى'],
                    ['locale' => 'es', 'text' => 'Mixto: otra combinación'],
                    ['locale' => 'pt', 'text' => 'Mestiço: outras origens '],
                    ['locale' => 'pl', 'text' => 'Mieszana: inna mieszana'],
                    ['locale' => 'lt', 'text' => 'Mišri: kita mišri'],
                    ['locale' => 'pa', 'text' => 'ਮਿਸ਼੍ਰਤ: ਹੋਰ ਮਿਸ਼੍ਰਤ'],
                ]
            ],
            [
                'question-key' => 'ethnicity', 
                'text' => 'Indian',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'هندي'],
                    ['locale' => 'es', 'text' => 'Indio'],
                    ['locale' => 'pt', 'text' => 'Indiano'],
                    ['locale' => 'pl', 'text' => 'Indyjska'],
                    ['locale' => 'lt', 'text' => 'Indijos'],
                    ['locale' => 'pa', 'text' => 'ਭਾਰਤੀ'],
                ]
            ],
            [
                'question-key' => 'ethnicity', 
                'text' => 'Pakistani',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'باكستاني'],
                    ['locale' => 'es', 'text' => 'Paquistaní'],
                    ['locale' => 'pt', 'text' => 'Paquistanês'],
                    ['locale' => 'pl', 'text' => 'Pakistańska'],
                    ['locale' => 'lt', 'text' => 'Pakistano'],
                    ['locale' => 'pa', 'text' => 'ਪਾਕਿਸਤਾਨੀ'],
                ]
            ],
            [
                'question-key' => 'ethnicity', 
                'text' => 'Bangladeshi',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'بنغلاديشي'],
                    ['locale' => 'es', 'text' => 'Bangladesí'],
                    ['locale' => 'pt', 'text' => 'Bangladesh'],
                    ['locale' => 'pl', 'text' => 'Bangladeska'],
                    ['locale' => 'lt', 'text' => 'Bangladešo'],
                    ['locale' => 'pa', 'text' => 'ਬੰਗਲਾਦੇਸ਼ੀ'],
                ]
            ],
            [
                'question-key' => 'ethnicity', 
                'text' => 'Chinese',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'صيني'],
                    ['locale' => 'es', 'text' => 'Chino'],
                    ['locale' => 'pt', 'text' => 'Chinês'],
                    ['locale' => 'pl', 'text' => 'Chińska'],
                    ['locale' => 'lt', 'text' => 'Kinų'],
                    ['locale' => 'pa', 'text' => 'ਚੀਨੀ'],
                ]
            ],
            [
                'question-key' => 'ethnicity', 
                'text' => 'Other Asian background',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'خلفية آسيوية أخرى'],
                    ['locale' => 'es', 'text' => 'Asiático'],
                    ['locale' => 'pt', 'text' => 'Outras origens asiáticas'],
                    ['locale' => 'pl', 'text' => 'Inne pochodzenie azjatyckie'],
                    ['locale' => 'lt', 'text' => 'Kitų azijiečių'],
                    ['locale' => 'pa', 'text' => 'ਹੋਰ ਏਸ਼ਿਆਈ ਪਿਛੋਕਡ਼'],
                ]
            ],
            [
                'question-key' => 'ethnicity', 
                'text' => 'Black Caribbean',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'كاريبي أسود'],
                    ['locale' => 'es', 'text' => 'Caribeño negro'],
                    ['locale' => 'pt', 'text' => 'Negro caribenho'],
                    ['locale' => 'pl', 'text' => 'Czarna karaibska'],
                    ['locale' => 'lt', 'text' => 'Juodaodžių karibų'],
                    ['locale' => 'pa', 'text' => 'ਕਾਲਾ ਕੈਰੇਬੀਅਨ'],
                ]
            ],
            [
                'question-key' => 'ethnicity', 
                'text' => 'Black African',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'أفريقي أسود'],
                    ['locale' => 'es', 'text' => 'Africano negro'],
                    ['locale' => 'pt', 'text' => 'Negro africano'],
                    ['locale' => 'pl', 'text' => 'Czarna afrykańska'],
                    ['locale' => 'lt', 'text' => 'Juodaodžių afrikiečių'],
                    ['locale' => 'pa', 'text' => 'ਕਾਲਾ ਅਫ਼ਰੀਕੀ'],
                ]
            ],
            [
                'question-key' => 'ethnicity', 
                'text' => 'Other Black background',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'خلفية سوداء أخرى'],
                    ['locale' => 'es', 'text' => 'Negro de otro origen'],
                    ['locale' => 'pt', 'text' => 'Outras origens negras'],
                    ['locale' => 'pl', 'text' => 'Inne pochodzenie czarne'],
                    ['locale' => 'lt', 'text' => 'Kitų juodaodžių'],
                    ['locale' => 'pa', 'text' => 'ਹੋਰ ਕਾਲਾ ਪਿਛੋਕਡ਼'],
                ]
            ],
            [
                'question-key' => 'ethnicity', 
                'text' => 'Other Ethnic group',
                'translations' => [
                    ['locale' => 'ar', 'text' => 'مجموعة عرقية أخرى'],
                    ['locale' => 'es', 'text' => 'Otro grupo étnico'],
                    ['locale' => 'pt', 'text' => 'Outro grupo étnico'],
                    ['locale' => 'pl', 'text' => 'Inna grupa etniczna'],
                    ['locale' => 'lt', 'text' => 'Kita etninė grupė'],
                    ['locale' => 'pa', 'text' => 'ਹੋਰ ਨਸਲੀ ਸਮੂਹ'],
                ]
            ],

        );


            // Loop through each category above and create the record for it in the database
        foreach ($questionOptions as $option)
        {
            $question = DB::table('questions')->where('key', '=', $option["question-key"])->first();

            $id = DB::table('question_options')->insertGetId([
                'question_id' => $question->id,
                'text' => $option["text"]
            ]);

            DB::table('question_option_translations')->insert([
                'question_option_id' => $id,
                'locale' => 'en',
                'text' => $option["text"]
            ]);

            foreach ($option["translations"] as $translation) {
                DB::table('question_option_translations')->insert([
                    'question_option_id' => $id,
                    'locale' => $translation["locale"],
                    'text' => $translation["text"]
                ]);
            }
        }


        $questionOptions = array(
            [
                'text' => 'Strongly Agree', 
                'score' => 33, 
                'order' => 1,
                'translations' => [
                    ['locale' => 'ar', 'text' => 'موافق بشدة'],
                    ['locale' => 'es', 'text' => 'Muy de acuerdo'],
                    ['locale' => 'pt', 'text' => 'Concordo plenamente'],
                    ['locale' => 'pl', 'text' => 'zdecydowanie się zgadzam'],
                    ['locale' => 'lt', 'text' => 'Visiškai sutinku'],
                    ['locale' => 'pa', 'text' => 'ਜ਼ੋਰਦਾਰ ਲਈ ਸਹਿਮਤ'],
                ]
            ],
            [
                'text' => 'Agree', 
                'score' => 8, 
                'order' => 2,
                'translations' => [
                    ['locale' => 'ar', 'text' => 'أوافق'],
                    ['locale' => 'es', 'text' => 'De acuerdo'],
                    ['locale' => 'pt', 'text' => 'Concordo'],
                    ['locale' => 'pl', 'text' => 'Zgadzam się'],
                    ['locale' => 'lt', 'text' => 'Sutinku'],
                    ['locale' => 'pa', 'text' => 'ਸਹਿਮਤ'],
                ]
            ],
            [
                'text' => 'Disagree', 
                'score' => 6, 
                'order' => 3,
                'translations' => [
                    ['locale' => 'ar', 'text' => 'أعارض'],
                    ['locale' => 'es', 'text' => 'En desacuerdo'],
                    ['locale' => 'pt', 'text' => 'Discordo'],
                    ['locale' => 'pl', 'text' => 'Nie zgadzam się'],
                    ['locale' => 'lt', 'text' => 'Nesutinku'],
                    ['locale' => 'pa', 'text' => 'ਅਸਹਿਮਤ'],
                ]
            ],
            [
                'text' => 'Strongly Disagree', 
                'score' => 1, 
                'order' => 4,
                'translations' => [
                    ['locale' => 'ar', 'text' => 'يعارض بشده'],
                    ['locale' => 'es', 'text' => 'Muy en desacuerdo'],
                    ['locale' => 'pt', 'text' => 'Discordo fortemente'],
                    ['locale' => 'pl', 'text' => 'Zdecydowanie się nie zgadzam'],
                    ['locale' => 'lt', 'text' => 'Visiškai nesutinku'],
                    ['locale' => 'pa', 'text' => 'ਜ਼ੋਰਦਾਰ ਅਸਹਿਮਤ'],
                ]
            ],
            [
                'text' => 'Not Sure', 
                'score' => 2, 
                'order' => 5,
                'translations' => [
                    ['locale' => 'ar', 'text' => 'ست متأكد'],
                    ['locale' => 'es', 'text' => 'No estoy seguro'],
                    ['locale' => 'pt', 'text' => 'Não tenho a certeza'],
                    ['locale' => 'pl', 'text' => 'Nie mam zdania'],
                    ['locale' => 'lt', 'text' => 'Nesu tikras '],
                    ['locale' => 'pa', 'text' => 'ਪੱਕਾ ਨਹੀਂ'],
                ]
            ],
        );
        // rtw questions
        $setId = DB::table('question_sets')->where('key', '=', 'rtw-jl')->first();
        $questions = DB::table('questions')->where('question_set_id', '=', $setId->id)->get();

        foreach($questions as $question)
        {
            foreach ($questionOptions as $option) 
            {
                $id = DB::table('question_options')->insertGetId([
                    'question_id' => $question->id,
                    'text' => $option["text"],
                    'score' => $option["score"],
                    'order' => $option["order"]
                ]);

                DB::table('question_option_translations')->insert([
                    'question_option_id' => $id,
                    'locale' => 'en',
                    'text' => $option["text"]
                ]);

                foreach ($option["translations"] as $translation) {
                    DB::table('question_option_translations')->insert([
                        'question_option_id' => $id,
                        'locale' => $translation["locale"],
                        'text' => $translation["text"]
                    ]);
                }
            }

        }
    }
}
