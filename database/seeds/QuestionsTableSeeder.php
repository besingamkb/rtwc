<?php

use Illuminate\Database\Seeder;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // RTW Demographic Questions
        $setId = DB::table('question_sets')->where('key', '=', 'rtw-demographics')->first();

        $questions = array(
            [
                'key' => 'gender',
                'text' => 'What is your gender',
                'order' => 1,
                'type_id' => \App\Models\Question\Type::DROPDOWN_SINGLE,
                'translations' => [
                    ['locale' => 'ar', 'text' => 'ما جنسك'],
                    ['locale' => 'es', 'text' => 'Cuál es su género'],
                    ['locale' => 'pt', 'text' => 'Qual é o seu sexo'],
                    ['locale' => 'pl', 'text' => 'Jaka jest twoja płeć'],
                    ['locale' => 'lt', 'text' => 'Kokia jūsų lytis'],
                    ['locale' => 'pa', 'text' => 'ਆਪਣੇ ਲਿੰਗ ਕੀ ਹੈ'],
                ]
            ],
            [
                'key' => 'age',
                'text' => 'How old are you',
                'order' => 2,
                'type_id' => \App\Models\Question\Type::DROPDOWN_SINGLE,
                'translations' => [
                    ['locale' => 'ar', 'text' => 'كم عمرك'],
                    ['locale' => 'es', 'text' => 'Cuántos años tiene'],
                    ['locale' => 'pt', 'text' => 'Qual é a sua idade'],
                    ['locale' => 'pl', 'text' => 'Wiek'],
                    ['locale' => 'lt', 'text' => 'Koks Jūsų amžius'],
                    ['locale' => 'pa', 'text' => 'ਤੁਹਾਡੀ ਉਮਰ ਕਿੰਨੀ ਹੈ'],
                ]
            ],
            [
                'key' => 'employsyou',
                'text' => 'Who employs you',
                'order' => 3,
                'type_id' => \App\Models\Question\Type::DROPDOWN_SINGLE,
                'translations' => [
                    ['locale' => 'ar', 'text' => 'الذي يعمل لك'],
                    ['locale' => 'es', 'text' => 'Quién le da empleo'],
                    ['locale' => 'pt', 'text' => 'Que emprega'],
                    ['locale' => 'pl', 'text' => 'Kto zatrudnia cię'],
                    ['locale' => 'lt', 'text' => 'Kas dirba jums'],
                    ['locale' => 'pa', 'text' => 'ਜੋ ਤੁਹਾਨੂੰ ਨੌਕਰੀ'],
                ]
            ],
            [
                'key' => 'level',
                'text' => 'What level is your job',
                'order' => 4,
                'type_id' => \App\Models\Question\Type::DROPDOWN_SINGLE,
                'translations' => [
                    ['locale' => 'ar', 'text' => 'ما هو مستوى عملك'],
                    ['locale' => 'es', 'text' => 'Qué nivel es su trabajo'],
                    ['locale' => 'pt', 'text' => 'O nível é o seu trabalho'],
                    ['locale' => 'pl', 'text' => 'Jaki poziom jest zadanie'],
                    ['locale' => 'lt', 'text' => 'Kokio lygio yra jūsų darbas'],
                    ['locale' => 'pa', 'text' => 'ਕੀ ਹੈ ਦਾ ਪੱਧਰ ਆਪਣੇ ਨੌਕਰੀ ਹੈ'],
                ]
            ],
            [
                'key' => 'role',
                'text' => 'What type of role is your job',
                'order' => 5,
                'type_id' => \App\Models\Question\Type::DROPDOWN_SINGLE,
                'translations' => [
                    ['locale' => 'ar', 'text' => 'ما نوع الدور هو عملك ؟'],
                    ['locale' => 'es', 'text' => 'Qué tipo de papel es su trabajo'],
                    ['locale' => 'pt', 'text' => 'Que tipo de papel é o seu trabalho'],
                    ['locale' => 'pl', 'text' => 'Jaki typ roli jest twoja praca'],
                    ['locale' => 'lt', 'text' => 'Kokios vaidmenį jūsų darbas'],
                    ['locale' => 'pa', 'text' => 'ਤੁਹਾਡੀ ਨੌਕਰੀ ਭੂਮਿਕਾ ਦੀ ਕੀ ਕਿਸਮ ਹੈ'],
                ]
            ],
            [
                'key' => 'shift',
                'text' => 'What shift do you currently work',
                'order' => 6,
                'type_id' => \App\Models\Question\Type::DROPDOWN_SINGLE,
                'translations' => [
                    ['locale' => 'ar', 'text' => 'ما تحول هل تعمل حاليا ؟'],
                    ['locale' => 'es', 'text' => 'Lo turno trabaja usted actualmente'],
                    ['locale' => 'pt', 'text' => 'Que mudança que você trabalha atualmente'],
                    ['locale' => 'pl', 'text' => 'Jakie zmiany biegów czy obecnie działa'],
                    ['locale' => 'lt', 'text' => 'Kas perėjimas šiuo metu dirbate'],
                    ['locale' => 'pa', 'text' => 'ਤੁਹਾਨੂੰ ਇਸ ਵੇਲੇ ਕੀ ਸ਼ਿਫਟ ਨੂੰ ਕੰਮ ਕਰਦੇ ਹਨ'],
                ]
            ],
            [
                'key' => 'howlong',
                'text' => 'How long have you worked at your company',
                'order' => 7,
                'type_id' => \App\Models\Question\Type::DROPDOWN_SINGLE,
                'translations' => [
                    ['locale' => 'ar', 'text' => 'منذ متى وأنت تعمل في شركتك'],
                    ['locale' => 'es', 'text' => 'Cuánto tiempo ha trabajado para su empresa'],
                    ['locale' => 'pt', 'text' => 'Há quanto tempo você trabalhou para sua empresa'],
                    ['locale' => 'pl', 'text' => 'Jak długo pracujesz w firmie'],
                    ['locale' => 'lt', 'text' => 'Kaip ilgai jums dirbo organizacijoje'],
                    ['locale' => 'pa', 'text' => 'ਕਿੰਨਾ ਚਿਰ ਤੁਹਾਨੂੰ ਆਪਣੇ ਕੰਪਨੀ ਦੇ \'ਤੇ ਕੰਮ ਕੀਤਾ ਹੈ'],
                ]
            ],
            [
                'key' => 'ethnicity',
                'text' => 'What is your ethnicity',
                'order' => 8,
                'type_id' => \App\Models\Question\Type::DROPDOWN_SINGLE,
                'translations' => [
                    ['locale' => 'ar', 'text' => 'ما هو العرق الخاص بك'],
                    ['locale' => 'es', 'text' => 'Cuál es tu etnia'],
                    ['locale' => 'pt', 'text' => 'Qual é a sua etnia'],
                    ['locale' => 'pl', 'text' => 'Jakie jest twoje pochodzenie etniczne'],
                    ['locale' => 'lt', 'text' => 'Kokia jūsų tautybė'],
                    ['locale' => 'pa', 'text' => 'ਆਪਣੇ ਨਸਲੀ ਕੀ ਹੈ'],
                ]
            ],
        );

        // Loop through each category above and create the record for it in the database
        foreach ($questions as $question) {
            $id = DB::table('questions')->insertGetId([
                'question_set_id' => $setId->id,
                'order' => $question['order'],
                'type_id' => (isset($question['type_id']) ? $question['type_id'] : \App\Models\Question\Type::TEXT),
                'key' => $question['key'],
                'text' => $question['text']
            ]);

            DB::table('question_translations')->insert([
                'question_id' => $id,
                'locale' => 'en',
                'text' => $question['text']
            ]);

            foreach ($question["translations"] as $translation) {
                DB::table('question_translations')->insert([
                    'question_id' => $id,
                    'locale' => $translation["locale"],
                    'text' => $translation["text"]
                ]);
            }
        }

        // RTW Questions
        $setId = DB::table('question_sets')->where('key', '=', 'rtw-jl')->first();

        $questions = array(
            [
                'key' => 'rtw-1',
                'category-key' => 'discrimination',
                'text' => 'I am treated with fairness and respect',
                'order' => 1,
                'type_id' => \App\Models\Question\Type::RADIO,
                'translations' => [
                    ['locale' => 'ar', 'text' => 'أُعامَل بإنصاف واحترام'],
                    ['locale' => 'es', 'text' => 'Se me trata con justicia y respeto'],
                    ['locale' => 'pa', 'text' => 'ਮੇਰੇ ਨਾਲ ਨਿਰਪੱਖ ਅਤੇ ਸਤਿਕਾਰ ਵਾਲਾ ਵਿਹਾਰ ਕੀਤਾ ਜਾਂਦਾ ਹੈ'],
                    ['locale' => 'pt', 'text' => 'Sou tratado(a) com justiça e respeito'],
                    ['locale' => 'pl', 'text' => 'Jestem traktowany(-a) sprawiedliwie i z szacunkiem.'],
                    ['locale' => 'lt', 'text' => 'Su manimi elgiamasi sąžiningai ir pagarbiai'],
                ]
            ],
            [
                'key' => 'rtw-2',
                'category-key' => 'discrimination',
                'text' => 'My manager and I work to balance my personal needs with those of the business',
                'order' => 2,
                'type_id' => \App\Models\Question\Type::RADIO,
                'translations' => [
                    ['locale' => 'ar', 'text' => 'يتعاون معي مديري للموازنة بين احتياجاتي الشخصية واحتياجات العمل'],
                    [
                        'locale' => 'es',
                        'text' => 'Mi director y yo trabajamos para equilibrar mis necesidades personales con las de la empresa'
                    ],
                    [
                        'locale' => 'pa',
                        'text' => 'ਮੇਰਾ ਮੈਨੇਜਰ ਅਤੇ ਮੈਂ ਮੇਰੀਆਂ ਨਿੱਜੀ ਜ਼ਰੂਰਤਾਂ ਦਾ ਕਾਰੋਬਾਰ ਨਾਲ ਜੁੜੀਆਂ ਜ਼ਰੂਰਤਾਂ ਨਾਲ ਸੰਤੁਲਨ ਬਣਾਉਣ ਲਈ ਕੰਮ ਕਰਦੇ ਹਾਂ'
                    ],
                    [
                        'locale' => 'pt',
                        'text' => 'O meu supervisor e eu trabalhamos para equilibrar as minhas necessidades pessoais com as da empresa'
                    ],
                    [
                        'locale' => 'pl',
                        'text' => 'Wraz z moim przełożonym staramy się znajdywać kompromis pomiędzy moimi osobistymi potrzebami a potrzebami przedsiębiorstwa.'
                    ],
                    [
                        'locale' => 'lt',
                        'text' => 'Mano vadovas ir aš stengiamės suderinti mano asmeninius poreikius ir darbą'
                    ],
                ]
            ],
            [
                'key' => 'rtw-3',
                'category-key' => 'discrimination',
                'text' => 'I am recognised and acknowledged when I do something well',
                'order' => 3,
                'type_id' => \App\Models\Question\Type::RADIO,
                'translations' => [
                    ['locale' => 'ar', 'text' => 'أحظى بالتقدير والتكريم عندما أؤدي بكفاءة'],
                    ['locale' => 'es', 'text' => 'Se me reconocen y agradecen las cosas que hago bien'],
                    [
                        'locale' => 'pa',
                        'text' => 'ਜੇ ਮੈਂ ਕੋਈ ਚੰਗਾ ਕੰਮ ਕਰਦਾ ਹਾਂ, ਤਾਂ ਉਸ ਦੀ ਕਦਰ ਕੀਤੀ ਜਾਂਦੀ ਹੈ ਅਤੇ ਉਸ ਨੂੰ ਮਾਣਤਾ ਦਿੱਤੀ ਜਾਂਦੀ ਹੈ'
                    ],
                    ['locale' => 'pt', 'text' => 'Sou reconhecido(a) e considerado(a) quando faço algo bem'],
                    ['locale' => 'pl', 'text' => 'Jestem chwalony(-a), gdy zrobię coś dobrze.'],
                    ['locale' => 'lt', 'text' => 'Mane pripažįsta ir giria, kai ką nors gerai padarau'],
                ]
            ],
            [
                'key' => 'rtw-4',
                'category-key' => 'safety',
                'text' => 'I am satisfied with my physical working conditions',
                'order' => 4,
                'type_id' => \App\Models\Question\Type::RADIO,
                'translations' => [
                    ['locale' => 'ar', 'text' => 'أنا راضٍ عن ظروف عملي المادية'],
                    ['locale' => 'es', 'text' => 'Estoy satisfecho con las condiciones físicas de mi trabajo'],
                    ['locale' => 'pa', 'text' => 'ਮੈਂ ਆਪਣੀਆਂ ਕੰਮ ਦੀਆਂ ਭੌਤਿਕ ਸਥਿਤੀਆਂ ਤੋਂ ਸੰਤੁਸ਼ਟ ਹਾਂ'],
                    ['locale' => 'pt', 'text' => 'Estou satisfeito(a) com as minhas condições físicas de trabalho'],
                    ['locale' => 'pl', 'text' => 'Jestem zadowolony(-a) z moich fizycznych warunków pracy.'],
                    ['locale' => 'lt', 'text' => 'Esu patenkinta(s) fizinėmis darbo sąlygomis'],
                ]
            ],
            [
                'key' => 'rtw-5',
                'category-key' => 'safety',
                'text' => 'The equipment and machinery I use enables me to do my work safely',
                'order' => 5,
                'type_id' => \App\Models\Question\Type::RADIO,
                'translations' => [
                    ['locale' => 'ar', 'text' => 'المعدات والماكينات التي أستخدمها تعينني على أداء عملي في أمان'],
                    [
                        'locale' => 'es',
                        'text' => 'El equipo y la maquinaria que utilizo me permiten hacer mi trabajo de forma segura'
                    ],
                    [
                        'locale' => 'pa',
                        'text' => 'ਮੈਂ ਜਿਸ ਉਪਕਰਣ ਅਤੇ ਮਸ਼ੀਨਰੀ ਦੀ ਵਰਤੋਂ ਕਰਦਾ ਹਾਂ, ਉਹ ਮੈਨੂੰ ਮੇਰਾ ਕੰਮ ਸੁਰੱਖਿਅਤ ਢੰਗ ਨਾਲ ਕਰਨ ਦੇ ਸਮਰੱਥ ਬਣਾਉਂਦੀ ਹੈ '
                    ],
                    [
                        'locale' => 'pt',
                        'text' => 'O equipamento e a maquinaria que utilizo permitem-me fazer o meu trabalho em segurança'
                    ],
                    [
                        'locale' => 'pl',
                        'text' => 'Sprzęt i maszyny, z których korzystam, umożliwiają bezpieczne wykonywanie moich obowiązków.'
                    ],
                    [
                        'locale' => 'lt',
                        'text' => 'Įranga ir mechanizmai, kuriuos naudoju, leidžia man saugiai atlikti darbą'
                    ],
                ]
            ],
            [
                'key' => 'rtw-6',
                'category-key' => 'safety',
                'text' => 'I am regularly shown how to use machines at work without harming myself and others',
                'order' => 6,
                'type_id' => \App\Models\Question\Type::RADIO,
                'translations' => [
                    [
                        'locale' => 'ar',
                        'text' => 'أحظى بشرح منتظم عن كيفية استخدام الماكينات في العمل دون أن أضر نفسي أو أضر بالآخرين'
                    ],
                    [
                        'locale' => 'es',
                        'text' => 'De forma regular se me enseña a utilizar las máquinas con las que trabajo sin hacerme daño a mí mismo ni a otros'
                    ],
                    [
                        'locale' => 'pa',
                        'text' => 'ਮੈਨੂੰ ਨੇਮ ਨਾਲ ਸਮਝਾਇਆ ਜਾਂਦਾ ਹੈ ਕਿ ਆਪਣੇ ਆਪ ਨੂੰ ਅਤੇ ਹੋਰਨਾਂ ਨੂੰ ਨੁਕਸਾਨ ਪਹੁੰਚਾਏ ਬਿਨਾ, ਕੰਮ \'ਤੇ ਮਸ਼ੀਨਾਂ ਦੀ ਵਰਤੋਂ ਕਿਵੇਂ ਕਰਨੀ ਹੈ '
                    ],
                    [
                        'locale' => 'pt',
                        'text' => 'Recebo formação regularmente sobre como usar máquinas no trabalho sem me magoar a mim próprio(a) e aos outros'
                    ],
                    [
                        'locale' => 'pl',
                        'text' => 'W pracy pokazywane jest często, jak korzystać z maszyn w sposób bezpieczny dla mnie i innych osób.'
                    ],
                    [
                        'locale' => 'lt',
                        'text' => 'Man reguliariai rodo, kaip naudotis mašinomis, nesužeidžiant savęs ir kitų'
                    ],
                ]
            ],
            [
                'key' => 'rtw-7',
                'category-key' => 'freelychosen',
                'text' => 'I am not forced to work overtime if I don\'t want to',
                'order' => 7,
                'type_id' => \App\Models\Question\Type::RADIO,
                'translations' => [
                    ['locale' => 'ar', 'text' => 'لست مضطرًا للعمل لوقت إضافي إذا كنت لا أريد ذلك'],
                    [
                        'locale' => 'es',
                        'text' => 'No se me obliga a trabajar horas extraordinarias si no quiero hacerlo'
                    ],
                    ['locale' => 'pa', 'text' => 'ਜੇ ਮੈਂ ਨਹੀਂ ਚਾਹੁੰਦਾ, ਤਾਂ ਮੇਰੇ ਤੋਂ ਜ਼ਬਰਦਸਤੀ ਓਵਰਟਾਈਮ ਨਹੀਂ ਕਰਾਇਆ ਜਾਂਦਾ'],
                    ['locale' => 'pt', 'text' => 'Não sou forçado(a) a trabalhar horas extra se eu não quiser'],
                    [
                        'locale' => 'pl',
                        'text' => 'Nie jestem zmuszany(-a) do pracy w godzinach nadliczbowych, jeśli sobie tego nie życzę.'
                    ],
                    ['locale' => 'lt', 'text' => 'Manęs neverčia dirbti viršvalandžių, jeigu nenoriu'],
                ]
            ],
            [
                'key' => 'rtw-8',
                'category-key' => 'freedom',
                'text' => 'I can join any unions or worker committees without being treated differently by managers or other workers',
                'order' => 8,
                'type_id' => \App\Models\Question\Type::RADIO,
                'translations' => [
                    [
                        'locale' => 'ar',
                        'text' => 'بوسعي الانضمام إلى أية نقابة أو لجان عمالية دون التعرض لمعاملة غريبة من المديرين أو من عاملين آخرين'
                    ],
                    [
                        'locale' => 'es',
                        'text' => 'Puedo afiliarme a sindicatos o comités de trabajadores sin que por ello mis directores ni otros trabajadores me traten de forma diferente'
                    ],
                    [
                        'locale' => 'pa',
                        'text' => 'ਮੈਂ ਕਿਸੇ ਵੀ ਯੂਨੀਅਨਾਂ ਜਾਂ ਵਰਕਰ ਕਮੇਟੀਆਂ ਵਿਚ ਸ਼ਾਮਲ ਹੋ ਸਕਦਾ ਹਾਂ, ਇਸ ਕਰਕੇ ਮੈਨੇਜਰਾਂ ਅਤੇ ਹੋਰਨਾਂ ਵਰਕਰਾਂ ਦੁਆਰਾ ਮੇਰੇ ਨਾਲ ਵੱਖਰੇ ਢੰਗ ਨਾਲ ਵਿਹਾਰ ਨਹੀਂ ਕੀਤਾ ਜਾਂਦਾ '
                    ],
                    [
                        'locale' => 'pt',
                        'text' => 'Posso aderir a quaisquer sindicatos ou comissões de trabalhadores sem ser tratado(a) de forma diferente por administradores ou outros trabalhadores'
                    ],
                    [
                        'locale' => 'pl',
                        'text' => ']Mam możliwość dołączenia do związków zawodowych bez zmiany podejścia mojego przełożonego lub innych pracowników do mnie.'
                    ],
                    [
                        'locale' => 'lt',
                        'text' => 'Galiu įstoti į profsąjungas ar darbuotojų komitetus be baimės, kad vadovų ar kitų darbuotojų elgesys su manimi pasikeis'
                    ],
                ]
            ],
            [
                'key' => 'rtw-9',
                'category-key' => 'hours',
                'text' => 'I normally work less than 60 hours per week',
                'order' => 9,
                'type_id' => \App\Models\Question\Type::RADIO,
                'translations' => [
                    ['locale' => 'ar', 'text' => 'أعمل في المعتاد أقل من 60 ساعة أسبوعيًا'],
                    ['locale' => 'es', 'text' => 'Normalmente trabajo menos de 60 horas a la semana'],
                    ['locale' => 'pa', 'text' => 'ਮੈਂ ਹਰ ਹਫ਼ਤੇ ਆਮ ਤੌਰ \'ਤੇ 60 ਘੰਟਿਆਂ ਤੋਂ ਘੱਟ ਕੰਮ ਕਰਦਾ ਹਾਂ'],
                    ['locale' => 'pt', 'text' => 'Por norma, trabalho menos de 60 horas por semana'],
                    ['locale' => 'pl', 'text' => 'Zazwyczaj pracuję krócej niż 60 godzin tygodniowo.'],
                    ['locale' => 'lt', 'text' => 'Paprastai dirbu mažiau negu 60 valandų per savaitę'],
                ]
            ],
            [
                'key' => 'rtw-10',
                'category-key' => 'wages',
                'text' => 'I am satisfied with the pay rate for my job',
                'order' => 10,
                'type_id' => \App\Models\Question\Type::RADIO,
                'translations' => [
                    ['locale' => 'ar', 'text' => 'أنا راضٍ عن مستوى أجري في هذه الوظيفة'],
                    ['locale' => 'es', 'text' => 'Estoy satisfecho con la remuneración por mi trabajo'],
                    ['locale' => 'pa', 'text' => 'ਮੈਂ ਆਪਣੇ ਕੰਮ ਲਈ ਭੁਗਤਾਨ ਕੀਤੀ ਜਾਂਦੀ ਉਜਰਤ ਤੋਂ ਸੰਤੁਸ਼ਟ ਹਾਂ'],
                    ['locale' => 'pt', 'text' => 'Estou satisfeito(a) com a tabela salarial para a minha função'],
                    ['locale' => 'pl', 'text' => 'Jestem zadowolony(-a) z wynagrodzenia otrzymywanego za moją pracę.'],
                    ['locale' => 'lt', 'text' => 'Darbo užmokesčio tarifas mane tenkina'],
                ]
            ],
            [
                'key' => 'rtw-11',
                'category-key' => 'wages',
                'text' => 'My pay reflects my performance',
                'order' => 11,
                'type_id' => \App\Models\Question\Type::RADIO,
                'translations' => [
                    ['locale' => 'ar', 'text' => 'أجري مرتبط بأدائي'],
                    ['locale' => 'es', 'text' => 'Mi paga refleja mi rendimiento'],
                    ['locale' => 'pa', 'text' => 'ਮੇਰੀ ਤਨਖ਼ਾਹ ਤੋਂ ਮੇਰੀ ਕਾਰਗੁਜ਼ਾਰੀ ਦਾ ਪਤਾ ਲੱਗਦਾ ਹੈ'],
                    ['locale' => 'pt', 'text' => 'O meu salário reflete o meu desempenho'],
                    ['locale' => 'pl', 'text' => 'Moje wynagrodzenie odzwierciedla moją wydajność.'],
                    ['locale' => 'lt', 'text' => 'Mano atlyginimas atitinka mano atliekamą darbą'],
                ]
            ],
            [
                'key' => 'rtw-12',
                'category-key' => 'engagement',
                'text' => 'I make good use of my skills and abilities in my job',
                'order' => 12,
                'type_id' => \App\Models\Question\Type::RADIO,
                'translations' => [
                    ['locale' => 'ar', 'text' => 'أحسن استغلال مهاراتي وقدراتي في وظيفتي'],
                    ['locale' => 'es', 'text' => 'Hago buen uso de mis habilidades y capacidad en mi trabajo'],
                    [
                        'locale' => 'pa',
                        'text' => 'ਮੈਂ ਆਪਣੇ ਕੰਮ ਵਿਚ ਆਪਣੀਆਂ ਮੁਹਾਰਤਾਂ ਅਤੇ ਸਮਰੱਥਾ ਦੀ ਚੰਗੀ ਤਰ੍ਹਾਂ ਵਰਤੋਂ ਕਰਦਾ ਹਾਂ'
                    ],
                    ['locale' => 'pt', 'text' => 'Faço bom uso das minhas competências e capacidades no meu trabalho'],
                    ['locale' => 'pl', 'text' => 'Odpowiednio wykorzystuję swoje umiejętności i zdolności w pracy.'],
                    ['locale' => 'lt', 'text' => 'Gerai išnaudoju savo įgūdžius ir sugebėjimus darbe'],
                ]
            ],
            [
                'key' => 'rtw-13',
                'category-key' => 'engagement',
                'text' => 'We have enough staff members to get the job done',
                'order' => 13,
                'type_id' => \App\Models\Question\Type::RADIO,
                'translations' => [
                    ['locale' => 'ar', 'text' => 'لدينا عددٍ كافٍ من الموظفين لإنجاز مهام العمل'],
                    ['locale' => 'es', 'text' => 'Contamos con personal suficiente para la realización del trabajo'],
                    ['locale' => 'pa', 'text' => 'ਕੰਮ ਪੂਰਾ ਕਰਨ ਲਈ ਸਾਡੇ ਕੋਲ ਢੁਕਵੇਂ ਸਟਾਫ਼ ਮੈਂਬਰ ਹਨ'],
                    ['locale' => 'pt', 'text' => 'Temos funcionários suficientes para fazer o trabalho'],
                    [
                        'locale' => 'pl',
                        'text' => 'Dysponujemy odpowiednią liczbą pracowników, umożliwiającą wykonywanie obowiązków.'
                    ],
                    ['locale' => 'lt', 'text' => 'Turime pakankamai etatinių darbuotojų darbui atlikti'],
                ]
            ],
            [
                'key' => 'rtw-14',
                'category-key' => 'engagement',
                'text' => 'I have opportunities to keep developing my skills and potential',
                'order' => 14,
                'type_id' => \App\Models\Question\Type::RADIO,
                'translations' => [
                    ['locale' => 'ar', 'text' => 'عندي فرص لتطوير مهاراتي وإمكاناتي'],
                    [
                        'locale' => 'es',
                        'text' => 'Tengo oportunidades para seguir desarrollando mis habilidades y potencial'
                    ],
                    [
                        'locale' => 'pa',
                        'text' => 'ਮੇਰੇ ਕੋਲ ਆਪਣੀਆਂ ਮੁਹਾਰਤਾਂ ਅਤੇ ਸਮਰੱਥਾ ਵਿਚ ਵਾਧਾ ਕਰਦੇ ਰਹਿਣ ਲਈ ਮੌਕੇ ਹੁੰਦੇ ਹਨ '
                    ],
                    [
                        'locale' => 'pt',
                        'text' => 'Tenho a oportunidade de continuar a desenvolver as minhas competências e o meu potencial'
                    ],
                    [
                        'locale' => 'pl',
                        'text' => 'Mam możliwość rozwoju umiejętności i realizowania swojego potencjału.'
                    ],
                    ['locale' => 'lt', 'text' => 'Turiu galimybių vystyti savo darbo įgūdžius ir potencialą'],
                ]
            ],
            [
                'key' => 'rtw-15',
                'category-key' => 'engagement',
                'text' => 'My manager encourages us to find better ways of doing things as a team',
                'order' => 15,
                'type_id' => \App\Models\Question\Type::RADIO,
                'translations' => [
                    ['locale' => 'ar', 'text' => 'يشجعنا المدير على إيجاد سبل أفضل لتنفيذ مهام العمل كفريق'],
                    [
                        'locale' => 'es',
                        'text' => 'Mi director nos anima a buscar formas mejores de hacer las cosas como equipo'
                    ],
                    [
                        'locale' => 'pa',
                        'text' => 'ਮੇਰਾ ਮੈਨੇਜਰ ਇੱਕ ਟੀਮ ਵੱਜੋਂ ਕੰਮ ਕਰਨ ਦੇ ਬਿਹਤਰ ਤਰੀਕਿਆਂ ਦਾ ਪਤਾ ਲਾਉਣ ਲਈ ਸਾਨੂੰ ਪ੍ਰੇਰਿਤ ਕਰਦਾ ਹੈ '
                    ],
                    [
                        'locale' => 'pt',
                        'text' => 'O meu supervisor encoraja-nos a encontrar melhores formas de fazermos as coisas em equipa'
                    ],
                    [
                        'locale' => 'pl',
                        'text' => 'Mój przełożony zachęca nas do poszukiwania lepszych metod pracy zespołowej.'
                    ],
                    ['locale' => 'lt', 'text' => 'Mano vadovas padrąsina mus stengtis dirbti kaip komandai'],
                ]
            ],
            [
                'key' => 'rtw-16',
                'category-key' => 'engagement',
                'text' => 'My experience of the recruitment process for this role was positive',
                'order' => 16,
                'type_id' => \App\Models\Question\Type::RADIO,
                'translations' => [
                    ['locale' => 'ar', 'text' => 'انطباعي عن إجراءات الاستقدام لشغل هذه الوظيفة هو انطباع إيجابي'],
                    [
                        'locale' => 'es',
                        'text' => 'Mi experiencia en el proceso de selección para este puesto fue positiva'
                    ],
                    ['locale' => 'pa', 'text' => 'ਇਸ ਕੰਮ ਲਈ ਭਰਤੀ ਦੀ ਪ੍ਰਕਿਰਿਆ ਦਾ ਮੇਰਾ ਅਨੁਭਵ ਉਸਾਰੂ ਸੀ'],
                    [
                        'locale' => 'pt',
                        'text' => 'A minha experiência do processo de recrutamento para esta função foi positiva'
                    ],
                    [
                        'locale' => 'pl',
                        'text' => 'Moje doświadczenia z procesu rekrutacyjnego na to stanowisko są pozytywne.'
                    ],
                    ['locale' => 'lt', 'text' => 'Man patiko samdymo šiam darbui procesas'],
                ]
            ],
            [
                'key' => 'rtw-17',
                'category-key' => 'communication',
                'text' => 'I feel comfortable questioning the way things are done where I work',
                'order' => 17,
                'type_id' => \App\Models\Question\Type::RADIO,
                'translations' => [
                    ['locale' => 'ar', 'text' => 'أشعر بالارتياح عندما أعترض على أسلوب تنفيذ بعض مهام العمل'],
                    [
                        'locale' => 'es',
                        'text' => 'Me siento cómodo cuestionando la forma de hacer las cosas donde trabajo'
                    ],
                    [
                        'locale' => 'pa',
                        'text' => 'ਆਪਣੀ ਕੰਮ ਕਰਨ ਵਾਲੀ ਥਾਂ \'ਤੇ ਕੰਮ ਕਰਨ ਦੇ ਤਰੀਕਿਆਂ ਬਾਰੇ ਸੁਆਲ ਪੁੱਛਣ ਵੇਲੇ ਮੈਂ ਸੁਖਾਵਾਂ ਮਹਿਸੂਸ ਕਰਦਾ ਹਾਂ'
                    ],
                    [
                        'locale' => 'pt',
                        'text' => 'Sinto-me confortável quando questiono a forma como as coisas são feitas no meu local de trabalho'
                    ],
                    ['locale' => 'pl', 'text' => 'Nie obawiam się podważać zasadności stosowanych metod pracy.'],
                    ['locale' => 'lt', 'text' => 'Galiu drąsiai paklausti apie viską, kas vyksta darbe'],
                ]
            ],
            [
                'key' => 'rtw-18',
                'category-key' => 'communication',
                'text' => 'I feel included and informed about decisions and changes in our workplace',
                'order' => 18,
                'type_id' => \App\Models\Question\Type::RADIO,
                'translations' => [
                    [
                        'locale' => 'ar',
                        'text' => 'أشعر بالاحتواء وأتلقى الإخطارات بالقرارات والتغييرات التي تحدث في مكان العمل'
                    ],
                    [
                        'locale' => 'es',
                        'text' => 'Siento que se me tiene en cuenta y se me informa sobre las decisiones y los cambios en nuestro lugar de trabajo'
                    ],
                    [
                        'locale' => 'pa',
                        'text' => 'ਮੈਨੂੰ ਲੱਗਦਾ ਹੈ ਕਿ ਸਾਡੀ ਕੰਮ ਵਾਲੀ ਥਾਂ \'ਤੇ ਫ਼ੈਸਲਿਆਂ ਅਤੇ ਤਬਦੀਲੀਆਂ ਬਾਰੇ ਮੈਨੂੰ ਸ਼ਾਮਲ ਕੀਤਾ ਜਾਂਦਾ ਹੈ ਅਤੇ ਦੱਸਿਆ ਜਾਂਦਾ ਹੈ'
                    ],
                    [
                        'locale' => 'pt',
                        'text' => 'Sinto-me incluído(a) e informado(a) sobre decisões e mudanças no meu local de trabalho'
                    ],
                    [
                        'locale' => 'pl',
                        'text' => 'Czuję się częścią zespołu i jestem informowany(-a) o zmianach dotyczących mojego miejsca pracy.'
                    ],
                    [
                        'locale' => 'lt',
                        'text' => 'Mane vertina ir informuoja apie sprendimus ir pasikeitimus mūsų darbo vietoje'
                    ],
                ]
            ],
            [
                'key' => 'rtw-19',
                'category-key' => 'communication',
                'text' => 'I am kept up to date about job and progression opportunities and activities at my company',
                'order' => 19,
                'type_id' => \App\Models\Question\Type::RADIO,
                'translations' => [
                    [
                        'locale' => 'ar',
                        'text' => 'أشعر بالاطلاع على المستجدات المتعلقة بالوظيفة وفرص الترقي والأنشطة الخاصة بشركتي'
                    ],
                    [
                        'locale' => 'es',
                        'text' => 'Se me informa de las oportunidades y actividades laborales y de progresión que se llevan a cabo en mi empresa'
                    ],
                    [
                        'locale' => 'pa',
                        'text' => 'ਮੈਨੂੰ ਨੌਕਰੀ ਅਤੇ ਆਪਣੀ ਕੰਪਨੀ ਵਿਚ ਤਰੱਕੀ ਦੇ ਮੌਕਿਆਂ ਅਤੇ ਸਰਗਰਮੀਆਂ ਬਾਰੇ ਨਵੀਂ ਤੋਂ ਨਵੀਂ ਜਾਣਕਾਰੀ ਦਿੱਤੀ ਜਾਂਦੀ ਹੈ '
                    ],
                    [
                        'locale' => 'pt',
                        'text' => 'Sou mantido(a) ao corrente sobre as oportunidades e a progressão de trabalho e atividades na minha empresa'
                    ],
                    [
                        'locale' => 'pl',
                        'text' => 'Jestem na bieżąco informowany(-a) o możliwościach awansu i nowych ofertach pracy w mojej firmie.'
                    ],
                    [
                        'locale' => 'lt',
                        'text' => 'Mane nuolat informuoja apie paaukštinimo galimybes ir veiklą mano bendrovėje'
                    ],
                ]
            ],
            [
                'key' => 'rtw-20',
                'category-key' => 'culture',
                'text' => 'The business respects individual differences (eg culture and backgrounds)',
                'order' => 20,
                'type_id' => \App\Models\Question\Type::RADIO,
                'translations' => [
                    ['locale' => 'ar', 'text' => 'الشركة تحترم الاختلافات الفردية (مثال: الثقافات والانتماءات)'],
                    [
                        'locale' => 'es',
                        'text' => 'La empresa respeta las diferencias individuales (p.ej., las culturales y de procedencia)'
                    ],
                    [
                        'locale' => 'pa',
                        'text' => 'ਕਾਰੋਬਾਰ ਵਿਅਕਤੀਗਤ ਵੱਖਰੇਵੇਂ ਦਾ ਸਨਮਾਨ ਕਰਦਾ ਹੈ (ਜਿਵੇਂ ਸਭਿਆਚਾਰ ਅਤੇ ਪਿਛੋਕੜ)'
                    ],
                    [
                        'locale' => 'pt',
                        'text' => 'A empresa respeita as diferenças individuais (por ex. cultura e origens)'
                    ],
                    [
                        'locale' => 'pl',
                        'text' => 'Moje przedsiębiorstwo szanuje różnice pomiędzy pracownikami (np. kulturowe i wynikające z przynależności do grup społecznych).'
                    ],
                    [
                        'locale' => 'lt',
                        'text' => 'Darbe atsižvelgiama į individualius skirtumus (pvz. kultūrą ir kilmę)'
                    ],
                ]
            ],
            [
                'key' => 'rtw-21',
                'category-key' => 'culture',
                'text' => 'There is a strong sense of teamwork where I work',
                'order' => 21,
                'type_id' => \App\Models\Question\Type::RADIO,
                'translations' => [
                    ['locale' => 'ar', 'text' => 'توجد روح قوية للعمل الجماعي في مكان عملي'],
                    ['locale' => 'es', 'text' => 'Donde trabajo hay un fuerte sentido de trabajo en equipo'],
                    ['locale' => 'pa', 'text' => 'ਮੇਰੇ ਕੰਮ ਕਰਨ ਵਾਲੀ ਥਾਂ ਵਿਚ ਮਿਲਕੇ ਕੰਮ ਕਰਨ ਦਾ ਮਜ਼ਬੂਤ ਜਜ਼ਬਾ ਹੈ'],
                    [
                        'locale' => 'pt',
                        'text' => 'Existe um forte sentido de trabalho em equipa no meu local de trabalho'
                    ],
                    [
                        'locale' => 'pl',
                        'text' => 'W moim miejscu pracy panuje głębokie zaangażowanie w pracę zespołową.'
                    ],
                    ['locale' => 'lt', 'text' => 'Darbe vyrauja stipri komandinė dvasia'],
                ]
            ],
            [
                'key' => 'rtw-22',
                'category-key' => 'culture',
                'text' => 'Personal advice and support is available if I need it',
                'order' => 22,
                'type_id' => \App\Models\Question\Type::RADIO,
                'translations' => [
                    ['locale' => 'ar', 'text' => 'أجد النصح الشخصي والمساندة عندما أحتاج لهما'],
                    ['locale' => 'es', 'text' => 'Dispongo de asesoramiento y apoyo personal si los necesito'],
                    ['locale' => 'pa', 'text' => 'ਜੇ ਮੈਨੂੰ ਲੋੜ ਪੈਂਦੀ ਹੈ, ਤਾਂ ਨਿੱਜੀ ਸਲਾਹ ਅਤੇ ਸਹਾਇਤਾ ਮਿਲਦੀ ਹੈ'],
                    [
                        'locale' => 'pt',
                        'text' => 'Tenho aconselhamento e apoio pessoal à minha disposição se precisar deles'
                    ],
                    ['locale' => 'pl', 'text' => 'W razie potrzeby mogę skorzystać z doradztwa i wsparcia.'],
                    ['locale' => 'lt', 'text' => 'Prireikus visada sulaukiu asmeninių patarimų ir palaikymo'],
                ]
            ],
            [
                'key' => 'rtw-23',
                'category-key' => 'culture',
                'text' => 'I am satisfied and happy to work for my company',
                'order' => 23,
                'type_id' => \App\Models\Question\Type::RADIO,
                'translations' => [
                    ['locale' => 'ar', 'text' => 'أنا راض عن عملي لدى الشركة وسعيد به'],
                    ['locale' => 'es', 'text' => 'Estoy satisfecho y contento de trabajar para mi empresa'],
                    ['locale' => 'pa', 'text' => 'ਮੈਂ ਆਪਣੀ ਕੰਪਨੀ ਲਈ ਕੰਮ ਕਰਕੇ ਸੰਤੁਸ਼ਟ ਅਤੇ ਖ਼ੁਸ਼ ਹਾਂ'],
                    ['locale' => 'pt', 'text' => 'Estou satisfeito(a) e feliz por trabalhar para a minha empresa'],
                    ['locale' => 'pl', 'text' => 'Jestem zadowolony(-a) i cieszę się, że mogę tu pracować'],
                    ['locale' => 'lt', 'text' => 'Darbas mano bendrovėje teikia pasitenkinimą ir džiaugsmą'],
                ]
            ],
        );

        // Loop through each question above and create the record for it in the database
        foreach ($questions as $question) {
            $category = DB::table('question_categories')->where('key', '=', $question['category-key'])->first();

            $id = DB::table('questions')->insertGetId([
                'category_id' => $category->id,
                'question_set_id' => $setId->id,
                'order' => $question['order'],
                'type_id' => (isset($question['type_id']) ? $question['type_id'] : \App\Models\Question\Type::TEXT),
                'key' => $question['key'],
                'text' => $question['text']
            ]);

            DB::table('question_translations')->insert([
                'question_id' => $id,
                'locale' => 'en',
                'text' => $question['text']
            ]);

            foreach ($question["translations"] as $translation) {
                DB::table('question_translations')->insert([
                    'question_id' => $id,
                    'locale' => $translation["locale"],
                    'text' => $translation["text"]
                ]);
            }
        }

        // RTW Employer Questions
        $setId = DB::table('question_sets')->where('key', '=', 'rtw-employer')->first();

        $questions = [
            // Page 1 questions.
            [

                'key' => 'directly-employed-workers',
                'category-key' => 'organisation',
                'text' => 'Number of directly employed workers (including Fixed Term)',
                'type_id' => \App\Models\Question\Type::NUMBER,
            ],
            [
                'key' => 'use-of-labour',
                'category-key' => 'organisation',
                'text' => 'Use of labour providers in that location?',
                'type_id' => \App\Models\Question\Type::BOOLEAN
            ],
            [
                'key' => 'full-time-employed-workers',
                'category-key' => 'organisation',
                'text' => 'Number of full-time equivalents',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            [
                'key' => 'male-workers',
                'category-key' => 'demographics',
                'text' => 'Number of male workers',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            [
                'key' => 'female-workers',
                'category-key' => 'demographics',
                'text' => 'Number of female workers',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            [
                'key' => 'line-managers',
                'category-key' => 'demographics',
                'text' => 'Number of line managers',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            [
                'key' => 'middle-managers',
                'category-key' => 'demographics',
                'text' => 'Number of middle managers',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            [
                'key' => 'board-directors',
                'category-key' => 'demographics',
                'text' => 'Number of board directors',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            [
                'key' => 'hr-department',
                'category-key' => 'demographics',
                'text' => 'Size of HR Department',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            [
                'key' => 'first-languages',
                'category-key' => 'demographics',
                'text' => 'Number of first languages',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            [
                'key' => 'white-british',
                'category-key' => 'demographics',
                'text' => 'White British',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            [
                'key' => 'white-irish',
                'category-key' => 'demographics',
                'text' => 'White Irish',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            [
                'key' => 'other-white-background',
                'category-key' => 'demographics',
                'text' => 'Other White background',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            [
                'key' => 'white-and-black-caribbean',
                'category-key' => 'demographics',
                'text' => 'Mixed: White and Black Caribbean',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            [
                'key' => 'white-and-black-african',
                'category-key' => 'demographics',
                'text' => 'Mixed: White and Black African',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            [
                'key' => 'white-and-asian',
                'category-key' => 'demographics',
                'text' => 'Mixed: White and Asian',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            [
                'key' => 'other-mixed-background',
                'category-key' => 'demographics',
                'text' => 'Mixed: Other mixed background',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            [
                'key' => 'indian',
                'category-key' => 'demographics',
                'text' => 'Indian',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            [
                'key' => 'pakistani',
                'category-key' => 'demographics',
                'text' => 'Pakistani',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            [
                'key' => 'bangladeshi',
                'category-key' => 'demographics',
                'text' => 'Bangladeshi',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            [
                'key' => 'chinese',
                'category-key' => 'demographics',
                'text' => 'Chinese',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            [
                'key' => 'other-asian-background',
                'category-key' => 'demographics',
                'text' => 'Other Asian Background',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            [
                'key' => 'black-caribbean',
                'category-key' => 'demographics',
                'text' => 'Black Caribbean',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            [
                'key' => 'black-african',
                'category-key' => 'demographics',
                'text' => 'Black African',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            [
                'key' => 'other-black-background',
                'category-key' => 'demographics',
                'text' => 'Other Black background',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            [
                'key' => 'other-ethnic-group',
                'category-key' => 'demographics',
                'text' => 'Other Ethnic group',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            // Page 2 questions.
            // Communication
            [
                'key' => 'information-communication-policy',
                'category-key' => 'communication-employer',
                'text' => 'Do you have an information/communication policy?',
                'type_id' => \App\Models\Question\Type::BOOLEAN
            ],
            [
                'key' => 'multi-language-policy',
                'category-key' => 'communication-employer',
                'text' => 'Have you adopted a multi-language information/communication policy?',
                'type_id' => \App\Models\Question\Type::BOOLEAN
            ],
            [
                'key' => 'english-training',
                'category-key' => 'communication-employer',
                'text' => 'Do you provide English language training to your workforce?',
                'type_id' => \App\Models\Question\Type::BOOLEAN
            ],
            // HR
            [
                'key' => 'colleagues-within-hr-role',
                'category-key' => 'hr',
                'text' => 'Number of colleagues who operate within a HR role across the site',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            [
                'key' => 'dedicated-to-training',
                'category-key' => 'hr',
                'text' => 'Do you have a person working at the site who is dedicated to training?',
                'type_id' => \App\Models\Question\Type::BOOLEAN
            ],
            [
                'key' => 'hr-strategy-in-place',
                'category-key' => 'hr',
                'text' => 'Do you have a HR strategy in place?',
                'type_id' => \App\Models\Question\Type::BOOLEAN
            ],
            // Workplace Conflict & Culture
            [
                'key' => 'grievances-raised-in-last-year',
                'category-key' => 'workplace-conflict',
                'text' => 'How many grievances have been raised within the last 12 months?',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            [
                'key' => 'grievances-upheld-in-last-year',
                'category-key' => 'workplace-conflict',
                'text' => 'How many grievances were upheld?',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            [
                'key' => 'grievance-procedure',
                'category-key' => 'workplace-conflict',
                'text' => 'Do you have a grievance procedure?',
                'type_id' => \App\Models\Question\Type::BOOLEAN
            ],
            [
                'key' => 'employment-tribunals-in-last-year',
                'category-key' => 'workplace-conflict',
                'text' => 'How many Employment Tribunals has your worksite undertaken over the last 12 months?',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            [
                'key' => 'encourage-mediation',
                'category-key' => 'workplace-conflict',
                'text' => 'Do you encourage mediation as part of your grievance procedure?',
                'type_id' => \App\Models\Question\Type::BOOLEAN
            ],
            [
                'key' => 'mediations-completed-in-last-year',
                'category-key' => 'workplace-conflict',
                'text' => 'How many mediations have you completed over the last 12 months?',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            [
                'key' => 'equality-diversity-training',
                'category-key' => 'workplace-conflict',
                'text' => 'Do you provide Equality & Diversity training to Supervisors and Managers?',
                'type_id' => \App\Models\Question\Type::BOOLEAN
            ],
            [
                'key' => 'equality-diversity-training-to-all-workers',
                'category-key' => 'workplace-conflict',
                'text' => 'Do you provide Equality and Diversity training to all workers?',
                'type_id' => \App\Models\Question\Type::BOOLEAN
            ],
            [
                'key' => 'whistleblowing-policy',
                'category-key' => 'workplace-conflict',
                'text' => 'Do you have a whistleblowing policy in place?',
                'type_id' => \App\Models\Question\Type::BOOLEAN
            ],
            // Engagement
            [
                'key' => 'carry-out-annual-staff-engagement-survey',
                'category-key' => 'engagement',
                'text' => 'Do you carry out at least an annual staff engagement survey?',
                'type_id' => \App\Models\Question\Type::BOOLEAN
            ],
            // Performance
            [
                'key' => 'performance-appraisals-every-year',
                'category-key' => 'performance',
                'text' => 'Do you provide performance appraisals, at least annually, to all employees?',
                'type_id' => \App\Models\Question\Type::BOOLEAN
            ],
            [
                'key' => 'performance-capability-policy-process',
                'category-key' => 'performance',
                'text' => 'Do you have an under-performance/capability policy/process in place?',
                'type_id' => \App\Models\Question\Type::BOOLEAN
            ],
            [
                'key' => 'disciplinary-cases-in-last-year',
                'category-key' => 'performance',
                'text' => 'How many disciplinary cases have been carried out during the last 12 months?',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            [
                'key' => 'link-performance-to-pay',
                'category-key' => 'performance',
                'text' => 'Do you link performance to pay?',
                'type_id' => \App\Models\Question\Type::RADIO // 0 = No, 1 = yes, 2 = Partial
            ],
            // Absence
            [
                'key' => 'sickness-unauthorised-absence-in-last-year',
                'category-key' => 'absence',
                'text' => 'How many days sickness/unauthorised absence have your employees taken in the last 12 months?',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            // Turnover
            [
                'key' => 'people-left-in-last-year',
                'category-key' => 'turnover',
                'text' => 'How many people have left (voluntary or involuntary) the organisation over the last 12 month (please note this does not include subcontractors, temporary or agency workers)',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            [
                'key' => 'management-roles-recruited-in-last-year',
                'category-key' => 'turnover',
                'text' => 'How many management/supervisory roles have you recruited over the last 12 months?',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            [
                'key' => 'production-roles-recruited-in-last-year',
                'category-key' => 'turnover',
                'text' => 'How many production roles have you recruited in the last 12 months?',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            [
                'key' => 'operational-roles-recruited-in-last-year',
                'category-key' => 'turnover',
                'text' => 'How many operational roles (e.g. HR, finance, logistics, IT) have you recruited in the last 12 months?',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            // Training
            [
                'key' => 'total-spend-external-training-employees-in-last-year',
                'category-key' => 'training',
                'text' => 'What was your total spend on external training for your employees in the last 12 months?',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            [
                'key' => 'total-spend-external-training-agency-workers-in-last-year',
                'category-key' => 'training',
                'text' => 'What was your total spend on external training for Agency Workers in the last 12 months?',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            [
                'key' => 'recorded-internal-training-sessions-employees-in-last-year',
                'category-key' => 'training',
                'text' => 'How many recorded internal training sessions have your employees attended in the last 12 months?',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            [
                'key' => 'recorder-internal-training-sessions-agency-workers-in-last-year',
                'category-key' => 'training',
                'text' => 'How many recorded internal training sessions have you delivered to Agency Workers in the last 12 months?',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            // Agency Costs
            [
                'key' => 'cost-labour-provider-in-last-year',
                'category-key' => 'agency-costs',
                'text' => 'Please indicate the cost of your labour provider in the last 12 months? (this includes provider commission, cost and agency salary)',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
            [
                'key' => 'agency-workers-used-in-last-year',
                'category-key' => 'agency-costs',
                'text' => 'In the last 12 months how many agency workers have you used?',
                'type_id' => \App\Models\Question\Type::NUMBER
            ],
        ];

        // Loop through each question above and create the record for it in the database.
        foreach ($questions as $questionKey => $question) {
            $category = DB::table('question_categories')->where('key', '=', $question['category-key'])->first();

            $id = DB::table('questions')->insertGetId([
                'question_set_id' => $setId->id,
                'order' => $questionKey + 1,
                'category_id' => $category->id,
                'type_id' => (isset($question['type_id']) ? $question['type_id'] : \App\Models\Question\Type::TEXT),
                'key' => $question['key'],
                'text' => $question['text']
            ]);

            DB::table('question_translations')->insert([
                'question_id' => $id,
                'locale' => 'en',
                'text' => $question['text']
            ]);
        }
    }
}
