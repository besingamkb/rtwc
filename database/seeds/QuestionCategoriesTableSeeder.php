<?php

use Illuminate\Database\Seeder;

class QuestionCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = array(
            [
                'key' => 'operational',
                'name' => 'Operational'
            ],
            [
                'key' => 'sustainability',
                'name' => 'Sustainability'
            ],
            [
                'key' => 'people',
                'name' => 'People'
            ],
            [
                'key' => 'employer',
                'name' => 'Employer'
            ],
        );

        foreach ($categories as $category) {
            DB::table('question_categories')->insert([
                'key' => $category['key'],
                'name' => $category['name']
            ]);
        }

        $subcategories = array(
            ['parent-key' => 'operational',     'key' => 'discrimination',          'name' => 'Discrimination & Worker Mistreatment'],
            ['parent-key' => 'operational',     'key' => 'safety',                  'name' => 'Health & Safety Management'],
            ['parent-key' => 'operational',     'key' => 'freelychosen',            'name' => 'Employment is Freely Chosen'],
            ['parent-key' => 'operational',     'key' => 'freedom',                 'name' => 'Freedom of Association'],
            ['parent-key' => 'operational',     'key' => 'hours',                   'name' => 'Working Hours'],
            ['parent-key' => 'operational',     'key' => 'wages',                   'name' => 'Living Wages'],
            ['parent-key' => 'sustainability',  'key' => 'community',               'name' => 'Community Engagement'],
            ['parent-key' => 'sustainability',  'key' => 'environmental',           'name' => 'Environmental Impact'],
            ['parent-key' => 'people',          'key' => 'engagement',              'name' => 'Employee Engagement'],
            ['parent-key' => 'people',          'key' => 'communication',           'name' => 'Communication & Trust'],
            ['parent-key' => 'people',          'key' => 'culture',                 'name' => 'Culture & Cohesion'],
            ['parent-key' => 'employer',        'key' => 'organisation',            'name' => 'Organisation Details'],
            ['parent-key' => 'employer',        'key' => 'demographics',            'name' => 'Employee Demographics'],
            ['parent-key' => 'employer',        'key' => 'communication-employer',  'name' => 'Communication'],
            ['parent-key' => 'employer',        'key' => 'hr',                      'name' => 'HR'],
            ['parent-key' => 'employer',        'key' => 'workplace-conflict',      'name' => 'Workplace Conflict & Culture'],
            ['parent-key' => 'employer',        'key' => 'engagement-employer',     'name' => 'Engagement'],
            ['parent-key' => 'employer',        'key' => 'performance',             'name' => 'Performance'],
            ['parent-key' => 'employer',        'key' => 'absence',                 'name' => 'Absence'],
            ['parent-key' => 'employer',        'key' => 'turnover',                'name' => 'Turnover'],
            ['parent-key' => 'employer',        'key' => 'training',                'name' => 'Training'],
            ['parent-key' => 'employer',        'key' => 'agency-costs',            'name' => 'Agency Costs'],
        );

        // Loop through each user above and create the record for them in the database
        foreach ($subcategories as $subcategory) {
            $parentId = DB::table('question_categories')->where('key', $subcategory['parent-key'])->first();

            DB::table('question_categories')->insert([
                'parent_id' => $parentId->id,
                'key' => $subcategory['key'],
                'name' => $subcategory['name']
            ]);
        }
    }
}
