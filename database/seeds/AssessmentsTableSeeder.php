<?php

use Illuminate\Database\Seeder;

class AssessmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $account = DB::table('accounts')->first();

        $questionSet = DB::table('question_sets')->where('key', '=', 'rtw-jl')->first();

        $startDate = \Carbon\Carbon::now()->startOfMonth()->toDateString();
        $endDate = \Carbon\Carbon::now()->endOfMonth()->toDateString();

        \App\Models\Assessment::create([
            'key' => 'jadwe29dl',
            'name' => 'RTW',
            'question_set_id' => $questionSet->id,
            'account_id' => $account->id,
            'start_date' => $startDate,
            'end_date' => $endDate,
            'locales' => 'en,es,pt,pa,pl,lt,ar',
            'worker_count' => 50
        ]);
    }
}
