<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMaxQuestionResults extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Adds max question scores to the questions table.
        Schema::table('questions', function (Blueprint $table) {
            $table->decimal('max_score')->after('additional_text');
        });

        // Apply max score to all questions.
        $questions = \App\Models\Question::all();

        foreach ($questions as $questionSet) {
            $options = $questionSet->options;

            $questionTotals = $options->max('score');

            $questionSet->max_score = $questionTotals;
            $questionSet->save();
        }

        // Adds max question scores to the questions table.
        Schema::table('question_sets', function (Blueprint $table) {
            $table->decimal('max_score')->after('finished_text');
        });

        // Apply max score to all question sets.
        $questionSets = \App\Models\QuestionSet::all();

        foreach ($questionSets as $questionSet) {
            $questions = $questionSet->questions;

            $questionTotals = $questions->sum(function ($option) {
                return $option['max_score'];
            });

            $questionSet->max_score = $questionTotals;
            $questionSet->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('questions', function (Blueprint $table) {
            $table->dropColumn('max_score');
        });

        Schema::table('question_sets', function (Blueprint $table) {
            $table->dropColumn('max_score');
        });
    }
}
