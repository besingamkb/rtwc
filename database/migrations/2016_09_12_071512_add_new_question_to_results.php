<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewQuestionToResults extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('results_employer', function (Blueprint $table) {
            $table->unsignedInteger('average_percentage_of_labour_agency_workers_ftse')->nullable()->after('use_of_labour');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('results_employer', function (Blueprint $table) {
            $table->dropColumn('average_percentage_of_labour_agency_workers_ftse');
        });
    }
}
