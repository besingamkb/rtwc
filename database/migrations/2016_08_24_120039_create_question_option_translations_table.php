<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionOptionTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_option_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('question_option_id')->unsigned();
            $table->string('text');
            $table->string('locale')->index();

            $table->unique(['question_option_id','locale']);
            $table->foreign('question_option_id')->references('id')->on('question_options')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('question_option_translations');
    }
}
