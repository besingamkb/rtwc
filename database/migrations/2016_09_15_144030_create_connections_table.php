<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConnectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('connections', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('requester_id');
            $table->foreign(['requester_id'])->references('id')->on('accounts');

            $table->unsignedInteger('requestee_id');
            $table->foreign(['requestee_id'])->references('id')->on('accounts');

            $table->boolean('is_accepted')->nullable()->default(null);
            $table->timestamp('response_timestamp')->nullable();

            $table->unsignedInteger('created_by')->nullable();
            $table->foreign(['created_by'])->references('id')->on('users');

            $table->unsignedInteger('updated_by')->nullable();
            $table->foreign(['updated_by'])->references('id')->on('users');

            $table->unsignedInteger('deleted_by')->nullable();
            $table->foreign(['deleted_by'])->references('id')->on('users');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::drop('connections');
        Schema::enableForeignKeyConstraints();
    }
}
