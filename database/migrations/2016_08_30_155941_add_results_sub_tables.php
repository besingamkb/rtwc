<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddResultsSubTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Little check because there was a migration added in master that was not pulled across into develop.
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('results_worker');
        Schema::dropIfExists('results_employer');
        Schema::enableForeignKeyConstraints();

        Schema::table('answers', function (Blueprint $table) {
            $table->text('answer_text')->nullable()->after('question_option_id');
        });

        Schema::create('results_employer', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('result_id');

            $table->unsignedInteger('directly_employed_workers')->default(0);
            $table->boolean('use_of_labour')->default(0);
            $table->unsignedInteger('full_time_employed_workers')->default(0);
            $table->unsignedInteger('male_workers')->default(0);
            $table->unsignedInteger('female_workers')->default(0);
            $table->unsignedInteger('line_managers')->default(0);
            $table->unsignedInteger('middle_managers')->default(0);
            $table->unsignedInteger('board_directors')->default(0);
            $table->unsignedInteger('hr_department')->default(0);
            $table->unsignedInteger('first_languages')->default(0);
            $table->unsignedInteger('white_british')->default(0);
            $table->unsignedInteger('white_irish')->default(0);
            $table->unsignedInteger('other_white_background')->default(0);
            $table->unsignedInteger('white_and_black_caribbean')->default(0);
            $table->unsignedInteger('white_and_black_african')->default(0);
            $table->unsignedInteger('white_and_asian')->default(0);
            $table->unsignedInteger('other_mixed_background')->default(0);
            $table->unsignedInteger('indian')->default(0);
            $table->unsignedInteger('pakistani')->default(0);
            $table->unsignedInteger('bangladeshi')->default(0);
            $table->unsignedInteger('chinese')->default(0);
            $table->unsignedInteger('other_asian_background')->default(0);
            $table->unsignedInteger('black_caribbean')->default(0);
            $table->unsignedInteger('black_african')->default(0);
            $table->unsignedInteger('other_black_background')->default(0);
            $table->unsignedInteger('other_ethnic_group')->default(0);

            $table->timestamps();

            $table->foreign('result_id')->references('id')->on('results');
        });

        Schema::create('results_worker', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('result_id');
            $table->timestamps();

            $table->foreign('result_id')->references('id')->on('results');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::drop('results_worker');
        Schema::drop('results_employer');
        Schema::enableForeignKeyConstraints();

        Schema::table('answers', function (Blueprint $table) {
            $table->dropColumn('answer_text');
        });
    }
}
