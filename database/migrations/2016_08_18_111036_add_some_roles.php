<?php

use Illuminate\Database\Migrations\Migration;

class AddSomeRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $admin = \App\Models\Entrust\Role::forceCreate([
            'name' => 'admin',
            'display_name' => 'Admin',
            'description' => 'Admin role'
        ]);

        $user = \App\Models\Entrust\Role::forceCreate([
            'name' => 'user',
            'display_name' => 'User',
            'description' => 'Standard user role'
        ]);

        $canLogin = \App\Models\Entrust\Permission::forceCreate([
            'name' => 'can_login',
            'display_name' => 'Can login',
            'description' => 'A user can login to the system'
        ]);

        $admin->attachPermission($canLogin);
        $user->attachPermission($canLogin);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $admin = \App\Models\Entrust\Role::where('name', 'admin')->first();
        $user = \App\Models\Entrust\Role::where('name', 'user')->first();

        $canLogin = \App\Models\Entrust\Permission::where('name', 'can_login')->first();

        if($canLogin){
            $admin->detachPermission($canLogin);
            $user->detachPermission($canLogin);

            $canLogin->delete();
        }

        if($admin){
            $admin->delete();
        }

        if($user){
            $user->delete();
        }
    }
}
