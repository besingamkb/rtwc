<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSoftDeletesForAssessments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('questions', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('question_categories', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('question_sets', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('assessments', function (Blueprint $table) {
            $table->softDeletes();
            $table->string('name')->after('key');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assessments', function (Blueprint $table) {
            $table->dropSoftDeletes();
            $table->dropColumn('name');
        });

        Schema::table('question_sets', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('question_categories', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('questions', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
    }
}
