<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('results', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('assessment_id')->unsigned();
            $table->foreign('assessment_id')->references('id')->on('assessments');
            $table->integer('status')->default(0);
            $table->string('ip_address')->default(0);
            $table->string('locale');
            $table->integer('gender');
            $table->integer('age');
            $table->integer('employsyou');
            $table->integer('level');
            $table->integer('role');
            $table->integer('shift');
            $table->integer('howlong');
            $table->integer('ethnicity');
            $table->decimal('total_score', 5, 2);
            $table->decimal('discrimination_score', 5, 2);
            $table->decimal('safety_score', 5, 2);
            $table->decimal('freelychosen_score', 5, 2);
            $table->decimal('freedom_score', 5, 2);
            $table->decimal('hours_score', 5, 2);
            $table->decimal('wages_score', 5, 2);
            $table->decimal('community_score', 5, 2);
            $table->decimal('environmental_score', 5, 2);
            $table->decimal('engagement_score', 5, 2);
            $table->decimal('communication_score', 5, 2);
            $table->decimal('culture_score', 5, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('results');
    }
}
