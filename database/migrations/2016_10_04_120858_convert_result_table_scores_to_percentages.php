<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Result;
use App\Models\QuestionSet;

class ConvertResultTableScoresToPercentages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Seeders run after migrations so this will not be applicable for Travis.
        /*if (config('app.env') != 'testing') {

            // get category maximums
            $categoriesMax = [];

            $questions = QuestionSet::where('key', '=', 'rtw-jl')->firstOrFail()->questions;

            foreach ($questions as $question) {
                $categoryKey = $question->category->key . "_score";
                $categoryMax = (isset($categoriesMax[$categoryKey]) ? $categoriesMax[$categoryKey] : 0) + $question->max_score;
                $categoriesMax[$categoryKey] = $categoryMax;
            }

            //
            $results = Result::all();
            foreach($results as $result){

                $result->total_score = $result->total_score / $result->assessment->questionSet->max_score;

                foreach ($categoriesMax as $categoryKey => $categoryMax) {
                    $result->$categoryKey = $result->$categoryKey / $categoryMax;
                }

                $result->save();
            }

        }*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
