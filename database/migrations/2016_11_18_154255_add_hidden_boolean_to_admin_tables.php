<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHiddenBooleanToAdminTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('question_sets', function (Blueprint $table) {
            $table->boolean('is_editable')->default(true)->after('name');
        });

        $questionSet = \App\Models\QuestionSet::where('key', 'rtw-demographics')->first();

        if($questionSet){
            $questionSet->is_editable = false;
            $questionSet->save();
        }

        $questionSet = \App\Models\QuestionSet::where('key', 'rtw-employer')->first();

        if($questionSet){
            $questionSet->is_editable = false;
            $questionSet->save();
        }

        $questionSet = \App\Models\QuestionSet::where('key', 'rtw')->first();
        
        if($questionSet){
            $questionSet->is_editable = false;
            $questionSet->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('question_sets', function (Blueprint $table) {
            $table->dropColumn('is_editable');
        });
    }
}
