<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdditionalTextToAssessmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table(
            'assessments',
            function (Blueprint $table) {
                $table->boolean('additional_text')->after('locales');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table(
            'assessments',
            function (Blueprint $table) {
                $table->dropColumn('additional_text');
            }
        );
    }
}
