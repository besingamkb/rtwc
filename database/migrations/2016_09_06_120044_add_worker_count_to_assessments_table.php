<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWorkerCountToAssessmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table(
            'assessments',
            function (Blueprint $table) {
                $table->integer('worker_count')->after('question_set_id');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table(
            'assessments',
            function (Blueprint $table) {
                $table->dropColumn('worker_count');
            }
        );
    }
}
