<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAlertCountToResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('results', function (Blueprint $table) {
            $table->unsignedInteger('alert_count')->default(0)->after('ethnicity');
        });

        $sql = "UPDATE results SET alert_count = (SELECT count(*) FROM alerts WHERE alerts.result_id=results.id) WHERE results.question_set_id=2 AND results.status=10";
        DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('results', function (Blueprint $table) {
            $table->dropColumn('alert_count');
        });
    }
}
