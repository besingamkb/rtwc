<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionSetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'question_sets',
            function (Blueprint $table) {
                $table->increments('id');
                $table->string('key')->unique();
                $table->string('name');
                $table->text('welcome_text');
                $table->text('intro_text');
                $table->text('finished_text');
                $table->string('type');
                $table->integer('status');
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('question_sets');
    }
}
