<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewEmployerQuestionsResults extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('results_employer', function (Blueprint $table) {
            // Change previous results to nullable.
            $table->unsignedInteger('directly_employed_workers')->nullable()->change();
            $table->unsignedInteger('use_of_labour')->nullable()->change();
            $table->unsignedInteger('full_time_employed_workers')->nullable()->change();
            $table->unsignedInteger('male_workers')->nullable()->change();
            $table->unsignedInteger('female_workers')->nullable()->change();
            $table->unsignedInteger('line_managers')->nullable()->change();
            $table->unsignedInteger('middle_managers')->nullable()->change();
            $table->unsignedInteger('board_directors')->nullable()->change();
            $table->unsignedInteger('hr_department')->nullable()->change();
            $table->unsignedInteger('first_languages')->nullable()->change();
            $table->unsignedInteger('white_british')->nullable()->change();
            $table->unsignedInteger('white_irish')->nullable()->change();
            $table->unsignedInteger('other_white_background')->nullable()->change();
            $table->unsignedInteger('white_and_black_caribbean')->nullable()->change();
            $table->unsignedInteger('white_and_black_african')->nullable()->change();
            $table->unsignedInteger('white_and_asian')->nullable()->change();
            $table->unsignedInteger('other_mixed_background')->nullable()->change();
            $table->unsignedInteger('indian')->nullable()->change();
            $table->unsignedInteger('pakistani')->nullable()->change();
            $table->unsignedInteger('bangladeshi')->nullable()->change();
            $table->unsignedInteger('chinese')->nullable()->change();
            $table->unsignedInteger('other_asian_background')->nullable()->change();
            $table->unsignedInteger('black_caribbean')->nullable()->change();
            $table->unsignedInteger('black_african')->nullable()->change();
            $table->unsignedInteger('other_black_background')->nullable()->change();
            $table->unsignedInteger('other_ethnic_group')->nullable()->change();

            // Create page 2 result columns.
            $table->unsignedInteger('information_communication_policy')->nullable()->after('other_ethnic_group');
            $table->unsignedInteger('multi_language_policy')->nullable()->after('other_ethnic_group');
            $table->unsignedInteger('english_training')->nullable()->after('other_ethnic_group');
            $table->unsignedInteger('colleagues_within_hr_role')->nullable()->after('other_ethnic_group');
            $table->unsignedInteger('dedicated_to_training')->nullable()->after('other_ethnic_group');
            $table->unsignedInteger('hr_strategy_in_place')->nullable()->after('other_ethnic_group');
            $table->unsignedInteger('grievances_raised_in_last_year')->nullable()->after('other_ethnic_group');
            $table->unsignedInteger('grievances_upheld_in_last_year')->nullable()->after('other_ethnic_group');
            $table->unsignedInteger('grievance_procedure')->nullable()->after('other_ethnic_group');
            $table->unsignedInteger('employment_tribunals_in_last_year')->nullable()->after('other_ethnic_group');
            $table->unsignedInteger('encourage_mediation')->nullable()->after('other_ethnic_group');
            $table->unsignedInteger('mediations_completed_in_last_year')->nullable()->after('other_ethnic_group');
            $table->unsignedInteger('equality_diversity_training')->nullable()->after('other_ethnic_group');
            $table->unsignedInteger('equality_diversity_training_to_all_workers')->nullable()->after('other_ethnic_group');
            $table->unsignedInteger('whistleblowing_policy')->nullable()->after('other_ethnic_group');
            $table->unsignedInteger('carry_out_annual_staff_engagement_survey')->nullable()->after('other_ethnic_group');
            $table->unsignedInteger('performance_appraisals_every_year')->nullable()->after('other_ethnic_group');
            $table->unsignedInteger('performance_capability_policy_process')->nullable()->after('other_ethnic_group');
            $table->unsignedInteger('disciplinary_cases_in_last_year')->nullable()->after('other_ethnic_group');
            $table->unsignedInteger('link_performance_to_pay')->nullable()->after('other_ethnic_group');
            $table->unsignedInteger('sickness_unauthorised_absence_in_last_year')->nullable()->after('other_ethnic_group');
            $table->unsignedInteger('people_left_in_last_year')->nullable()->after('other_ethnic_group');
            $table->unsignedInteger('management_roles_recruited_in_last_year')->nullable()->after('other_ethnic_group');
            $table->unsignedInteger('production_roles_recruited_in_last_year')->nullable()->after('other_ethnic_group');
            $table->unsignedInteger('operational_roles_recruited_in_last_year')->nullable()->after('other_ethnic_group');
            $table->unsignedInteger('total_spend_external_training_employees_in_last_year')->nullable()->after('other_ethnic_group');
            $table->unsignedInteger('total_spend_external_training_agency_workers_in_last_year')->nullable()->after('other_ethnic_group');
            $table->unsignedInteger('recorded_internal_training_sessions_employees_in_last_year')->nullable()->after('other_ethnic_group');
            $table->unsignedInteger('recorder_internal_training_sessions_agency_workers_in_last_year')->nullable()->after('other_ethnic_group');
            $table->unsignedInteger('cost_labour_provider_in_last_year')->nullable()->after('other_ethnic_group');
            $table->unsignedInteger('agency_workers_used_in_last_year')->nullable()->after('other_ethnic_group');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('results_employer', function (Blueprint $table) {
            // Revert previous results to not nullable.
            $table->unsignedInteger('directly_employed_workers')->nullable(false)->change();
            $table->unsignedInteger('use_of_labour')->nullable(false)->change();
            $table->unsignedInteger('full_time_employed_workers')->nullable(false)->change();
            $table->unsignedInteger('male_workers')->nullable(false)->change();
            $table->unsignedInteger('female_workers')->nullable(false)->change();
            $table->unsignedInteger('line_managers')->nullable(false)->change();
            $table->unsignedInteger('middle_managers')->nullable(false)->change();
            $table->unsignedInteger('board_directors')->nullable(false)->change();
            $table->unsignedInteger('hr_department')->nullable(false)->change();
            $table->unsignedInteger('first_languages')->nullable(false)->change();
            $table->unsignedInteger('white_british')->nullable(false)->change();
            $table->unsignedInteger('white_irish')->nullable(false)->change();
            $table->unsignedInteger('other_white_background')->nullable(false)->change();
            $table->unsignedInteger('white_and_black_caribbean')->nullable(false)->change();
            $table->unsignedInteger('white_and_black_african')->nullable(false)->change();
            $table->unsignedInteger('white_and_asian')->nullable(false)->change();
            $table->unsignedInteger('other_mixed_background')->nullable(false)->change();
            $table->unsignedInteger('indian')->nullable(false)->change();
            $table->unsignedInteger('pakistani')->nullable(false)->change();
            $table->unsignedInteger('bangladeshi')->nullable(false)->change();
            $table->unsignedInteger('chinese')->nullable(false)->change();
            $table->unsignedInteger('other_asian_background')->nullable(false)->change();
            $table->unsignedInteger('black_caribbean')->nullable(false)->change();
            $table->unsignedInteger('black_african')->nullable(false)->change();
            $table->unsignedInteger('other_black_background')->nullable(false)->change();
            $table->unsignedInteger('other_ethnic_group')->nullable(false)->change();

            // Drop page 2 employer result columns.
            $table->dropColumn('information_communication_policy');
            $table->dropColumn('multi_language_policy');
            $table->dropColumn('english_training');
            $table->dropColumn('colleagues_within_hr_role');
            $table->dropColumn('dedicated_to_training');
            $table->dropColumn('hr_strategy_in_place');
            $table->dropColumn('grievances_raised_in_last_year');
            $table->dropColumn('grievances_upheld_in_last_year');
            $table->dropColumn('grievance_procedure');
            $table->dropColumn('employment_tribunals_in_last_year');
            $table->dropColumn('encourage_mediation');
            $table->dropColumn('mediations_completed_in_last_year');
            $table->dropColumn('equality_diversity_training');
            $table->dropColumn('equality_diversity_training_to_all_workers');
            $table->dropColumn('whistleblowing_policy');
            $table->dropColumn('carry_out_annual_staff_engagement_survey');
            $table->dropColumn('performance_appraisals_every_year');
            $table->dropColumn('performance_capability_policy_process');
            $table->dropColumn('disciplinary_cases_in_last_year');
            $table->dropColumn('link_performance_to_pay');
            $table->dropColumn('sickness_unauthorised_absence_in_last_year');
            $table->dropColumn('people_left_in_last_year');
            $table->dropColumn('management_roles_recruited_in_last_year');
            $table->dropColumn('production_roles_recruited_in_last_year');
            $table->dropColumn('operational_roles_recruited_in_last_year');
            $table->dropColumn('total_spend_external_training_employees_in_last_year');
            $table->dropColumn('total_spend_external_training_agency_workers_in_last_year');
            $table->dropColumn('recorded_internal_training_sessions_employees_in_last_year');
            $table->dropColumn('recorder_internal_training_sessions_agency_workers_in_last_year');
            $table->dropColumn('cost_labour_provider_in_last_year');
            $table->dropColumn('agency_workers_used_in_last_year');
        });
    }
}
