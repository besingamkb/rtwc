<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MergeIsCompletedToStatus10 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Mark all completed results as a status of 10.
        $results = \App\Models\Result::all();

        foreach ($results as $result) {
            if ($result->is_completed) {
                $result->status = 10;
                $result->save();
            }
        }

        // Drop the is_completed column.
        Schema::table('results', function (Blueprint $table) {
            $table->dropColumn('is_completed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Re-add the is_completed column.
        Schema::table('results', function (Blueprint $table) {
            $table->boolean('is_completed');
        });
    }
}
