<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'questions',
            function (Blueprint $table) {
                $table->increments('id');
                $table->integer('question_set_id')->unsigned();
                $table->foreign('question_set_id')->references('id')->on('question_sets');
                $table->integer('category_id')->unsigned()->nullable();
                $table->foreign('category_id')->references('id')->on('question_categories');
                $table->integer('order');
                $table->string('key');
                $table->unique(['question_set_id','key']);
                $table->integer('type');
                $table->string('text');
                $table->text('additional_text');
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('questions');
    }
}
