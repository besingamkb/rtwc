<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQuestionSetToResults extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('results', function (Blueprint $table) {
            $table->unsignedInteger('question_set_id')->after('assessment_id');
        });

        // Set all of the existing results question set ID.
        $workerQuestionSet = \App\Models\QuestionSet::where('key', 'rtw-jl')->first();

        $results = \App\Models\Result::all();

        foreach ($results as $result) {
            $result->question_set_id = $workerQuestionSet->id;
            $result->save();
        }

        // Set the foreign key restraint after the edit because they will be 0 initially.
        Schema::table('results', function (Blueprint $table) {
            $table->foreign(['question_set_id'])->references('id')->on('question_sets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::enableForeignKeyConstraints();

        Schema::table('results', function (Blueprint $table) {
            $table->dropForeign(['question_set_id']);
            $table->dropColumn('question_set_id');
        });

        Schema::disableForeignKeyConstraints();
    }
}
