<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionSetTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_set_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('question_set_id')->unsigned();
            $table->text('welcome_text');
            $table->text('intro_text');
            $table->text('finished_text'); 
            $table->string('locale')->index();

            $table->unique(['question_set_id','locale']);
            $table->foreign('question_set_id')->references('id')->on('question_sets')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('question_set_translations');
    }
}
