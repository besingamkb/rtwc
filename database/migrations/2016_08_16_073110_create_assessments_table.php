<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssessmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create(
            'assessments',
            function (Blueprint $table) {
                $table->increments('id');
                $table->string('key')->unique();
                $table->integer('account_id')->unsigned();
                $table->foreign('account_id')->references('id')->on('accounts');
                $table->integer('question_set_id')->unsigned();
                $table->foreign('question_set_id')->references('id')->on('question_sets');
                $table->date('start_date');
                $table->date('end_date');
                $table->integer('status');
                $table->string('locales');
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('assessments');
    }
}
