<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use League\Csv\Reader;
use App\Models\QuestionSet;

class AddRomanianLanguageToJlQuestionSet extends Migration
{
    protected $language = 'ro';
    protected $questionSet = 'rtw-jl';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Seeders run after migrations so this will not be applicable for Travis.
        /*if (config('app.env') != 'testing') {

            $questionSetText = [
                    'welcome_text' => 'Bună ziua și bun venit la Responsible Trade Worldwide',
                    'intro_text' => 'Avem deosebita plăcere de a vă informa că efectuăm un sondaj independent în numele organizației dumneavoastră.

    Completarea acestui chestionar nu ar trebui să dureze mai mult de 10 minute. Orice informații furnizate vor rămâne confidențiale și anonime. 

    Citiți fiecare enunț și indicați în ce măsură sunteți sau nu de acord bifând una dintre opțiuni.',
                    'finished_text' => 'Vă mulțumim pentru participarea la această evaluare. Opinia dumneavoastră și munca întreprinsă în cadrul organizației sunt relevante pentru efectuarea cu succes a evaluării.',
            ];

            $rtwQuestionOptions = [
                'Strongly Agree' => 'Total de acord',
                'Agree' => 'De acord',
                'Disagree' => 'Nu sunt de acord',
                'Strongly Disagree' => 'Nu sunt deloc de acord',
                'Not Sure' => 'Nu sunt sigur(ă)',
            ];

            // update the question set translations
            $questionSet = QuestionSet::key($this->questionSet)->firstOrFail();

            $questionSetTranslation = $questionSet->getNewTranslation($this->language);
            $questionSetTranslation->question_set_id = $questionSet->id;
            $questionSetTranslation->welcome_text = $questionSetText['welcome_text'];
            $questionSetTranslation->intro_text = $questionSetText['intro_text'];
            $questionSetTranslation->finished_text = $questionSetText['finished_text'];
            $questionSetTranslation->save();


            // get the questions csv
            $csv = Reader::createFromPath(storage_path('imports/' . $this->language . '/questions.csv'));

            // skip the first row as it's headers
            $rows = $csv->setOffset(1)->fetchAll();

            // loop through each question adding the translation
            foreach($rows as $row){

                // skip any blank rows
                if($row[0]==""){
                    continue;
                }

                // find the question
                $question = QuestionSet::key($row[0])
                    ->firstOrFail()
                    ->questions()
                    ->with('options')
                    ->key($row[1])
                    ->firstOrFail();

                // create the new translation
                $questionTranslation = $question->getNewTranslation($this->language);
                $questionTranslation->question_id = $question->id;
                $questionTranslation->text = $row[3];
                if(isset($row[4]) && $row[4]!='')
                {
                    $questionTranslation->additional_text = $row[4];
                }
                $questionTranslation->save();

                // if this question type is rtw then add the rtw option translations
                if($row[0]=="rtw-jl") 
                {
                    foreach($question->options as $questionOption)
                    {
                        $questionOptionTranslation = $questionOption->getNewTranslation($this->language);
                        $questionOptionTranslation->question_option_id = $questionOption->id;
                        $questionOptionTranslation->text = $rtwQuestionOptions[$questionOption->text];
                        $questionOptionTranslation->save();
                    }
                }
            }


            // get the question options csv
            $csv = Reader::createFromPath(storage_path('imports/' . $this->language . '/options.csv'));

            // skip the first row as it's headers
            $rows = $csv->setOffset(1)->fetchAll();

            // loop through each question option adding the translation
            foreach($rows as $row){

                // skip any blank rows
                if($row[0]==""){
                    continue;
                }

                // find the question 
                $question = QuestionSet::key($row[0])
                    ->firstOrFail()
                    ->questions()
                    ->with('options')
                    ->key($row[1])
                    ->firstOrFail();

                // find the question option
                $questionOption = $question->options()->where('text', $row[2])->firstOrFail();

                $questionOptionTranslation = $questionOption->getNewTranslation($this->language);
                $questionOptionTranslation->question_option_id = $questionOption->id;
                $questionOptionTranslation->text = $row[3];
                $questionOptionTranslation->save();
            }
        }*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        App\Models\QuestionSetTranslation::where('locale', $this->language)->delete();
        App\Models\QuestionTranslation::where('locale', $this->language)->delete();
        App\Models\QuestionOptionTranslation::where('locale', $this->language)->delete();
    }
}
