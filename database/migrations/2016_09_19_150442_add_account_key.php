<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAccountKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounts', function (Blueprint $table) {
            $table->string('key')->after('id');
        });

        // Seeders run after migrations so this will not be applicable for Travis.
        /*if (config('app.env') != 'testing') {
            \App\Models\Account::where('name', 'So Technology Ltd')
                ->first()->forceFill(['key' => 'sotech4232'])->save();
            \App\Models\Account::where('name', 'Hypnos')
                ->first()->forceFill(['key' => 'hyp2211'])->save();
            \App\Models\Account::where('name', 'Mereway Kitchens')
                ->first()->forceFill(['key' => 'merek8674'])->save();
            \App\Models\Account::where('name', 'Whitehall Fabrications Ltd')
                ->first()->forceFill(['key' => 'whitfab2211'])->save();
            \App\Models\Account::where('name', 'Gainsborough')
                ->first()->forceFill(['key' => 'gains1120'])->save();
            \App\Models\Account::where('name', 'Westbridge 2')
                ->first()->forceFill(['key' => 'westb1124'])->save();
            \App\Models\Account::where('name', 'Parker Knoll')
                ->first()->forceFill(['key' => 'park2049'])->save();
            \App\Models\Account::where('name', 'Cyan Tests Ltd')
                ->first()->forceFill(['key' => 'cyant2281'])->save();
        }*/

        Schema::table('accounts', function (Blueprint $table) {
            $table->string('key')->unique()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounts', function (Blueprint $table) {
            $table->dropColumn('key');
        });
    }
}
