export function RoutesConfig($stateProvider, $urlRouterProvider) {
    'ngInject';

    let getView = (viewName) => {
        return `./views/app/pages/${viewName}/${viewName}.page.html`;
    };

    let getSubView = (parentViewName, viewName) => {
        return `./views/app/pages/${parentViewName}/${viewName}/${viewName}.page.html`;
    };

    $urlRouterProvider.otherwise('/dashboard');

    $stateProvider
        .state('app', {
            abstract: true,
            data: {},
            views: {
                header: {
                    templateUrl: getView('header')
                },
                sidebar: {
                    templateUrl: getView('sidebar')
                },
                main: {}
            }
        })

        /**
         * Dashboard Routes.
         */
        .state('app.dashboard', {
            url: '/dashboard',
            views: {
                'main@': {
                    templateUrl: getView('dashboard')
                }
            },
            data: {
                class: 'user dashboard'
            }
        })

        /**
         * Connection Routes.
         */
        .state('app.connections', {
            url: '/connections',
            views: {
                'main@': {
                    templateUrl: getView('connections')
                }
            },
            data: {
                class: 'user connections'
            }
        })

        /**
         * Reporting Routes.
         */
        .state('app.reporting-assessments', {
            url: '/reporting/assessments',
            views: {
                'main@': {
                    templateUrl: getSubView('reporting', 'assessments')
                }
            },
            data: {
                class: 'user reporting assessments'
            }
        })
        .state('app.reporting-summary', {
            url: '/reporting/summary',
            views: {
                'main@': {
                    templateUrl: getSubView('reporting', 'summary')
                }
            },
            data: {
                class: 'user reporting summary'
            }
        })
        .state('app.reporting-benchmark', {
            url: '/reporting/benchmark',
            views: {
                'main@': {
                    templateUrl: getSubView('reporting', 'benchmark')
                }
            },
            data: {
                class: 'user reporting benchmark'
            }
        })
        .state('app.reporting-demographics', {
            url: '/reporting/demographics',
            views: {
                'main@': {
                    templateUrl: getSubView('reporting', 'demographics')
                }
            },
            data: {
                class: 'user reporting demographics'
            }
        })
        .state('app.reporting-detail', {
            url: '/reporting/detail',
            views: {
                'main@': {
                    templateUrl: getSubView('reporting', 'detail')
                }
            },
            data: {
                class: 'user reporting detail'
            }
        })
        .state('app.reporting-alerts', {
            url: '/reporting/alerts',
            views: {
                'main@': {
                    templateUrl: getSubView('reporting', 'alerts')
                }
            },
            data: {
                class: 'user reporting alerts'
            }
        })
        .state('app.reporting-metrics', {
            url: '/reporting/metrics',
            views: {
                'main@': {
                    templateUrl: getSubView('reporting', 'metrics')
                }
            },
            data: {
                class: 'user reporting metrics'
            }
        })
        .state('app.reporting-comparative', {
            url: '/reporting/comparative',
            views: {
                'main@': {
                    templateUrl: getSubView('reporting', 'comparative')
                }
            },
            data: {
                class: 'user reporting comparative'
            }
        })

        /**
         * Assessment Routes.
         */
        .state('app.assessments', {
            url: '/assessments',
            views: {
                'main@': {
                    templateUrl: getView('assessments')
                }
            },
            data: {
                class: 'user assessments'
            }
        })
        .state('app.assessments-assessment', {
            url: '/assessments/:assessmentID',
            views: {
                'main@': {
                    templateUrl: getSubView('assessments', 'assessment')
                }
            },
            data: {
                class: 'user assessments assessment'
            }
        })
        .state('app.assessment-take-employer', {
            url: '/assessment/take/:assessmentKey',
            views: {
                'main@': {
                    templateUrl: getSubView('assessments', 'employer')
                }
            },
            data: {
                class: 'user assessment employer'
            }
        })

        /**
         * Alert Routes.
         */
        .state('app.alerts', {
            url: '/alerts',
            views: {
                'main@': {
                    templateUrl: getView('alerts')
                }
            },
            data: {
                class: 'user alerts'
            }
        })

        /**
         * Help Routes.
         */
        .state('app.help', {
            url: '/help',
            views: {
                'main@': {
                    templateUrl: getView('help')
                }
            },
            data: {
                class: 'user help'
            }
        })

        /**
         * Admin Routes.
         */
        .state('app.admin-accounts', {
            url: '/admin/accounts',
            views: {
                'main@': {
                    templateUrl: getSubView('admin', 'accounts')
                }
            },
            data: {
                class: 'user admin accounts'
            }
        })
        .state('app.admin-assessments', {
            url: '/admin/assessments',
            views: {
                'main@': {
                    templateUrl: getSubView('admin', 'assessments')
                }
            },
            data: {
                class: 'user admin assessments'
            }
        })
        .state('app.admin-users', {
            url: '/admin/users',
            views: {
                'main@': {
                    templateUrl: getSubView('admin', 'users')
                }
            },
            data: {
                class: 'user admin users'
            }
        })
        .state('app.admin-question-sets', {
            url: '/admin/question-sets',
            views: {
                'main@': {
                    templateUrl: getSubView('admin', 'question-sets')
                }
            },
            data: {
                class: 'user admin question-sets'
            }
        })
        .state('app.admin-question-categories', {
            url: '/admin/question-categories',
            views: {
                'main@': {
                    templateUrl: getSubView('admin', 'question-categories')
                }
            },
            data: {
                class: 'user admin question-categories'
            }
        })
        .state('app.admin-questions', {
            url: '/admin/questions',
            views: {
                'main@': {
                    templateUrl: getSubView('admin', 'questions')
                }
            },
            data: {
                class: 'user admin questions'
            }
        })
        .state('app.admin-question-set-questions', {
            url: '/admin/question-sets/:id/questions',
            views: {
                'main@': {
                    templateUrl: getSubView('admin', 'questions')
                }
            },
            data: {
                class: 'user admin questions'
            }
        })

        /**
         * Account Routes.
         */
        .state('app.login', {
            url: '/login',
            views: {
                'main@': {
                    templateUrl: getView('login')
                }
            },
            data: {
                class: 'admin login auth-screen'
            }
        })
        .state('app.forgot_password', {
            url: '/forgot-password',
            views: {
                'main@': {
                    templateUrl: getView('forgot-password')
                }
            },
            data: {
                class: 'admin forgot-password auth-screen'
            }
        })
        .state('app.reset_password', {
            url: '/reset-password/:email/:token',
            views: {
                'main@': {
                    templateUrl: getView('reset-password')
                }
            },
            data: {
                class: 'admin reset-password auth-screen'
            }
        });
}
