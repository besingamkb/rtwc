import {RoutesRun} from './run/routes.run';
import {LoaderRun} from './run/site_loader.run';
import {BodyClassRun} from './run/body_class.run';
import {AuthCheckRun} from './run/auth_check.run';

angular.module('app.run')
    .run(RoutesRun)
    .run(LoaderRun)
    .run(BodyClassRun)
    .run(AuthCheckRun)
    .run(function (amMoment) {
        amMoment.changeLocale('Europe/London');
    });
