class ReportingDetailController {
    constructor(API, $log, ToastService, $rootScope) {
        'ngInject';

        this.API = API;
        this.$log = $log;
        this.ToastService = ToastService;
        this.$rootScope = $rootScope;
    }

    $onInit() {
        this.$rootScope.hideSidebar = false;
        this.$rootScope.hideViewLoadingCover = true;
        this.$rootScope.sidebar = 'reporting';

        this.$rootScope.page = {
            key: 'reporting-detail',
            name: 'RTW Report – Detail View'
        };

        // Map the global getReportData function to the local getReportData function.
        this.$rootScope.getReportData = () => {
            this.getReportData();
        };

        // Compact the selected assessment ID's down to their ID's only.
        this.assessments = this.$rootScope.$localStorage.reporting.assessments.map((assessment) => {
            return assessment.id;
        });
    }

    $postLink() {
        this.getReportData();
    }

    /**
     * Fetch the report data from the categories API.
     */
    getReportData() {

        // if no assessments selected redirect and toast
        if(!this.$rootScope.$localStorage.reporting.assessments || this.$rootScope.$localStorage.reporting.assessments.length==0){
            this.ToastService.show('No Assessments Selected');
            //window.location.href = '#/reporting/assessments';
            return false;
        }

        // Fire off the report data request for categories.
        this.API.one('reporting/detail').customPOST({
            assessments: this.assessments,
            filters: this.$rootScope.filterForm
        }).then((response) => {
                if (!response.data) {
                    return false;
                }

                // Order all categories into parent category > child category.
                this.questions = [];
                this.categoryTotals = [];
                this.total = 0;

                this.parentCategoryId = 0;
                this.categoryId = 0;

                // Make all sub-categories hidden and normal categories not-hidden.
                angular.forEach(response.data, (question, questionKey) => {

                    // parent categories = 0, categories = 1, questions = 2
                    question.level = 2;

                    // set all the questions to hidden
                    question.is_hidden = true;

                    // if it's the first hit on a parent category add it to the array
                    if(question.parent_category_id!=this.parentCategoryId){
                        this.questions.push({ 
                            text: question.parent_category_name, 
                            score: 0,
                            level: 0,
                            category_id: question.parent_category_id,
                            is_category: true,
                            is_showing_children: true,
                        });
                        this.parentCategoryId = question.parent_category_id;
                    }

                    // if it's the first hit on a category add it to the array
                    if(question.category_id!=this.categoryId){
                        this.questions.push({ 
                            text: question.category_name, 
                            score: 0,
                            level: 1,
                            category_id: question.category_id,
                            is_category: true,
                            is_showing_children: false,
                        });
                        this.categoryId = question.category_id;
                    }

                    // add to the category totals array (parent categories)
                    if(this.categoryTotals[question.parent_category_id]) {
                        this.categoryTotals[question.parent_category_id].total += question.total;
                        this.categoryTotals[question.parent_category_id].count += 1;
                    } else {
                        this.categoryTotals[question.parent_category_id] = { total: question.total, count: 1 }; 
                    }

                    // add to the category totals array
                    if(this.categoryTotals[question.category_id]) {
                        this.categoryTotals[question.category_id].total += question.total;
                        this.categoryTotals[question.category_id].count += 1;
                    } else {
                        this.categoryTotals[question.category_id] = { total: question.total, count: 1 }; 
                    }

                    // for the total score
                    this.total += question.total;

                    // add the question to questions array
                    this.questions.push(question);
                });

                this.total = this.total / response.data.length;

            }, (response) => {
                this.showCharts = false;
                this.ToastService.show(response.data.errors[0]);
            });
    }


    /**
     * Show or hide all children categories for a given parent ID.
     *
     * @param category
     * 
     */

    showChildren(category) {
        for (let question of this.questions) {
            if ((question.category_id != category.category_id) || question.is_category) {
                continue;
            }
            question.is_hidden = (question.is_hidden != true);
        }

        category.is_showing_children = (category.is_showing_children != true);
    }
}

export const ReportingDetailComponent = {
    templateUrl: './views/app/components/reporting/detail/detail.component.html',
    controller: ReportingDetailController,
    controllerAs: 'vm',
    bindings: {}
};
