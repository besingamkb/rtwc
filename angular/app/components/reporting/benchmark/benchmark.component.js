class ReportingBenchmarkController {
    constructor(API, $log, ToastService, $rootScope, $scope) {
        'ngInject';

        this.API = API;
        this.$log = $log;
        this.ToastService = ToastService;
        this.$rootScope = $rootScope;
        this.$scope = $scope;
    }

    $onInit() {
        this.$rootScope.hideSidebar = false;
        this.$rootScope.hideViewLoadingCover = true;
        this.$rootScope.sidebar = 'reporting';

        this.$rootScope.page = {
            key: 'reporting-benchmark',
            name: 'RTW Report - Benchmark'
        };

        // Map the global getReportData function to the local getReportData function.
        this.$rootScope.getReportData = () => {
            this.getReportData();
        };

        // Compact the selected assessment ID's down to their ID's only.
        this.assessments = this.$rootScope.$localStorage.reporting.assessments.map((assessment) => {
            return assessment.id;
        });
    }

    $postLink() {
        this.getReportData();
    }

    getReportData() {

        // if no assessments selected redirect and toast
        if(!this.$rootScope.$localStorage.reporting.assessments || this.$rootScope.$localStorage.reporting.assessments.length==0){
            this.ToastService.show('No Assessments Selected');
            //window.location.href = '#/reporting/assessments';
            return false;
        }

        // Map chart key names to text names.
        this.chartList = {
            // Not used - Just stored for future reference - We show this demographic as 2 percentages instead.
            benchmark: {
                title: 'Benchmark',
                type: 'scatter'
            },
        };

        angular.forEach(this.chartList, (chartData, chartKey) => {
            this.$scope[chartKey] = {
                chart: {
                    type: 'scatter',
                    zoomType: 'xy'
                },
                title: {
                    text: ''
                },
                xAxis: {
                    title: {
                        text: 'Alerts'
                    },
                },
                yAxis: {
                    title: {
                        text: 'Score'
                    }
                },
                plotOptions: {
                    scatter: {
                        marker: {
                            radius: 5,
                            states: {
                                hover: {
                                    enabled: false,
                                    lineWidth: 0,

                                }
                            }
                        },
                        states: {
                            hover: {
                                marker: {
                                    enabled: false
                                }
                            }
                        },
                    }
                },
                series: [{
                    name: 'Your assessments',
                    data: [],
                    color: '#aac52b',
                    lineWidth:0,
                    tooltip: {
                        headerFormat: '',
                        pointFormat: '{point.x} Alerts, {point.y:.0f}% Score'
                    },
                },{
                    name: 'Other assessments',
                    data: [],
                    color: '#40b5a5',
                    lineWidth:0,
                    tooltip: {
                        headerFormat: '',
                        pointFormat: '{point.x} Alerts, {point.y:.0f}% Score'
                    },
                }]
            };
        });

        // Fire off the report data request for benchmark data
        this.API.one('reporting/benchmark').customPOST({
            assessments: this.assessments,
            filters: this.$rootScope.filterForm
        })
            .then((response) => {
                if (!response.data) {
                    return false;
                }

                // Place all the actual values into the chart.
                this.$scope.benchmark.series[0].data = response.data.map((values) => {
                    console.log(values.account_name);
                    return {
                        category: values.account_name,
                        y: values.total_score, 
                        x: values.total_alerts
                    }
                });

            });

        // Fire off the report data request for benchmark data
        this.API.one('reporting/benchmark-all').customPOST({
            assessments: this.assessments,
            filters: this.$rootScope.filterForm
        })
            .then((response) => {
                if (!response.data) {
                    return false;
                }

                // Place all the actual values into the chart.
                this.$scope.benchmark.series[1].data = response.data.map((values) => {
                    return {
                        y: values.total_score, 
                        x: values.total_alerts
                    }
                });
                
            });
        
    }
}

export const ReportingBenchmarkComponent = {
    templateUrl: './views/app/components/reporting/benchmark/benchmark.component.html',
    controller: ReportingBenchmarkController,
    controllerAs: 'vm',
    bindings: {}
};
