class ReportingFilterController {
    constructor(API, $log, ToastService, $rootScope, $localStorage) {
        'ngInject';

        this.API = API;
        this.$log = $log;
        this.ToastService = ToastService;
        this.$rootScope = $rootScope;
        this.$localStorage = $localStorage;
    }

    $onInit() {
        // Create local lets for class elements so they can be accessed in the $rootScope function further down.
        let $rootScope = this.$rootScope;
        let $log = this.$log;

        // Global $rootScope function to turn off and on the reporting filters.
        this.$rootScope.toggleReportingFilters = function () {
            if ($rootScope.showReportingFilters == true) {
                $rootScope.showReportingFilters = false;
                $log.debug('Reporting filters turned off');
            }
            else {
                $rootScope.showReportingFilters = true;
                $log.debug('Reporting filters turned on');
            }
        };

        this.getAssessments();
    }

    $postLink() {
        this.getFilterOptions();
    }

    /**
     * Get filter options.
     */
    getFilterOptions() {
        this.API.one('reporting/filter-options').get().then((response) => {
            this.filterOptions = response.data;
        });
    }

    /**
     * Returns all the child assessments for this parent assessment.
     */
    getAssessments() {
        this.API.one('assessment').get()
            .then((response) => {
                this.assessments = response.data;
                this.removeAssessmentsForDisconnectedAccounts(response.data);
                this.$log.debug(response.data);
            });
    }

    clearFilters() {
        this.$rootScope.filterForm = null;
    }

    /**
     * Removes any assessments that have been applied for filtering but
     * the user no longer has access to.
     *
     * @param connectedAssessments
     */
    removeAssessmentsForDisconnectedAccounts(connectedAssessments) {
        let newAssessmentsList = [];

        for (var selectedAssessment of this.$localStorage.reporting.assessments) {
            for (var connectedAssessment of connectedAssessments) {
                if (selectedAssessment.id == connectedAssessment.id) {
                    newAssessmentsList.push(connectedAssessment);
                    break;
                }
            }
        }

        this.$localStorage.reporting.assessments = newAssessmentsList;
    }
}

export const ReportingFilterComponent = {
    templateUrl: './views/app/components/reporting/filter/filter.component.html',
    controller: ReportingFilterController,
    controllerAs: 'vm',
    bindings: {}
};
