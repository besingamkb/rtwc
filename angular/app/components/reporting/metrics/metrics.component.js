class ReportingMetricsController {
    constructor(API, $log, ToastService, $rootScope) {
        'ngInject';

        this.API = API;
        this.$log = $log;
        this.ToastService = ToastService;
        this.$rootScope = $rootScope;
    }

    $onInit() {
        this.$rootScope.hideSidebar = false;
        this.$rootScope.hideViewLoadingCover = true;
        this.$rootScope.sidebar = 'reporting';

        this.$rootScope.page = {
            key: 'reporting-metrics',
            name: 'Reporting - Metrics'
        };

        // Map the global getReportData function to the local getReportData function.
        this.$rootScope.getReportData = () => {
            this.getReportData();
        };

        // Compact the selected assessment ID's down to their ID's only.
        this.assessments = this.$rootScope.$localStorage.reporting.assessments.map(function (assessment) {
            return assessment.id;
        });
    }

    $postLink() {
        this.getReportData();
    }

    getReportData() {

        // if no assessments selected redirect and toast
        if(!this.$rootScope.$localStorage.reporting.assessments || this.$rootScope.$localStorage.reporting.assessments.length==0){
            this.ToastService.show('No Assessments Selected');
            //window.location.href = '#/reporting/assessments';
            return false;
        }

        // Fire off the report data request for scores.
        this.API.one('reporting/metrics').customPOST({
            assessments: this.assessments,
            filters: this.$rootScope.filterForm
        })
            .then((response) => {
                this.metrics = response.data;

                for (let metric of Object.keys(this.metrics)) {
                    this.metrics[metric][0].show_description = false;
                }
            }, (response) => {
                this.ToastService.show(response.data.errors[0]);
            });
    }
}

export const ReportingMetricsComponent = {
    templateUrl: './views/app/components/reporting/metrics/metrics.component.html',
    controller: ReportingMetricsController,
    controllerAs: 'vm',
    bindings: {}
};
