class ReportingComparativeController {
    constructor(API, $log, ToastService, $rootScope, $scope) {
        'ngInject';

        this.API = API;
        this.$log = $log;
        this.ToastService = ToastService;
        this.$rootScope = $rootScope;
        this.$scope = $scope;
    }

    $onInit() {
        this.$rootScope.hideSidebar = false;
        this.$rootScope.hideViewLoadingCover = true;
        this.$rootScope.sidebar = 'reporting';

        this.$rootScope.page = {
            key: 'reporting-comparative',
            name: 'RTW Report - Comparative'
        };

        // Map the global getReportData function to the local getReportData function.
        this.$rootScope.getReportData = () => {
            this.getReportData();
        };

        // Compact the selected assessment ID's down to their ID's only.
        this.assessments = this.$rootScope.$localStorage.reporting.assessments.map(function (assessment) {
            return assessment.id;
        });
    }

    $postLink() {
        this.getReportData();
    }

    /**
     * Get report data from the API.
     */
    getReportData() {

        // if no assessments selected redirect and toast
        if(!this.$rootScope.$localStorage.reporting.assessments || this.$rootScope.$localStorage.reporting.assessments.length==0){
            this.ToastService.show('No Assessments Selected');
            //window.location.href = '#/reporting/assessments';
            return false;
        }
        
        // Fire off the report data request.
        this.API.one('reporting/categories-comparatives').customPOST({
            assessments: this.assessments,
            filters: this.$rootScope.filterForm
        })
            .then((response) => {
                if (!response.data) {
                    return false;
                }

                this.buildCharts(response.data);
            }, (response) => {
                this.showCharts = false;
                this.ToastService.show(response.data.errors[0]);
            });

        // Fire off the report data request.
        this.API.one('reporting/metrics-comparatives').customPOST({
            assessments: this.assessments,
            filters: this.$rootScope.filterForm
        })
            .then((response) => {
                if (!response.data) {
                    return false;
                }

                this.buildCharts(response.data);
            });
    }

    /**
     * Initialize and format the charts with the provided API data.
     *
     * @param data
     */
    buildCharts(data) {
        // Map chart key names to text names.
        this.chartList = {
            overallscore: {
                title: 'Overall Score',
                type: 'column',
                yAxisLabel: 'Score',
                tooltipLabel: '%'
            },
            communication: {
                title: 'Communication & Trust',
                type: 'column',
                yAxisLabel: 'Score',
                tooltipLabel: '%'
            },
            culture: {
                title: 'Culture & Cohesion',
                type: 'column',
                yAxisLabel: 'Score',
                tooltipLabel: '%'
            },
            discrimination: {
                title: 'Discrimination',
                type: 'column',
                yAxisLabel: 'Score',
                tooltipLabel: '%'
            },
            engagement: {
                title: 'Employee Engagement',
                type: 'column',
                yAxisLabel: 'Score',
                tooltipLabel: '%'
            },
            freedom: {
                title: 'Freedom of Association',
                type: 'column',
                yAxisLabel: 'Score',
                tooltipLabel: '%'
            },
            freelychosen: {
                title: 'Freely Chosen',
                type: 'column',
                yAxisLabel: 'Score',
                tooltipLabel: '%'
            },
            hours: {
                title: 'Working Hours',
                type: 'column',
                yAxisLabel: 'Score',
                tooltipLabel: '%'
            },
            safety: {
                title: 'Health & Safety Management',
                type: 'column',
                yAxisLabel: 'Score',
                tooltipLabel: '%'
            },
            wages: {
                title: 'Living Wages',
                type: 'column',
                yAxisLabel: 'Score',
                tooltipLabel: '%'
            },
            operational: {
                title: 'Operational',
                type: 'column',
                yAxisLabel: 'Score',
                tooltipLabel: '%'
            },
            people: {
                title: 'People',
                type: 'column',
                yAxisLabel: 'Score',
                tooltipLabel: '%'
            },
            average_unauthorised_absence_days: {
                title: 'People',
                type: 'column',
                yAxisLabel: 'Days',
                tooltipLabel: ' Days'
            },
            cost_of_disciplinary_activity: {
                title: 'People',
                type: 'column',
                yAxisLabel: 'Pounds',
                tooltipLabel: ''
            },
            cost_of_grievances: {
                title: 'People',
                type: 'column',
                yAxisLabel: 'Pounds',
                tooltipLabel: ''
            },
            cost_of_labour_provider_per_worker: {
                title: 'People',
                type: 'column',
                yAxisLabel: 'Pounds',
                tooltipLabel: ''
            },
            cost_of_mediation: {
                title: 'People',
                type: 'column',
                yAxisLabel: 'Pounds',
                tooltipLabel: ''
            },
            cost_of_recruitment: {
                title: 'People',
                type: 'column',
                yAxisLabel: 'Pounds',
                tooltipLabel: ''
            },
            cost_of_training_per_agency_worker: {
                title: 'People',
                type: 'column',
                yAxisLabel: 'Pounds',
                tooltipLabel: ''
            },
            cost_of_training_per_employee: {
                title: 'People',
                type: 'column',
                yAxisLabel: 'Pounds',
                tooltipLabel: ''
            },
            cost_of_unauthorised_absence: {
                title: 'People',
                type: 'column',
                yAxisLabel: 'Pounds',
                tooltipLabel: ''
            },
            cost_of_unauthorised_absence_per_employee: {
                title: 'People',
                type: 'column',
                yAxisLabel: 'Pounds',
                tooltipLabel: ''
            },
            turnover_rate: {
                title: 'People',
                type: 'column',
                yAxisLabel: 'Percent',
                tooltipLabel: '%'
            }
        };

        // Setup the initial chart data.
        angular.forEach(data[Object.keys(data)[0]], (chartData, chartKey) => {
            this.$scope[chartKey] = {
                options: {
                    credits: {
                        enabled: false
                    },
                    exporting: {
                        enabled: false
                    },
                    xAxis: {
                        categories: [],
                    },
                    yAxis: {
                        min: 0,
                        gridLineWidth: 1,
                        title: {
                            text: this.chartList[chartKey].yAxisLabel
                        },
                        labels: {
                            enabled: true
                        },
                    },
                    chart: {
                        height: 280,
                        type: this.chartList[chartKey].type
                    },
                    legend: {
                        align: 'center',
                        verticalAlign: 'bottom'
                    },
                    plotOptions: {
                        column: {
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: false,
                            tooltip: {
                                pointFormat: '<b>{point.y:.2f}' + this.chartList[chartKey].tooltipLabel + '</b>'
                            },
                        },
                        series: {
                            pointWidth: 10
                        }
                    },
                },
                title: this.chartList[chartKey].title,
                series: [{
                    data: [],
                    colors: [
                        '#aac52b', '#357da9', '#ab73c0', '#40b5a5', '#ea6555'
                    ],
                    colorByPoint: true,
                }]
            };
        });

        // Add data to the charts.
        angular.forEach(data, (assessments) => {
            angular.forEach(assessments, (chartData, chartKey) => {
                if (this.$scope[chartKey] == 'undefined') {
                    return false;
                }

                // Place all the actual values into the chart.
                this.$scope[chartKey].series[0].name = chartData.map((values) => values.name);
                this.$scope[chartKey].series[0].data.push(chartData.map((values) => values.total));

                this.$scope[chartKey].options.xAxis.categories.push(chartData.map((values) => values.account_name));
            });
        });
    }
}

export const ReportingComparativeComponent = {
    templateUrl: './views/app/components/reporting/comparative/comparative.component.html',
    controller: ReportingComparativeController,
    controllerAs: 'vm',
    bindings: {}
};
