class ReportingDemographicsController {
    constructor(API, $log, ToastService, $rootScope, $scope) {
        'ngInject';

        this.API = API;
        this.$log = $log;
        this.ToastService = ToastService;
        this.$rootScope = $rootScope;
        this.$scope = $scope;
    }

    $onInit() {
        this.$rootScope.hideSidebar = false;
        this.$rootScope.hideViewLoadingCover = true;
        this.$rootScope.sidebar = 'reporting';
        this.showCharts = true;

        this.$rootScope.page = {
            key: 'reporting-demographics',
            name: 'RTW Report – Demographics'
        };

        // Map the global getReportData function to the local getReportData function.
        this.$rootScope.getReportData = () => {
            this.getReportData();
        };

        // Compact the selected assessment ID's down to their ID's only.
        this.assessments = this.$rootScope.$localStorage.reporting.assessments.map(function (assessment) {
            return assessment.id;
        });
    }

    $postLink() {
        this.getReportData();
    }

    /**
     * Get report data from the API.
     */
    getReportData() {

        // if no assessments selected redirect and toast
        if(!this.$rootScope.$localStorage.reporting.assessments || this.$rootScope.$localStorage.reporting.assessments.length==0){
            this.ToastService.show('No Assessments Selected');
            //window.location.href = '#/reporting/assessments';
            return false;
        }

        // Fire off the report data request.
        this.API.one('reporting/demographics').customPOST({
            assessments: this.assessments,
            filters: this.$rootScope.filterForm
        })
            .then((response) => {
                if (!response.data) {
                    this.show = false;
                    return false;
                }

                this.showCharts = true;

                this.buildCharts(response.data);
                
            }, (response) => {
                this.showCharts = false;
                this.ToastService.show(response.data.errors[0]);
            });
    }

    /**
     * Initialize and format the charts with the provided API data.
     *
     * @param data
     */
    buildCharts(data) {
        // Map chart key names to text names.
        this.chartList = {
            // Not used - Just stored for future reference - We show this demographic as 2 percentages instead.
            gender: {
                title: 'Gender',
                type: ''
            },
            age: {
                title: 'Age',
                type: 'bar'
            },
            employsyou: {
                title: 'Employment Type',
                type: 'pie'
            },
            level: {
                title: 'Job Position',
                type: 'pie'
            },
            role: {
                title: 'Job Role',
                type: 'column'
            },
            shift: {
                title: 'Shift Type',
                type: 'column'
            },
            howlong: {
                title: 'Job Tenure',
                type: 'bar'
            },
            // Not used, just stored in case we need it again in future - Removing this will break Highcharts.
            ethnicity: {
                title: 'Ethnicity',
                type: 'column'
            },
            ethnicity1: {
                title: 'Ethnicity',
                type: 'bar'
            },
            ethnicity2: {
                title: 'Ethnicity',
                type: 'bar'
            }
        };

        // Set percentages for genders as they are not real charts.
        this.chartList.gender = this.formatGenderData(data.gender, this.chartList.gender);

        // Split ethnicity into 2 separate graphs.
        // We do this by first making copies of the ethnicity data set so we don't affect the original.
        let ethnicityCopy = data.ethnicity.slice(0),
            ethnicityCopy2 = data.ethnicity.slice(0);

        // To get the first graph we splice the ethnicity data set in half and store this as ethnicity1.
        data.ethnicity1 = ethnicityCopy.splice(0,
            Math.ceil(data.ethnicity.length / 2));

        // To get he second graph we splice the ethnicity data set in half but take the 2nd half and store this as ethnicity2
        data.ethnicity2 = ethnicityCopy2.splice(Math.ceil(data.ethnicity.length / 2),
            data.ethnicity.length - data.ethnicity1.length);

        // With the above we round up to the nearest whole value using Math.ceil(). This ensures that
        // we always get the bigger half on the 1st graph and the smaller half on the 2nd graph.

        // Setup the initial chart data.
        angular.forEach(data, (chartData, chartKey) => {
            this.$scope[chartKey] = {
                options: {
                    credits: {
                        enabled: false
                    },
                    exporting: {
                        enabled: false
                    },
                    yAxis: {
                        min: 0,
                        gridLineWidth: 1,
                        title: {
                            text: 'Workers',
                            align: 'high'
                        },
                        labels: {
                            enabled: true
                        },
                    },
                    chart: {
                        height: 280,
                        type: this.chartList[chartKey].type
                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: false,
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: true,
                            tooltip: {
                                pointFormat: '<b>{point.y} workers</b>'
                            },
                        },
                        bar: {
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: false,
                            tooltip: {
                                pointFormat: '<b>{point.y} workers</b>'
                            },
                        },
                        column: {
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: false,
                            tooltip: {
                                pointFormat: '<b>{point.y} workers</b>'
                            },
                        },
                        series: {
                            pointWidth: 10
                        }
                    },
                },
                title: this.chartList[chartKey].title,
                series: [{
                    name: this.chartList[chartKey].title,
                    data: [],
                    colors: [
                        '#aac52b', '#357da9', '#ab73c0', '#40b5a5', '#ea6555'
                    ],
                    colorByPoint: true,
                }]
            };
        });

        // Add data to the charts.
        angular.forEach(data, (chartData, chartKey) => {
            let thisChart = this.$scope[chartKey];

            if (thisChart == 'undefined') {
                return false;
            }

            // Add the labels to the chart.
            thisChart.xAxis = {
                categories: chartData.map(function (values) {
                    return values.text;
                })
            };

            // Place all the actual values into the chart.
            thisChart.series[0].data = chartData.map(function (values) {
                return {
                    name: values.text,
                    y: values.count
                };
            });
        });
    }

    /**
     * Format the gender data and return the new format.
     *
     * @param genderData
     * @param chartGenderData
     * @returns {*}
     */
    formatGenderData(genderData, chartGenderData) {
        // Duplicate the original data for gender and gender chart data.
        let genderDataCopy = genderData.slice(0);
        let chartGenderDataCopy = JSON.parse(JSON.stringify(chartGenderData)); // Copying objects is weird...

        // If only 1 gender is selected then make the genderTotal the total of that gender, otherwise
        // we can add the counts together to get both genders totals.
        let genderTotal = 0;

        if (genderDataCopy.length > 1) {
            genderTotal = chartGenderDataCopy.total = genderDataCopy.reduce((a, b) => a.count + b.count);
        }
        else {
            genderTotal = chartGenderDataCopy.total = genderDataCopy[0].count;
        }

        let genderMaleTotal = chartGenderDataCopy.male_total = genderDataCopy.filter(el => el.text == 'Male');
        let genderFemaleTotal = chartGenderDataCopy.female_total = genderDataCopy.filter(el => el.text == 'Female');

        // If the reporting filters for Male or Female are turned off then provide 0 for that gender.
        genderMaleTotal = (genderMaleTotal.length ? parseInt(genderMaleTotal[0].count) : 0);
        genderFemaleTotal = (genderFemaleTotal.length ? parseInt(genderFemaleTotal[0].count) : 0);

        // Only work out percentages if the gender totals are not 0.
        chartGenderDataCopy.male_percentage = (genderMaleTotal != 0 ?
            Math.round(genderMaleTotal / genderTotal * 100) : 0);
        chartGenderDataCopy.female_percentage = (genderFemaleTotal != 0 ?
            Math.round(genderFemaleTotal / genderTotal * 100) : 0);

        this.$log.debug(chartGenderDataCopy);

        return chartGenderDataCopy;
    }
}

export const ReportingDemographicsComponent = {
    templateUrl: './views/app/components/reporting/demographics/demographics.component.html',
    controller: ReportingDemographicsController,
    controllerAs: 'vm',
    bindings: {}
};
