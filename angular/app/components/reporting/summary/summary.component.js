class ReportingSummaryController {
    constructor(API, $log, ToastService, $rootScope, $scope) {
        'ngInject';

        this.API = API;
        this.$log = $log;
        this.ToastService = ToastService;
        this.$rootScope = $rootScope;
        this.$scope = $scope;
    }

    $onInit() {
        this.$rootScope.hideSidebar = false;
        this.$rootScope.hideViewLoadingCover = true;
        this.$rootScope.sidebar = 'reporting';
        this.showCharts = true;

        this.$rootScope.page = {
            key: 'reporting-summary',
            name: 'RTW Report - Summary'
        };

        // Map the global getReportData function to the local getReportData function.
        this.$rootScope.getReportData = () => {
            this.getReportData();
        };

        // Compact the selected assessment ID's down to their ID's only.
        this.assessments = this.$rootScope.$localStorage.reporting.assessments.map((assessment) => {
            return assessment.id;
        });
    }

    $postLink() {
        this.getReportData();
    }

    getReportData() {

        // if no assessments selected redirect and toast
        if(!this.$rootScope.$localStorage.reporting.assessments || this.$rootScope.$localStorage.reporting.assessments.length==0){
            this.ToastService.show('No Assessments Selected');
            //window.location.href = '#/reporting/assessments';
            return false;
        }

        // Map chart key names to text names.
        this.chartList = {
            // Not used - Just stored for future reference - We show this demographic as 2 percentages instead.
            ethnicity: {
                title: 'Worker Ethnicities',
                type: 'pie'
            },
            categories: {
                title: 'Categories',
                type: 'bar'
            }
        };

        angular.forEach(this.chartList, (chartData, chartKey) => {
            this.$scope[chartKey] = {
                options: {
                    credits: {
                        enabled: false
                    },
                    exporting: {
                        enabled: false
                    },
                    yAxis: {
                        min: 0,
                        gridLineWidth: 1,
                        title: {
                            text: 'Workers',
                            align: 'high'
                        },
                        labels: {
                            enabled: true
                        },
                    },
                    chart: {
                        height: 280,
                        type: this.chartList[chartKey].type
                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: false,
                            dataLabels: {
                                enabled: false,
                            },
                            showInLegend: true,
                            tooltip: {
                                pointFormat: '<b>{point.y} workers</b>'
                            },
                        },
                        bar: {
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: false,
                            tooltip: {
                                pointFormat: '<b>{point.y} %</b>'
                            },
                        },
                        column: {
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: false
                        },
                        series: {
                            pointWidth: 10
                        }
                    },
                },
                title: this.chartList[chartKey].title,
                series: [{
                    name: this.chartList[chartKey].title,
                    data: [],
                    colors: [
                        '#aac52b', '#357da9', '#ab73c0', '#40b5a5', '#ea6555'
                    ],
                    colorByPoint: true,
                }]
            };
        });

        // Fire off the report data request for worker ethnicities.
        this.API.one('reporting/summary').customPOST({
            assessments: this.assessments,
            filters: this.$rootScope.filterForm
        })
            .then((response) => {
                if (!response.data) {
                    return false;
                }

                this.showCharts = true;

                // Add the labels to the chart.
                this.$scope.ethnicity.xAxis = {
                    categories: response.data.ethnicity.map((values) => {
                        return values.text;
                    })
                };

                // Place all the actual values into the chart.
                this.$scope.ethnicity.series[0].data = response.data.ethnicity.map((values) => {
                    return {
                        name: values.text,
                        y: values.count
                    };
                });
            }, (response) => {
                this.showCharts = false;
                this.ToastService.show(response.data.errors[0]);
            });

        // Fire off the report data request for scores.
        this.API.one('reporting/scores').customPOST({
            assessments: this.assessments,
            filters: this.$rootScope.filterForm
        })
            .then((response) => {
                if (!response.data) {
                    return false;
                }

                // Add the labels to the chart.
                this.scores = {
                    total: Math.round(response.data[0].total_score)
                };
            });

        // Fire off the report data request for scores.
        this.API.one('reporting/alerts').customPOST({
            assessments: this.assessments,
            filters: this.$rootScope.filterForm
        })
            .then((response) => {
                if (!response.data) {
                    return false;
                }

                // Add the labels to the chart.
                this.alerts = {
                    total: Math.round(response.data[0].total_alerts)
                };
            });

        // Fire off the report data request for categories.
        this.API.one('reporting/categories').customPOST({
            assessments: this.assessments,
            filters: this.$rootScope.filterForm
        })
            .then((response) => {
                if (!response.data) {
                    return false;
                }

                // Only get the lowest 3 categories.
                response.data = response.data.sort((a, b) => b.total - a.total);
                response.data = response.data.splice(response.data.length - 3, response.data.length);

                // Add the labels to the chart.
                this.$scope.categories.xAxis = {
                    categories: response.data.map((values) => {
                        return values.name;
                    })
                };

                // Place all the actual values into the chart.
                this.$scope.categories.series[0].data = response.data.map((values) => {
                    return {
                        name: values.name,
                        y: Math.round(values.total) // For some reason SUM() in SQL from API comes in as a string and not an integer.
                    };
                });
            });
    }
}

export const ReportingSummaryComponent = {
    templateUrl: './views/app/components/reporting/summary/summary.component.html',
    controller: ReportingSummaryController,
    controllerAs: 'vm',
    bindings: {}
};
