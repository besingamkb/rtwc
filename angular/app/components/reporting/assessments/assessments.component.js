class ReportingAssessmentsController {
    constructor(API, $log, ToastService, $rootScope, $localStorage, $window) {
        'ngInject';

        this.API = API;
        this.$log = $log;
        this.ToastService = ToastService;
        this.$rootScope = $rootScope;
        this.$localStorage = $localStorage;
        this.$window = $window;
    }

    $onInit() {
        this.$rootScope.hideSidebar = false;
        this.$rootScope.hideViewLoadingCover = true;
        this.$rootScope.sidebar = 'reporting';

        this.$rootScope.page = {
            key: 'reporting-assessments',
            name: 'RTW Report'
        };

        this.getAssessments();

        // If a reporting $localStorage does not exist then create them.
        if (!this.$localStorage.reporting) {
            this.$localStorage.reporting = {
                assessments: []
            };
        }
    }

    /**
     * Returns all the child assessments for this parent assessment.
     */
    getAssessments() {
        this.API.one('assessment/search/reporting').post()
            .then((response) => {
                this.assessments = response.data;
                this.$log.debug(response.data);
            });
    }

    /**
     * Attempts to download the generated PDF from the API by given params.
     * Todo: Needs to be changed because URI has a short maximum length.
     */
    downloadPDF() {
        this.selectedAssessments = this.$localStorage.reporting.assessments.map((assessment) => {
            return assessment.id;
        });

        this.$window.open('api/reporting/pdf?token=' + this.$localStorage.jwt + '&assessments=' +
            this.selectedAssessments + '&filters=' + this.$rootScope.filterForm);
    }

    /**
     * Adds a new assessment to the users reporting in $localStorage.
     *
     * @param assessment
     */
    toggleReportingAssessment(assessment) {

        let isSelected = this.reportingAssessmentIsSelected(assessment);

        if (isSelected == -1) {
            this.$localStorage.reporting.assessments.push(assessment);
            this.ToastService.show('Assessment ' + assessment.key + ' added to reporting');
        }

        if (isSelected > -1) {
            this.$localStorage.reporting.assessments.splice(isSelected, 1);
            this.ToastService.show('Assessment ' + assessment.key + ' removed from reporting');
        }
    }

    /**
     * Checks if the assessment is in the reporting assessments list.
     * If it is then return it's index placement in the array.
     *
     * @param assessment
     * @returns Number
     */
    reportingAssessmentIsSelected(assessment) {
        return this.$rootScope.findByProperty(this.$localStorage.reporting.assessments, 'id', assessment.id);
    }
}

export const ReportingAssessmentsComponent = {
    templateUrl: './views/app/components/reporting/assessments/assessments.component.html',
    controller: ReportingAssessmentsController,
    controllerAs: 'vm',
    bindings: {}
};
