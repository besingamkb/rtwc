class AdminQuestionsController {
    constructor(API, $log, ToastService, $rootScope, $mdDialog, DialogService, $document, $scope) {
        'ngInject';

        this.API = API;
        this.$log = $log;
        this.ToastService = ToastService;
        this.$rootScope = $rootScope;
        this.$mdDialog = $mdDialog;
        this.DialogService = DialogService;
        this.$document = $document;
        this.$scope = $scope;
    }

    $onInit() {
        this.$rootScope.hideSidebar = false;
        this.$rootScope.hideViewLoadingCover = true;
        this.$rootScope.sidebar = 'admin';

        this.$rootScope.page = {
            key: 'admin-questions',
            name: 'Admin - Questions'
        };

        this.getQuestions();

        // Load data for drop-downs.
        this.getQuestionSets();
        this.getQuestionCategories();

        // Setup dialogs.
        this.questionDialog = {
            type: null,
            data: {},
            create: () => {
                this.questionDialog.type = 'create';
                this.questionDialog.data = {};
                this.questionDialog.open();
            },
            edit: (question) => {
                this.questionDialog.type = 'edit';
                this.questionDialog.data = angular.copy(question);
                this.questionDialog.open();
            },
            store: (question) => {
                // Store logic goes here.
                this.$rootScope.hideSiteLoadingCover = false;

                this.API.one('admin/question').customPOST(question).then(() => {
                    this.getQuestions();
                    this.questionDialog.close();
                }, () => {
                    this.$rootScope.hideSiteLoadingCover = true;
                });
            },
            update: (question) => {
                // Update logic goes here.
                this.$rootScope.hideSiteLoadingCover = false;

                this.API.one('admin/question/' + question.id).customPUT(question).then(() => {
                    this.getQuestions();
                    this.questionDialog.close();
                }, () => {
                    this.$rootScope.hideSiteLoadingCover = true;
                });
            },
            delete: (question) => {
                // Delete logic goes here.
                let dialog = this.$mdDialog.confirm()
                    .title('Delete Question')
                    .textContent('Are you sure you want to delete this question?')
                    .ok('Delete')
                    .cancel('Cancel');

                this.$mdDialog.show(dialog).then(() => {
                    this.$rootScope.hideSiteLoadingCover = false;

                    // Attempt to delete the resource.
                    this.API.one('admin/question').customDELETE(question.id).then(() => {
                        this.getQuestions();
                    }, () => {
                        this.$rootScope.hideSiteLoadingCover = true;
                    });
                });
            },
            open: () => {
                this.$mdDialog.show({
                    scope: this.$scope,
                    preserveScope: true,
                    parent: angular.element(this.$document.body),
                    clickOutsideToClose: true,
                    templateUrl: './views/app/components/admin/questions/dialogs/question.dialog.html'
                });
            },
            close: () => {
                this.$mdDialog.hide();
            }
        };
    }

    /**
     * Gets all questions.
     */
    getQuestions() {
        // Add pre-create code here.
        // eg. Validation, pre-processing, etc.
        this.$rootScope.hideSiteLoadingCover = false;

        // Attempt to get the resource.
        this.API.one('admin/question').get().then((response) => {
            this.questions = response.data;
            this.$rootScope.hideSiteLoadingCover = true;
        }, () => {
            this.$rootScope.hideSiteLoadingCover = true;
        });
    }

    /**
     * Gets all question sets.
     */
    getQuestionSets() {
        // Add pre-create code here.
        // eg. Validation, pre-processing, etc.
        this.$rootScope.hideSiteLoadingCover = false;

        // Attempt to get the resource.
        this.API.one('admin/question-set').get().then((response) => {
            this.questionSets = response.data;
            this.$rootScope.hideSiteLoadingCover = true;
        }, () => {
            this.$rootScope.hideSiteLoadingCover = true;
        });
    }

    /**
     * Gets all question categories.
     */
    getQuestionCategories() {
        // Add pre-create code here.
        // eg. Validation, pre-processing, etc.
        this.$rootScope.hideSiteLoadingCover = false;

        // Attempt to get the resource.
        this.API.one('admin/question-category').get().then((response) => {
            this.questionCategories = response.data;
            this.$rootScope.hideSiteLoadingCover = true;
        }, () => {
            this.$rootScope.hideSiteLoadingCover = true;
        });
    }
}

export const AdminQuestionsComponent = {
    templateUrl: './views/app/components/admin/questions/questions.component.html',
    controller: AdminQuestionsController,
    controllerAs: 'vm',
    bindings: {}
};
