class AdminQuestionSetsController {
    constructor(API, $log, ToastService, $rootScope, $mdDialog, DialogService, $document, $scope) {
        'ngInject';

        this.API = API;
        this.$log = $log;
        this.ToastService = ToastService;
        this.$rootScope = $rootScope;
        this.$mdDialog = $mdDialog;
        this.DialogService = DialogService;
        this.$document = $document;
        this.$scope = $scope;
    }

    $onInit() {
        this.$rootScope.hideSidebar = false;
        this.$rootScope.hideViewLoadingCover = true;
        this.$rootScope.sidebar = 'admin';

        this.$rootScope.page = {
            key: 'admin-question-sets',
            name: 'Admin - Question Sets'
        };

        this.getQuestionSets();

        // Setup dialogs.
        this.questionSetDialog = {
            type: null,
            data: {},
            create: () => {
                this.questionSetDialog.type = 'create';
                this.questionSetDialog.data = {};
                this.questionSetDialog.open();
            },
            edit: (questionSet) => {
                this.questionSetDialog.type = 'edit';
                this.questionSetDialog.data = angular.copy(questionSet);
                this.questionSetDialog.open();
            },
            store: (questionSet) => {
                // Store logic goes here.
                this.$rootScope.hideSiteLoadingCover = false;

                this.API.one('admin/question-set').customPOST(questionSet).then(() => {
                    this.getQuestionSets();
                    this.questionSetDialog.close();
                }, () => {
                    this.$rootScope.hideSiteLoadingCover = true;
                });
            },
            update: (questionSet) => {
                // Update logic goes here.
                this.$rootScope.hideSiteLoadingCover = false;

                this.API.one('admin/question-set/' + questionSet.id).customPUT(questionSet).then(() => {
                    this.getQuestionSets();
                    this.questionSetDialog.close();
                }, () => {
                    this.$rootScope.hideSiteLoadingCover = true;
                });
            },
            delete: (questionSet) => {
                // Delete logic goes here.
                let dialog = this.$mdDialog.confirm()
                    .title('Delete Question Set')
                    .textContent('Are you sure you want to delete this question set?')
                    .ok('Delete')
                    .cancel('Cancel');

                this.$mdDialog.show(dialog).then(() => {
                    this.$rootScope.hideSiteLoadingCover = false;

                    // Attempt to delete the resource.
                    this.API.one('admin/question-set').customDELETE(questionSet.id).then(() => {
                        this.getQuestionSets();
                    }, () => {
                        this.$rootScope.hideSiteLoadingCover = true;
                    });
                });
            },
            open: () => {
                this.$mdDialog.show({
                    scope: this.$scope,
                    preserveScope: true,
                    parent: angular.element(this.$document.body),
                    clickOutsideToClose: true,
                    templateUrl: './views/app/components/admin/question-sets/dialogs/question-set.dialog.html'
                });
            },
            close: () => {
                this.$mdDialog.hide();
            }
        };
    }

    /**
     * Gets all question sets.
     */
    getQuestionSets() {
        // Add pre-create code here.
        // eg. Validation, pre-processing, etc.
        this.$rootScope.hideSiteLoadingCover = false;

        // Attempt to get the resource.
        this.API.one('admin/question-set').get().then((response) => {
            this.questionSets = response.data;
            this.$rootScope.hideSiteLoadingCover = true;
        }, () => {
            this.$rootScope.hideSiteLoadingCover = true;
        });
    }
}

export const AdminQuestionSetsComponent = {
    templateUrl: './views/app/components/admin/question-sets/question-sets.component.html',
    controller: AdminQuestionSetsController,
    controllerAs: 'vm',
    bindings: {}
};
