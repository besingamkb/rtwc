class AdminQuestionCategoriesController {
    constructor(API, $log, ToastService, $rootScope, $mdDialog, DialogService, $document, $scope) {
        'ngInject';

        this.API = API;
        this.$log = $log;
        this.ToastService = ToastService;
        this.$rootScope = $rootScope;
        this.$mdDialog = $mdDialog;
        this.DialogService = DialogService;
        this.$document = $document;
        this.$scope = $scope;
    }

    $onInit() {
        this.$rootScope.hideSidebar = false;
        this.$rootScope.hideViewLoadingCover = true;
        this.$rootScope.sidebar = 'admin';

        this.$rootScope.page = {
            key: 'admin-question-categories',
            name: 'Admin - Question Categories'
        };

        this.getQuestionCategories();

        // Setup dialogs.
        this.questionCategoryDialog = {
            type: null,
            data: {},
            create: () => {
                this.questionCategoryDialog.type = 'create';
                this.questionCategoryDialog.data = {};
                this.questionCategoryDialog.open();
            },
            edit: (questionCategory) => {
                this.questionCategoryDialog.type = 'edit';
                this.questionCategoryDialog.data = angular.copy(questionCategory);
                this.questionCategoryDialog.open();
            },
            store: (questionCategory) => {
                // Store logic goes here.
                this.$rootScope.hideSiteLoadingCover = false;

                this.API.one('admin/question-category').customPOST(questionCategory).then(() => {
                    this.getQuestionCategories();
                    this.questionCategoryDialog.close();
                }, () => {
                    this.$rootScope.hideSiteLoadingCover = true;
                });
            },
            update: (questionCategory) => {
                // Update logic goes here.
                this.$rootScope.hideSiteLoadingCover = false;

                this.API.one('admin/question-category/' + questionCategory.id).customPUT(questionCategory).then(() => {
                    this.getQuestionCategories();
                    this.questionCategoryDialog.close();
                }, () => {
                    this.$rootScope.hideSiteLoadingCover = true;
                });
            },
            delete: (questionCategory) => {
                // Delete logic goes here.
                let dialog = this.$mdDialog.confirm()
                    .title('Delete Question Category')
                    .textContent('Are you sure you want to delete this question category?')
                    .ok('Delete')
                    .cancel('Cancel');

                this.$mdDialog.show(dialog).then(() => {
                    this.$rootScope.hideSiteLoadingCover = false;

                    // Attempt to delete the resource.
                    this.API.one('admin/question-category').customDELETE(questionCategory.id).then(() => {
                        this.getQuestionCategories();
                    }, () => {
                        this.$rootScope.hideSiteLoadingCover = true;
                    });
                });
            },
            open: () => {
                this.$mdDialog.show({
                    scope: this.$scope,
                    preserveScope: true,
                    parent: angular.element(this.$document.body),
                    clickOutsideToClose: true,
                    templateUrl: './views/app/components/admin/question-categories/dialogs/question-category.dialog.html'
                });
            },
            close: () => {
                this.$mdDialog.hide();
            }
        };
    }

    /**
     * Gets all question categories.
     */
    getQuestionCategories() {
        // Add pre-create code here.
        // eg. Validation, pre-processing, etc.
        this.$rootScope.hideSiteLoadingCover = false;

        // Attempt to get the resource.
        this.API.one('admin/question-category').get().then((response) => {
            this.questionCategories = response.data;
            this.$rootScope.hideSiteLoadingCover = true;
        }, () => {
            this.$rootScope.hideSiteLoadingCover = true;
        });
    }
}

export const AdminQuestionCategoriesComponent = {
    templateUrl: './views/app/components/admin/question-categories/question-categories.component.html',
    controller: AdminQuestionCategoriesController,
    controllerAs: 'vm',
    bindings: {}
};
