class AdminAssessmentsController {
    constructor(API, $log, ToastService, $rootScope, $mdDialog, DialogService, $document, $scope) {
        'ngInject';

        this.API = API;
        this.$log = $log;
        this.ToastService = ToastService;
        this.$rootScope = $rootScope;
        this.$mdDialog = $mdDialog;
        this.DialogService = DialogService;
        this.$document = $document;
        this.$scope = $scope;
    }

    $onInit() {
        this.$rootScope.hideSidebar = false;
        this.$rootScope.hideViewLoadingCover = true;
        this.$rootScope.sidebar = 'admin';

        this.$rootScope.page = {
            key: 'admin-assessments',
            name: 'Admin - Assessments'
        };

        this.getAssessments();

        // Load data for drop-downs.
        this.getAccounts();
        this.getQuestionSets();

        // Setup dialogs.
        this.assessmentDialog = {
            type: null,
            data: {},
            create: () => {
                this.assessmentDialog.type = 'create';
                this.assessmentDialog.data = {};
                this.assessmentDialog.open();
            },
            edit: (assessment) => {
                this.assessmentDialog.type = 'edit';
                this.assessmentDialog.data = angular.copy(assessment);
                this.assessmentDialog.open();
            },
            store: (assessment) => {
                // Store logic goes here.
                this.$rootScope.hideSiteLoadingCover = false;

                this.API.one('admin/assessment').customPOST(assessment).then(() => {
                    this.getAssessments();
                    this.assessmentDialog.close();
                }, () => {
                    this.$rootScope.hideSiteLoadingCover = true;
                });
            },
            update: (assessment) => {
                // Update logic goes here.
                this.$rootScope.hideSiteLoadingCover = false;

                this.API.one('admin/assessment/' + assessment.id).customPUT(assessment).then(() => {
                    this.getAssessments();
                    this.assessmentDialog.close();
                }, () => {
                    this.$rootScope.hideSiteLoadingCover = true;
                });
            },
            delete: (assessment) => {
                // Delete logic goes here.
                let dialog = this.$mdDialog.confirm()
                    .title('Delete Assessment')
                    .textContent('Are you sure you want to delete this assessment?')
                    .ok('Delete')
                    .cancel('Cancel');

                this.$mdDialog.show(dialog).then(() => {
                    this.$rootScope.hideSiteLoadingCover = false;

                    // Attempt to delete the resource.
                    this.API.one('admin/assessment').customDELETE(assessment.id).then(() => {
                        this.getAssessments();
                    }, () => {
                        this.$rootScope.hideSiteLoadingCover = true;
                    });
                });
            },
            open: () => {
                this.$mdDialog.show({
                    scope: this.$scope,
                    preserveScope: true,
                    parent: angular.element(this.$document.body),
                    clickOutsideToClose: true,
                    templateUrl: './views/app/components/admin/assessments/dialogs/assessment.dialog.html'
                });
            },
            close: () => {
                this.$mdDialog.hide();
            }
        };
    }

    /**
     * Gets all assessments.
     */
    getAssessments() {
        // Add pre-create code here.
        // eg. Validation, pre-processing, etc.
        this.$rootScope.hideSiteLoadingCover = false;

        // Attempt to get the resource.
        this.API.one('admin/assessment').get().then((response) => {
            this.assessments = response.data;
            this.$rootScope.hideSiteLoadingCover = true;
        }, () => {
            this.$rootScope.hideSiteLoadingCover = true;
        });
    }

    /**
     * Gets all accounts.
     */
    getAccounts() {
        // Add pre-create code here.
        // eg. Validation, pre-processing, etc.
        this.$rootScope.hideSiteLoadingCover = false;

        // Attempt to get the resource.
        this.API.one('admin/account').get().then((response) => {
            this.accounts = response.data;
            this.$rootScope.hideSiteLoadingCover = true;
        }, () => {
            this.$rootScope.hideSiteLoadingCover = true;
        });
    }

    /**
     * Gets all question sets.
     */
    getQuestionSets() {
        // Add pre-create code here.
        // eg. Validation, pre-processing, etc.
        this.$rootScope.hideSiteLoadingCover = false;

        // Attempt to get the resource.
        this.API.one('admin/question-set').get().then((response) => {
            this.questionSets = response.data;
            this.$rootScope.hideSiteLoadingCover = true;
        }, () => {
            this.$rootScope.hideSiteLoadingCover = true;
        });
    }
}

export const AdminAssessmentsComponent = {
    templateUrl: './views/app/components/admin/assessments/assessments.component.html',
    controller: AdminAssessmentsController,
    controllerAs: 'vm',
    bindings: {}
};
