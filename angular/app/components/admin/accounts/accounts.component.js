class AdminAccountsController {
    constructor(API, $log, ToastService, $rootScope, $mdDialog, DialogService, $document, $scope) {
        'ngInject';

        this.API = API;
        this.$log = $log;
        this.ToastService = ToastService;
        this.$rootScope = $rootScope;
        this.$mdDialog = $mdDialog;
        this.DialogService = DialogService;
        this.$document = $document;
        this.$scope = $scope;
    }

    $onInit() {
        this.$rootScope.hideSidebar = false;
        this.$rootScope.hideViewLoadingCover = true;
        this.$rootScope.sidebar = 'admin';

        this.$rootScope.page = {
            key: 'admin-accounts',
            name: 'Admin - Accounts'
        };

        this.getAccounts();

        // Setup dialogs.
        this.accountDialog = {
            type: null,
            data: {},
            create: () => {
                this.accountDialog.type = 'create';
                this.accountDialog.data = {};
                this.accountDialog.open();
            },
            edit: (account) => {
                this.accountDialog.type = 'edit';
                this.accountDialog.data = angular.copy(account);
                this.accountDialog.open();
            },
            store: (account) => {
                // Store logic goes here.
                this.$rootScope.hideSiteLoadingCover = false;

                this.API.one('admin/account').customPOST(account).then(() => {
                    this.getAccounts();
                    this.accountDialog.close();
                }, () => {
                    this.$rootScope.hideSiteLoadingCover = true;
                });
            },
            update: (account) => {
                // Update logic goes here.
                this.$rootScope.hideSiteLoadingCover = false;

                this.API.one('admin/account/' + account.id).customPUT(account).then(() => {
                    this.getAccounts();
                    this.accountDialog.close();
                }, () => {
                    this.$rootScope.hideSiteLoadingCover = true;
                });
            },
            delete: (account) => {
                // Delete logic goes here.
                let dialog = this.$mdDialog.confirm()
                    .title('Delete Account')
                    .textContent('Are you sure you want to delete this account?')
                    .ok('Delete')
                    .cancel('Cancel');

                this.$mdDialog.show(dialog).then(() => {
                    this.$rootScope.hideSiteLoadingCover = false;

                    // Attempt to delete the resource.
                    this.API.one('admin/account').customDELETE(account.id).then(() => {
                        this.getAccounts();
                    }, () => {
                        this.$rootScope.hideSiteLoadingCover = true;
                    });
                });
            },
            open: () => {
                this.$mdDialog.show({
                    scope: this.$scope,
                    preserveScope: true,
                    parent: angular.element(this.$document.body),
                    clickOutsideToClose: true,
                    templateUrl: './views/app/components/admin/accounts/dialogs/account.dialog.html'
                });
            },
            close: () => {
                this.$mdDialog.hide();
            }
        };
    }

    /**
     * Gets all accounts.
     */
    getAccounts() {
        // Add pre-create code here.
        // eg. Validation, pre-processing, etc.
        this.$rootScope.hideSiteLoadingCover = false;

        // Attempt to get the resource.
        this.API.one('admin/account').get().then((response) => {
            this.accounts = response.data;
            this.$rootScope.hideSiteLoadingCover = true;
        }, () => {
            this.$rootScope.hideSiteLoadingCover = true;
        });
    }
}

export const AdminAccountsComponent = {
    templateUrl: './views/app/components/admin/accounts/accounts.component.html',
    controller: AdminAccountsController,
    controllerAs: 'vm',
    bindings: {}
};
