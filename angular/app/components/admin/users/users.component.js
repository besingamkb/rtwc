class AdminUsersController {
    constructor(API, $log, ToastService, $rootScope, $mdDialog, DialogService, $document, $scope) {
        'ngInject';

        this.API = API;
        this.$log = $log;
        this.ToastService = ToastService;
        this.$rootScope = $rootScope;
        this.$mdDialog = $mdDialog;
        this.DialogService = DialogService;
        this.$document = $document;
        this.$scope = $scope;
    }

    $onInit() {
        this.$rootScope.hideSidebar = false;
        this.$rootScope.hideViewLoadingCover = true;
        this.$rootScope.sidebar = 'admin';

        this.$rootScope.page = {
            key: 'admin-users',
            name: 'Admin - Users'
        };

        this.getUsers();

        // Load data for drop-downs.
        this.getAccounts();

        // Setup dialogs.
        this.userDialog = {
            type: null,
            data: {},
            create: () => {
                this.userDialog.type = 'create';
                this.userDialog.data = {};
                this.userDialog.open();
            },
            edit: (user) => {
                this.userDialog.type = 'edit';
                this.userDialog.data = angular.copy(user);
                this.userDialog.open();
            },
            store: (user) => {
                // Store logic goes here.
                this.$rootScope.hideSiteLoadingCover = false;

                this.API.one('admin/user').customPOST(user).then(() => {
                    this.getUsers();
                    this.userDialog.close();
                }, () => {
                    this.$rootScope.hideSiteLoadingCover = true;
                });
            },
            update: (user) => {
                // Update logic goes here.
                this.$rootScope.hideSiteLoadingCover = false;

                this.API.one('admin/user/' + user.id).customPUT(user).then(() => {
                    this.getUsers();
                    this.userDialog.close();
                }, () => {
                    this.$rootScope.hideSiteLoadingCover = true;
                });
            },
            delete: (user) => {
                // Delete logic goes here.
                let dialog = this.$mdDialog.confirm()
                    .title('Delete User')
                    .textContent('Are you sure you want to delete this user?')
                    .ok('Delete')
                    .cancel('Cancel');

                this.$mdDialog.show(dialog).then(() => {
                    this.$rootScope.hideSiteLoadingCover = false;

                    // Attempt to delete the resource.
                    this.API.one('admin/user').customDELETE(user.id).then(() => {
                        this.getUsers();
                    }, () => {
                        this.$rootScope.hideSiteLoadingCover = true;
                    });
                });
            },
            open: () => {
                this.$mdDialog.show({
                    scope: this.$scope,
                    preserveScope: true,
                    parent: angular.element(this.$document.body),
                    clickOutsideToClose: true,
                    templateUrl: './views/app/components/admin/users/dialogs/user.dialog.html'
                });
            },
            close: () => {
                this.$mdDialog.hide();
            }
        };
    }

    /**
     * Gets all users.
     */
    getUsers() {
        // Add pre-create code here.
        // eg. Validation, pre-processing, etc.
        this.$rootScope.hideSiteLoadingCover = false;

        // Attempt to get the resource.
        this.API.one('admin/user').get().then((response) => {
            this.users = response.data;
            this.$rootScope.hideSiteLoadingCover = true;
        }, () => {
            this.$rootScope.hideSiteLoadingCover = true;
        });
    }

    /**
     * Gets all accounts.
     */
    getAccounts() {
        // Add pre-create code here.
        // eg. Validation, pre-processing, etc.
        this.$rootScope.hideSiteLoadingCover = false;

        // Attempt to get the resource.
        this.API.one('admin/account').get().then((response) => {
            this.accounts = response.data;
            this.$rootScope.hideSiteLoadingCover = true;
        }, () => {
            this.$rootScope.hideSiteLoadingCover = true;
        });
    }
}

export const AdminUsersComponent = {
    templateUrl: './views/app/components/admin/users/users.component.html',
    controller: AdminUsersController,
    controllerAs: 'vm',
    bindings: {}
};
