class LoginFormController {
    constructor($auth,
                ToastService,
                $location,
                $window,
                $log,
                $localStorage,
                $state,
                $rootScope) {
        'ngInject';

        this.$auth = $auth;
        this.ToastService = ToastService;
        this.$location = $location;
        this.$window = $window;
        this.$rootScope = $rootScope;
        this.$localStorage = $localStorage;
        this.$log = $log;
        this.$state = $state;
    }

    $onInit() {
        this.user = {};
        this.user.email = '';
        this.user.password = '';

        if (this.$localStorage.jwt && this.$localStorage.user) {
            this.$state.go('app.assessment');
        }
        else {
            this.$rootScope.user = this.$localStorage.user = false;
            this.$rootScope.jwt = this.$localStorage.jwt = '';
        }

        this.$rootScope.hideHeader = true;
        this.$rootScope.hideSidebar = true;
        this.$rootScope.hideViewLoadingCover = true;
    }

    login() {
        let user = {
            email: this.user.email,
            password: this.user.password
        };

        this.$auth.login(user)
            .then((response) => {
                this.$auth.setToken(response.data);

                this.ToastService.show('Logged in successfully');

                window.location = '/';

                delete this.$localStorage.previous_url;

                this.$rootScope.user = this.$localStorage.user = response.data.data.user;
                this.$rootScope.jwt = this.$localStorage.jwt = response.data.data.token;

                this.$log.debug(response);
            })
            .catch(this.failedLogin.bind(this));
    }

    failedLogin(response) {
        if (response.status === 422) {
            for (let error in response.data.errors) {
                return this.ToastService.error(response.data.errors[error][0]);
            }
        }
        this.ToastService.error(response.statusText);
    }
}

export const LoginFormComponent = {
    templateUrl: './views/app/components/login-form/login-form.component.html',
    controller: LoginFormController,
    controllerAs: 'vm',
    bindings: {}
};
