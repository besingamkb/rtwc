class ConnectionsAddController {
    constructor(API, $log, ToastService, $state) {
        'ngInject';

        this.API = API;
        this.$log = $log;
        this.ToastService = ToastService;
        this.$state = $state;
    }

    $onInit() {

    }

    submitNewConnectionRequest(formData) {
        this.API.one('connection').customPOST(formData)
            .then((response) => {
                this.ToastService.show('Connection request has been sent to ' + formData.account_key);
                this.$log.debug(response.data);
                this.$state.reload();
            }, (response) => {
                this.ToastService.error(response.data.errors[0]);
                this.$log.debug(response.data);
            });
    }
}

export const ConnectionsAddComponent = {
    templateUrl: './views/app/components/connections/add/add.component.html',
    controller: ConnectionsAddController,
    controllerAs: 'vm',
    bindings: {
        showAddConnection: '='
    }
};
