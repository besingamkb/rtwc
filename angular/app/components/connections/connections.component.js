class ConnectionsController {
    constructor($log, API, $rootScope, $state, ToastService) {
        'ngInject';

        this.$log = $log;
        this.API = API;
        this.$rootScope = $rootScope;
        this.$state = $state;
        this.ToastService = ToastService;
    }

    $onInit() {
        this.$rootScope.hideSidebar = true;
        this.$rootScope.hideViewLoadingCover = true;

        this.showAddConnection = false;

        this.$rootScope.page = {
            key: 'connections',
            name: 'Connections'
        };

        this.getConnections();
    }

    /**
     * Returns all connections.
     */
    getConnections() {
        this.API.one('connection').get()
            .then((response) => {
                this.sentConnections = response.data.sent;
                this.receivedConnections = response.data.received;
                this.$log.debug(response.data);
            });
    }

    /**
     * Accept a connection request.
     *
     * @param $id
     */
    acceptConnectionRequest($id) {
        this.API.one('connection/' + $id + '/accept').customPOST()
            .then((response) => {
                this.ToastService.show('Connection has been accepted');
                this.getConnections();
                this.$log.debug(response.data);
                this.$state.reload();
            });
    }

    /**
     * Reject a connection request.
     *
     * @param $id
     */
    rejectConnectionRequest($id) {
        this.API.one('connection/' + $id + '/reject').customPOST()
            .then((response) => {
                this.ToastService.show('Connection has been rejected');
                this.getConnections();
                this.$log.debug(response.data);
                this.$state.reload();
            });
    }

    /**
     * Delete an existing accepted connection.
     *
     * @param $id
     */
    deleteConnection($id) {
        this.API.one('connection/' + $id).customDELETE()
            .then((response) => {
                this.ToastService.show('Connection has been deleted');
                this.getConnections();
                this.$log.debug(response.data);
                this.$state.reload();
            });
    }
}

export const ConnectionsComponent = {
    templateUrl: './views/app/components/connections/connections.component.html',
    controller: ConnectionsController,
    controllerAs: 'vm',
    bindings: {}
};
