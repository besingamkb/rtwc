class UsersListController {
    constructor(API, $rootScope, $location) {
        'ngInject';

        this.API = API;
        this.$rootScope = $rootScope;
        this.$location = $location;
    }

    $onInit() {
        this.get(1);

        this.results = {};

        this.$rootScope.page = {
            key: 'results',
            name: ''
        }
    }

    get(page) {
        this.$rootScope.hideViewLoadingCover = false;
        this.API.one('admin/results?page=' + page).get()
            .then((response) => {
                this.results.paginate = response;
                this.$rootScope.hideViewLoadingCover = true;
            });
    }

    view(id) {
        this.$location.path('answers/' + id);
    }

    getQuestionTotals() {
        this.$rootScope.hideViewLoadingCover = false;
        this.API.one('admin/question-totals').get()
            .then((response) => {
                this.questionTotals = response;
                this.$rootScope.hideViewLoadingCover = true;
            });
    }

    percentageScore(score, key) {
        if (this.questionTotals) {
            return (100 / this.questionTotals[key]) * score;
        }
    }
}

export const UsersListComponent = {
    templateUrl: './views/app/components/users-list/users-list.component.html',
    controller: UsersListController,
    controllerAs: 'vm',
    bindings: {}
};
