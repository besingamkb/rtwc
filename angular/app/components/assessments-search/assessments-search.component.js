class AssessmentsSearchController {
    constructor($rootScope, $log, API, moment) {
        'ngInject';

        this.$rootScope = $rootScope;
        this.$log = $log;
        this.API = API;
        this.moment = moment;
    }

    $onInit() {
        this.getConnectedAccounts();
    }

    getConnectedAccounts() {
        this.API.one('account/connected/inbound').get()
            .then((response) => {
                this.connectedAccounts = response.data;
                this.$log.debug(response.data);
            });
    }

    sendSearch(formData) {
        let formDataCopy = angular.copy(formData);

        if (formData &&
            formDataCopy.filters &&
            formDataCopy.filters.start_date) {
            formDataCopy.filters.start_date = this.moment(formDataCopy.filters.start_date).add(1, 'hour'); // Needs changing.
        }

        if (formData &&
            formDataCopy.filters &&
            formDataCopy.filters.end_date) {
            formDataCopy.filters.end_date = this.moment(formDataCopy.filters.end_date).add(1, 'hour'); // Needs changing.
        }

        

        // change the end point if we're in reporting.
        this.searchEndpoint = 'assessment/search'
        if(this.reporting==true){
            this.searchEndpoint = 'assessment/search/reporting';
        }

        this.API.one(this.searchEndpoint).customPOST(formDataCopy)
            .then((response) => {
                this.assessments = response.data;
                this.$log.debug(response.data);
            });
    }
}

export const AssessmentsSearchComponent = {
    templateUrl: './views/app/components/assessments-search/assessments-search.component.html',
    controller: AssessmentsSearchController,
    controllerAs: 'vm',
    bindings: {
        assessments: '=',
        reporting: '='
    }
};
