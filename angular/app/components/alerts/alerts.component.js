class AlertsController {
    constructor($log, API, $rootScope, $state, ToastService) {
        'ngInject';

        this.$log = $log;
        this.API = API;
        this.$rootScope = $rootScope;
        this.$state = $state;
        this.ToastService = ToastService;
    }

    $onInit() {
        this.$rootScope.hideSidebar = true;
        this.$rootScope.hideViewLoadingCover = false;

        this.$rootScope.page = {
            key: 'alerts',
            name: 'Alerts'
        };

        this.getAlerts();
    }

    /**
     * Returns all alerts.
     */
    getAlerts() {
        this.API.one('alert').get()
            .then((response) => {
                this.alerts = response.data.data;
                this.$rootScope.hideViewLoadingCover = true;
                this.$log.debug(response.data);
            });
    }
}

export const AlertsComponent = {
    templateUrl: './views/app/components/alerts/alerts.component.html',
    controller: AlertsController,
    controllerAs: 'vm',
    bindings: {}
};
