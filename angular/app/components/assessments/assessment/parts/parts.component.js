class AssessmentsAssessmentPartsController {
    constructor(API, $log, ToastService, $stateParams, $localStorage, $location, $window, $timeout) {
        'ngInject';

        this.API = API;
        this.$log = $log;
        this.ToastService = ToastService;
        this.$stateParams = $stateParams;
        this.$localStorage = $localStorage;
        this.$location = $location;
        this.$window = $window;
        this.$timeout = $timeout;
    }

    $onInit() {
        this.getAssessments();
    }

    /**
     * Returns all the child assessments for this parent assessment.
     */
    getAssessments() {
        this.API.one('assessment/' + this.$stateParams.assessmentID + '/parts').get()
            .then((response) => {
                this.connection = response.data;
            });
    }

    /**
     * Show a Toast alert when the link has been copied to the users clipboard.
     */
    copyLinkConfirm() {
        this.ToastService.show('Assessment link has been copied to your clipboard');
    }

    /**
     * Set reporting to this assessment and then redirect.
     *
     * @param assessment
     */
    goToReporting(assessment) {
        this.$localStorage.reporting = {
            assessments: [assessment]
        };

        if (assessment.type == 'employer') {
            this.$location.path('reporting/metrics');
        }

        else {
            this.$location.path('reporting/summary');
        }
    }

    /**
     * Attempt to generate and download an export of the assessment in it's current state.
     *
     * @param assessment
     */
    getExport(assessment) {
        let downloadPage;
        this.API.one('assessment/' + assessment.id + '/export/additional-text').customPOST().then(response => {
            this.ToastService.show('Export successful');
            this.$window.open('about:blank', 'downloadPage');
            downloadPage = this.$window.open(response.data, 'downloadPage');
        }).then(() => {
            this.$timeout(() => {
                downloadPage.close('downloadPage')
            }, 500);
        });
    }

    /**
     * Return the percentage for completed assessments against total expected assessments.
     *
     * @param current
     * @param max
     * @returns {number}
     */
    calculateProgressPercentage(current, max) {
        return Math.ceil((current / max) * 100);
    }
}

export const AssessmentsAssessmentPartsComponent = {
    templateUrl: './views/app/components/assessments/assessment/parts/parts.component.html',
    controller: AssessmentsAssessmentPartsController,
    controllerAs: 'vm',
    bindings: {}
};
