class AssessmentsAssessmentController {
    constructor(API, $log, $stateParams, $rootScope) {
        'ngInject';

        this.API = API;
        this.$log = $log;
        this.$stateParams = $stateParams;
        this.$rootScope = $rootScope;
    }

    $onInit() {
        this.$rootScope.hideSidebar = true;
        this.$rootScope.hideViewLoadingCover = true;

        this.$rootScope.page = {
            key: 'assessment-detail',
            name: 'Assessment'
        }

        this.getAssessment();
    }

    getAssessment() {
        this.API.one('assessment/' + this.$stateParams.assessmentID).get()
            .then((response) => {
                this.assessment = response.data;
            });
    }
}

export const AssessmentsAssessmentComponent = {
    templateUrl: './views/app/components/assessments/assessment/assessment.component.html',
    controller: AssessmentsAssessmentController,
    controllerAs: 'vm',
    bindings: {}
};
