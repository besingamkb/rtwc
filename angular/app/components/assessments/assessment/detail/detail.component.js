class AssessmentsAssessmentDetailController {
    constructor(API, $log, $stateParams) {
        'ngInject';

        this.API = API;
        this.$log = $log;
        this.$stateParams = $stateParams;
    }
}

export const AssessmentsAssessmentDetailComponent = {
    templateUrl: './views/app/components/assessments/assessment/detail/detail.component.html',
    controller: AssessmentsAssessmentDetailController,
    controllerAs: 'vm',
    bindings: {
        assessment: '='
    }
};
