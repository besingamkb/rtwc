class AssessmentsEmployerController {
    constructor(API, $rootScope, DialogService, $timeout, $document, $scope, $log, ToastService, $stateParams,
                $anchorScroll, $mdDialog, $state) {
        'ngInject';

        this.API = API;
        this.$rootScope = $rootScope;
        this.$timeout = $timeout;
        this.$document = $document;
        this.$scope = $scope;
        this.ToastService = ToastService;
        this.$mdDialog = $mdDialog;
        this.$log = $log;
        this.$stateParams = $stateParams;
        this.$anchorScroll = $anchorScroll;
        this.$state = $state;
    }

    $onInit() {
        this.$rootScope.hideSidebar = true;

        this.$rootScope.page = {
            key: 'assessments-employer',
            name: 'Employer Assessment'
        };

        let assessmentKey = this.$stateParams.assessmentKey;

        if (!assessmentKey) {
            return false;
        }

        this.connection = {};
        this.form = {};
        this.assessmentPage = 1;

        this.getAssessmentByKey(assessmentKey);
    }

    /**
     * Returns the assessment, fetching by it's key.
     */
    getAssessmentByKey(assessmentKey) {
        this.API.one('assessment/get-by-key/' + assessmentKey).get()
            .then((response) => {
                this.getAssessmentAsForm(response.data.id);
                this.$log.debug(response.data);

                if (this.$rootScope.user.account_id != response.data.account_id) {
                    // this.$state.go('app.dashboard');
                }
            });
    }

    /**
     * Returns the assessment with questions under their respective categories.
     */
    getAssessmentAsForm(assessmentID) {
        this.API.one('assessment/' + assessmentID + '/employer/form').get()
            .then((response) => {
                this.connection = response.data;

                if (!Array.isArray(this.connection._results)) {
                    this.form = this.connection._results;
                }

                this.$log.debug(response.data);
                this.$rootScope.hideViewLoadingCover = true;
            });
    }

    /**
     * Attempts to submit or resubmit an assessment.
     *
     * @param formData
     */
    saveForm(formData) {
        if (formData['use-of-labour'] == 0) {
            formData['average-percentage-of-labour-agency-workers-ftse'] = null;
        }

        this.$log.debug(formData);

        this.API.one('assessment/' + this.connection.id + '/submit').customPOST(formData)
            .then((response) => {
                this.$state.go('app.dashboard');
                this.$log.debug(response.data);
            });
    }

    /**
     * Shows a confirm dialog and sets this assessment to fully submitted.
     *
     * @param formData
     */
    saveFormConfirm(formData) {
        let confirm = this.$mdDialog.confirm()
            .title('Submit Assessment')
            .textContent('Are you sure you wish to submit this assessment? You will not be able to ' +
                're-enter your results at a later time.')
            .ok('Submit')
            .cancel('Cancel');

        this.$mdDialog.show(confirm).then(() => {
            formData.status = 10;
            this.saveForm(formData);
        });
    }

    /**
     * Goes to the next or previous page of the assessment.
     *
     * @param page
     */
    changePage(page) {
        this.assessmentPage = page;
    }

    /**
     * Returns the corresponding result value for a question if this assessment has been submitted already.
     *
     * @param questionKey
     * @returns {*}
     */
    findResultFromQuestionKey(questionKey) {
        if (typeof this.connection === 'undefined' ||
            typeof this.connection._results === 'undefined' ||
            typeof this.connection._results[questionKey] === 'undefined') {
            return false;
        }

        return this.connection._results[questionKey];
    }

    /**
     * Returns the corresponding question text for a question question key.
     *
     * @param questionKey
     * @returns {*}
     */
    findQuestionTextFromQuestionKey(questionKey) {
        if (typeof this.connection === 'undefined' ||
            typeof this.connection.question_set === 'undefined' ||
            typeof this.connection.question_set.questions === 'undefined') {
            return false;
        }

        let filteredQuestions = this.connection.question_set.questions.filter(function (el) {
            return el.key == questionKey;
        });

        if (filteredQuestions.length < 1) {
            return false;
        }

        return filteredQuestions[0].text;
    }
}

export const AssessmentsEmployerComponent = {
    templateUrl: './views/app/components/assessments/employer/employer.component.html',
    controller: AssessmentsEmployerController,
    controllerAs: 'vm',
    bindings: {}
};
