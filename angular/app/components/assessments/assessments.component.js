class AssessmentsController {
    constructor(API, $log, ToastService, $rootScope) {
        'ngInject';

        this.API = API;
        this.$log = $log;
        this.ToastService = ToastService;
        this.$rootScope = $rootScope;
    }

    $onInit() {
        this.$rootScope.hideSidebar = true;
        this.$rootScope.hideViewLoadingCover = true;

        this.$rootScope.page = {
            key: 'assessments',
            name: 'Assessments'
        };

        this.getAssessments();
    }

    /**
     * Returns all the child assessments for this parent assessment.
     */
    getAssessments() {
        this.API.one('assessment').get()
            .then((response) => {
                this.assessments = response.data;
                this.$log.debug(response.data);
            });
    }

    /**
     * Show a Toast alert when the link has been copied to the users clipboard.
     */
    copyLinkConfirm() {
        this.ToastService.show('Assessment link has been copied to your clipboard');
    }

    /**
     * Return the percentage for completed assessments against total expected assessments.
     *
     * @param current
     * @param max
     * @returns {number}
     */
    calculateProgressPercentage(current, max) {
        return Math.ceil((current / max) * 100);
    }
}

export const AssessmentsComponent = {
    templateUrl: './views/app/components/assessments/assessments.component.html',
    controller: AssessmentsController,
    controllerAs: 'vm'
};
