class SiteViewLoaderController {
    constructor() {
        'ngInject';
    }
}

export const SiteViewLoaderComponent = {
    templateUrl: './views/app/components/site-view-loader/site-view-loader.component.html',
    controller: SiteViewLoaderController,
    controllerAs: 'vm',
    bindings: {}
}