class ProgressBarController {
    constructor($log) {
        'ngInject';

        this.$log = $log;
    }

    $onInit() {
        if (!this.min || !this.max) {
            this.$log.error('Could not calculate progress percentage because there is no min or max value');
            return false;
        }

        this.calculateProgressPercentage(this.min, this.max);
    }

    /**
     * Return the percentage for completed assessments against total expected assessments.
     *
     * @param min
     * @param max
     * @returns {number}
     */
    calculateProgressPercentage(min, max) {
        this.percentage = Math.round((min / max)) * 100;
    }
}

export const ProgressBarComponent = {
    templateUrl: './views/app/components/progress-bar/progress-bar.component.html',
    controller: ProgressBarController,
    controllerAs: 'vm',
    bind: {
        min: '=',
        max: '='
    }
};
