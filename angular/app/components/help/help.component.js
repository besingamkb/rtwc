class HelpController {
    constructor($rootScope) {
        'ngInject';

        this.$rootScope = $rootScope;
    }

    $onInit() {
        this.$rootScope.hideSidebar = true;
        this.$rootScope.hideViewLoadingCover = true;

        this.$rootScope.page = {
            key: 'help',
            name: 'Help'
        }
    }
}

export const HelpComponent = {
    templateUrl: './views/app/components/help/help.component.html',
    controller: HelpController,
    controllerAs: 'vm',
    bindings: {}
};
