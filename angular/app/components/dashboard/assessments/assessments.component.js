class DashboardAssessmentsController {
    constructor(API, $log, ToastService) {
        'ngInject';

        this.API = API;
        this.$log = $log;
        this.ToastService = ToastService;
    }

    $onInit() {
        this.getAssessments();
    }

    /**
     * Returns all the child assessments for this parent assessment.
     */
    getAssessments() {
        this.API.one('assessment').get()
            .then((response) => {
                this.connection = response.data;
                this.$log.debug(response.data);
            });
    }

    /**
     * Show a Toast alert when the link has been copied to the users clipboard.
     */
    copyLinkConfirm() {
        this.ToastService.show('Assessment link has been copied to your clipboard');
    }

    /**
     * Return the percentage for completed assessments against total expected assessments.
     *
     * @param current
     * @param max
     * @returns {number}
     */
    calculateProgressPercentage(current, max) {
        return Math.ceil((current / max) * 100);
    }
}

export const DashboardAssessmentsComponent = {
    templateUrl: './views/app/components/dashboard/assessments/assessments.component.html',
    controller: DashboardAssessmentsController,
    controllerAs: 'vm',
    bindings: {}
};
