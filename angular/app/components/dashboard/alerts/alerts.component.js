class DashboardAlertsController {
    constructor(API, $log, ToastService) {
        'ngInject';

        this.API = API;
        this.$log = $log;
        this.ToastService = ToastService;
    }

    $onInit() {
        this.getAlerts();
    }

    /**
     * Returns all the child assessments for this parent assessment.
     */
    getAlerts() {
        this.API.one('alert').get()
            .then((response) => {
                this.alert = response.data.data;
                this.$log.debug(response.data);
            });
    }

}

export const DashboardAlertsComponent = {
    templateUrl: './views/app/components/dashboard/alerts/alerts.component.html',
    controller: DashboardAlertsController,
    controllerAs: 'vm',
    bindings: {}
};
