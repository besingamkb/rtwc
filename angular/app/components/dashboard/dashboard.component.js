class DashboardController {
    constructor($rootScope) {
        'ngInject';

        this.$rootScope = $rootScope;
    }

    $onInit() {
        this.$rootScope.hideSidebar = true;
        this.$rootScope.hideViewLoadingCover = true;

        this.$rootScope.page = {
            key: 'dashboard',
            name: 'Dashboard'
        };
    }
}

export const DashboardComponent = {
    templateUrl: './views/app/components/dashboard/dashboard.component.html',
    controller: DashboardController,
    controllerAs: 'vm',
    bindings: {}
};
