export function AuthCheckRun($rootScope, $localStorage, $location, jwtHelper, $state) {
    'ngInject';

    $rootScope.user = $localStorage.user;

    $rootScope.logout = function () {
        $localStorage.jwt = '';
        $rootScope.user = '';

        $state.go('app.login');
    };

    var authCheck = function () {
        if (!$localStorage.jwt) {
            setPreviousUrl();
            $rootScope.logout();
        } else {
            if (jwtHelper.isTokenExpired($localStorage.jwt)) {
                setPreviousUrl();
                $rootScope.logout();
            }
        }
    };

    var setPreviousUrl = function () {
        let currentUrl = $location.absUrl();

        if (!$localStorage.previous_url) {
            $localStorage.previous_url = currentUrl;
        }
    };

    authCheck();

    var deregisterLocationChange = $rootScope.$on('$locationChangeStart', function () {
        authCheck();
        $rootScope.navOpen = false;
    });

    $rootScope.$on('$destroy', deregisterLocationChange);
}
