export function BodyClassRun($rootScope, $state) {
    'ngInject';

    $rootScope.bodyClass = function () {
        if ($state.current.data) {
            return $state.current.data.class;
        }
    }
}
