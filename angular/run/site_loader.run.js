export function LoaderRun($rootScope, $localStorage) {
    'ngInject';

    /**
     * Global $rootScope variables.
     */
    $rootScope.hideSiteLoadingCover = true;
    $rootScope.hideViewLoadingCover = false;
    $rootScope.hideHeader = false;
    $rootScope.hideSidebar = false;
    $rootScope.sidebar = 'default';

    $rootScope.$localStorage = $localStorage;

    /**
     * Global $rootScope functions.
     */
    $rootScope.findByProperty = function (array, property, value) {
        for (var i = 0; i < array.length; i += 1) {
            if (array[i][property] === value) {
                return i;
            }
        }
        return -1;
    }
}
