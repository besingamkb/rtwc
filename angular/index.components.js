import {DashboardComponent} from './app/components/dashboard/dashboard.component';
import {DashboardAssessmentsComponent} from './app/components/dashboard/assessments/assessments.component';
import {DashboardAlertsComponent} from './app/components/dashboard/alerts/alerts.component';

import {AssessmentsComponent} from './app/components/assessments/assessments.component';
import {AssessmentsAssessmentComponent} from './app/components/assessments/assessment/assessment.component';
import {AssessmentsAssessmentDetailComponent} from './app/components/assessments/assessment/detail/detail.component';
import {AssessmentsAssessmentPartsComponent} from './app/components/assessments/assessment/parts/parts.component';

import {AssessmentsSearchComponent} from './app/components/assessments-search/assessments-search.component';

import {AssessmentsEmployerComponent} from './app/components/assessments/employer/employer.component';

import {ProgressBarComponent} from './app/components/progress-bar/progress-bar.component';

import {ConnectionsComponent} from './app/components/connections/connections.component';
import {ConnectionsAddComponent} from './app/components/connections/add/add.component';

import {AlertsComponent} from './app/components/alerts/alerts.component';

import {ReportingAlertsComponent} from './app/components/reporting/alerts/alerts.component';
import {ReportingAssessmentsComponent} from './app/components/reporting/assessments/assessments.component';
import {ReportingComparativeComponent} from './app/components/reporting/comparative/comparative.component';
import {ReportingDemographicsComponent} from './app/components/reporting/demographics/demographics.component';
import {ReportingDetailComponent} from './app/components/reporting/detail/detail.component';
import {ReportingMetricsComponent} from './app/components/reporting/metrics/metrics.component';
import {ReportingSummaryComponent} from './app/components/reporting/summary/summary.component';
import {ReportingBenchmarkComponent} from './app/components/reporting/benchmark/benchmark.component';

import {ReportingFilterComponent} from './app/components/reporting/filter/filter.component';

import {HelpComponent} from './app/components/help/help.component';

import {AdminAccountsComponent} from './app/components/admin/accounts/accounts.component';
import {AdminAssessmentsComponent} from './app/components/admin/assessments/assessments.component';
import {AdminUsersComponent} from './app/components/admin/users/users.component';
import {AdminQuestionSetsComponent} from './app/components/admin/question-sets/question-sets.component';
import {AdminQuestionCategoriesComponent} from './app/components/admin/question-categories/question-categories.component';
import {AdminQuestionsComponent} from './app/components/admin/questions/questions.component';

import {UsersListComponent} from './app/components/users-list/users-list.component';
import {ResetPasswordComponent} from './app/components/reset-password/reset-password.component';
import {ForgotPasswordComponent} from './app/components/forgot-password/forgot-password.component';
import {LoginFormComponent} from './app/components/login-form/login-form.component';

import {SiteViewLoaderComponent} from './app/components/site-view-loader/site-view-loader.component';

angular.module('app.components')
    .component('dashboard', DashboardComponent)
    .component('dashboardAssessments', DashboardAssessmentsComponent)
    .component('dashboardAlerts', DashboardAlertsComponent)

    .component('assessments', AssessmentsComponent)
    .component('assessmentsAssessment', AssessmentsAssessmentComponent)
    .component('assessmentsAssessmentDetail', AssessmentsAssessmentDetailComponent)
    .component('assessmentsAssessmentParts', AssessmentsAssessmentPartsComponent)

    .component('assessmentsSearch', AssessmentsSearchComponent)

    .component('assessmentEmployer', AssessmentsEmployerComponent)

    .component('progressBar', ProgressBarComponent)

    .component('connections', ConnectionsComponent)
    .component('connectionsAdd', ConnectionsAddComponent)

    .component('alerts', AlertsComponent)

    .component('reportingAlerts', ReportingAlertsComponent)
    .component('reportingAssessments', ReportingAssessmentsComponent)
    .component('reportingComparative', ReportingComparativeComponent)
    .component('reportingDemographics', ReportingDemographicsComponent)
    .component('reportingDetail', ReportingDetailComponent)
    .component('reportingMetrics', ReportingMetricsComponent)
    .component('reportingSummary', ReportingSummaryComponent)
    .component('reportingBenchmark', ReportingBenchmarkComponent)

    .component('reportingFilter', ReportingFilterComponent)

    .component('help', HelpComponent)

    .component('adminAccounts', AdminAccountsComponent)
    .component('adminAssessments', AdminAssessmentsComponent)
    .component('adminUsers', AdminUsersComponent)
    .component('adminQuestionSets', AdminQuestionSetsComponent)
    .component('adminQuestionCategories', AdminQuestionCategoriesComponent)
    .component('adminQuestions', AdminQuestionsComponent)

    .component('usersList', UsersListComponent)
    .component('resetPassword', ResetPasswordComponent)
    .component('forgotPassword', ForgotPasswordComponent)
    .component('loginForm', LoginFormComponent)
    .component('siteViewLoader', SiteViewLoaderComponent);
