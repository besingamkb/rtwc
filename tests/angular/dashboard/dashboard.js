describe('test that dashboard loads', function () {
    it('should have a title', function () {
        browser.get(browser.baseUrl);

        expect(browser.getTitle()).toEqual('Responsible Trade Worldwide');
    });
});
