<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as IlluminateTestCase;
use Illuminate\Contracts\Console\Kernel;

class TestCase extends IlluminateTestCase
{
    protected $baseUrl;

    private $authUser;
    private $authUserToken;
    private $headers = [];

    public function createApplication()
    {
        $app = require __DIR__ . '/../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        $this->baseUrl = config('APP_DOMAIN');

        return $app;
    }

    public function getAuthUser()
    {
        if (!$this->authUser) {
            $this->setAuthUserToken();
        }

        return $this->authUser;
    }

    public function getAuthUserToken()
    {
        if (!$this->authUserToken) {
            $this->setAuthUserToken();
        }

        return $this->authUserToken;
    }

    private function setAuthUserToken($userID = 1)
    {
        $authUser = \App\Models\User::find($userID);

        $this->authUser = $authUser;
        $this->authUserToken = \JWTAuth::fromUser($authUser);

        \JWTAuth::setToken($this->authUserToken);

        \Auth::login($this->authUser);
    }

    public function setHeaders($headers)
    {
        if (!isset($headers['Authorization'])) {
            $headers['Authorization'] = 'Bearer ' . $this->getAuthUserToken();
        }

        $this->headers = $headers;
    }

    public function authUserGet($uri, $headers = [])
    {
        $this->setHeaders($headers);

        return $this->get($uri, $this->headers);
    }

    public function authUserPost($uri, $parameters = [], $headers = [])
    {
        $this->setHeaders($headers);

        return $this->post($uri, $parameters, $headers);
    }

    public function authUserPut($uri, $parameters = [], $headers = [])
    {
        $this->setHeaders($headers);

        return $this->put($uri, $parameters, $headers);
    }

    public function authUserDelete($uri, $parameters = [], $headers)
    {
        $this->setHeaders($headers);

        return $this->delete($uri, $parameters, $headers);
    }

    public function authUserCall(
        $method,
        $uri,
        $headers = [],
        $cookies = [],
        $files = [],
        $server = [],
        $content = null
    ) {
        $this->setHeaders($headers);

        return $this->call($method, $uri, $headers, $cookies, $files, $server, $content);
    }

    public function seeApiSuccess()
    {
        return $this->seeJsonKey('data');
    }

    public function seeValidationError()
    {
        $this->assertResponseStatus(422);

        return $this->see('"errors":');
    }

    public function seeApiError($error_code)
    {
        $this->assertResponseStatus($error_code);

        return $this->see('"errors":');
    }

    public function seeJsonKey($entity)
    {
        return $this->see('"' . $entity . '":');
    }

    public function seeJsonValue($value)
    {
        return $this->see('"' . $value . '"');
    }

    public function seeJsonArray($entity)
    {
        return $this->see('"' . $entity . '":[');
    }

    public function seeJsonObject($entity)
    {
        return $this->see('"' . $entity . '":{');
    }

    public function seeJsonKeyValue($key, $value)
    {
        return $this->see('"' . $key . '":' . $value);
    }

    public function seeJsonKeyValueString($key, $value)
    {
        return $this->see('"' . $key . '":"' . $value . '"');
    }

    public function seeJsonArrayNotEmpty($key)
    {
        return $this->see('"' . $key . '":[]', true);
    }

    public function seeJsonArrayEmpty($key)
    {
        return $this->see('"' . $key . '":[]');
    }

    public function seeJsonObjectNotEmpty($key)
    {
        return $this->see('"' . $key . '":{}', true);
    }

    public function seeJsonObjectEmpty($key)
    {
        return $this->see('"' . $key . '":{}');
    }
}
