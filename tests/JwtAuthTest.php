<?php

namespace Tests;

use Illuminate\Foundation\Testing\DatabaseTransactions;

class JwtAuthTest extends TestCase
{
    use DatabaseTransactions;

    public function testSuccessfulLogin()
    {
        $user = factory(\App\Models\User::class)->create([
            'password' => bcrypt('test12345'),
        ]);

        $this->post('/api/auth/login', [
            'email' => $user->email,
            'password' => 'test12345',
        ])
            ->seeApiSuccess()
            ->seeJsonKeyValueString('email', $user->email)
            ->seeJsonKey('token')
            ->dontSee('"password"');
    }

    public function testFailedLogin()
    {
        $user = factory(\App\Models\User::class)->create();

        $this->post('/api/auth/login', [
            'email' => $user->email,
            'password' => str_random(10),
        ])
            ->seeApiError(401)
            ->dontSee($user->email)
            ->dontSee('"token"');
    }

    public function testSuccessfulRegistration()
    {
        $user = factory(\App\Models\User::class)->make();

        $this->post('/api/auth/register', [
            'name' => $user->name,
            'email' => $user->email,
            'password' => 'test15125',
        ])
            ->seeApiSuccess()
            ->seeJsonKeyValueString('email', $user->email)
            ->seeJsonKey('token');
    }
}
