<?php
//
//namespace Tests\API\Question;
//
//use Tests\TestCase;
//
//class AssessmentTest extends TestCase
//{
//    use \Illuminate\Foundation\Testing\DatabaseTransactions;
//
//    public function test_that_an_assessment_can_be_fetched_by_its_id()
//    {
//        $this->getAuthUser();
//
//        $assessment = \App\Models\Assessment::where('key', 'jadwe29dl')->first();
//        $assessmentAsArray = $assessment->toArray();
//
//        $response = $this->authUserGet('/api/assessment/' . $assessment->id);
//
//        $response->seeJsonContains($assessmentAsArray)
//            ->seeApiSuccess();
//    }
//
//    public function test_that_an_assessment_can_be_fetched_by_its_key()
//    {
//        $this->getAuthUser();
//
//        $assessment = \App\Models\Assessment::where('key', 'jadwe29dl')->first();
//        $assessmentAsArray = $assessment->toArray();
//
//        $response = $this->authUserGet('/api/assessment/get-by-key/' . $assessment->key);
//
//        $response->seeJsonContains($assessmentAsArray)
//            ->seeApiSuccess();
//    }
//
//    /*public function test_that_an_employer_assessment_can_be_submitted()
//    {
//        $this->getAuthUser();
//
//        $assessment = \App\Models\Assessment::where('key', 'jadwe29dl')->first();
//
//        // We don't need to provide all of the expected results; just a few of them to check.
//        // But we will need to change this once server side assessment validation is implemented.
//        $response = $this->authUserPost('/api/assessment/' . $assessment->id . '/submit', [
//            'use_of_labour' => 1,
//            'full_time_employed_workers' => 25,
//            'white_irish' => 15
//        ]);
//
//        $response->seeJsonKeyValue('use_of_labour', 1)
//            ->seeJsonKeyValue('full_time_employed_workers', 25)
//            ->seeJsonKeyValue('white_irish', 15)
//            ->seeApiSuccess();
//    }*/
//
//    /*public function test_that_an_employer_assessment_can_be_resubmitted()
//    {
//        $this->getAuthUser();
//
//        $assessment = \App\Models\Assessment::where('key', 'jadwe29dl')->first();
//
//        // By submitting an assessment with empty values we assume all results will have 0 values.
//        $assessment->submitAssessment([]);
//
//        // We don't need to provide all of the expected results; just a few of them to check.
//        // But we will need to change this once server side assessment validation is implemented.
//        $response = $this->authUserPut('/api/assessment/' . $assessment->id . '/resubmit', [
//            'use_of_labour' => 1,
//            'full_time_employed_workers' => 20,
//            'white_irish' => 15
//        ]);
//
//        $response->seeJsonKeyValue('use_of_labour', 1)
//            ->seeJsonKeyValue('full_time_employed_workers', 20)
//            ->seeJsonKeyValue('white_irish', 15)
//            ->seeApiSuccess();
//    }*/
//
//    /**
//     * If this test fails please make sure your results_employer and results table are clear.
//     * This test needs further improvement to counteract this issue.
//     */
//    public function test_that_an_employer_assessment_form_is_fetched_properly_if_it_has_not_been_previously_submitted()
//    {
//        $this->getAuthUser();
//
//        $assessment = \App\Models\Assessment::where('key', 'jadwe29dl')->first();
//
//        $response = $this->authUserGet('/api/assessment/' . $assessment->id . '/employer/form');
//
//        $response->seeJsonArray('_questions')
//            ->seeJsonArray('_categories')
//            ->seeJsonArrayEmpty('_results')
//            ->seeJsonKeyValue('submitted', 'false')
//            ->seeApiSuccess();
//    }
//
//    public function test_that_an_employer_assessment_form_is_fetched_properly_if_it_has_been_previously_submitted()
//    {
//        $this->getAuthUser();
//
//        $assessment = \App\Models\Assessment::where('key', 'jadwe29dl')->first();
//        $assessment->submitAssessment([]);
//
//        $response = $this->authUserGet('/api/assessment/' . $assessment->id . '/employer/form');
//
//        $response->seeJsonArray('_questions')
//            ->seeJsonArray('_categories')
//            // ->seeJsonArrayNotEmpty('_results') // This works in the app but not in tests, needs looking at.
//            ->seeJsonKeyValue('submitted', 'true')
//            ->seeApiSuccess();
//    }
//}
