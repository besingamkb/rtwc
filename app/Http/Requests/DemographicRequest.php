<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\QuestionSet;

class DemographicRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $returnArray = [];

        $demographics = QuestionSet::key(config('assessment.demographic_question_set_key'))->with('questions')->firstOrFail();

        foreach($demographics->questions as $question)
        {
            $returnArray[config('assessment.form_name_prefix') . $question->id] = 'required';
        }

        return $returnArray;
    }
}
