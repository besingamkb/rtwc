<?php

namespace App\Http\Requests\Admin\Assessment;

use App\Http\Requests\APIRequest;

class CreateRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'key' => 'required|string|unique:assessments,key',
            'name' => 'required|string',
            'account_id' => 'required|integer|exists:accounts,id',
            'question_set_id' => 'required|integer|exists:question_sets,id',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'worker_count' => 'required|integer'
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'key.required' => 'The assessment ID field is required'
        ];
    }
}
