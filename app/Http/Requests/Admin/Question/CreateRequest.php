<?php

namespace App\Http\Requests\Admin\Question;

use App\Http\Requests\APIRequest;

class CreateRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'key' => 'required|string|unique:questions,key',
            'name' => 'required|string',
            'category_id' => 'integer|exists:question_categories,id',
            'question_set_id' => 'required|integer|exists:question_sets,id',
            'type_id' => 'required|integer',
            'text' => 'required|string'
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
