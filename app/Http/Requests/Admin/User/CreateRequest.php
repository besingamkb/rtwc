<?php

namespace App\Http\Requests\Admin\User;

use App\Http\Requests\APIRequest;

class CreateRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'email' => 'required|email|unique:users,email',
            'account_id' => 'required|integer|exists:accounts,id',
            'username' => 'required|string|min:8|unique:users,username',
            'password' => 'min:8',
            'password_confirm' => 'same:password'
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'password_confirm.same' => 'Passwords do not match'
        ];
    }
}
