<?php

namespace App\Http\Requests\Assessment;

use App\Http\Requests\Request;

class SearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'assessment_key' => 'string',
            'start_date' => 'date',
            'end_date' => 'date',
            'account_id' => 'integer'
        ];
    }
}
