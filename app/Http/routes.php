<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */
Route::resource('questions-set', 'Admin\QuestionSetsController');
Route::group(['middleware' => ['web']], function () {
    // Laravel Angular routes
    Route::get('/', 'AngularController@serveApp');

    Route::group(['prefix' => 'assessment/{assessment}', 'namespace' => 'Assessment', 'middleware' => 'assessment'],
        function () {
            Route::get('/', 'AssessmentController@showLocales')
                ->name('assessment-locales');
            Route::get('/demographics/{locale}', 'AssessmentController@showDemographics')
                ->name('assessment-demographics');
            Route::post('/demographics/{locale}/store', 'AssessmentController@storeDemographics')
                ->name('assessment-demographics-store');
            Route::get('/questions', 'AssessmentController@showQuestions')
                ->name('assessment-questions');
            Route::post('/questions/store', 'AssessmentController@storeQuestions')
                ->name('assessment-questions-store');
            Route::get('/additional', 'AssessmentController@showAdditional')
                ->name('assessment-additional');
            Route::post('/additional/store', 'AssessmentController@storeAdditional')
                ->name('assessment-additional-store');
            Route::get('/complete', 'AssessmentController@showComplete')
                ->name('assessment-complete');
        });

    Route::get('/assessment-invalid', 'Assessment\AssessmentController@showInvalid')->name('assessment-invalid');

    Route::get('/unsupported-browser', 'AngularController@unsupported');

    Route::get('/test', 'Admin\QuestionController@index');
});

if (env('APP_MODE') == "development") {
    Route::resource('questions-set', 'Admin\QuestionSetsController');
}

//public API routes
$api->group(['middleware' => ['api']], function ($api) {
    // Authentication routes
    $api->post('auth/login', 'Auth\AuthController@login');
    $api->post('auth/register', 'Auth\AuthController@register');

    // Password reset routes
    $api->post('auth/password/email', 'Auth\PasswordResetController@sendResetLinkEmail');
    $api->get('auth/password/verify', 'Auth\PasswordResetController@verify');
    $api->post('auth/password/reset', 'Auth\PasswordResetController@reset');
});

//protected API routes with JWT (must be logged in)
$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    // Question routes
    $api->get('admin/results', 'Admin\ResultsController@paginate');
    // $api->get('admin/question-totals', 'Admin\QuestionsController@totals');
    // $api->resource('admin/answers', 'Admin\AnswersController');
    $api->resource('questions', 'Admin\QuestionController');
    $api->resource('question-options', 'Admin\QuestionOptionsController');
    $api->resource('question-options-translation', 'Admin\QuestionOptionTranslationController');
    $api->resource('questions-set', 'Admin\QuestionSetsController');
    $api->resource('questions-category', 'Admin\QuestionSetsController');
    $api->resource('question-categories', 'Admin\QuestionCategories');
    $api->resource('question-translations', 'Admin\QuestionTranslationController');
    $api->resource('question-set-translation', 'Admin\QuestionSetTranslationController');

    // All these API requests should be under the API namespace.
    $api->group(['namespace' => 'API'], function ($api) {
        // Connection endpoints.
        $api->post('connection/{id}/reject', 'ConnectionController@rejectRequest');
        $api->post('connection/{id}/accept', 'ConnectionController@acceptRequest');
        $api->resource('connection', 'ConnectionController');

        // Account endpoints.
        $api->get('account/connection', 'AccountController@connections');
        $api->get('account/connected/{direction?}', 'AccountController@getConnectedAccounts');

        // Assessment endpoints.
        $api->get('assessment/{assessmentID}/{part}/form', 'AssessmentController@showInSubmissionFormat');
        $api->post('assessment/{assessmentID}/submit', 'AssessmentController@submitAssessment');
        $api->put('assessment/{assessmentID}/resubmit', 'AssessmentController@resubmitAssessment');
        $api->get('assessment/{assessmentID}/parts', 'AssessmentController@getAssessmentParts');
        $api->get('assessment/get-by-key/{assessmentKey}', 'AssessmentController@getByKey');
        $api->post('assessment/search', 'AssessmentController@search');
        $api->post('assessment/{assessmentID}/export/{type}', 'AssessmentController@export');
        $api->post('assessment/search/reporting', 'AssessmentController@searchReporting');
        $api->resource('assessment', 'AssessmentController');

        // Alert endpoints.
        $api->resource('alert', 'AlertController');

        // Reporting endpoints.
        $api->get('reporting/filter-options', 'ReportingController@getFilterOptions');
        $api->post('reporting/summary', 'ReportingController@getSummary');
        $api->post('reporting/benchmark', 'ReportingController@getBenchmark');
        $api->post('reporting/benchmark-all', 'ReportingController@getBenchmarkAll');
        $api->post('reporting/scores', 'ReportingController@getScoreTotals');
        $api->post('reporting/alerts', 'ReportingController@getAlertTotals');
        $api->post('reporting/demographics', 'ReportingController@getDemographics');
        $api->post('reporting/categories', 'ReportingController@getCategories');
        $api->post('reporting/category-alerts', 'ReportingController@getCategoryAlerts');
        $api->post('reporting/detail', 'ReportingController@getDetail');
        $api->post('reporting/metrics', 'ReportingController@getMetrics');

        $api->post('reporting/categories-comparatives', 'ReportingController@getCategoriesComparatives');
        $api->post('reporting/metrics-comparatives', 'ReportingController@getMetricsComparatives');

        $api->get('reporting/pdf', 'ReportingController@getPdf');

        // Question endpoints.
        $api->resource('question', 'QuestionController');

        // Question Set endpoints.
        $api->get('question-set/{questionSetID}/question', 'QuestionController@indexByQuestionSet');
        $api->resource('question-set', 'QuestionSetController');

        // Admin endpoints.
        $api->group([
            'namespace' => 'Admin',
            'prefix' => 'admin'
        ], function ($api) {
            $api->resource('account', 'AccountController');
            $api->resource('assessment', 'AssessmentController');
            $api->resource('user', 'UserController');
            $api->resource('question-set', 'QuestionSetController');
            $api->resource('question', 'QuestionController');
            $api->resource('question-category', 'QuestionCategoryController');
        });
    });
});
