<?php

namespace App\Http\Controllers;

use Tymon\JWTAuth\JWTAuth;

class APIController extends Controller
{
    /**
     * Store the currently authenticated user.
     *
     * @var
     */
    protected $authUser;

    /**
     * Store the currently authenticated users account.
     *
     * @var
     */
    protected $authAccount;

    /**
     * AlertController constructor.
     *
     * @param JWTAuth $auth
     */
    public function __construct(JWTAuth $auth)
    {
        $this->authUser = $auth->parseToken()->authenticate();
        $this->authAccount = $this->authUser->account;
    }
}
