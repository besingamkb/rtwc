<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Question;
use App\Models\QuestionTranslation;
use Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class QuestionTranslationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dprint($request->all());
        $validator = Validator::make($request->all(), [
            'question_id' => 'required',
            'locale' => 'required',
            'text' => 'required',
            'additional_text' => 'required'
        ]);

        if ($validator->fails()) {
            return Response::json(['errors' => $validator->errors()->first()], 202);
        }

        $translation = new QuestionTranslation;
        $translation->question_id = ($request->get('question_id')) ? $request->get('question_id') : "";
        $translation->locale = ($request->get('locale')) ? $request->get('locale') : "";
        $translation->text = ($request->get('text')) ? $request->get('text') : "";
        $translation->additional_text = ($request->get('additional_text')) ? $request->get('additional_text') : "";

        $translation->save();

        return Response::json(['message' => 'Question Translation Saved!'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $translation = QuestionTranslation::where('question_id', $id)->get();

        $locales = config('translatable.locales');
        $available_locale = [];

        foreach ($translation as $trans) {
            if (($key = array_search($trans->locale, $locales)) !== false) {
                unset($locales[$key]);
            }
        }

        return Response::json(['translations' => $translation, 'locale' => $locales], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $translation = QuestionTranslation::find($id);
            $translation->locale = ($request->get('locale')) ? $request->get('locale') : "";
            $translation->text = ($request->get('text')) ? $request->get('text') : "";
            $translation->additional_text = ($request->get('additional_text')) ? $request->get('additional_text') : "";

            $translation->save();

            if ($translation->locale == "en") {
                $question = Question::find($request->get('question_id'));
                $question->text = $translation->text;
                $question->additional_text = ($request->get('additional_text')) ? $request->get('additional_text') : "";

                $question->save();
            }

            return Response::json(['message' => 'Translation Updated!'], 200);
        } catch(\Exception $e) {
            return Response::json(['errors' => $e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
