<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Question;
use App\Models\QuestionSet;
use App\Models\QuestionCategory;
use App\Models\QuestionTranslation;
use App\Models\QuestionSetTranslation;
use Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class QuestionSetsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sets = [];
        $question_sets = QuestionSet::all();

        foreach($question_sets as $question_set) {
            $questions = Question::where('question_set_id', $question_set->id)->get();

            $set = [];
            $set = $question_set;
            $set['question_count'] = $questions->count();
            $set_locales = [];
            foreach($questions as $question) {
                $translations = QuestionTranslation::where('question_id', $question->id)->get();

                foreach($translations as $translation) {
                    array_push($set_locales, $translation->locale);
                }
            }

            $set['locales'] = count(array_unique($set_locales));
            array_push($sets, $set);
        }

        return Response::json(['sets' => $sets], 200, array(), JSON_PRETTY_PRINT);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'key' => 'required|unique:question_sets',
            'name' => 'required',
            'type' => 'required',
            'welcome_text' => 'required',
            'intro_text' => 'required',
            'finished_text' => 'required'
        ]);

        if ($validator->fails()) {
            return Response::json(['errors' => $validator->errors()->first()], 202);
        }

        $sets = new QuestionSet;

        $sets->key = ($request->get('key')) ? $request->get('key') : "";
        $sets->name = ($request->get('name')) ? $request->get('name') : "";
        $sets->type = ($request->get('type')) ? $request->get('type') : "";
        $sets->status = QuestionSet::STATUS;
        $sets->welcome_text = ($request->get('welcome_text')) ? $request->get('welcome_text') : "";
        $sets->intro_text = ($request->get('intro_text')) ? $request->get('intro_text') : "";
        $sets->finished_text = ($request->get('finished_text')) ? $request->get('finished_text') : "";

        $sets->save();

        return Response::json(['message' => 'Question Set Saved!'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $questions = [];
        $categories = QuestionCategory::all();

        $set_questions = Question::where('question_set_id', $id)->orderBy('updated_at', 'desc')->get();
        foreach($set_questions as $question) {
            $set_question = $question;
            $translations = QuestionTranslation::where('question_id', $question->id)->get();
            $question_locales = [];

            foreach($translations as $translation) {
                array_push($question_locales, $translation->locale);
            }
            $set_question['locales_count'] = count(array_unique($question_locales));

            array_push($questions, $set_question);
        }

        return Response::json(['questions' => $questions, 'categories' => $categories], 200, array(), JSON_PRETTY_PRINT);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $set = QuestionSet::find($id);
            $set->key = ($request->get('key')) ? $request->get('key') : "";
            $set->name = ($request->get('name')) ? $request->get('name') : "";
            $set->type = ($request->get('type')) ? $request->get('type') : "";
            $set->welcome_text = ($request->get('welcome_text')) ? $request->get('welcome_text') : "";
            $set->intro_text = ($request->get('intro_text')) ? $request->get('intro_text') : "";
            $set->finished_text = ($request->get('finished_text')) ? $request->get('finished_text') : "";

            $set->save();

            return Response::json(['message' => 'Question Set Updated!'], 200);
        } catch(\Exception $e) {
            return Response::json(['errors' => $e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
