<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Models\User;
use App\Models\Answer;
use App\Models\Question;

class ResultsController extends Controller
{
    public function __construct()
    {
       // Apply the jwt.auth middleware to all methods in this controller
       // except for the authenticate method. We don't want to prevent
       // the user from retrieving their token if they don't already have it
       $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params = $request->all();
        if($params){
            $users = User::orderBy('created_at', 'desc')
                        ->where('created_at', '>=', $params["start"])
                        ->where('created_at', '<=', $params["end"])
                        ->where("username", "=", "")
                        ->get();
        }else{
            $users = User::where("username", "=", "")->orderBy('created_at', 'desc')->get();
        }
        return $users->toJson();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $user = User::find($id);
        $answers = Answer::with('question')->where('user_id', $id)->get();
        $user['answers'] = $answers;
        return $user->toJson();
    }

}
