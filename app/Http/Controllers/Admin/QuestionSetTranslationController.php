<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Question;
use App\Models\QuestionSetTranslation;
use Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class QuestionSetTranslationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'question_set_id' => 'required',
                'locale' => 'required',
                'welcome_text' => 'required',
                'intro_text' => 'required',
                'finished_text' => 'required'
            ]);

            if ($validator->fails()) {
                return Response::json(['errors' => $validator->errors()->first()], 202);
            }

            $translation = new QuestionSetTranslation;
            $translation->question_set_id = ($request->get('question_set_id')) ? $request->get('question_set_id') : "";
            $translation->locale = ($request->get('locale')) ? $request->get('locale') : "";
            $translation->welcome_text = ($request->get('welcome_text')) ? $request->get('welcome_text') : "";
            $translation->intro_text = ($request->get('intro_text')) ? $request->get('intro_text') : "";
            $translation->finished_text = ($request->get('finished_text')) ? $request->get('finished_text') : "";

            $translation->save();

            return Response::json(['message' => 'Question Option Translation Saved!'], 200);
        } catch(\Exception $e) {
            return Response::json(['errors' => $e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $translation = QuestionSetTranslation::where('question_set_id', $id)->get();

        $locales = config('translatable.locales');
        $available_locale = [];

        foreach ($translation as $trans) {
            if (($key = array_search($trans->locale, $locales)) !== false) {
                unset($locales[$key]);
            }
        }

        return Response::json(['translations' => $translation, 'locale' => $locales], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $translation = QuestionSetTranslation::find($id);
            $translation->locale = ($request->get('locale')) ? $request->get('locale') : "";
            $translation->welcome_text = ($request->get('welcome_text')) ? $request->get('welcome_text') : "";
            $translation->intro_text = ($request->get('intro_text')) ? $request->get('intro_text') : "";
            $translation->finished_text = ($request->get('finished_text')) ? $request->get('finished_text') : "";

            $translation->save();

            if ($translation->locale == "en") {
                $question = Question::find($request->get('question_set_id'));
                $question->welcome_text = $translation->welcome_text;
                $question->intro_text = $translation->intro_text;
                $question->finished_text = $translation->finished_text;

                $question->save();
            }

            return Response::json(['message' => 'Translation Updated!'], 200);
        } catch(\Exception $e) {
            return Response::json(['errors' => $e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
