<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\QuestionOption;
use App\Models\QuestionOptionTranslation;

class QuestionOptionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dprint($request->all(), true);
        $validator = Validator::make($request->all(), [
            'question_id' => 'required',
            'option' => 'required',
            'score' => 'required',
            'order' => 'required'
        ]);

        if ($validator->fails()) {
            return Response::json(['errors' => $validator->errors()->first()], 202);
        }

        try {

          $option = new QuestionOption;
          $option->question_id = ($request->get('question_id')) ? $request->get('question_id') : "";
          $option->text = ($request->get('option')) ? $request->get('option') : "";
          $option->score = ($request->get('score')) ? $request->get('score') : "";
          $option->order = ($request->get('order')) ? $request->get('order') : "";

          $option->save();

          $translation = new QuestionOptionTranslation();
          $translation->question_id = $question->id;
          $translation->text = $option->text;
          $translation->locale = 'en';
          $translation->save();
          // $this->saveDefaultTranslation($question->id, $text);

          return Response::json(['message' => 'Question Option Saved!'], 200);
        } catch (\Exception $e) {
          die($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $options = QuestionOption::where('question_id', $id)->get();

        return Response::json(['question_options' => $options], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $question = QuestionOption::find($id);
            $question->question_id = ($request->get('question_id')) ? $request->get('question_id') : "";
            $question->order = ($request->get('order')) ? $request->get('order') : "";
            $question->text = ($request->get('text')) ? $request->get('text') : "";
            $question->score = ($request->get('score')) ? $request->get('score') : "";

            $question->save();

            return Response::json(['message' => 'Question Updated!'], 200);
        } catch(\Exception $e) {
            return Response::json(['errors' => $e->getMessage()], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
