<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Answer;
use Illuminate\Http\Request;

class AnswersController extends Controller
{
    public function __construct()
    {
        // Apply the jwt.auth middleware to all methods in this controller
        // except for the authenticate method. We don't want to prevent
        // the user from retrieving their token if they don't already have it
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params = $request->all();
        if ($params) {
            $answers = Answer::where('created_at', '>=', $params["start"])
                ->where('created_at', '<=', $params["end"])
                ->get();
        } else {
            $answers = Answer::orderBy('created_at', 'desc')->get();
        }
        return $answers->toJson();
    }

}
