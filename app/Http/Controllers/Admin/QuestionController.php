<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Question;
use App\Models\QuestionCategory;
use App\Models\QuestionTranslation;
use Illuminate\Http\Request;
use Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Question::with('set', 'category')->orderBy('updated_at', 'desc')->get();
        // dprint($questions);

        return Response::json(['questions' => $questions], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    //   dprint($request->all(), true);
      $validator = Validator::make($request->all(), [
          'category_id' => 'required',
          'setId' => 'required',
          'key' => 'required|unique:questions',
          'text' => 'required',
          'additional_text' => 'required'
      ]);

      if ($validator->fails()) {
          return Response::json(['errors' => $validator->errors()->first()], 202);
      }

      try {
        $question = new Question;

        $question->category_id = ($request->get('category_id')) ? $request->get('category_id') : "";
        $question->question_set_id = ($request->get('setId')) ? $request->get('setId') : "";
        $question->key = ($request->get('key')) ? $request->get('key') : "";
        $question->text = ($request->get('text')) ? $request->get('text') : "";
        $question->additional_text = ($request->get('additional_text')) ? $request->get('additional_text') : "";
        // $question->translate('en')->text = ($request->get('text')) ? $request->get('text') : "";

        $question->save();

        return Response::json(['message' => 'Question Set Saved!'], 200);
      } catch (\Exception $e) {
        die($e->getMessage());
      }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $question = Question::find($id);
            $question->question_set_id = ($request->get('question_set_id')) ? $request->get('question_set_id') : "";
            $question->category_id = ($request->get('category_id')) ? $request->get('category_id') : "";
            $question->text = ($request->get('text')) ? $request->get('text') : "";

            $question->save();

            return Response::json(['message' => 'Question Updated!'], 200);
        } catch(\Exception $e) {
            return Response::json(['errors' => $e->getMessage()], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
