<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Question;
use DB;
use Illuminate\Http\Request;

class QuestionsController extends Controller
{
    public function __construct()
    {
        // Apply the jwt.auth middleware to all methods in this controller
        // except for the authenticate method. We don't want to prevent
        // the user from retrieving their token if they don't already have it
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params = $request->all();
        if ($params) {

            $questions = Question::select(DB::raw('*, (SELECT COUNT(*) FROM answers WHERE answers.question_id = questions.id AND answers.answer=1 AND answers.created_at >= "' . $params['start'] . '" AND answers.created_at <= "' . $params['end'] . '") AS yes
              , (SELECT COUNT(*) FROM answers WHERE answers.question_id = questions.id AND answers.answer=0 AND answers.created_at >= "' . $params['start'] . '" AND answers.created_at <= "' . $params['end'] . '") AS no'))
                ->orderBy('id')
                ->get();
        } else {
            $questions = Question::select(DB::raw('*, (SELECT COUNT(*) FROM answers WHERE answers.question_id = questions.id AND answers.answer=1) AS yes
              , (SELECT COUNT(*) FROM answers WHERE answers.question_id = questions.id AND answers.answer=0) AS no'))
                ->orderBy('id')
                ->get();
        }

        return $questions->toJson();
    }

}
