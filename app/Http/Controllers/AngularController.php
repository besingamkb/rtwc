<?php

namespace App\Http\Controllers;

class AngularController extends Controller
{
    public function serveApp()
    {
        return view('angular');
    }

    public function unsupported()
    {
        return view('unsupported_browser');
    }
}
