<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Controllers\APIController;
use App\Http\Requests\ReportRequest;
use App\Models\QuestionSet;
use App\Models\Report;
use App\Models\Reporting;

class ReportingController extends APIController
{
    /**
     * Returns all demographic filter options.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFilterOptions()
    {
        $options = new Reporting();
        $options = $options->filterOptions();

        return response()->json([
            'data' => $options
        ]);
    }

    /**
     * Get the basic summary info.
     *
     * @param ReportRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSummary(ReportRequest $request)
    {
        $results = new Reporting();
        $results = $results->buildGenericQuery($request->input('assessments'));

        if ($filters = $request->input('filters')) {
            $results = $results->applyFiltersToQuery($filters);
        }

        if (!$results->countCheck()) {
            return response()->json([
                'errors' => [
                    config('reporting.resultSetMessage')
                ]
            ], 403);
        }

        $results = $results->buildSummaryQuery()->get();

        $results = Reporting::formatAssociativeByKey($results);

        return response()->json([
            'data' => $results
        ]);
    }

    /**
     * Get the basic scores info from results for these assessments.
     *
     * @param ReportRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getScoreTotals(ReportRequest $request)
    {
        $results = new Reporting();
        $results = $results->buildGenericQuery($request->input('assessments'));

        if ($filters = $request->input('filters')) {
            $results = $results->applyFiltersToQuery($filters);
        }

        if (!$results->countCheck()) {
            return response()->json([
                'errors' => [
                    config('reporting.resultSetMessage')
                ]
            ], 403);
        }

        $results = $results->buildScoreTotalsQuery()->get();

        $results = Reporting::castTotals($results, 'max_score');
        $results = Reporting::castTotals($results, 'total_score');

        return response()->json([
            'data' => $results
        ]);
    }

    /**
     * Get the alerts total from results for these assessments.
     *
     * @param ReportRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAlertTotals(ReportRequest $request)
    {
        $results = new Reporting();
        $results = $results->buildGenericQuery($request->input('assessments'));

        if ($filters = $request->input('filters')) {
            $results = $results->applyFiltersToQuery($filters);
        }

        if (!$results->countCheck()) {
            return response()->json([
                'errors' => [
                    config('reporting.resultSetMessage')
                ]
            ], 403);
        }

        $results = $results->buildCategoryAlertsQuery()->get();

        $results = Reporting::appendParentCategoryTotals($results);
        $results = Reporting::castTotals($results, 'total');

        // Sum up all alerts.
        $totalAlerts = 0;

        foreach ($results as $result) {
            // Only add up parents to save PHP memory time - Super fast mode.
            if ($result->parent_id != 0) {
                continue;
            }

            $totalAlerts += $result->total;
        }

        return response()->json([
            'data' => [
                ['total_alerts' => $totalAlerts]
            ]
        ]);
    }

    /**
     * Get the demographics info.
     *
     * @param ReportRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDemographics(ReportRequest $request)
    {
        $results = new Reporting();
        $results = $results->buildGenericQuery($request->input('assessments'));

        if ($filters = $request->input('filters')) {
            $results = $results->applyFiltersToQuery($filters);
        }

        if (!$results->countCheck()) {
            return response()->json([
                'errors' => [
                    config('reporting.resultSetMessage')
                ]
            ], 403);
        }

        $results = $results->buildSummaryQuery()->get();

        $results = Reporting::formatAssociativeByKey($results);

        return response()->json([
            'data' => $results
        ]);
    }

    /**
     * Get the category sums.
     *
     * @param ReportRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCategories(ReportRequest $request)
    {
        $results = new Reporting();
        $results = $results->buildGenericQuery($request->input('assessments'));

        if ($filters = $request->input('filters')) {
            $results = $results->applyFiltersToQuery($filters);
        }

        if (!$results->countCheck()) {
            return response()->json([
                'errors' => [
                    config('reporting.resultSetMessage')
                ]
            ], 403);
        }

        $results = $results->buildCategoriesQuery()->get();

        $results = Reporting::appendParentCategoryTotals($results, true);
        $results = Reporting::castTotals($results);

        return response()->json([
            'data' => $results
        ]);
    }

    /**
     * Get the category sums.
     *
     * @param ReportRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDetail(ReportRequest $request)
    {
        $results = new Reporting();
        $results = $results->buildGenericQuery($request->input('assessments'));

        if ($filters = $request->input('filters')) {
            $results = $results->applyFiltersToQuery($filters);
        }

        if (!$results->countCheck()) {
            return response()->json([
                'errors' => [
                    config('reporting.resultSetMessage')
                ]
            ], 403);
        }

        $results = $results->buildDetailQuery()->get();

        $results = Reporting::castTotals($results);

        return response()->json([
            'data' => $results
        ]);
    }

    /**
     * Get the report metrics for employer assessments.
     *
     * @param ReportRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMetrics(ReportRequest $request)
    {
        $results = new Reporting();
        $results = $results->buildGenericEmployerQuery($request->input('assessments'));

        $results = $results->buildMetricsQuery()->get();

        $results = Reporting::castTotals($results);
        $results = Reporting::formatAssociativeByKey($results);

        return response()->json([
            'data' => $results
        ]);
    }

    /**
     * Get the benchmark scores for the selected assessments
     *
     * @param ReportRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBenchmark(ReportRequest $request)
    {
        $results = new Reporting();
        $results = $results->buildGenericQuery($request->input('assessments'));

        if ($filters = $request->input('filters')) {
            $results = $results->applyFiltersToQuery($filters);
        }

        if (!$results->countCheck()) {
            return response()->json([
                'errors' => [
                    config('reporting.resultSetMessage')
                ]
            ], 403);
        }

        $results = $results->buildBenchmarkQuery(true)->get();
        $results = Reporting::castTotals($results, "total_score");
        $results = Reporting::castTotals($results, "total_alerts");

        return response()->json([
            'data' => $results
        ]);
    }


    /**
     * Get all other benchmark scores
     *
     * @param ReportRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBenchmarkAll(ReportRequest $request)
    {
        $results = new Reporting();
        $results = $results->buildBenchmarkGenericQuery($request->input('assessments'));

        if ($filters = $request->input('filters')) {
            $results = $results->applyFiltersToQuery($filters);
        }

        if (!$results->countCheck()) {
            return response()->json([
                'errors' => [
                    config('reporting.resultSetMessage')
                ]
            ], 403);
        }

        $results = $results->buildBenchmarkQuery()->get();
        $results = Reporting::castTotals($results, "total_score");
        $results = Reporting::castTotals($results, "total_alerts");

        return response()->json([
            'data' => $results
        ]);
    }

    /**
     * Get the historical categories comparative figures on a per assessment basis.
     * This includes the overall score percentage.
     *
     * @param ReportRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCategoriesComparatives(ReportRequest $request)
    {
        $results = new Reporting();
        $results = $results->buildGenericQueryPerAssessment($request->input('assessments'));

        if ($filters = $request->input('filters')) {
            $results = $results->applyFiltersToQuery($filters);
        }

        if (!$results->countCheck()) {
            return response()->json([
                'errors' => [
                    config('reporting.resultSetMessage')
                ]
            ], 403);
        }

        $results = $results->buildCategoriesQueryPerAssessment()->get();

        $results = Reporting::sortResultsIntoAssessments($results);

        $results = array_map(function ($resultSet) {
            $resultSet = Reporting::formatAssociativeByKey($resultSet);
            $resultSet = Reporting::castTotals($resultSet);

            return $resultSet;
        }, $results);

        return response()->json([
            'data' => $results
        ]);
    }

    /**
     * Get the historical metric comparative figures on a per assessment basis.
     *
     * @param ReportRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMetricsComparatives(ReportRequest $request)
    {
        $results = new Reporting();
        $results = $results->buildGenericEmployerQueryPerAssessment($request->input('assessments'));

        if ($filters = $request->input('filters')) {
            $results = $results->applyFiltersToQuery($filters);
        }

        if (!$results->countCheck()) {
            return response()->json([
                'errors' => [
                    config('reporting.resultSetMessage')
                ]
            ], 403);
        }

        $results = $results->buildMetricsQueryPerAssessment()->get();

        $results = Reporting::sortResultsIntoAssessments($results);

        $results = array_map(function ($resultSet) {
            $resultSet = Reporting::formatAssociativeByKey($resultSet);
            $resultSet = Reporting::castTotals($resultSet);

            return $resultSet;
        }, $results);

        return response()->json([
            'data' => $results
        ]);
    }

    /**
     * Display the specified PDF in the browser.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getPdf(Request $request)
    {
        $report = new Report;
        $report->getResultsReport($request, $this->authUser->id);
    }
}
