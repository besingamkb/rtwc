<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\APIController;
use App\Http\Requests\Connection\SubmitRequest;
use App\Models\Account;
use App\Models\Connection;

class ConnectionController extends APIController
{
    /**
     * Return all requested and accepted connections for the authorised account.
     *
     * Was not sure whether to put these functions in the model or controller.
     * Could determine sent or received in the connection model but you would have
     * to perform queries on other tables which can be
     * quite slow if being run every model fetch.
     */
    public function index()
    {
        // Connection requests that have been SENT to another account and ACCEPTED.
        $acceptedSentConnections = $this->authAccount
            ->sentConnections()
            ->acceptedConnections()
            ->with([
                'requestee'
            ])
            ->get()
            ->toArray();

        foreach ($acceptedSentConnections as $index => $connection) {
            $acceptedSentConnections[$index]['direction'] = 'sent';
        }

        // Connection requests that have been SENT to another account and REJECTED.
        $requestedSentConnections = $this->authAccount
            ->sentConnections()
            ->requestedConnections()
            ->with([
                'requestee'
            ])
            ->get()
            ->toArray();

        foreach ($requestedSentConnections as $index => $connection) {
            $requestedSentConnections[$index]['direction'] = 'sent';
        }

        // Connection requests that have been RECEIVED by this account but not ACCEPTED or REJECTED.
        $receivedConnectionRequests = $this->authAccount
            ->receivedConnections()
            ->requestedConnections()
            ->with([
                'requester'
            ])
            ->get()
            ->toArray();

        foreach ($receivedConnectionRequests as $index => $connection) {
            $receivedConnectionRequests[$index]['direction'] = 'received';
        }

        // Connection requests that have been RECEIVED by this account and have been ACCEPTED.
        $acceptedReceivedConnections = $this->authAccount
            ->receivedConnections()
            ->acceptedConnections()
            ->with([
                'requester'
            ])
            ->get()
            ->toArray();

        foreach ($acceptedReceivedConnections as $index => $connection) {
            $acceptedReceivedConnections[$index]['direction'] = 'received';
        }

        return response()->json([
            'data' => [
                'sent' => array_merge(
                    $acceptedSentConnections,
                    $requestedSentConnections
                ),
                'received' => array_merge(
                    $receivedConnectionRequests,
                    $acceptedReceivedConnections
                )
            ]
        ]);
    }

    /**
     * Attempt to save a new connection request.
     *
     * @param SubmitRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function store(SubmitRequest $request)
    {
        $requesteeAccount = new Account();
        $requesteeAccount = $requesteeAccount->where('key', $request->input('account_key'))->first();

        if (!$requesteeAccount) {
            return response([
                'errors' => [
                    'An account with that key could not be found'
                ]
            ], 404);
        }

        $connection = new Connection();
        $connection = $connection->where([
            'requester_id' => $this->authAccount->id,
            'requestee_id' => $requesteeAccount->id
        ])->get();

        if ($connection->count()) {
            return response([
                'errors' => [
                    'A connection request has already been requested or accepted between these accounts'
                ]
            ], 403);
        }

        $connection = new Connection();
        $connection = $connection->forceCreate([
            'requester_id' => $this->authAccount->id,
            'requestee_id' => $requesteeAccount->id
        ]);

        return response([
            'data' => $connection
        ]);
    }

    /**
     * Accepts a connection request.
     *
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function acceptRequest($id)
    {
        $connection = new Connection();
        $connection = $connection->find($id);

        $connection = $connection->acceptRequest();

        return response([
            'data' => $connection
        ]);
    }

    /**
     * Rejects a connection request.
     *
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function rejectRequest($id)
    {
        $connection = new Connection();
        $connection = $connection->find($id);

        $connection = $connection->rejectRequest();

        return response([
            'data' => $connection
        ]);
    }

    /**
     * Deletes an existing accepted connection.
     *
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function destroy($id)
    {
        $connection = new Connection();
        $connection = $connection->find($id);

        $connection = $connection->delete();

        return response([
            'data' => $connection
        ]);
    }
}
