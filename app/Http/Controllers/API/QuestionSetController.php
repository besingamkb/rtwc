<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\APIController;
use App\Models\QuestionSet;

class QuestionSetController extends APIController
{
    /**
     * Return all question sets.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $question = new QuestionSet();
        $question = $question->all();

        if (!$question->count()) {
            return response()->json([
                'errors' => [
                    'No question sets found'
                ]
            ], 404);
        }

        return response()->json([
            'data' => $question
        ]);
    }

    /**
     * Return a question set by ID.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $question = new QuestionSet();
        $question = $question->findOrFail($id);

        if (!$question->count()) {
            return response()->json([
                'errors' => [
                    'No question sets found'
                ]
            ], 404);
        }

        return response()->json([
            'data' => $question
        ]);
    }
}
