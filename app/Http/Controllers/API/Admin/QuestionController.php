<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\APIController;
use App\Http\Requests\Admin\Question\CreateRequest;
use App\Http\Requests\Admin\Question\UpdateRequest;
use App\Models\Question;

class QuestionController extends APIController
{
    /**
     * Returns an index all questions.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $questions = new Question();
        $questions = $questions->with([
            'questionSet',
            'category'
        ])
            ->select([
                'questions.*',
            ])
            ->join('question_sets', 'question_sets.id', '=', 'questions.question_set_id')
            ->where('question_sets.is_editable', true)
            ->get();

        if (!count($questions)) {
            return response()->json([
                'errors' => [
                    'No questions could be found'
                ]
            ], 404);
        }

        return response()->json([
            'data' => $questions
        ], 200);
    }

    /**
     * Attempts to create a new question.
     *
     * @param CreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateRequest $request)
    {
        $question = new Question();
        $questionCreate = $question->create($request->all());

        if (!$questionCreate) {
            return response()->json([
                'errors' => [
                    'Question could not be created'
                ]
            ], 500);
        }

        return response()->json([
            'data' => $question
        ], 200);
    }

    /**
     * Returns a single question.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $question = new Question();
        $question = $question->findOrFail($id);

        $question->load([
            'questionSet',
            'category'
        ]);

        return response()->json([
            'data' => $question
        ], 200);
    }

    /**
     * Attempts to update an existing question.
     *
     * @param $id
     * @param UpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, UpdateRequest $request)
    {
        $question = new Question();
        $question = $question->findOrFail($id);
        $questionUpdate = $question->update($request->all());

        if (!$questionUpdate) {
            return response()->json([
                'errors' => [
                    'Question could not be updated'
                ]
            ], 500);
        }

        return response()->json([
            'data' => $question
        ], 200);
    }

    /**
     * Attempts to delete an existing question.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $question = new Question();
        $question = $question->findOrFail($id);
        $question = $question->delete();

        if (!$question) {
            return response()->json([
                'errors' => [
                    'Question could not be deleted'
                ]
            ], 500);
        }

        return response()->json([
            'data' => []
        ], 200);
    }
}
