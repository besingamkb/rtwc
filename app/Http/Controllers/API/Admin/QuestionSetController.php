<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\APIController;
use App\Http\Requests\Admin\QuestionSet\CreateRequest;
use App\Http\Requests\Admin\QuestionSet\UpdateRequest;
use App\Models\QuestionSet;

class QuestionSetController extends APIController
{
    /**
     * Returns an index all questionSets.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $questionSets = new QuestionSet();
        $questionSets = $questionSets->editable()->get();

        if (!count($questionSets)) {
            return response()->json([
                'errors' => [
                    'No question sets could be found'
                ]
            ], 404);
        }

        return response()->json([
            'data' => $questionSets
        ], 200);
    }

    /**
     * Attempts to create a new question set.
     *
     * @param CreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateRequest $request)
    {
        $questionSet = new QuestionSet();
        $questionSetCreate = $questionSet->create($request->all());

        if (!$questionSetCreate) {
            return response()->json([
                'errors' => [
                    'Question set could not be created'
                ]
            ], 500);
        }

        return response()->json([
            'data' => $questionSet
        ], 200);
    }

    /**
     * Returns a single question set.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $questionSet = new QuestionSet();
        $questionSet = $questionSet->findOrFail($id);

        return response()->json([
            'data' => $questionSet
        ], 200);
    }

    /**
     * Attempts to update an existing question set.
     *
     * @param $id
     * @param UpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, UpdateRequest $request)
    {
        $questionSet = new QuestionSet();
        $questionSet = $questionSet->findOrFail($id);
        $questionSetUpdate = $questionSet->update($request->all());

        if (!$questionSetUpdate) {
            return response()->json([
                'errors' => [
                    'Question set could not be updated'
                ]
            ], 500);
        }

        return response()->json([
            'data' => $questionSet
        ], 200);
    }

    /**
     * Attempts to delete an existing question set.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $questionSet = new QuestionSet();
        $questionSet = $questionSet->findOrFail($id);
        $questionSet = $questionSet->delete();

        if (!$questionSet) {
            return response()->json([
                'errors' => [
                    'Question set could not be deleted'
                ]
            ], 500);
        }

        return response()->json([
            'data' => []
        ], 200);
    }
}
