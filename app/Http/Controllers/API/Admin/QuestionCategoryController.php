<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\APIController;
use App\Http\Requests\Admin\QuestionCategory\CreateRequest;
use App\Http\Requests\Admin\QuestionCategory\UpdateRequest;
use App\Models\QuestionCategory;

class QuestionCategoryController extends APIController
{
    /**
     * Returns an index all question categories.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $questionCategories = new QuestionCategory();
        $questionCategories = $questionCategories->all();

        if (!count($questionCategories)) {
            return response()->json([
                'errors' => [
                    'No question categories could be found'
                ]
            ], 404);
        }

        return response()->json([
            'data' => $questionCategories
        ], 200);
    }

    /**
     * Attempts to create a new question.
     *
     * @param CreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateRequest $request)
    {
        $question = new QuestionCategory();
        $questionCreate = $question->create($request->all());

        if (!$questionCreate) {
            return response()->json([
                'errors' => [
                    'QuestionCategory could not be created'
                ]
            ], 500);
        }

        return response()->json([
            'data' => $question
        ], 200);
    }

    /**
     * Returns a single question.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $question = new QuestionCategory();
        $question = $question->findOrFail($id);

        return response()->json([
            'data' => $question
        ], 200);
    }

    /**
     * Attempts to update an existing question.
     *
     * @param $id
     * @param UpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, UpdateRequest $request)
    {
        $question = new QuestionCategory();
        $question = $question->findOrFail($id);
        $questionUpdate = $question->update($request->all());

        if (!$questionUpdate) {
            return response()->json([
                'errors' => [
                    'QuestionCategory could not be updated'
                ]
            ], 500);
        }

        return response()->json([
            'data' => $question
        ], 200);
    }

    /**
     * Attempts to delete an existing question.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $question = new QuestionCategory();
        $question = $question->findOrFail($id);
        $question = $question->delete();

        if (!$question) {
            return response()->json([
                'errors' => [
                    'QuestionCategory could not be deleted'
                ]
            ], 500);
        }

        return response()->json([
            'data' => []
        ], 200);
    }
}
