<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\APIController;
use App\Http\Requests\Admin\Assessment\CreateRequest;
use App\Http\Requests\Admin\Assessment\UpdateRequest;
use App\Models\Assessment;

class AssessmentController extends APIController
{
    /**
     * Returns an index all assessments.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $assessments = new Assessment();
        $assessments = $assessments->with([
            'account'
        ])->get();

        if (!count($assessments)) {
            return response()->json([
                'errors' => [
                    'No assessments could be found'
                ]
            ], 404);
        }

        return response()->json([
            'data' => $assessments
        ], 200);
    }

    /**
     * Attempts to create a new assessment.
     *
     * @param CreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateRequest $request)
    {
        $assessment = new Assessment();
        $assessmentCreate = $assessment->create($request->all());

        if (!$assessmentCreate) {
            return response()->json([
                'errors' => [
                    'Assessment could not be created'
                ]
            ], 500);
        }

        return response()->json([
            'data' => $assessment
        ], 200);
    }

    /**
     * Returns a single assessment.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $assessment = new Assessment();
        $assessment = $assessment->findOrFail($id);

        $assessment->load([
            'account'
        ]);

        return response()->json([
            'data' => $assessment
        ], 200);
    }

    /**
     * Attempts to update an existing assessment.
     *
     * @param $id
     * @param UpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, UpdateRequest $request)
    {
        $assessment = new Assessment();
        $assessment = $assessment->findOrFail($id);
        $assessmentUpdate = $assessment->update($request->all());

        if (!$assessmentUpdate) {
            return response()->json([
                'errors' => [
                    'Assessment could not be updated'
                ]
            ], 500);
        }

        return response()->json([
            'data' => $assessment
        ], 200);
    }

    /**
     * Attempts to delete an existing assessment.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $assessment = new Assessment();
        $assessment = $assessment->findOrFail($id);
        $assessment = $assessment->delete();

        if (!$assessment) {
            return response()->json([
                'errors' => [
                    'Assessment could not be deleted'
                ]
            ], 500);
        }

        return response()->json([
            'data' => []
        ], 200);
    }
}
