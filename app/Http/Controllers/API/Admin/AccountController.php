<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\APIController;
use App\Http\Requests\Admin\Account\CreateRequest;
use App\Http\Requests\Admin\Account\UpdateRequest;
use App\Models\Account;

class AccountController extends APIController
{
    /**
     * Returns an index all accounts.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $accounts = new Account();
        $accounts = $accounts->all();

        if (!count($accounts)) {
            return response()->json([
                'errors' => [
                    'No accounts could be found'
                ]
            ], 404);
        }

        return response()->json([
            'data' => $accounts
        ], 200);
    }

    /**
     * Attempts to create a new account.
     *
     * @param CreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateRequest $request)
    {
        $account = new Account();
        $account = $account->create($request->all());

        if (!$account) {
            return response()->json([
                'errors' => [
                    'Account could not be created'
                ]
            ], 500);
        }

        return response()->json([
            'data' => $account
        ], 200);
    }

    /**
     * Returns a single account.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $account = new Account();
        $account = $account->findOrFail($id);

        return response()->json([
            'data' => $account
        ], 200);
    }

    /**
     * Attempts to update an existing account.
     *
     * @param $id
     * @param UpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, UpdateRequest $request)
    {
        $account = new Account();
        $account = $account->findOrFail($id);
        $accountUpdate = $account->update($request->all());

        if (!$accountUpdate) {
            return response()->json([
                'errors' => [
                    'Account could not be updated'
                ]
            ], 500);
        }

        return response()->json([
            'data' => $account
        ], 200);
    }

    /**
     * Attempts to delete an existing account.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $account = new Account();
        $account = $account->findOrFail($id);
        $account = $account->delete();

        if (!$account) {
            return response()->json([
                'errors' => [
                    'Account could not be deleted'
                ]
            ], 500);
        }

        return response()->json([
            'data' => []
        ], 200);
    }
}
