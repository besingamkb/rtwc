<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\APIController;
use App\Http\Requests\Admin\User\CreateRequest;
use App\Http\Requests\Admin\User\UpdateRequest;
use App\Models\User;

class UserController extends APIController
{
    /**
     * Returns an index all users.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $users = new User();
        $users = $users->with([
            'account'
        ])->get();

        if (!count($users)) {
            return response()->json([
                'errors' => [
                    'No users could be found'
                ]
            ], 404);
        }

        return response()->json([
            'data' => $users
        ], 200);
    }

    /**
     * Attempts to create a new user.
     *
     * @param CreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateRequest $request)
    {
        $user = new User();
        $user = $user->create($request->all());

        if (!$user) {
            return response()->json([
                'errors' => [
                    'User could not be created'
                ]
            ], 500);
        }

        return response()->json([
            'data' => $user
        ], 200);
    }

    /**
     * Returns a single user.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $user = new User();
        $user = $user->findOrFail($id);

        $user->load([
            'account'
        ]);

        return response()->json([
            'data' => $user
        ], 200);
    }

    /**
     * Attempts to update an existing user.
     *
     * @param $id
     * @param UpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, UpdateRequest $request)
    {
        $user = new User();
        $user = $user->findOrFail($id);
        $userUpdate = $user->update($request->all());

        if (!$userUpdate) {
            return response()->json([
                'errors' => [
                    'User could not be updated'
                ]
            ], 500);
        }

        return response()->json([
            'data' => $user
        ], 200);
    }

    /**
     * Attempts to delete an existing user.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $user = new User();
        $user = $user->findOrFail($id);
        $user = $user->delete();

        if (!$user) {
            return response()->json([
                'errors' => [
                    'User could not be deleted'
                ]
            ], 500);
        }

        return response()->json([
            'data' => []
        ], 200);
    }
}
