<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\APIController;
use App\Models\Account;

class AccountController extends APIController
{
    /**
     * Returns all accounts connected to the authorised
     * account including the authorised account.
     *
     * @param string $direction
     * @return \Illuminate\Http\JsonResponse
     */
    public function getConnectedAccounts($direction = 'both')
    {
        $connectedAccounts = [];

        switch ($direction) {
            case 'both':
                $connectedAccounts = $this->authAccount->connectedAccounts;
                break;
            case 'inbound':
                $connectedAccounts = $this->authAccount->connectedInboundAccounts;
                break;
            case 'outbound':
                $connectedAccounts = $this->authAccount->connectedOutboundAccounts;
                break;
        }

        $connectedAccounts = collect($connectedAccounts);
        $connectedAccounts->push($this->authAccount);

        if (!$connectedAccounts) {
            return response()->json([
                'errors' => [
                    'No connected accounts found'
                ]
            ]);
        }

        return response()->json([
            'data' => $connectedAccounts
        ]);
    }
}
