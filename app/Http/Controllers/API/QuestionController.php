<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\APIController;
use App\Models\QuestionSet;

class QuestionController extends APIController
{
    /**
     * Return all questions for a given question set ID.
     *
     * @param $questionSetID
     * @return \Illuminate\Http\JsonResponse
     */
    public function indexByQuestionSet($questionSetID)
    {
        $questionSet = new QuestionSet();
        $questionSet = $questionSet->findOrFail($questionSetID);

        $question = $questionSet->questions;

        if (!$question->count()) {
            return response()->json([
                'errors' => [
                    'No questions found'
                ]
            ], 404);
        }

        return response()->json([
            'data' => $question
        ]);
    }
}
