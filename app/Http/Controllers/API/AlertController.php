<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\APIController;

class AlertController extends APIController
{
    /**
     * Return all alerts relevant to logged in user.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $alerts = $this->authAccount
            ->alerts()
            ->with('assessment')
            ->orderBy('created_at', 'desc')
            ->paginate(5);

        return response()->json([
            'data' => $alerts
        ]);
    }
}
