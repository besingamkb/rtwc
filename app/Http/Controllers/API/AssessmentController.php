<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\APIController;
use App\Http\Requests\Assessment\Employer\SubmitRequest;
use App\Http\Requests\Assessment\SearchRequest;
use App\Models\Assessment;
use App\Models\Export\AssessmentExport;

class AssessmentController extends APIController
{
    /**
     * Return all assessments.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        // Get this authorised accounts assessments.
        $assessments = $this->authAccount
            ->assessments()
            ->with([
                'account'
            ])
            ->get()
            ->toArray();

        return response()->json([
            'data' => $assessments
        ]);
    }

    /**
     * Return an assessment by Key.
     *
     * @param $key
     * @return \Illuminate\Http\JsonResponse
     */
    public function getByKey($key)
    {
        $assessment = new Assessment();
        $assessment = $assessment->where('key', $key)->first();

        if (!$assessment) {
            return response()->json([
                'errors' => [
                    'Assessment not found'
                ]
            ], 404);
        }

        return $this->show($assessment->id);
    }

    /**
     * Return an assessment by ID.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        \DB::enableQueryLog();
        $assessments = $this->authAccount
            ->assessments()
            ->with('account')
            ->get();

        $assessments = $assessments->whereLoose('id', $id)->first();
        return response()->json([
            'data' => $assessments
        ]);
    }

    /**
     * Search, filter and sort assessments.
     *
     * @param SearchRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(SearchRequest $request, $reporting = false)
    {
        $connectedAssessmentIDs = $this->authAccount->assessments->pluck('id');

        $assessments = new Assessment();

        $query = $assessments->whereIn('id', $connectedAssessmentIDs);

        // include account as we show account name
        $query = $query->with('account');

        // if reporting only include completed assessments
        if ($reporting) {
            $query = $query->where('assessments.status', 10);
        }

        if ($request->filters) {
            $query = $assessments->createFilterQuery($request->filters, $query);
        }

        if ($request->sort) {
            $query = $assessments->createSortQuery($request->sort, $query);
        }

        if ($request->search != "") {
            $query = $assessments->createSearchQuery($request->search, $query);
        }

        $query = $query->paginate();

        return response()->json($query);
    }

    /**
     * Search, filter and sort assessments.
     *
     * @param SearchRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchReporting(SearchRequest $request)
    {
        return $this->search($request, true);
    }

    /**
     * Return assessment parts that belong to the assessment.
     * Should return a worker part and employer part.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAssessmentParts($id)
    {
        $assessment = new Assessment();
        $assessment = $assessment->findOrFail($id);

        $workerPart = $assessment->getWorkerPart();
        $employerPart = $assessment->getEmployerPart();

        return response()->json([
            'data' => [
                'worker' => $workerPart,
                'employer' => $employerPart
            ]
        ]);
    }

    /**
     * Return an assessment with all questions and their categories correctly ordered and ready to be taken.
     *
     * Normally called when an employer needs to take the assessment.
     *
     * @param $id
     * @param $part
     * @return \Illuminate\Http\JsonResponse
     */
    public function showInSubmissionFormat($id, $part)
    {
        $assessment = new Assessment();
        $assessment = $assessment->findOrFail($id);

        switch ($part) {
            case 'employer':
                $assessment = $assessment->formatEmployerPartForSubmission();
                break;
            case 'worker':
                return response()->json([
                    'errors' => [
                        'This assessment cannot be taken in this way'
                    ]
                ], 500);
            default:
                return response()->json([
                    'errors' => [
                        'No assessment could be found with this ID'
                    ]
                ], 404);
        }

        return response()->json([
            'data' => $assessment
        ]);
    }

    /**
     * Submits a test against a given assessment ID.
     *
     * @param $id
     * @param SubmitRequest $submitRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function submitAssessment($id, SubmitRequest $submitRequest)
    {
        $assessment = new Assessment();
        $assessment = $assessment->findOrFail($id);

        $submissionResult = $assessment->submitAssessment($submitRequest->all());

        if (!$submissionResult) {
            return response()->json([
                'errors' => [
                    'Assessment could not be submitted'
                ]
            ], 500);
        }

        // Load in the results of the submission.
        $assessment->load([
            'results',
            'results.resultsEmployer'
        ]);

        return response()->json([
            'data' => $assessment
        ]);
    }

    /**
     * Resubmits a test against a given assessment ID.
     *
     * @param $id
     * @param SubmitRequest $submitRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function resubmitAssessment($id, SubmitRequest $submitRequest)
    {
        $assessment = new Assessment();
        $assessment = $assessment->findOrFail($id);

        $submissionResult = $assessment->resubmitAssessment($submitRequest->all());

        if (!$submissionResult) {
            return response()->json([
                'errors' => [
                    'Assessment could not be submitted'
                ]
            ], 500);
        }

        // Load in the results of the submission.
        $assessment->load([
            'results',
            'results.resultsEmployer'
        ]);

        return response()->json([
            'data' => $assessment
        ]);
    }

    /**
     * Attempts to export an assessment from the provided ID and export type.
     *
     * @param $id
     * @param $type
     * @return \Illuminate\Http\JsonResponse
     */
    public function export($id, $type)
    {
        $assessment = new Assessment();
        $assessment = $assessment->findOrFail($id);

        $export = null;
        $type = str_replace('-', '_', $type);

        switch ($type) {
            case 'additional_text':
                $export = new AssessmentExport();
                $export = $export->setAssessments($assessment->id)
                    ->setType($type)
                    ->build();
                break;
            default:
                // If no type is listed above then return 404.
                return response()->json([
                    'errors' => [
                        'No export of ' . str_replace('-', ' ', $type) . ' type found'
                    ]
                ], 404);
        }

        // If the export could not be built.
        if (!$export) {
            return response()->json([
                'errors' => [
                    'Export failed to build correctly'
                ]
            ], 500);
        }

        // Get the export as an output steam.
        $export = $export->get();

        // Return the file.
        return response()->json([
            'data' => $export
        ]);
    }
}
