<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Question;

class QuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->session()->has('id')) {
            return redirect('/');
        }

        $questions = Question::orderBy('id', 'asc')
            ->get()->shuffle();

        $questionsJson = array();

        foreach($questions as $question)
        {
            $questionsJson[$question->id] = 0;
        }

        $parameters = array(
            'questions' => $questions,
            'questionsJson' => json_encode($questionsJson),
        );

        return view('questions', $parameters);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request)
    {

        $questions = Question::orderBy('id', 'asc')->get();

        return $questions;
    }

    /**
     * Get totals for cumulative section points
     *
     * @param  string  $token
     * @return \Illuminate\Http\Response
     */
    public function totals()
    {
        $questions = Question::orderBy('id', 'asc')->get();
        $response = new \Response;

        $questionTotals = array(
            "score" => 0
        );

        foreach($questions as $question){
            $questionTotals[$question->section] = $questionTotals[$question->section] + 1;
            $questionTotals["score"] = $questionTotals["score"] + 1;
        }

        $response->data = $questionTotals;

        return $response->data = $questionTotals;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function start()
    {
        //
        return view('start');
    }
}
