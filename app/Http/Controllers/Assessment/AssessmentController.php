<?php

namespace App\Http\Controllers\Assessment;

use App\Models\Result;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Assessment;
use App\Models\QuestionSet;
use App;
use App\Models\User;
use App\Models\Report;
use App\Http\Requests\DemographicRequest;

class AssessmentController extends Controller
{
    /**
     * Displays the locales for the current assessment
     *
     * @param Request $request
     * @param Assessment $assessment
     * @return \Illuminate\Http\Response
     */
    public function showLocales(Request $request, Assessment $assessment)
    {
        $request->session()->flush();

        // this page always should be english
        App::setLocale('en');

        $parameters = array(
            'assessment' => $assessment,
        );

        return view('assessments/rtw/worker/locales', $parameters);
    }

    /**
     * Displays the start questions for the current assessment
     *
     * @param Request $request
     * @param Assessment $assessment
     * @param $locale
     * @return \Illuminate\Http\Response
     */
    public function showDemographics(Request $request, Assessment $assessment, $locale)
    {
        // set the locale to the chosen one
        App::setLocale($locale);
        $request->session()->put('locale', $locale);

        // get the demographics question set
        $demographics = QuestionSet::key(config('assessment.demographic_question_set_key'))->with('questions')->firstOrFail();

        $parameters = array(
            'assessment' => $assessment,
            'questionSet' => $demographics
        );

        return view('assessments/rtw/worker/demographics', $parameters);
    }


    /**
     * Stores the demograhpic questions for the current assessment
     * and redirects to question
     *
     * @param DemographicRequest $request
     * @param Assessment $assessment
     * @param $locale
     * @return \Illuminate\Http\Response
     */
    public function storeDemographics(DemographicRequest $request, Assessment $assessment, $locale)
    {
        //create the result and redirect to questions
        $input = $request->all();

        $result = new Result();
        $id = $result->new($input, $locale, $assessment);

        $request->session()->put('id', $id);

        return redirect()->route('assessment-questions', $assessment->key);
    }


    /**
     * Displays the main questions for the current assessment
     *
     * @param Request $request
     * @param Assessment $assessment
     * @return \Illuminate\Http\Response
     */
    public function showQuestions(Request $request, Assessment $assessment)
    {
        // if there is no ID in the session get them back to the start
        if (!$request->session()->has('id')) {
            return redirect()->route('assessment-locales', $assessment->key);
        }

        // get the result record TODO: fail better
        $result = Result::findOrFail($request->session()->get("id"));

        // make sure the locale is set to choosen/stored locale
        App::setLocale($result->locale);

        // get the questions related to the result assessment
        $questions = $result->assessment->questionSet->questions->shuffle();

        $parameters = array(
            'assessment' => $result->assessment,
            'questions' => $questions
        );

        return view('assessments/rtw/worker/questions', $parameters);
    }


    /**
     * Stores the main questions for the current assessment
     * and redirects to finish page
     *
     * @param Request $request
     * @param Assessment $assessment
     * @return \Illuminate\Http\Response
     */
    public function storeQuestions(Request $request, Assessment $assessment)
    {
        // if there is no ID in the session get them back to the start
        if (!$request->session()->has('id')) {
            return redirect()->route('assessment-locales', $assessment->key);
        }

        $input = $request->all();

        // get the result from the id stored in the session
        $result = Result::find($request->session()->get("id"));

        // store the answers
        $result->complete($input);

        if ($assessment->additional_text) {
            // redirect to additional text page
            return redirect()->route('assessment-additional', $assessment->key);
        } else {
            // forget the id
            $request->session()->forget('id');

            // redirect to thanks page
            return redirect()->route('assessment-complete', $assessment->key);
        }
    }


    /**
     * Displays the additional free text field for the current assessment
     *
     * @param Request $request
     * @param Assessment $assessment
     * @return \Illuminate\Http\Response
     */
    public function showAdditional(Request $request, Assessment $assessment)
    {
        // if there is no ID in the session get them back to the start
        if (!$request->session()->has('id')) {
            return redirect()->route('assessment-locales', $assessment->key);
        }

        // get the result record TODO: fail better
        $result = Result::findOrFail($request->session()->get("id"));

        // make sure the locale is set to choosen/stored locale
        App::setLocale($result->locale);

        $parameters = array(
            'assessment' => $result->assessment
        );

        return view('assessments/rtw/worker/additional', $parameters);
    }


    /**
     * Stores the additional text
     *
     * @param Request $request
     * @param Assessment $assessment
     * @return \Illuminate\Http\Response
     */
    public function storeAdditional(Request $request, Assessment $assessment)
    {
        // if there is no ID in the session get them back to the start
        if (!$request->session()->has('id')) {
            return redirect()->route('assessment-locales', $assessment->key);
        }

        //create the result and redirect to questions
        $input = $request->all();

        // get the result from the id stored in the session
        $result = Result::find($request->session()->get("id"));

        // store the answers
        $result->additional($input);

        // forget the id
        $request->session()->forget('id');

        // redirect to thanks page
        return redirect()->route('assessment-complete', $assessment->key);
    }


    /**
     * Displays the complete page for the current assessment
     *
     * @param Request $request
     * @param Assessment $assessment
     * @return \Illuminate\Http\Response
     */
    public function showComplete(Request $request, Assessment $assessment)
    {
        App::setLocale($request->session()->get('locale'));

        $parameters = array(
            'assessment' => $assessment,
        );

        return view('assessments/rtw/worker/complete', $parameters);
    }
}
