<?php

namespace App\Http\Middleware;

use App\Models\Assessment\Type;
use Closure;

class Assessment
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Assessment is bound to the route.
        $assessment = $request->route()->assessment;

        // If the assessment is not active, through a not available response.
        if (!$assessment->isActive()) {
            return response('Assessment not available', 403);
        }

        return $next($request);
    }
}
