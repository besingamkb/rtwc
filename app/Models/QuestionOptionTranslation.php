<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionOptionTranslation extends Model
{
    protected $fillable = [
        'text'
    ];
}
