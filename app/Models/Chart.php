<?php

namespace App\Models;

use Auth;
use Config;
use Illuminate\Database\Eloquent\Model;
use File;

class Chart extends Model
{
    /**
     * Attempts to create the chart options.
     *
     * @param array $data
     * @param String $type
     * @param String $name
     * @param array $dimensions
     */
    public function makeChart(array $data, String $type, String $name, array $dimensions = [])
    {
        // Set the currently authenticated user.
        $authUser = Auth::user();

        // Generate the chart temporary folders if they do not exist.
        // All chart files go under the authenticated users ID.
        $tmpDirectory = storage_path('charts/tmp');
        $chartDirectory = $tmpDirectory . '/' . $authUser->id;

        if (!File::exists($tmpDirectory)) {
            File::makeDirectory($tmpDirectory);
        }

        if (!File::exists($chartDirectory)) {
            File::makeDirectory($chartDirectory);
        }

        // Set dimensions of chart.
        if (!isset($dimensions['h'])) {
            $dimensions['h'] = 180;
        }

        // Create the chart options file.
        $chartOptions = file_get_contents(storage_path('charts/templates/general.json'));

        // Fill JSON with data and type.
        $chartOptions = str_replace('{{height}}', $dimensions['h'], $chartOptions);
        $chartOptions = str_replace('{{data}}', json_encode($data), $chartOptions);
        $chartOptions = str_replace('{{type}}', $type, $chartOptions);

        $chartOptionsPath = $chartDirectory . '/' . $name . '.json';

        // Save the chart options file as JSON.
        File::put($chartOptionsPath, $chartOptions);

        // Create the chart.
        $this->make($chartOptionsPath, $name, $chartDirectory);
    }

    /**
     * Actually make the chart and store it as an image.
     *
     * @param $chartOptionsPath
     * @param $name
     * @param $chartDirectory
     */
    protected function make($chartOptionsPath, $name, $chartDirectory)
    {
        // Get the chart config.
        $config = Config::get('charts');

        // Set variables for use in the EXEC command for PhantomJS.
        $phantomJS = $config['phantomjs'];
        $highchartsConvert = $config['highcharts_convert'];
        $constr = $config['constr'];
        $outFile = $chartDirectory . '/' . $name . '.png';

        // Execute the PhantomJS script to generate and save the chart image.
        exec("$phantomJS $highchartsConvert -infile $chartOptionsPath -constr $constr -outfile $outFile -scale 3");
    }

    protected function targetScore($num)
    {
        $num = $num + 5;
        if ($num > 100) {
            $num = 100;
        }
        return $num;
    }

    protected function colorCode($num)
    {
        $codes = ['#DE301D', '#E67E22', '#F1C40F', '#8E44AD', '#2ECC71'];

        $code;

        if ($num < 21) {
            $code = $codes[0];
        } else {
            if ($num < 41) {
                $code = $codes[1];
            } else {
                if ($num < 60) {
                    $code = $codes[2];
                } else {
                    if ($num < 91) {
                        $code = $codes[3];
                    } else {
                        $code = $codes[4];
                    }
                }
            }
        }

        return $code;
    }
}
