<?php

namespace App\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class QuestionSet extends Model
{
    use Translatable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'question_sets';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key',
        'name',
        'type',
        'welcome_text',
        'intro_text',
        'finished_text',
        'status'
    ];

    /**
     * The model name and namespace of the Translation model.
     *
     * @var string
     */
    public $translationModel = 'App\Models\QuestionSetTranslation';

    /**
     * The columns to be translated.
     *
     * @var array
     */
    public $translatedAttributes = [
        'welcome_text',
        'intro_text',
        'finished_text'
    ];

    // 1: Default
    // 2: Draft
    // 3: Live
    const STATUS = 1;

    /**
     * Return all questions for this question set.
     *
     * @return mixed
     */
    public function questions()
    {
        return $this->hasMany(Question::class)->orderBy('order')->orderBy('id');
    }

    /**
     * Scope a query to only include a certain key.
     *
     * @param $query
     * @param $key
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeKey($query, $key)
    {
        return $query->where('key', $key);
    }

    /**
     * Only return editable question sets.
     *
     * @param $query
     * @return mixed
     */
    public function scopeEditable($query)
    {
        return $query->where('is_editable', true);
    }
}
