<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;

class QuestionOption extends Model
{
	use Translatable;

    public $translationModel = 'App\Models\QuestionOptionTranslation';

    protected $table = 'question_options';

    public $translatedAttributes = ['text'];

	public function question()
	{
		return $this->belongsTo('App\Models\Question');
	}
}
