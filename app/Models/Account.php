<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Account extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key',
        'name'
    ];

    /**
     * Return this accounts users.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }

    /**
     * Return all assessments this account can view.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function assessments()
    {
        $accountID = $this->id;

        return $this->hasMany(Assessment::class)
            ->orWhere(function ($q) use ($accountID) {
                $q->whereIn('account_id', function ($q) use ($accountID) {
                    $q->select('requestee_id')
                        ->from('connections')
                        ->where('requester_id', $accountID)
                        ->where('is_accepted', true);
                });
            });
    }

    /**
     * Return this accounts alerts.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function alerts()
    {
        return $this->hasManyThrough(Alert::class, Assessment::class);
    }

    /**
     * Returns this accounts sent connections.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sentConnections()
    {
        return $this->hasMany(Connection::class, 'requester_id');
    }

    /**
     * Returns this accounts received connections.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function receivedConnections()
    {
        return $this->hasMany(Connection::class, 'requestee_id');
    }

    /**
     * Returns all successfully connected outbound accounts.
     *
     * @return mixed
     */
    public function connectedOutboundAccounts()
    {
        return $this->belongsToMany(Account::class, 'connections', 'requestee_id', 'requester_id')
            ->wherePivot('is_accepted', '=', true);
    }

    /**
     * Returns all successfully connected inbound accounts.
     *
     * @return mixed
     */
    public function connectedInboundAccounts()
    {
        return $this->belongsToMany(Account::class, 'connections', 'requester_id', 'requestee_id')
            ->wherePivot('is_accepted', '=', true);
    }

    /**
     * Returns all successfully connected accounts whether they are inbound or outbound.
     *
     * @return mixed
     */
    public function connectedAccounts()
    {
        // Do not need this yet.
    }

    /**
     * Returns an uppercase version of the account key.
     *
     * @param $value
     * @return mixed
     */
    public function getKeyAttribute($value)
    {
        return strtoupper($value);
    }

    /**
     * Return this accounts assessments.
     *
     * @param $query
     * @param $accountID
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function scopeForAccount($query, $accountID)
    {
        $query->where('account_id', $accountID);
    }
}
