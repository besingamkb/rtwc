<?php

namespace App\Models\Export;

use App\Models\Export;
use DB;

class AssessmentExport extends Export
{
    /**
     * Set the default CSV directory.
     *
     * @var string
     */
    protected $csvDirectory = 'exports/assessments';

    /**
     * Store all assessment IDs for the export.
     *
     * @var array
     */
    protected $assessmentIDs = [];

    /**
     * Sets the assessment ID for this export.
     *
     * @param $ids
     * @return $this
     */
    public function setAssessments($ids)
    {
        // Turn single ID into an array.
        if (!is_array($ids)) {
            $ids = [$ids];
        }

        // Set assessment IDs.
        $this->assessmentIDs = $ids;

        return $this;
    }

    /**
     * Attempt to build the CSV based on the data set against this class.
     *
     * @param array $data
     * @return Export|bool
     */
    public function build(array $data = [])
    {
        // Perform different queries based on the export type.
        switch ($this->exportType) {
            case 'additional_text':
                $data = DB::table('results')
                    ->select('additional_text')
                    ->whereIn('assessment_id', $this->assessmentIDs)
                    // Strip out empty additional text fields.
                    ->where('additional_text', '<>', '')
                    ->get();
                break;
            default:
        }

        // If no results are found.
        if (!count($data)) {
            return false;
        }

        // Convert data to array from STD object.
        $data = collect($data)->map(function ($result) {
            return (array)$result;
        })->toArray();

        // Call the parent build function.
        return parent::build($data);
    }
}
