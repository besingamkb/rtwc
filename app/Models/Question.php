<?php

namespace App\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Hash;

class Question extends Model
{
    use Translatable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'questions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'question_set_id',
        'category_id',
        'text'
    ];

    /**
     * The model name and namespace of the Translation model.
     *
     * @var string
     */
    public $translationModel = 'App\Models\QuestionTranslation';

    /**
     * The columns to be translated.
     *
     * @var array
     */
    public $translatedAttributes = [
        'text',
        'additional_text'
    ];

    /**
     * Return this questions question set.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function questionSet()
    {
        return $this->belongsTo(QuestionSet::class);
    }

    /**
     * Return this questions category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(QuestionCategory::class);
    }

    public function options_()
    {
        return $this->hasMany('App\Models\QuestionOption');
    }

    // Todo: The below "orderby" should really be put into a scope. this should really be the hasMany on it's own...
    public function options()
    {
        return $this->hasMany(QuestionOption::class)
            ->orderBy('order')
            ->orderBy('id');
    }

    /**
     * Scope a query to only include a certain key.
     *
     * @param $query
     * @param $key
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeKey($query, $key)
    {
        return $query->where('key', $key);
    }

    /**
     * Todo: Bad naming... Left here so it does not break legacy code... Needs to be removed in future.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function set()
    {
        return $this->belongsTo('App\Models\QuestionSet');
    }
}
