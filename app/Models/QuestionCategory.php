<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionCategory extends Model
{
    protected $table = "question_categories";

    protected $fillable = [
        'key',
        'name',
        'parent_id',
    ];

    public function questions()
    {
        return $this->hasMany('App\Models\Question');
    }


    /**
     * Scope a query to only include child categories.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeChild($query)
    {
        return $query->where('parent', 0);
    }
}
