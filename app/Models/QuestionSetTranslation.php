<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionSetTranslation extends Model
{
    protected $fillable = [
        'welcome_text',
        'intro_text',
        'finished_text'
    ];
}
