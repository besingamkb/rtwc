<?php

namespace App\Models\Result;

use App\Models\Result;
use Illuminate\Database\Eloquent\Model;

class Worker extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'results_worker';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The relationships that should be touched on save.
     *
     * @var array
     */
    protected $touches = ['result'];

    /**
     * The relations to eager load on every query.
     *
     * The parent table should really always be attached to this record as they are essentially the same table
     * but just broken out into 2 for each result types columns.
     *
     * See \App\Models\Result class for more info from the other side of this relation.
     *
     * @var array
     */
    /*protected $with = [
        'results'
    ];*/

    /**
     * Returns this tables parent record.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function result()
    {
        return $this->belongsTo(Result::class);
    }
}
