<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reporting extends Model
{
    /**
     * Stores the latest query being run.
     *
     * @var null
     */
    protected $query = null;

    /**
     * Stores multiple queries.
     *
     * @var array
     */
    protected $queries = [];

    /**
     * Store assessments.
     *
     * @var array
     */
    protected $assessments = [];

    /**
     * Get all possible filter option values.
     *
     * @return array
     */
    public function filterOptions()
    {
        $demographicsQuestionSet = new QuestionSet();
        $demographicsQuestionSet = $demographicsQuestionSet->where('key', 'rtw-demographics')->first();

        $questions = $demographicsQuestionSet->questions;

        $options = [];

        foreach ($questions as $question) {
            $options[$question->key] = $question->options->map(function ($option) {
                return [
                    'id' => $option->id,
                    'text' => $option->text
                ];
            });
        }

        return $options;
    }

    /**
     * Get all possible category options for this assessment.
     *
     * @return array
     */
    public function categories()
    {
        // Filter all results down to the worker assessment by it's ID.
        // Todo: This must be changed later because more assessments might be added.
        $questionSet = new QuestionSet();
        $questionSet = $questionSet->key('rtw-jl')->first();

        // Set what should be returned from both category queries.
        $selectList = [
            'question_categories.key',
            'question_categories.id',
            'question_categories.name',
            'question_categories.parent_id'
        ];

        // Get all possible categories for the question set.
        $categories = \DB::table('question_categories')
            ->select($selectList)
            ->join(
                'questions',
                'question_categories.id',
                '=',
                'questions.category_id'
            )
            ->where('question_set_id', $questionSet->id)
            ->groupBy('question_categories.key')
            ->get();

        // Get all parent categories for the categories in the question set.
        $parentCategories = \DB::table('question_categories')
            ->select($selectList)
            ->join(
                'question_categories as question_categories_join',
                'question_categories.id',
                '=',
                'question_categories_join.parent_id'
            )
            ->whereIn('question_categories.id', array_map(function ($category) {
                return $category->parent_id;
            }, $categories))
            ->groupBy('question_categories.key')
            ->get();

        // Merge the 2 result sets together and convert them to a collection object set.
        $categories = collect(array_merge($categories, $parentCategories));

        // Strip out all the columns we don't need.
        $categories->map(function ($category) {
            return [
                'id' => $category->id,
                'key' => $category->key,
                'name' => $category->name,
                'parent_id' => $category->parent_id
            ];
        });

        // Return the completely formatted category result set keyed by it's KEY column in the database table.
        return $categories->keyBy('key')->toArray();
    }

    /**
     * Apply filters to the query.
     *
     * @param $filters
     * @return $this
     */
    public function applyFiltersToQuery($filters)
    {
        if (count($filters)) {
            foreach ($filters as $filterSetKey => $filterSet) {
                if (!is_array($filterSet)) {
                    continue;
                }

                if (!count($filterSet)) {
                    continue;
                }

                $this->query->where(function ($query) use ($filterSetKey, $filterSet) {
                    foreach ($filterSet as $filterValue) {
                        $query->orWhere($filterSetKey, $filterValue);
                    }
                });
            }
        }

        return $this;
    }

    public function countCheck()
    {
        $assessmentsCount = clone $this->query;
        $assessmentsCount = $assessmentsCount->count();

        if ($assessmentsCount < 5) {
            return false;
        }

        return true;
    }

    /**
     * Build the initial generic query for all results.
     *
     * @param $assessments
     * @return Reporting
     */
    public function buildGenericQuery($assessments)
    {
        // Start building the query.
        $this->query = \DB::table('results');

        // Store assessments against model.
        $this->assessments = $assessments;

        // Add all assessments that are selected.
        $this->query->whereIn('results.assessment_id', $assessments);

        // Only results that are completed.
        $this->query->where('results.status', 10);

        // Filter all results down to the worker assessment by it's ID.
        // Todo: This must be changed later because more assessments might be added.
        $questionSet = new QuestionSet();
        $questionSet = $questionSet->key('rtw-jl')->first();
        $this->query->where('results.question_set_id', $questionSet->id);

        // only want to include completed assessments
        $this->query->where('assessments.status', 10)
            ->join('assessments', 'assessments.id', '=', 'results.assessment_id');

        return $this;
    }


    /**
     * Build the initial generic query for benchmark which is slight different to above
     *
     * @param $assessments
     * @return Reporting
     */
    public function buildBenchmarkGenericQuery($assessments)
    {
        // Start building the query.
        $this->query = \DB::table('results');

        // exclude assessments being viewed as they're shown seperatly
        $this->query->whereNotIn('results.assessment_id', $assessments);

        // Only results that are completed.
        $this->query->where('results.status', 10);

        // Filter all results down to the worker assessment by it's ID.
        // Todo: This must be changed later because more assessments might be added.
        $questionSet = new QuestionSet();
        $questionSet = $questionSet->key('rtw-jl')->first();
        $this->query->where('results.question_set_id', $questionSet->id);

        // only want to include completed assessments
        $this->query->where('assessments.status', 10)
            ->join('assessments', 'assessments.id', '=', 'results.assessment_id');

        return $this;
    }

    /**
     * Build the initial generic worker queries for each assessment individually.
     *
     * @param $assessments
     * @return Reporting
     */
    public function buildGenericQueryPerAssessment($assessments)
    {
        // Start building the query.
        $this->query = \DB::table('results');

        // Store assessments against model.
        $this->assessments = $assessments;

        // Filter all results down to the worker assessment by it's ID.
        // Todo: This must be changed later because more assessments might be added.
        $questionSet = new QuestionSet();
        $questionSet = $questionSet->key('rtw-jl')->first();

        foreach ($assessments as $assessment) {
            // Clone the query.
            $query = clone $this->query;

            // Add all assessments that are selected.
            $query->where('results.assessment_id', $assessment);

            // Only results that are completed.
            $query->where('results.status', 10);

            // Append the correct question set ID to the query.
            $query->where('results.question_set_id', $questionSet->id);

            // Add the query to the queries array.
            $this->queries[] = $query;
        }

        return $this;
    }

    /**
     * Build the initial generic query for all results.
     * Todo: Try and create another base query off this function and the buildGenericQuery() function
     *
     * @param $assessments
     * @return Reporting
     */
    public function buildGenericEmployerQuery($assessments)
    {
        // Start building the query.
        $this->query = \DB::table('results');

        // Store assessments against model.
        $this->assessments = $assessments;

        // Add all assessments that are selected.
        $this->query->whereIn('results.assessment_id', $assessments);

        // Only results that are completed.
        $this->query->where('results.status', 10);

        // Filter all results down to the worker assessment by it's ID.
        // Todo: This must be changed later because more assessments might be added.
        $questionSet = new QuestionSet();
        $questionSet = $questionSet->key('rtw-employer')->first();

        $this->query->where('results.question_set_id', $questionSet->id);

        return $this;
    }

    /**
     * Build the initial generic employer queries for each assessment individually.
     *
     * @param $assessments
     * @return Reporting
     */
    public function buildGenericEmployerQueryPerAssessment($assessments)
    {
        // Start building the query.
        $this->query = \DB::table('results');

        // Store assessments against model.
        $this->assessments = $assessments;

        // Filter all results down to the worker assessment by it's ID.
        // Todo: This must be changed later because more assessments might be added.
        $questionSet = new QuestionSet();
        $questionSet = $questionSet->key('rtw-employer')->first();

        foreach ($assessments as $assessment) {
            // Clone the query.
            $query = clone $this->query;

            // Add all assessments that are selected.
            $query->where('results.assessment_id', $assessment);

            // Only results that are completed.
            $query->where('results.status', 10);

            // Append the correct question set ID to the query.
            $query->where('results.question_set_id', $questionSet->id);

            // Add the query to the queries array.
            $this->queries[] = $query;
        }

        return $this;
    }

    /**
     * Gets the format for required reporting summary information.
     *
     * @return $this
     */
    public function buildSummaryQuery()
    {
        // Get all possible summary options.
        $filterOptions = $this->filterOptions();

        // Iterate over all options and generate new queries for each one and return them.
        foreach ($filterOptions as $optionKey => $option) {
            // By cloning the query way we can keep building new queries with the
            // original structure instead of just updating the original query.
            $query = clone $this->query;
            $query = $query
                ->addSelect('questions.key', 'question_options.text')
                ->addSelect(\DB::raw('count(' . $optionKey . ') as count'))
                ->join('question_options', 'question_options.id', '=', 'results.' . $optionKey)
                ->join('questions', 'questions.id', '=', 'question_options.question_id')
                ->groupBy($optionKey);

            $this->queries[] = $query;
        }

        return $this;
    }

    /**
     * Gets the format for employer metrics calculations.
     *
     * @return $this
     */
    public function buildMetricsQuery()
    {
        // Set up the metric types.
        $metricTypes = [
            'cost_of_grievances',
            'cost_of_mediation',
            'cost_of_disciplinary_activity',
            'average_unauthorised_absence_days',
            'cost_of_unauthorised_absence',
            'cost_of_unauthorised_absence_per_employee',
            'turnover_rate',
            'cost_of_recruitment',
            'cost_of_training_per_employee',
            'cost_of_training_per_agency_worker',
            'cost_of_labour_provider_per_worker'
        ];

        //
        foreach ($metricTypes as $metricType) {
            // By cloning the query way we can keep building new queries with the
            // original structure instead of just updating the original query.
            $query = clone $this->query;

            // Join on employer results.
            $query->leftJoin('results_employer', 'results_employer.result_id', '=', 'results.id');

            // Do different calculations for each metric type.
            switch ($metricType) {
                case 'cost_of_grievances':
                    // (£6,000  x no of grievances provided in response) + (£18,898.83 x no of tribunals provided in response)
                    $query->addSelect(\DB::raw('((3000 * sum(grievances_raised_in_last_year)) + (18898.83 * sum(employment_tribunals_in_last_year))) as total'));
                    $query->addSelect(\DB::raw('(46677.55 * ' . count($this->assessments) . ') as average'));
                    $query->addSelect(\DB::raw('"The cost of Grievance is produced by calculating the number of grievances and tribunals provided within the employer metrics assessment, against the industry average cost associated to conflict resolution within the workplace" as description'));
                    break;
                case 'cost_of_mediation':
                    // £2,250 x no of mediations provided in response
                    $query->addSelect(\DB::raw('(2250 * sum(mediations_completed_in_last_year)) as total'));
                    $query->addSelect(\DB::raw('(0) as average'));
                    $query->addSelect(\DB::raw('"The cost of Mediations is produced by calculating the number of mediations provided within the employer metrics assessment, against the industry average cost associated to conflict resolution within the workplace" as description'));
                    break;
                case 'cost_of_disciplinary_activity':
                    // £4,500 x no of disciplinary cases provided in response
                    $query->addSelect(\DB::raw('(4500 * sum(disciplinary_cases_in_last_year)) as total'));
                    $query->addSelect(\DB::raw('(58500 * ' . count($this->assessments) . ') as average'));
                    $query->addSelect(\DB::raw('"The cost of Disciplinary Activity is produced by calculating the number of disciplinary cases provided within the employer metrics assessment, against the industry average cost associated to a disciplinary case within the workplace" as description'));
                    break;
                case 'average_unauthorised_absence_days':
                    // No of days absence provided / by number of employees
                    $query->addSelect(\DB::raw('(sum(sickness_unauthorised_absence_in_last_year) / sum(directly_employed_workers)) as total'));
                    $query->addSelect(\DB::raw('(5.9) as average'));
                    $query->addSelect(\DB::raw('"The number of unauthorised absence days per employee is produced by calculating the number of unauthorised days absence (including sickness) provided in the employer metrics assessment, this figure is then divided by the number of employees" as description'));
                    break;
                case 'cost_of_unauthorised_absence':
                    // £105.60 x no of days absence provided in response
                    $query->addSelect(\DB::raw('(105.60 * sum(sickness_unauthorised_absence_in_last_year)) as total'));
                    $query->addSelect(\DB::raw('(623.04) as average'));
                    $query->addSelect(\DB::raw('"The cost of Unauthorised Absence is produced by calculating the number of unauthorised days absence (including sickness), provided in the employer metrics assessment, against the industry average salary of job role associated to front line workers" as description'));
                    break;
                case 'cost_of_unauthorised_absence_per_employee':
                    // (£105.60 x no of days absence provided in response) / by number of employees
                    $query->addSelect(\DB::raw('((105.60 * sum(sickness_unauthorised_absence_in_last_year)) / sum(directly_employed_workers)) as total'));
                    $query->addSelect(\DB::raw('(623.04) as average'));
                    $query->addSelect(\DB::raw('"The cost of Unauthorised Absence per Employee is produced by calculating the number of unauthorised days absence (including sickness), provided in the employer metrics assessment, against the industry average salary of job role associated to front line workers, and divided by the number of employees" as description'));
                    break;
                case 'turnover_rate':
                    // Employees left in the last 12 months / ((Directly employed workers + Employees left in the last 12 months - total recruited) + Directly employed workers / 2)
                    $query->addSelect(\DB::raw('(sum(people_left_in_last_year) / ((sum(directly_employed_workers) + sum(people_left_in_last_year) - (sum(management_roles_recruited_in_last_year) + sum(operational_roles_recruited_in_last_year) + sum(production_roles_recruited_in_last_year)) + sum(directly_employed_workers)) / 2)) * 100 as total'));
                    $query->addSelect(\DB::raw('(10.20) as average'));
                    $query->addSelect(\DB::raw('"The rate of Turnover is produced by calculating the number of employees working within the organisation, against the number of employees that left the organisation in the past 12 months, as provided in the employer metrics assessment, with the result represented as a percentage" as description'));
                    break;
                case 'cost_of_recruitment':
                    // (No of Manager Roles recruited x £12,333) + (no of Production Roles recruited x £3,924) + (no of Operational roles recruited x £3,924)
                    $query->addSelect(\DB::raw('((sum(management_roles_recruited_in_last_year) * 12333) + (sum(production_roles_recruited_in_last_year) * 3924) + (sum(operational_roles_recruited_in_last_year) * 3924)) as total'));
                    $query->addSelect(\DB::raw('(0) as average'));
                    $query->addSelect(\DB::raw('"The cost of Recruitment is produced by calculating the combined number of management, production and operational roles that were recruited in the past 12 months, against the industry average cost associated to recruitment within the workplace" as description'));
                    break;
                case 'cost_of_training_per_employee':
                    // Total spend on external training for employees in last 12 months / Number of directly employed workers
                    $query->addSelect(\DB::raw('(sum(total_spend_external_training_employees_in_last_year) / sum(directly_employed_workers)) as total'));
                    $query->addSelect(\DB::raw('(325) as average'));
                    $query->addSelect(\DB::raw('"The cost of Training is produced by calculating the total cost of internal and external training rolled out within the past 12 months, as provided in the employer metrics assessment and decided by the number of employees" as description'));
                    break;
                case 'cost_of_training_per_agency_worker':
                    // Total spend on external training for agency workers / In the last 12 months how many agency workers have you used
                    $query->addSelect(\DB::raw('(sum(total_spend_external_training_agency_workers_in_last_year) / sum(agency_workers_used_in_last_year)) as total'));
                    $query->addSelect(\DB::raw('(325) as average'));
                    $query->addSelect(\DB::raw('"The cost of Training is produced by calculating the total cost of internal and external training rolled out to agency workers within the past 12 months, as provided in the employer metrics assessment and divided by the number of employees" as description'));
                    break;
                case 'cost_of_labour_provider_per_worker':
                    // £4,140 as a number
                    $query->addSelect(\DB::raw('(4140) as total'));
                    $query->addSelect(\DB::raw('(4140) as average'));
                    $query->addSelect(\DB::raw('"Recruitment cost can vary, but when using recruitment organisations, 15% of the annual salary is a typical fee" as description'));
                    break;
                default:
            }

            // Add name of metric type to the query.
            $query->addSelect(\DB::raw('"' . $metricType . '" as "key"'));

            // Add this query to the queries list.
            $this->queries[] = $query;
        }

        return $this;
    }

    /**
     * Build the total scores from the results set.
     *
     * @return $this
     */
    public function buildScoreTotalsQuery()
    {
        // Select the max score possible for the question set and average the percentage for all scores.
        $this->query->addSelect(\DB::raw('question_sets.max_score'))
            ->addSelect(\DB::raw('avg(total_score) * 100 as total_score'))
            ->join('question_sets', 'question_sets.id', '=', 'results.question_set_id');

        // Store the query in the queries array.
        $this->queries[] = $this->query;

        return $this;
    }

    /**
     * Gets the format for required reporting category sums information.
     *
     * @return $this
     */
    public function buildCategoriesQuery()
    {
        // Get all possible categories.
        $categories = $this->categories();

        // Iterate over all options and generate new queries for each one and return them.
        foreach ($categories as $categoryKey => $category) {
            // By cloning the query way we can keep building new queries with the
            // original structure instead of just updating the original query.
            $query = clone $this->query;

            // Column names have _score appended to the end.
            $columnName = $categoryKey . '_score';

            // Add selects for category columns
            $query = $this->addCategoryQuerySelects($query, $category);

            // Work out the SUM of all results as a percentage of the total COUNT of all results.
            // Only do this if the category is a child category
            if ($category->parent_id != 0) {
                $query->addSelect(\DB::raw('sum(' . $columnName . ') / (count(' . $columnName . ')) * 100 as total'));
            }

            // Ensure we don't get anymore results that the one per category.
            $query->groupBy('key');

            // Store the queries against the class variable.
            $this->queries[] = $query;
        }

        return $this;
    }

    /**
     * Gets the format for required reporting category comparative information.
     *
     * @return $this
     */
    public function buildCategoriesQueryPerAssessment()
    {
        // Get all possible categories.
        $categories = $this->categories();

        // Store queries against local variable and empty queries array.
        $queries = $this->queries;
        $this->queries = [];

        //
        foreach ($queries as $query) {
            // Iterate over all options and generate new queries for each one and return them.
            foreach ($categories as $categoryKey => $category) {
                // Make unique copy of the existing query.
                $query = clone $query;

                // Column names have _score appended to the end.
                $columnName = $categoryKey . '_score';

                // Add selects for category columns
                $query = $this->addCategoryQuerySelects($query, $category);

                // Work out the SUM of all results as a percentage of the total COUNT of all results.
                // Only do this if the category is a child category
                if ($category->parent_id != 0) {
                    $query->addSelect(\DB::raw('sum(' . $columnName . ') / (count(' . $columnName . ')) * 100 as total'));
                }

                // Add assessment information.
                // We need to make each join unique so it doesn't affect other queries.
                // Todo: Investigate why we have to do this because the clone function should prevent this.
                $query->join(
                    'assessments as assessments_' . $categoryKey,
                    'assessments_' . $categoryKey . '.id',
                    '=',
                    'results.assessment_id'
                );

                // join accounts to get account name
                $query->join(
                    'accounts as accounts_' . $categoryKey,
                    'accounts_' . $categoryKey . '.id',
                    '=',
                    'assessments_' . $categoryKey . '.account_id'
                );

                $query->addSelect('results.assessment_id');
                $query->addSelect('assessments_' . $categoryKey . '.key as assessment_key');
                $query->addSelect('accounts_' . $categoryKey . '.name as account_name');

                // Ensure we don't get anymore results that the one per category.
                $query->groupBy('key');

                // Store the queries against the class variable.
                $this->queries[] = $query;
            }

            //
            $query = clone $query;

            // Select the max score possible for the question set and average the percentage for all scores.
            $query->addSelect(\DB::raw('question_sets.max_score'))
                ->addSelect(\DB::raw('avg(total_score) * 100 as total'))
                ->addSelect(\DB::raw('"Overall Score"' . ' as "name"'))
                ->addSelect(\DB::raw('"overallscore"' . ' as "key"'))
                ->join('question_sets', 'question_sets.id', '=', 'results.question_set_id');

            // Store the query in the queries array.
            $this->queries[] = $query;
        }

        return $this;
    }

    /**
     * Gets the format for required reporting comparative metrics information.
     *
     * @return $this
     */
    public function buildMetricsQueryPerAssessment()
    {
        // Set up the metric types.
        $metricTypes = [
            'cost_of_grievances',
            'cost_of_mediation',
            'cost_of_disciplinary_activity',
            'average_unauthorised_absence_days',
            'cost_of_unauthorised_absence',
            'cost_of_unauthorised_absence_per_employee',
            'turnover_rate',
            'cost_of_recruitment',
            'cost_of_training_per_employee',
            'cost_of_training_per_agency_worker',
            'cost_of_labour_provider_per_worker'
        ];

        // Store queries against local variable and empty queries array.
        $queries = $this->queries;
        $this->queries = [];

        //
        foreach ($queries as $query) {
            // Iterate over all options and generate new queries for each one and return them.
            foreach ($metricTypes as $metricTypeKey => $metricType) {
                //
                $query = clone $query;

                // Join on employer results.
                $query->leftJoin(
                    'results_employer as results_employer_' . $metricType,
                    'results_employer_' . $metricType . '.result_id',
                    '=',
                    'results.id'
                );

                // Do different calculations for each metric type.
                switch ($metricType) {
                    case 'cost_of_grievances':
                        // (£6,000  x no of grievances provided in response) + (£18,898.83 x no of tribunals provided in response)
                        $query->addSelect(\DB::raw('((3000 * sum(results_employer_' . $metricType . '.grievances_raised_in_last_year)) + (18898.83 * sum(results_employer_' . $metricType . '.employment_tribunals_in_last_year))) as total'));
                        $query->addSelect(\DB::raw('(46677.55 * ' . count($this->assessments) . ') as average'));
                        $query->addSelect(\DB::raw('"The cost of Grievance is produced by calculating the number of grievances and tribunals provided within the employer metrics assessment, against the industry average cost associated to conflict resolution within the workplace" as description'));
                        break;
                    case 'cost_of_mediation':
                        // £2,250 x no of mediations provided in response
                        $query->addSelect(\DB::raw('(2250 * sum(results_employer_' . $metricType . '.mediations_completed_in_last_year)) as total'));
                        $query->addSelect(\DB::raw('(0) as average'));
                        $query->addSelect(\DB::raw('"The cost of Mediations is produced by calculating the number of mediations provided within the employer metrics assessment, against the industry average cost associated to conflict resolution within the workplace" as description'));
                        break;
                    case 'cost_of_disciplinary_activity':
                        // £4,500 x no of disciplinary cases provided in response
                        $query->addSelect(\DB::raw('(4500 * sum(results_employer_' . $metricType . '.disciplinary_cases_in_last_year)) as total'));
                        $query->addSelect(\DB::raw('(58500 * ' . count($this->assessments) . ') as average'));
                        $query->addSelect(\DB::raw('"The cost of Disciplinary Activity is produced by calculating the number of disciplinary cases provided within the employer metrics assessment, against the industry average cost associated to a disciplinary case within the workplace" as description'));
                        break;
                    case 'average_unauthorised_absence_days':
                        // No of days absence provided / by number of employees
                        $query->addSelect(\DB::raw('(sum(results_employer_' . $metricType . '.sickness_unauthorised_absence_in_last_year) / sum(results_employer_' . $metricType . '.directly_employed_workers)) as total'));
                        $query->addSelect(\DB::raw('(5.9) as average'));
                        break;
                    case 'cost_of_unauthorised_absence':
                        // £105.60 x no of days absence provided in response
                        $query->addSelect(\DB::raw('(105.60 * sum(results_employer_' . $metricType . '.sickness_unauthorised_absence_in_last_year)) as total'));
                        $query->addSelect(\DB::raw('(623.04) as average'));
                        $query->addSelect(\DB::raw('"The cost of Unauthorised Absence is produced by calculating the number of unauthorised days absence (including sickness), provided in the employer metrics assessment, against the industry average salary of job role associated to front line workers" as description'));
                        break;
                    case 'cost_of_unauthorised_absence_per_employee':
                        // (£105.60 x no of days absence provided in response) / by number of employees
                        $query->addSelect(\DB::raw('((105.60 * sum(results_employer_' . $metricType . '.sickness_unauthorised_absence_in_last_year)) / sum(results_employer_' . $metricType . '.directly_employed_workers)) as total'));
                        $query->addSelect(\DB::raw('(623.04) as average'));
                        $query->addSelect(\DB::raw('"The cost of Unauthorised Absence per Employee is produced by calculating the number of unauthorised days absence (including sickness), provided in the employer metrics assessment, against the industry average salary of job role associated to front line workers, and divided by the number of employees" as description'));
                        break;
                    case 'turnover_rate':
                        // Employees left in the last 12 months / ((Directly employed workers + Employees left in the last 12 months - total recruited) + Directly employed workers / 2)
                        $query->addSelect(\DB::raw('(sum(results_employer_' . $metricType . '.people_left_in_last_year) / ((sum(results_employer_' . $metricType . '.directly_employed_workers) + sum(results_employer_' . $metricType . '.people_left_in_last_year) - (sum(results_employer_' . $metricType . '.management_roles_recruited_in_last_year) + sum(results_employer_' . $metricType . '.operational_roles_recruited_in_last_year) + sum(results_employer_' . $metricType . '.production_roles_recruited_in_last_year)) + sum(results_employer_' . $metricType . '.directly_employed_workers)) / 2)) * 100 as total'));
                        $query->addSelect(\DB::raw('(10.20) as average'));
                        $query->addSelect(\DB::raw('"The rate of Turnover is produced by calculating the number of employees working within the organisation, against the number of employees that left the organisation in the past 12 months, as provided in the employer metrics assessment, with the result represented as a percentage" as description'));
                        break;
                    case 'cost_of_recruitment':
                        // (No of Manager Roles recruited x £12,333) + (no of Production Roles recruited x £3,924) + (no of Operational roles recruited x £3,924)
                        $query->addSelect(\DB::raw('((sum(results_employer_' . $metricType . '.management_roles_recruited_in_last_year) * 12333) + (sum(results_employer_' . $metricType . '.production_roles_recruited_in_last_year) * 3924) + (sum(results_employer_' . $metricType . '.operational_roles_recruited_in_last_year) * 3924)) as total'));
                        $query->addSelect(\DB::raw('(0) as average'));
                        $query->addSelect(\DB::raw('"The cost of Recruitment is produced by calculating the combined number of management, production and operational roles that were recruited in the past 12 months, against the industry average cost associated to recruitment within the workplace" as description'));
                        break;
                    case 'cost_of_training_per_employee':
                        // Total spend on external training for employees in last 12 months / Number of directly employed workers
                        $query->addSelect(\DB::raw('(sum(results_employer_' . $metricType . '.total_spend_external_training_employees_in_last_year) / sum(results_employer_' . $metricType . '.directly_employed_workers)) as total'));
                        $query->addSelect(\DB::raw('(325) as average'));
                        $query->addSelect(\DB::raw('"The cost of Training is produced by calculating the total cost of internal and external training rolled out within the past 12 months, as provided in the employer metrics assessment and decided by the number of employees" as description'));
                        break;
                    case 'cost_of_training_per_agency_worker':
                        // Total spend on external training for agency workers / In the last 12 months how many agency workers have you used
                        $query->addSelect(\DB::raw('(sum(results_employer_' . $metricType . '.total_spend_external_training_agency_workers_in_last_year) / sum(results_employer_' . $metricType . '.agency_workers_used_in_last_year)) as total'));
                        $query->addSelect(\DB::raw('(325) as average'));
                        $query->addSelect(\DB::raw('"The cost of Training is produced by calculating the total cost of internal and external training rolled out to agency workers within the past 12 months, as provided in the employer metrics assessment and divided by the number of employees" as description'));
                        break;
                    case 'cost_of_labour_provider_per_worker':
                        // £4,140 as a number
                        $query->addSelect(\DB::raw('(4140) as total'));
                        $query->addSelect(\DB::raw('(4140) as average'));
                        $query->addSelect(\DB::raw('"Recruitment cost can vary, but when using recruitment organisations, 15% of the annual salary is a typical fee" as description'));
                        break;
                    default:
                }

                // Add assessment information.
                // We need to make each join unique so it doesn't affect other queries.
                // Todo: Investigate why we have to do this because the clone function should prevent this.
                $query->join(
                    'assessments as assessments_' . $metricType,
                    'assessments_' . $metricType . '.id',
                    '=',
                    'results.assessment_id'
                );

                // join accounts to get account name
                $query->join(
                    'accounts as accounts_' . $metricType,
                    'accounts_' . $metricType . '.id',
                    '=',
                    'assessments_' . $metricType . '.account_id'
                );

                $query->addSelect('results.assessment_id');
                $query->addSelect('assessments_' . $metricType . '.key as assessment_key');
                $query->addSelect('accounts_' . $metricType . '.name as account_name');

                // Add metrics descriptions.


                // Add name of metric type to the query.
                $query->addSelect(\DB::raw('"' . $metricType . '" as "key"'));

                // Add this query to the queries list.
                $this->queries[] = $query;
            }
        }

        return $this;
    }

    /**
     * Build the question / category scores from the results set.
     *
     * @return $this
     */
    public function buildDetailQuery()
    {

        // join the answers table so we can get the individual question score
        $this->query->join(
            'answers',
            'answers.result_id',
            '=',
            'results.id'
        );

        // join the questions table so we can get the question text
        $this->query->join(
            'questions',
            'questions.id',
            '=',
            'answers.question_id'
        );

        // we only want to get questions/answers based on the main question_set_id
        $this->query->where('questions.question_set_id', \DB::raw('results.question_set_id'));

        // join the options table so we can get the score for the answer
        $this->query->join(
            'question_options',
            'question_options.id',
            '=',
            'answers.question_option_id'
        );

        // join the category table so we can return the category text and id
        $this->query->join(
            'question_categories',
            'question_categories.id',
            '=',
            'questions.category_id'
        );

        // join the category table again by parent id so we can return the parent category text and id
        $this->query->join(
            'question_categories as parent_question_categories',
            'parent_question_categories.id',
            '=',
            'question_categories.parent_id'
        );

        // left join the alerts table to get the alerts count
        $this->query->leftJoin('alerts', function($join) {
             $join->on('alerts.result_id', '=', 'results.id');
             $join->on('alerts.question_option_id','=', 'answers.question_option_id');
        });


        $this->query->addSelect('questions.id');
        $this->query->addSelect('questions.text');
        $this->query->addSelect('question_categories.name as category_name');
        $this->query->addSelect('question_categories.id as category_id');
        $this->query->addSelect('parent_question_categories.name as parent_category_name');
        $this->query->addSelect('parent_question_categories.id as parent_category_id');
        $this->query->addSelect(\DB::raw('avg(question_options.score/questions.max_score) * 100 as total'));
        $this->query->addSelect(\DB::raw('count(alerts.id) as alerts'));

        $this->query->groupBy('questions.id');

        $this->query->orderBy('question_categories.id');
        $this->query->orderBy('questions.id');

        // Store the query in the queries array.
        $this->queries[] = $this->query;

        return $this;
    }

    /**
     * Gets the format for required reporting category alerts information.
     *
     * @return $this
     */
    public function buildCategoryAlertsQuery()
    {
        // Get all possible categories.
        $categories = $this->categories();

        // Iterate over all options and generate new queries for each one and return them.
        foreach ($categories as $categoryKey => $category) {
            // By cloning the query way we can keep building new queries with the
            // original structure instead of just updating the original query.
            $query = clone $this->query;

            // Column names have _score appended to the end.
            $columnName = $categoryKey . '_score';

            // Add selects for category columns
            $query = $this->addCategoryQuerySelects($query, $category);

            // Join the alerts table onto the query.
            $query->leftJoin('alerts', 'alerts.result_id', '=', 'results.id')
                ->leftJoin('question_options', 'question_options.id', '=', 'alerts.question_option_id')
                ->leftJoin('questions', 'questions.id', '=', 'question_options.question_id')
                ->where('question_categories.id', $category->id)
                ->whereNotNull('question_categories.key')
                ->orWhereNull('question_categories.id');

            // If the category IS NOT a parent.
            if ($category->parent_id != 0) {
                // Work out the SUM of all results as a percentage of the total COUNT of all results.
                // Only do this if the category is a child category
                $query->addSelect(\DB::raw('count(' . $columnName . ') as total'));

                // Do a full join instead of a left join so we don't get counts including empty alert rows.
                $query->join('question_categories', 'question_categories.id', '=', 'questions.category_id');
            }

            // If the category IS a parent.
            if ($category->parent_id == 0) {
                // Do a left join instead of a full join because the categories should have 0 alerts.
                $query->leftJoin('question_categories', 'question_categories.id', '=', 'questions.category_id');

                // Ensure we don't get anymore results that the one per category.
                $query->groupBy('key');
            }

            // Store the queries against the class variable.
            $this->queries[] = $query;
        }

        return $this;
    }

    /**
     * Adds query selects for category specific queries.
     *
     * @param $query
     * @param $category
     * @return mixed
     */
    public function addCategoryQuerySelects($query, $category)
    {
        return $query
            // Because there is no other way to link up the results to their original category records
            // via a join query we need to manually put it fake columns for the category key and name.
            ->addSelect(\DB::raw('"' . $category->id . '"' . ' as "id"'))
            ->addSelect(\DB::raw('"' . $category->name . '"' . ' as "name"'))
            ->addSelect(\DB::raw('"' . $category->key . '"' . ' as "key"'))
            ->addSelect(\DB::raw('"' . $category->parent_id . '"' . ' as "parent_id"'));
    }

    /**
     * Adds the total field to all parent categories for each child category in the results set.
     *
     * @param $results
     * @param bool $isPercentage
     * @return array
     */
    public static function appendParentCategoryTotals($results, $isPercentage = false)
    {
        //
        $results = array_map(function ($result) {
            return $result[0];
        }, $results);

        // Filter out the parent categories.
        $parentCategories = array_filter($results, function ($result) {
            return $result->parent_id == 0;
        });

        // Filter out the child categories.
        $childCategories = array_filter($results, function ($result) {
            return $result->parent_id != 0;
        });

        // Iterate over all parent categories then the child categories within them based on the child's parent ID.
        foreach ($parentCategories as $parentCategoryKey => $parentCategory) {
            $childCategoryCount = 0;
            $total = 0;

            foreach ($childCategories as $childCategory) {
                // If the child category does not match the parent just skip to next iteration to save time.
                if ($parentCategory->id != $childCategory->parent_id) {
                    continue;
                }

                // Add the sum to the total parent category sum and add a count for the amount of children iterated over.
                $childCategoryCount++;
                $total += $childCategory->total;
            }

            // Only do the following if we are working in percentages.
            // Because we are working in percentages we need to divide the total child percentages by the count of child categories.
            if ($isPercentage) {
                $parentCategories[$parentCategoryKey]->total = $total / $childCategoryCount;
            } else {
                $parentCategories[$parentCategoryKey]->total = $total;
            }
        }

        // Finally, return the merged children and parents as one mega array.
        return array_merge($parentCategories, $childCategories);
    }



    /**
     * Build the bench mark scores from the results set.
     *
     * @return $this
     */
    public function buildBenchmarkQuery($withAccountName = false)
    {
        if($withAccountName)
        {
            $this->query->join(
                'accounts',
                'accounts.id',
                '=',
                'assessments.account_id'
            );

            $this->query->addSelect('accounts.name as account_name');
        }

        $this->query
            ->addSelect(\DB::raw('assessments.key'))
            ->addSelect(\DB::raw('avg(results.total_score) * 100 as total_score'))
            ->addSelect(\DB::raw('sum(results.alert_count) as total_alerts'))
            ->groupBy('assessment_id')
            ->orderBy('total_alerts');

        // Store the query in the queries array.
        $this->queries[] = $this->query;

        return $this;
    }

    /**
     * Sort results into their specified assessments by assessment_key.
     *
     * @param $results
     * @return array
     */
    public static function sortResultsIntoAssessments($results)
    {
        $newResults = [];

        foreach ($results as $resultSet) {
            if (!$resultSet) {
                continue;
            }

            if (is_object($resultSet)) {
                $resultSet = [$resultSet];
            }

            if (is_array($resultSet)) {
                $resultSet = $resultSet[0];
            }

            if (!array_key_exists($resultSet->assessment_key, $newResults)) {
                $newResults[$resultSet->assessment_key] = [];
            }

            $newResults[$resultSet->assessment_key][] = $resultSet;
        }

        return $newResults;

    }

    /**
     * Finally perform the query.
     *
     * @return mixed
     */
    public function get()
    {
        $results = [];

        // If there is no query then return false;
        if (!$this->query) {
            return false;
        }

        // If there are no queries in the queries array then put the latest run query into it.
        if (count($this->queries) == 0) {
            $this->queries[] = $this->query;
        }

        // Iterate over all stored queries and run them to get the results.
        foreach ($this->queries as $query) {
            $result = $query->get();
            $results[] = $result;
        }

        // If the query result set has one then return only that.
        if (count($results) == 1) {
            return $results[0];
        }

        // Or return all of the results for the query results set.
        return $results;
    }

    /**
     * Returns a result set keyed by it's key.
     *
     * @param $results
     * @return array
     */
    public static function formatAssociativeByKey($results)
    {
        $newResultsFormat = [];

        foreach ($results as $result) {
            // If the result is an object then push it into it's own array.
            if (is_object($result)) {
                $result = [$result];
            }

            // Check that a first result exists.
            if (!isset($result[0])) {
                continue;
            }

            // Finally just set the new key as the current iteration result.
            $newResultsFormat[$result[0]->key] = $result;
        }

        return $newResultsFormat;
    }

    /**
     * Cast all totals to floats from strings.
     *
     * @param $results
     * @param string $key
     * @return array
     */
    public static function castTotals($results, $key = 'total')
    {
        // Map over all results and cast the given keys.
        $results = array_map(function ($result) use ($key) {
            if (is_array($result)) {
                // If the result is in array it probably only has 1 index so cast that.
                $result[0]->$key = (float)$result[0]->$key;
            } else {
                // Otherwise cast the stdClass.
                $result->$key = (float)$result->$key;
            }
            return $result;
        }, $results);

        return $results;
    }
}
