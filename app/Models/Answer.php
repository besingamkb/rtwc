<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'answers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'result_id',
        'question_id',
        'answer_id'
    ];

    /**
     * Return this answers question.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    /**
     * Return this answers question option.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function questionOption()
    {
        return $this->belongsTo(QuestionOption::class);
    }
}
