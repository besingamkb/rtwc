<?php

namespace App\Models;

use App\Models\Assessment\Type;
use App\Traits\Search;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;

class Assessment extends Model
{
    use SoftDeletes, Search;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key',
        'name',
        'account_id',
        'question_set_id',
        'start_date',
        'end_date',
        'worker_count'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'days_remaining_count',
        'remaining_count',
        'days_until_count'
    ];

    /**
     * Return this assessments question set.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function questionSet()
    {
        return $this->belongsTo(QuestionSet::class);
    }

    /**
     * Return this assessments results.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function results()
    {
        return $this->hasMany(Result::class);
    }

    /**
     * Return this assessments account.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    /**
     * Return this assessments alerts.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function alerts()
    {
        return $this->hasMany(Alert::class);
    }

    /**
     * Returns the amount of days remaining for the assessment.
     *
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getDaysRemainingCountAttribute()
    {
        $now = Carbon::now()->subDay();
        $endDate = Carbon::parse($this->end_date);
        $endDate = $endDate->addDays(1);

        $days = $now->diffInDays($endDate, false);

        // If the end date is before today return null days.
        if ($days <= 0) {
            return null;
        }

        return $days;
    }

    /**
     * Returns the amount of days until the assessment starts.
     *
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getDaysUntilCountAttribute()
    {
        $now = Carbon::now()->subDay();
        $startDate = Carbon::parse($this->start_date);

        // If the start date is after today return null days.
        if ($startDate->lt($now)) {
            return null;
        }

        return $now->diffInDays($startDate);
    }

    /**
     * Returns the total count of remaining assessments to be completed.
     *
     * @return mixed
     */
    public function getRemainingCountAttribute()
    {
        $employerType = Type::EMPLOYER;
        $workerType = Type::WORKER;

        // An employer assessment can only be completed once so we return 1 or 0.
        if ($this->type_id == $employerType) {
            return ($this->completed_count == 1 ? 0 : 1);
        }

        // For a worker assessment we return whatever the expected count is minus the completed count.
        if ($this->type_id == $workerType) {
            return ($this->completed_count - $this->worker_count);
        }
    }

    /**
     * Returns an uppercase version of the assessment key.
     *
     * @param $value
     * @return mixed
     */
    public function getKeyAttribute($value)
    {
        return strtoupper($value);
    }

    /**
     * Filters the assessments to a specific account by ID.
     *
     * @param $query
     * @param $accountID
     */
    public function scopeForAccount($query, $accountID)
    {
        return $query->where('account_id', $accountID);
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'key';
    }

    /**
     * Logic for whether an assessment is currently active.
     *
     * @return boolean
     */
    public function isActive()
    {
        if ($this->days_remaining_count === null) {
            return false;
        }
        return true;
    }

    /**
     * Generates a unique random key for the model
     *
     * @return string
     */
    private function generateKey()
    {
        $acceptedChars = 'azertyuiopqsdfghjklmwxcvbn0123456789';
        $max = strlen($acceptedChars) - 1;

        $newToken = "";
        $unique_pass = false;

        while (!$unique_pass) {
            for ($i = 0; $i < 10; $i++) {
                $newToken .= $acceptedChars{mt_rand(0, $max)};
            }

            $token = $this::where('key', '=', $newToken)->first();

            if (isset($token->id)) {
                $newToken = "";
            } else {
                $unique_pass = true;
            }
        }

        return $newToken;
    }

    /**
     * Logic to get the relevant locales for the assessment
     *
     * @return [type]
     */
    public function getLocales()
    {
        // if the locales has been set for the assessment
        // use those, otherwise fall back to the full set
        // from the translatable config
        if ($this->locales != "") {
            $localesArray = explode(",", $this->locales);

            return $localesArray;
        } else {
            return config('translatable.locales');
        }
    }

    /**
     * Get the worker part of the assessment.
     *
     * @return array
     */
    public function getWorkerPart()
    {
        $workerPart = $this->toArray();

        // Override the assessments name.
        $workerPart['name'] = 'Worker Satisfaction Assessment';
        $workerPart['type'] = 'worker';

        // Set the public URL for the worker part.
        $workerPart['public_url'] = url('/assessment/' . $this->key);

        return $workerPart;
    }

    /**
     * Returns just the worker assessment results.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function getWorkerPartResults()
    {
        // Get the employer question set and set it to the assessment in the first create function.
        $employerQuestionSet = new QuestionSet();
        $employerQuestionSet = $employerQuestionSet->where('key', 'rtw-employer')->first();

        return $this->results()->where('question_set_id', '<>', $employerQuestionSet->pluck('id'));
    }

    /**
     * Get the employer part of the assessment.
     *
     * This will essentially return the same as the worker part except the
     * values are returned as if there is only one result.
     *
     * @return array
     */
    public function getEmployerPart()
    {
        $employerPart = $this->toArray();

        // Override the assessments name.
        $employerPart['name'] = 'Employer Metrics Assessment';
        $employerPart['type'] = 'employer';

        // Get the employer question set.
        $employerQuestionSet = new QuestionSet();
        $employerQuestionSet = $employerQuestionSet->where('key', 'rtw-employer')->first();

        $employerPart['completed_count'] = $this->results()
            ->resultsForQuestionSet($employerQuestionSet->id)
            ->completedResults()
            ->count();

        // Set the worker count which will always be 1 for an employer.
        $employerPart['worker_count'] = 1;
        $employerPart['alert_count'] = 'N/A';

        // Set the public URL for the employer part.
        $employerPart['public_url'] = url('/#/assessment/take/' . $this->key);

        return $employerPart;
    }

    /**
     * Gets the parent result record if it exists for employer.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function getEmployerPartParentResults()
    {
        // Get the employer question set and set it to the assessment in the first create function.
        $employerQuestionSet = new QuestionSet();
        $employerQuestionSet = $employerQuestionSet->where('key', 'rtw-employer')->first();

        return $this->results()->where('question_set_id', $employerQuestionSet->pluck('id'));
    }

    /**
     * Format the assessment and all it's questions ready for an assessment.
     *
     * @return array
     */
    public function formatEmployerPartForSubmission()
    {
        $assessment = $this;

        // Convert the assessment model into an array as we no longer need to access Eloquent functions.
        $assessment = $assessment->toArray();

        // Load the employer assessment questions for this assessment.
        $employerQuestionSet = new QuestionSet();
        $employerQuestionSet = $employerQuestionSet->where('key', 'rtw-employer')->first();

        $employerQuestionSet->load([
            'questions',
            'questions.category'
        ]);

        // Attach the questions to the assessment.
        $assessment['question_set'] = $employerQuestionSet->toArray();

        // If this question set has no assessment we can't do anything with it.
        if (!$assessment['question_set']) {
            return null;
        }

        // Firstly, we need to get all unique category names and list them.
        $categories = [];
        $categoriesAsObjects = [];

        foreach ($assessment['question_set']['questions'] as $question) {
            $categoryAsObject = $question['category'];
            $categoryName = $question['category']['name'];

            // Set an empty _questions index on the categories array.
            $categoryAsObject['_questions'] = [];

            if (!in_array($categoryName, $categories)) {
                $categories[] = $categoryName;
                $categoriesAsObjects[] = $categoryAsObject;
            }
        }

        // We set the new formatted categories into the main assessment array.
        $assessment['question_set']['_categories'] = $categoriesAsObjects;

        // Secondly, we need to place all questions for each category under their respective category array index.
        foreach ($assessment['question_set']['questions'] as $question) {
            foreach ($assessment['question_set']['_categories'] as $categoryKey => $category) {
                if ($question['category_id'] == $category['id']) {
                    $assessment['question_set']['_categories'][$categoryKey]['_questions'][] = $question;
                }
            }
        }

        // Thirdly, we send back whether the assessment for this employer has been submitted already or not.
        $results = $this->results()
            ->resultsForQuestionSet($employerQuestionSet->id)
            ->first();

        // If the parent results record exists then get the child employer results table.
        if ($results) {
            $results = $results->resultsEmployer()->first();
        }

        $submitted = $assessment['submitted'] = ($results ? true : false);

        // Finally, we need to send back the results but only if the assessment has been submitted before.
        // If the assessment has results then we need to give this back.
        // They are formatted into an associative array with the key as the kebab-case question
        $assessment['_results'] = [];
        $resultsAsAssoc = [];

        if ($submitted) {
            $results = $results->toArray();

            foreach ($assessment['question_set']['questions'] as $question) {
                $formattedQuestionKey = str_replace('-', '_', $question['key']);

                foreach ($results as $column => $value) {
                    if ($formattedQuestionKey == $column) {
                        $resultsAsAssoc[$question['key']] = $value;
                    }
                }
            }

            // Finally attach the new results to the returned data.
            $assessment['_results'] = $resultsAsAssoc;
        }

        return $assessment;
    }

    /**
     * Attempt to store values in the assessment submission into the database.
     *
     * @param $assessmentData
     * @return bool|Model
     */
    public function submitAssessment($assessmentData)
    {
        $formattedAssessmentData = $this->hotfixFunctionForCleaningUpKeys($assessmentData);

        // Get the employer question set and set it to the assessment in the first create function.
        $employerQuestionSet = new QuestionSet();
        $employerQuestionSet = $employerQuestionSet->where('key', 'rtw-employer')->first();

        $results = $this->results()->resultsForQuestionSet($employerQuestionSet->id)->first();

        if (!$results) {
            // Create the parent results table.
            $results = $this->results()->create([
                'question_set_id' => $employerQuestionSet->id,
                'status' => (isset($formattedAssessmentData['status']) ? 10 : 0)
            ]);

            if (!$results) {
                return false;
            }
        } else {
            if (isset($formattedAssessmentData['status'])) {
                $results->status = 10;
                $results->save();
            }
        }

        // Unset the status field.
        unset($formattedAssessmentData['status']);

        $resultsEmployer = $results->resultsEmployer()->first();

        if (!$resultsEmployer) {
            // Create the child results table.
            $resultsEmployer = $results->resultsEmployer()->create($formattedAssessmentData);

            if (!$resultsEmployer) {
                $results->delete();
                return false;
            }
        } else {
            // Update the assessment.
            $updatedResults = $resultsEmployer->update($formattedAssessmentData);

            if (!$updatedResults) {
                return false;
            }
        }

        return $results;
    }

    /**
     * Attempt to update values in the existing assessment submission.
     *
     * @param $assessmentData
     * @return bool|Model
     */
    public function resubmitAssessment($assessmentData)
    {
        $formattedAssessmentData = $this->hotfixFunctionForCleaningUpKeys($assessmentData);

        // Load the employer assessment questions for this assessment.
        $employerQuestionSet = new QuestionSet();
        $employerQuestionSet = $employerQuestionSet->where('key', 'rtw-employer')->first();

        // If the status field is set then update it in the parent results table.
        $results = $this->results()->resultsForQuestionSet($employerQuestionSet->id)->first();

        if (isset($formattedAssessmentData['status'])) {
            $results->status = 10;
            $results->save();
        }

        // Get the results child table.
        $resultsEmployer = $results->resultsEmployer()->first();

        // Unset the status field.
        unset($formattedAssessmentData['status']);

        // Update the assessment.
        $updatedResults = $resultsEmployer->update($formattedAssessmentData);

        if (!$updatedResults) {
            return false;
        }

        return $updatedResults;
    }

    /**
     * Applies a filter.
     *
     * @param $filters
     * @param $query
     *
     * @return $query
     */
    public function createFilterQuery($filters, $query)
    {
        // If the the assessment key is like the one given.
        if (isset($filters['assessment_key'])) {
            $query->where('key', 'like', '%' . $filters['assessment_key'] . '%');
        }

        // If the assessment starts after the given start date.
        if (isset($filters['start_date'])) {
            $query->where('start_date',
                '<=',
                Carbon::parse($filters['start_date']));
        }

        // If the assessment ends before the given end date.
        if (isset($filters['end_date'])) {
            $query->where('end_date',
                '>=',
                Carbon::parse($filters['end_date']));
        }

        // If the assessment belongs to a given account.
        if (isset($filters['account_id'])) {
            $query->where('account_id', $filters['account_id']);
        }

        return $query;
    }

    /**
     * Performs a string search.
     *
     * @param string $search
     * @param $query
     *
     * @return $query
     */
    public function createSearchQuery($search, $query)
    {
        return $query;
    }

    /**
     * Applies a sort.
     *
     * @param $sort
     * @param $query
     *
     * @return $query
     */
    public function createSortQuery($sort, $query)
    {
        return $query;
    }

    /**
     * All keys in database should really be snake_case rather than kebab-case but can change this later.
     * The below is a little hack to convert them for now.
     *
     * Todo: Later on we will need to make all keys camel_case.
     *
     * @param $assessmentData
     * @return array
     */
    public function hotfixFunctionForCleaningUpKeys($assessmentData)
    {
        $formattedAssessmentData = [];

        foreach ($assessmentData as $fieldKey => $field) {
            $formattedAssessmentData[str_replace('-', '_', $fieldKey)] = $field;
        }

        return $formattedAssessmentData;
    }
}
