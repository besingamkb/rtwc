<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Alert extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'alerts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'diff_for_humans',
    ];

    /**
     * Return this alerts result.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function result()
    {
        return $this->belongsTo(Result::class);
    }

    /**
     * Return this alerts assessment.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function assessment()
    {
        return $this->belongsTo(Assessment::class);
    }

    /**
     * Return this alerts question option.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function questionOption()
    {
        return $this->belongsTo(QuestionOption::class);
    }

    /**
     * Returns the amount of days ago for the alert.
     *
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getDiffForHumansAttribute()
    {
        return $this->created_at->diffForHumans();
    }
}
