<?php

namespace App\Models\Question;

class Type
{
    const TEXT = 1;
    const NUMBER = 2;
    const BOOLEAN = 3;
    const DROPDOWN_SINGLE = 4;
    const DROPDOWN_MULTI = 5;
    const RADIO = 5;

    protected $options = null;

    /**
     * Gets all of the available question types.
     *
     * @return array
     */
    public function show()
    {
        if (is_null($this->options)) {
            $this->options = [
                self::TEXT => 'Text',
                self::NUMBER => 'Number',
                self::BOOLEAN => 'Boolean',
                self::DROPDOWN_SINGLE => 'Dropdown',
                self::DROPDOWN_MULTI => 'Multi-Select Dropdown',
                self::RADIO => 'Radio'
            ];
        }
        return $this->options;
    }
}
