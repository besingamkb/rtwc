<?php

namespace App\Models\Assessment;

class Type
{
    const PARENT = 1;
    const EMPLOYER = 2;
    const WORKER = 3;

    protected $options = null;

    /**
     * Gets all of the available assessment types.
     *
     * @return array
     */
    public function show()
    {
        if (is_null($this->options)) {
            $this->options = [
                self::PARENT => 'Parent Assessment',
                self::EMPLOYER => 'Employer Metrics Assessment',
                self::WORKER => 'Worker Satisfaction Assessment'
            ];
        }
        return $this->options;
    }
}
