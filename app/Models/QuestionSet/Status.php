<?php

namespace App\Models\QuestionSet;

class Status
{
    const DISABLED = 1;
    const ACTIVE = 2;

    protected $options = null;

    /**
     * Gets all of the available assessment types.
     *
     * @return array
     */
    public function show()
    {
        if (is_null($this->options)) {
            $this->options = [
                self::DISABLED => 'Disabled',
                self::ACTIVE => 'Active'
            ];
        }
        return $this->options;
    }
}
