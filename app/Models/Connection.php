<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Connection extends Model
{
    use SoftDeletes;

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'status'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'response_timestamp'
    ];

    /**
     * Returns this connections requestee account.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function requestee()
    {
        return $this->belongsTo(Account::class, 'requestee_id');
    }

    /**
     * Returns this connections requester account.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function requester()
    {
        return $this->belongsTo(Account::class, 'requester_id');
    }

    /**
     * Sets the connection to accepted.
     */
    public function acceptRequest()
    {
        $this->is_accepted = true;
        $this->response_timestamp = Carbon::now();
        $this->save();

        return $this;
    }

    /**
     * Sets the connection to rejected.
     */
    public function rejectRequest()
    {
        $this->is_accepted = false;
        $this->response_timestamp = Carbon::now();
        $this->deleted_at = Carbon::now();
        $this->save();

        return $this;
    }

    /**
     * Determine if the connection is accepted, rejected or requested.
     *
     * @return null|string
     */
    public function getStatusAttribute()
    {
        // If the connection is accepted.
        if ($this->is_accepted == true &&
            $this->response_timestamp != null
        ) {
            return 'accepted';
        }
        // If the connection is rejected.
        if ($this->is_accepted == false &&
            $this->response_timestamp != null
        ) {
            return 'rejected';
        }
        // If the connection is requested.
        if ($this->is_accepted == null &&
            $this->response_timestamp === null
        ) {
            return 'requested';
        }
        return null;
    }

    /**
     * Returns this accounts accepted connections.
     *
     * @param $query
     * @return
     */
    public function scopeAcceptedConnections($query)
    {
        return $query->where('is_accepted', true)->whereNotNull('response_timestamp');
    }

    /**
     * Returns this accounts rejected connections.
     *
     * @param $query
     * @return
     */
    public function scopeRejectedConnections($query)
    {
        return $query->where('is_accepted', false)->whereNotNull('response_timestamp');
    }

    /**
     * Returns this accounts connection requests.
     *
     * @param $query
     * @return
     */
    public function scopeRequestedConnections($query)
    {
        return $query->whereNull('is_accepted')->whereNull('response_timestamp');
    }
}
