<?php

namespace App\Models;

use fpdi\FPDI;

class Report
{
    /**
     * Stores the file name.
     *
     * @var string
     */
    protected $fileName = '.pdf';

    /**
     * Sets the PDF storage directory name.
     *
     * @var string
     */
    protected $pdfDirectory = 'pdfs';

    /**
     * Stores the absolute path for the PDF.
     *
     * @var string
     */
    protected $absPath = '';

    /**
     * Stores the auth user ID.
     *
     * @var null
     */
    protected $userID = null;

    /**
     * Report constructor.
     */
    public function __construct()
    {
        $this->absPath = storage_path($this->pdfDirectory);
        $this->fileName = time() . $this->fileName;
    }

    /**
     * Gets the base PDF file based on a passed section.
     *
     * @param string $section
     * @return string
     */
    protected function getPDFTemplate($section = '')
    {
        $method = 'get' . ucfirst($section) . 'Report';

        return $this->$method();
    }

    /**
     * Gets the blaze PDF template location.
     *
     * @return string
     */
    protected function getDefaultReport()
    {
        return $this->absPath . '/report.pdf';
    }

    /**
     * Gets a report personalised to the passed user user.
     *
     * @param $request
     * @param $userID
     */
    public function getResultsReport($request, $userID)
    {
        $this->userID = $userID;

        $template = $this->getPDFTemplate('default');
        $this->customiseReport($request, $template);
    }

    /**
     * Writes the users name and today's date on to the title page of the PDF.
     *
     * @param $request
     * @param $template
     */
    protected function customiseReport($request, $template)
    {
        // Create new instance of FPDI.
        $pdf = new FPDI();

        // Set the font styles for the PDF.
        $font = 'roboto';
        $fontLight = 'roboto-light';

        $pdf->fontpath = storage_path('/pdfs/fonts/');
        $pdf->addFont($font, '', 'Roboto-Regular.php');
        $pdf->addFont($fontLight, '', 'Roboto-Light.php');
        $pdf->SetFont($font, '', 15);

        // Get the max pages.
        $pageCount = $pdf->setSourceFile($template);

        // Set the score totals results.
        $scoreResults = $this->getBaseQuery($request);
        $scoreResults = $scoreResults->buildScoreTotalsQuery()->get();
        $scoreResults = Reporting::castTotals($scoreResults, 'total_score');
        $totalScore = $scoreResults[0]->total_score;

        // Get the categories data data
        $categoryResults = $this->getBaseQuery($request);
        $categoryResults = $categoryResults->buildCategoriesQuery()->get();
        $categoryResults = Reporting::appendParentCategoryTotals($categoryResults, true);
        $categoryResults = Reporting::castTotals($categoryResults);

        // Get the demographics results.
        $demographicResults = $this->getBaseQuery($request);
        $demographicResults = $demographicResults->buildSummaryQuery()->get();
        $demographicResults = Reporting::formatAssociativeByKey($demographicResults);

        // Get the detail results.
        $detailResults = $this->getBaseQuery($request);
        $detailResults = $detailResults->buildDetailQuery()->get();
        $detailResults = Reporting::castTotals($detailResults);

        // Get the metrics results.
        $metricResults = $this->getMetricBaseQuery($request);
        $metricResults = $metricResults->buildMetricsQuery()->get();
        $metricResults = Reporting::castTotals($metricResults);
        $metricResults = Reporting::formatAssociativeByKey($metricResults);

        foreach ($demographicResults as $resultKey => $result) {
            $demographicResults[$resultKey] = array_map(function ($result) {
                return [
                    'name' => $result->text,
                    'y' => $result->count
                ];
            }, $demographicResults[$resultKey]);
        }

        // Iterate over all the pages.
        for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
            $templateId = $pdf->importPage($pageNo);

            $size = $pdf->getTemplateSize($templateId);

            if ($size['w'] > $size['h']) {
                $pdf->AddPage('L', array($size['w'], $size['h']));
            } else {
                $pdf->AddPage('P', array($size['w'], $size['h']));
            }

            $pdf->useTemplate($templateId);

            if ($pageNo == 1) {
                $pdf->setFont($font);
                $pdf->SetTextColor(0, 0, 0);
                $pdf->SetFontSize(40);

                $pdf->SetXY(0, 140);
            }

            if ($pageNo == 4) {
                // Total score.
                $pdf->SetTextColor(170, 199, 17);
                $pdf->SetFontSize(24);
                $pdf->SetXY(163, 54);
                $pdf->Cell(17, 10, round($totalScore) . '%', 0, 1, 'C');

                // Adjust score results array for use with highcharts.
                $scoreResults = array_map(function ($result) {
                    return ['name' => $result->name, 'y' => $result->total];
                }, $categoryResults);

                // Order results by total.
                $scoreResults = array_values(array_sort($scoreResults, function ($result) {
                    return $result['y'];
                }));

                // Get top 3 results and bottom 3 results.
                $lowest3 = array_slice($scoreResults, 0, 3);
                $top3 = array_slice($scoreResults, -3, 3);

                // Create the bottom 3 result chart.
                $chart = new Chart;
                $chart->makeChart($lowest3, 'bar', 'bar-lowest');
                $pdf->Image(storage_path('charts/tmp/' . $this->userID . '/bar-lowest.png'), 30, 91.5, -300);

                // Create the top 3 result chart.
                $chart = new Chart;
                $chart->makeChart($top3, 'bar', 'bar-highest');
                $pdf->Image(storage_path('charts/tmp/' . $this->userID . '/bar-highest.png'), 30, 160.5, -300);

                // Workout total alert count.
                $totalAlerts = array_reduce($detailResults, function ($a, $b) {
                    return $a + $b->alerts;
                });

                // Alert count.
                $pdf->SetTextColor(170, 199, 17);
                $pdf->SetFontSize(24);
                $pdf->SetXY(55, 238);
                $pdf->Cell(17, 10, $totalAlerts, 0, 1, 'L');
            }

            if ($pageNo == 5) {
                // Get percentages for male and female gender types.
                $maleGender = array_filter($demographicResults['gender'], function ($gender) {
                    return $gender['name'] == 'Male';
                });

                $femaleGender = array_filter($demographicResults['gender'], function ($gender) {
                    return $gender['name'] == 'Female';
                });

                $maleGenderAmount = isset($maleGender[array_keys($maleGender)[0]]) ? $maleGender[array_keys($maleGender)[0]]['y'] : 0;
                $femaleGenderAmount = isset($femaleGender[array_keys($femaleGender)[0]]) ? $femaleGender[array_keys($femaleGender)[0]]['y'] : 0;

                $genderTotal = $maleGenderAmount + $femaleGenderAmount;

                $maleGenderPercentage = $maleGenderAmount / $genderTotal * 100;
                $femaleGenderPercentage = $femaleGenderAmount / $genderTotal * 100;

                // Male percentage.
                $pdf->SetTextColor(0, 159, 227);
                $pdf->SetFontSize(24);
                $pdf->SetXY(80, 91);
                $pdf->Cell(17, 10, round($maleGenderPercentage) . '%', 0, 1, 'L');

                // Female percentage.
                $pdf->SetTextColor(81, 40, 78);
                $pdf->SetFontSize(24);
                $pdf->SetXY(125, 91);
                $pdf->Cell(17, 10, round($femaleGenderPercentage) . '%', 0, 1, 'L');

                // Create the age chart.
                $chart = new Chart;
                $chart->makeChart($demographicResults['age'], 'bar', 'bar-age');
                $pdf->Image(storage_path('charts/tmp/' . $this->userID . '/bar-age.png'), 30, 142, -300);

                // Create employment type chart.
                $chart = new Chart;
                $chart->makeChart($demographicResults['employsyou'], 'bar', 'bar-employsyou');
                $pdf->Image(storage_path('charts/tmp/' . $this->userID . '/bar-employsyou.png'), 30, 210, -300);
            }

            if ($pageNo == 6) {
                // create the job position chart
                $chart = new Chart;
                $chart->makeChart($demographicResults['level'], 'bar', 'bar-level');
                $pdf->Image(storage_path('charts/tmp/' . $this->userID . '/bar-level.png'), 30, 74, -300);

                // create the job role chart
                $chart = new Chart;
                $chart->makeChart($demographicResults['role'], 'bar', 'bar-role');
                $pdf->Image(storage_path('charts/tmp/' . $this->userID . '/bar-role.png'), 30, 142, -300);

                // create the shift type chart
                $chart = new Chart;
                $chart->makeChart($demographicResults['shift'], 'bar', 'bar-shift');
                $pdf->Image(storage_path('charts/tmp/' . $this->userID . '/bar-shift.png'), 30, 209, -300);
            }

            if ($pageNo == 7) {
                // create the job tenure chart
                $chart = new Chart;
                $chart->makeChart($demographicResults['howlong'], 'bar', 'bar-howlong');
                $pdf->Image(storage_path('charts/tmp/' . $this->userID . '/bar-howlong.png'), 30, 74, -300);

                // create the ethnicity chart
                $chart = new Chart;
                $chart->makeChart($demographicResults['ethnicity'], 'bar', 'bar-ethnicity', ['h' => 355]);
                $pdf->Image(storage_path('charts/tmp/' . $this->userID . '/bar-ethnicity.png'), 30, 142, -300);
            }

            if ($pageNo == 8) {
                // total score in the top right
                $pdf->SetTextColor(170, 199, 17);
                $pdf->SetFontSize(24);
                $pdf->SetXY(162, 48);
                $pdf->Cell(17, 10, round($totalScore) . '%', 0, 1, 'C');

                $pdf->SetXY(31, 91);

                foreach ($categoryResults as $category) {
                    // just do the parent categories
                    if ($category->parent_id != 0) {
                        continue;
                    }

                    $y = $pdf->GetY();
                    $y += 5;
                    $pdf->SetFont($font, '', 9);
                    $pdf->SetTextColor(0, 0, 0);
                    $pdf->SetDrawColor(29, 29, 27);
                    $pdf->SetXY(31, $y);
                    $pdf->Cell(111, 9.3, $category->name, 0, 1, 'L');
                    $pdf->SetXY(151, $y);
                    $pdf->Cell(17, 9.3, round($category->total) . '%', 0, 1, 'L');
                    $pdf->Line(32, $pdf->GetY(), 176, $pdf->GetY());

                    // then find the child categories
                    foreach ($categoryResults as $childCategory) {
                        if ($childCategory->parent_id != $category->id) {
                            continue;
                        }

                        $y = $pdf->GetY();
                        $pdf->SetFont($fontLight, '', 9);
                        $pdf->SetDrawColor(218, 218, 218);
                        $pdf->SetX(31);
                        $pdf->Cell(111, 9.3, $childCategory->name, 0, 1, 'L');
                        $pdf->SetXY(151, $y);
                        $pdf->Cell(17, 9.3, round($childCategory->total) . '%', 0, 1, 'L');
                        $pdf->Line(32, $pdf->GetY(), 176, $pdf->GetY());
                    }
                }

                // total score in the bottom right
                $pdf->SetTextColor(170, 199, 17);
                $pdf->SetFont($font, '', 16);
                $pdf->SetXY(148, 237);
                $pdf->Cell(17, 10, round($totalScore) . '%', 0, 1, 'C');
            }

            if ($pageNo == 9) {
                $pdf->SetXY(31, 63);

                $currentCategoryId = 0;
                $categoryCount = 0;
                $page1Count = 6;

                foreach ($detailResults as $result) {
                    // print out the category once per category
                    if ($currentCategoryId != $result->category_id) {

                        // if we've done 4 categories on this page stop in 
                        // order to continue on the next page
                        if ($categoryCount == $page1Count) {
                            break;
                        }

                        $y = $pdf->GetY();
                        $y += 3;
                        $pdf->SetFont($font, '', 9);
                        $pdf->SetTextColor(0, 0, 0);
                        $pdf->SetDrawColor(29, 29, 27);
                        $pdf->SetXY(31, $y);
                        $pdf->Cell(111, 9.3, $result->category_name, 0, 1, 'L');
                        $pdf->Line(32, $pdf->GetY(), 176, $pdf->GetY());

                        $currentCategoryId = $result->category_id;

                        $categoryCount++;
                    }

                    // then print out the question
                    $y = $pdf->GetY();
                    $pdf->SetFont($fontLight, '', 9);
                    $pdf->SetDrawColor(218, 218, 218);
                    $pdf->SetXY(166, $y);
                    $pdf->Cell(17, 9.3, round($result->total) . '%', 0, 1, 'L');
                    $pdf->SetXY(31, $y + 2.2);
                    $pdf->MultiCell(125, 5, $result->text, 0, 'L');
                    $pdf->SetY($pdf->GetY() + 2.2);
                    $pdf->Line(32, $pdf->GetY(), 176, $pdf->GetY());
                }
            }

            if ($pageNo == 10) {
                $pdf->SetXY(31, 63);

                $currentCategoryId = 0;
                $categoryCount = 0;

                foreach ($detailResults as $result) {
                    // print out the category once per category
                    if ($currentCategoryId != $result->category_id) {
                        // skip the first 4 categories as we did those on previous page
                        $currentCategoryId = $result->category_id;
                        $categoryCount++;

                        if ($categoryCount <= $page1Count) {
                            continue;
                        }

                        $y = $pdf->GetY();
                        $y += 3;
                        $pdf->SetFont($font, '', 9);
                        $pdf->SetTextColor(0, 0, 0);
                        $pdf->SetDrawColor(29, 29, 27);
                        $pdf->SetXY(31, $y);
                        $pdf->Cell(111, 9.3, $result->category_name, 0, 1, 'L');
                        $pdf->Line(32, $pdf->GetY(), 176, $pdf->GetY());
                    }

                    // skip the first 4 categories as we did those on previous page
                    if ($categoryCount <= $page1Count) {
                        continue;
                    }

                    // then print out the question
                    $y = $pdf->GetY();
                    $pdf->SetFont($fontLight, '', 9);
                    $pdf->SetDrawColor(218, 218, 218);
                    $pdf->SetXY(166, $y);
                    $pdf->Cell(17, 9.3, round($result->total) . '%', 0, 1, 'L');
                    $pdf->SetXY(31, $y + 2.2);
                    $pdf->MultiCell(125, 5, $result->text, 0, 'L');
                    $pdf->SetY($pdf->GetY() + 2.2);
                    $pdf->Line(32, $pdf->GetY(), 176, $pdf->GetY());
                }
            }

            if ($pageNo == 11) {
                // Cost of grievance.
                $pdf->SetTextColor(234, 99, 79);
                $pdf->SetFontSize(18);
                $pdf->SetXY(43, 128);
                $pdf->Cell(17, 10,
                    iconv('UTF-8', 'ISO-8859-1', '£') . number_format(round($metricResults['cost_of_grievances'][0]->total)), 0, 1,
                    'L');

                $pdf->SetTextColor(234, 99, 79);
                $pdf->SetFontSize(12);
                $pdf->SetXY(43, 140);
                $pdf->Cell(17, 10,
                    iconv('UTF-8', 'ISO-8859-1', '£') . number_format(round($metricResults['cost_of_grievances'][0]->average)), 0,
                    1, 'L');

                // Cost of mediation.
                $pdf->SetTextColor(234, 99, 79);
                $pdf->SetFontSize(18);
                $pdf->SetXY(96, 128);
                $pdf->Cell(17, 10,
                    iconv('UTF-8', 'ISO-8859-1', '£') . number_format(round($metricResults['cost_of_mediation'][0]->total)), 0, 1,
                    'L');

                $pdf->SetTextColor(0, 0, 0);
                $pdf->SetFontSize(8);
                $pdf->SetXY(96, 140);
                $pdf->Cell(17, 10,
                    'No data available', 0,
                    1, 'L');

                // Cost of disciplinary activity.
                $pdf->SetTextColor(64, 182, 165);
                $pdf->SetFontSize(18);
                $pdf->SetXY(151, 128);
                $pdf->Cell(17, 10,
                    iconv('UTF-8', 'ISO-8859-1', '£') . number_format(round($metricResults['cost_of_disciplinary_activity'][0]->total)), 0, 1, 'L');

                $pdf->SetTextColor(64, 182, 165);
                $pdf->SetFontSize(12);
                $pdf->SetXY(151, 140);
                $pdf->Cell(17, 10,
                    iconv('UTF-8', 'ISO-8859-1', '£') . number_format(round($metricResults['cost_of_disciplinary_activity'][0]->average)), 0, 1, 'L');

                // Cost of recruitment.
                $pdf->SetTextColor(170, 199, 22);
                $pdf->SetFontSize(18);
                $pdf->SetXY(43, 179);
                $pdf->Cell(17, 10,
                    iconv('UTF-8', 'ISO-8859-1', '£') . number_format(round($metricResults['cost_of_recruitment'][0]->total)), 0, 1,
                    'L');

                $pdf->SetTextColor(0, 0, 0);
                $pdf->SetFontSize(8);
                $pdf->SetXY(43, 190);
                $pdf->Cell(17, 10,
                    'No data available', 0, 1,
                    'L');

                // Training costs per employee.
                $pdf->SetTextColor(234, 99, 79);
                $pdf->SetFontSize(18);
                $pdf->SetXY(96, 179);
                $pdf->Cell(17, 10, iconv('UTF-8', 'ISO-8859-1',
                        '£') . number_format(round($metricResults['cost_of_training_per_employee'][0]->total)), 0, 1, 'L');

                $pdf->SetTextColor(234, 99, 79);
                $pdf->SetFontSize(12);
                $pdf->SetXY(96, 190);
                $pdf->Cell(17, 10, iconv('UTF-8', 'ISO-8859-1',
                        '£') . number_format(round($metricResults['cost_of_training_per_employee'][0]->average)), 0, 1, 'L');

                // Training costs per agency worker.
                $pdf->SetTextColor(81, 191, 211);
                $pdf->SetFontSize(18);
                $pdf->SetXY(151, 179);
                $pdf->Cell(17, 10, iconv('UTF-8', 'ISO-8859-1',
                        '£') . number_format(round($metricResults['cost_of_training_per_agency_worker'][0]->total)), 0, 1, 'L');

                $pdf->SetTextColor(81, 191, 211);
                $pdf->SetFontSize(12);
                $pdf->SetXY(151, 190);
                $pdf->Cell(17, 10, iconv('UTF-8', 'ISO-8859-1',
                        '£') . number_format(round($metricResults['cost_of_training_per_agency_worker'][0]->average)), 0, 1, 'L');
            }

            if ($pageNo == 12) {
                // Cost of labour provider per worker.
                $pdf->SetTextColor(77, 142, 181);
                $pdf->SetFontSize(18);
                $pdf->SetXY(43, 81);
                $pdf->Cell(17, 10, iconv('UTF-8', 'ISO-8859-1',
                        '£') . number_format(round($metricResults['cost_of_labour_provider_per_worker'][0]->total)), 0, 1, 'L');

                $pdf->SetTextColor(77, 142, 181);
                $pdf->SetFontSize(12);
                $pdf->SetXY(43, 93);
                $pdf->Cell(17, 10, iconv('UTF-8', 'ISO-8859-1',
                        '£') . number_format(round($metricResults['cost_of_labour_provider_per_worker'][0]->average)), 0, 1, 'L');

                // Turnover rate.
                $pdf->SetTextColor(64, 182, 165);
                $pdf->SetFontSize(18);
                $pdf->SetXY(96, 81);
                $pdf->Cell(17, 10, round($metricResults['turnover_rate'][0]->total, 2) . '%', 0, 1, 'L');

                $pdf->SetTextColor(64, 182, 165);
                $pdf->SetFontSize(12);
                $pdf->SetXY(96, 93);
                $pdf->Cell(17, 10, round($metricResults['turnover_rate'][0]->average, 2) . '%', 0, 1, 'L');

                // Average days taken per employee.
                $pdf->SetTextColor(170, 199, 22);
                $pdf->SetFontSize(18);
                $pdf->SetXY(151, 81);
                $pdf->Cell(17, 10, round($metricResults['average_unauthorised_absence_days'][0]->total, 2), 0, 1, 'L');

                $pdf->SetTextColor(170, 199, 22);
                $pdf->SetFontSize(12);
                $pdf->SetXY(151, 93);
                $pdf->Cell(17, 10, round($metricResults['average_unauthorised_absence_days'][0]->average, 2), 0, 1,
                    'L');

                // Cost of unauthorised absence.
                $pdf->SetTextColor(77, 142, 181);
                $pdf->SetFontSize(18);
                $pdf->SetXY(43, 132);
                $pdf->Cell(17, 10,
                    iconv('UTF-8', 'ISO-8859-1', '£') . number_format(round($metricResults['cost_of_unauthorised_absence'][0]->total)), 0, 1, 'L');

                $pdf->SetTextColor(77, 142, 181);
                $pdf->SetFontSize(12);
                $pdf->SetXY(43, 144);
                $pdf->Cell(17, 10, iconv('UTF-8', 'ISO-8859-1',
                        '£') . number_format(round($metricResults['cost_of_unauthorised_absence'][0]->average, 2)), 0, 1, 'L');

                // Cost of unauthorised absence per employee.
                $pdf->SetTextColor(77, 142, 181);
                $pdf->SetFontSize(18);
                $pdf->SetXY(96, 132);
                $pdf->Cell(17, 10, iconv('UTF-8', 'ISO-8859-1',
                        '£') . number_format(round($metricResults['cost_of_unauthorised_absence_per_employee'][0]->total)), 0, 1,
                    'L');

                $pdf->SetTextColor(77, 142, 181);
                $pdf->SetFontSize(12);
                $pdf->SetXY(96, 144);
                $pdf->Cell(17, 10, iconv('UTF-8', 'ISO-8859-1',
                        '£') . number_format(round($metricResults['cost_of_unauthorised_absence_per_employee'][0]->average, 2)), 0, 1,
                    'L');
            }
        }

        $filename = explode('/', $template);
        $filename = end($filename);

        $pdf->Output($filename, 'I');
    }

    /**
     * Gets the base query.
     * Replicates the base query stored in the API Reporting Controller.
     *
     * @param $request
     * @return Reporting
     */
    protected function getBaseQuery($request)
    {
        $assessments = explode(',', $request->input('assessments'));
        $baseResults = new \App\Models\Reporting();
        $baseResults = $baseResults->buildGenericQuery($assessments);

        $filters = $request->input('filters');

        if ($filters != '' && $filters != 'undefined') {
            $filters = explode(',', $filters);
            $baseResults = $baseResults->applyFiltersToQuery($filters);
        }

        return $baseResults;
    }

    /**
     * Gets the base query for metrics.
     * Replicates the base query stored in the API Reporting Controller.
     *
     * @param $request
     * @return Reporting
     */
    protected function getMetricBaseQuery($request)
    {
        $assessments = explode(',', $request->input('assessments'));
        $baseResults = new \App\Models\Reporting();
        $baseResults = $baseResults->buildGenericEmployerQuery($assessments);

        $filters = $request->input('filters');

        if ($filters != '' && $filters != 'undefined') {
            $filters = explode(',', $filters);
            $baseResults = $baseResults->applyFiltersToQuery($filters);
        }

        return $baseResults;
    }
}
