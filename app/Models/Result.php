<?php

namespace App\Models;

use App\Models\Result\Employer;
use App\Models\Result\Worker;
use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The relations to eager load on every query.
     *
     * One of these will always be empty because the assessment type can only be one or the other.
     * There should be hardly no speed difference because a query on an empty
     * join is almost the same as a query with no join.
     *
     * Be wary that doing a raw query builder select will not attach these child tables in the results.
     *
     * @var array
     */
    // Todo: This is broken so commented out.
    /*protected $with = [
        'resultsWorker',
        'resultsEmployer'
    ];*/

    /**
     * Return the assessment for this result.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function assessment()
    {
        return $this->belongsTo(Assessment::class);
    }

    /**
     * Returns the actual results for this parent tables assessment.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function resultsWorker()
    {
        return $this->hasOne(Worker::class);
    }

    /**
     * Returns the actual results for this parent tables assesment.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function resultsEmployer()
    {
        return $this->hasOne(Employer::class);
    }

    /**
     * Returns the alerts for this result.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function alerts()
    {
        return $this->hasMany(Alert::class);
    }

    /**
     * new
     *
     * Creates a new result
     *
     * @param $input array
     * @return $id int
     */
    public function new($input, $locale, $assessment)
    {
        $this->assessment_id = $assessment->id;
        $this->status = 0;
        $this->question_set_id = $assessment->questionset->id;
        $this->ip_address = $this->getIpAddress();
        $this->locale = $locale;
        $this->save();

        foreach ($input as $key => $value) {
            // we only care about question key/value pairs
            if (strrpos($key, config('assessment.form_name_prefix')) === 0) {
                $questionId = str_replace(config('assessment.form_name_prefix'), "", $key);

                // store the answer in the answers table
                $answer = Answer::firstOrNew(['result_id' => $this->id, 'question_id' => $questionId]);
                $answer->question_option_id = $value;
                $answer->save();

                // also store answer in results table
                $questionKey = $answer->question->key;
                $this->$questionKey = $value;
            }
        }

        $this->save();

        return $this->id;
    }

    /**
     * complete
     *
     * Completes an assessment and stores the answers
     *
     * @param $input array
     * @return $id int
     */
    public function complete($input)
    {
        $alerts = 0;
        $categoriesMax = [];

        // store the answers against the result
        foreach ($input as $key => $value) {
            // we only care about question key/value pairs
            if (strrpos($key, config('assessment.form_name_prefix')) === 0) {
                $questionId = str_replace(config('assessment.form_name_prefix'), "", $key);

                // store the answer in the db
                $answer = Answer::firstOrNew(['result_id' => $this->id, 'question_id' => $questionId]);
                $answer->question_option_id = $value;
                $answer->save();

                $question = $answer->question;

                if ($question->category->key != "") {
                    $this->total_score += $answer->questionOption->score;

                    $categoryKey = $question->category->key . "_score";

                    $categoryMax = (isset($categoriesMax[$categoryKey]) ? $categoriesMax[$categoryKey] : 0) + $question->max_score;
                    $categoriesMax[$categoryKey] = $categoryMax;

                    $this->$categoryKey += $answer->questionOption->score;
                }

                // alerts
                if ($answer->questionOption->alert) {
                    $alerts++;

                    $alertText = "Category: " . $question->category->name . "\n" .
                        $question->text . "?";

                    $alert = new Alert();
                    $alert->assessment_id = $this->assessment->id;
                    $alert->result_id = $this->id;
                    $alert->question_option_id = $answer->questionOption->id;
                    $alert->text = $alertText;
                    $alert->save();
                }
            }
        }

        // adjust the total score to be a percentage 
        $this->total_score = $this->total_score / $this->assessment->questionSet->max_score;

        // save the number of alerts against the result
        $this->alert_count = $alerts;

        // adjust the category scores to be percentages
        foreach($categoriesMax as $key => $categoryMax)
        {
            $this->$key = $this->$key / $categoryMax;
        }

        $this->status = 10;

        if ($this->save()) {
            $assessment = $this->assessment;
            $assessment->completed_count++;
            $assessment->alert_count += $alerts;
            $assessment->save();
        }
    }

    /**
     * additional
     *
     * Stores the additional text field against the result
     *
     * @param $input array
     * @return $id int
     */
    public function additional($input)
    {
        $this->additional_text = $input["additional"];

        $this->save();
    }

    private function getIpAddress()
    {
        // get real IP address
        $ipAddress = "";
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ipAddresses = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ipAddress = trim(end($ipAddresses));
        } else {
            $ipAddress = $_SERVER['REMOTE_ADDR'];
        }

        return $ipAddress;
    }

    /**
     * Return results for a given question set.
     *
     * @param $query
     * @param $questionSetID
     * @return mixed
     */
    public function scopeResultsForQuestionSet($query, $questionSetID)
    {
        return $query->where('question_set_id', $questionSetID);
    }

    /**
     * Return opposite results for a given question set.
     *
     * @param $query
     * @param $questionSetID
     * @return mixed
     */
    public function scopeResultsForOppositeQuestionSet($query, $questionSetID)
    {
        return $query->where('question_set_id', '<>', $questionSetID);
    }

    /**
     * Return results that have been fully completed.
     *
     * @param $query
     * @return mixed
     */
    public function scopeCompletedResults($query)
    {
        return $query->where('status', 10);
    }
}
