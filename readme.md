[![Build Status](https://travis-ci.com/sotechnology/rtw-cyan.svg?token=ykXVQko3gcrz1s9sZUmm&branch=develop)](https://travis-ci.com/sotechnology/rtw-cyan)

## Installation

 <br /> Clone the git repo - `git clone https://github.com/sotechnology/test-skeleton.git`
 <br /> Run `composer install`
 <br /> npm install -g gulp bower
 <br /> npm install
 <br /> bower install
 <br /> Copy `.env.example` to `.env` and update environment variables:
 <br /> fix database credentials in .env
 <br /> Ammend Homestead.yaml and your hosts file.
 <br /> homestead up
 <br /> vagrant up
 <br /> php artisan migrate
 <br /> gulp
