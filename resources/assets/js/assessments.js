$(document).ready(function(){

	//*========== DEMOGRAHPICS ==============*//

	if($('.demographics').length){

		$('[data-toggle="popover"]').popover({
        	html : true,
        	container: 'body'
    	});

		$(document).on('submit', '.demographics form', function(e){
			if(validate())
			{
				$('.demographics .btn-submit').hide();
				$('.demographics .loader').show();
			}
			else
			{
				e.preventDefault();
			}
		});

		$(document).on('change', '.demographics select', function(){
			if($(this).val()!="") {
				$(this).closest('.form-group').removeClass('bg-danger');
			} else {
				$(this).closest('.form-group').addClass('bg-danger');
			}
		});

		var validate = function () {
			$valid = true;
			$('.demographics .form-group:visible').each(function( index ) {
				if($(this).find('select').val()=="")
				{
					$(this).addClass('bg-danger');
					$valid = false;
				}
			});
			return $valid;
		}

	}

	//*========== QUESTIONS ==============*//

	if($('.questions').length){

		$('[data-toggle="popover"]').popover();

		var elements = $(".questions .question");
		var index = 0;

		$(document).on('click', '.questions .next-btn', function(){

			if(validateCurrentFour())
			{
				if(showNextFour()===false)
				{
					$('.questions .question').hide();
					$('.questions .next-btn').hide();
					$('.questions .loader').show();
					$('.questions form').submit();
				}
			}

		});

		$(document).on('change', '.questions input', function(){
			$(this).closest('.form-group').removeClass('bg-danger');
		});

		var showNextFour = function () {
		    if (index >= elements.length) {
		        return false;
		    }
		    elements.hide().slice(index, index+4).fadeIn();
		    index = index+4;
		    return true;
		}

		var validateCurrentFour = function () {
			$valid = true;
			$('.questions .question .form-group:visible').each(function( index ) {
				if(!$(this).find('input:checked').val())
				{
					$(this).addClass('bg-danger');
					$valid = false;
				}
			});
			return $valid;
		}

		showNextFour(0);

	}


	//*========== ADDITIONAL TEXT FIELD ==============*//

	if($('.additional').length){

		$(document).on('click', '.additional .next-btn', function(){

			$('.additional .question').hide();
			$('.additional .next-btn').hide();
			$('.additional .loader').show();

		});

	}
});
