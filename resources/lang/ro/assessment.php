<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Assessment Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during the assessment for various
    | messages that we need to display to the user that fall outside the data
    | driven text
    |
    */

    'welcome' => 'Bună ziua și bun venit la Responsible Trade Worldwide',
    'language' => 'Selectați limba',
    'next' => 'Înainte',
    'end' => 'Încheiere evaluare',
    'please_select' => 'Selectați',
    'your_details' => 'Detaliile dumneavoastră',
    'thank_you' => 'Vă mulțumim!',
    'additional' => 'Vă rugăm să vă împărtășiți gândurile, sentimentele și opiniile cu privire la locul de muncă aici',
];
