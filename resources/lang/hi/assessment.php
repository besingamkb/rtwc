<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Assessment Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during the assessment for various
    | messages that we need to display to the user that fall outside the data
    | driven text
    |
    */

    'dir' => 'ltr',
    'welcome' => 'हेलो और रेस्पॉन्सिबल ट्रेल वर्ल्डवाइड में आपका स्वागत है',
    'language' => 'भाषा',
    'next' => 'अगला',
    'end' => 'अंत आकलन',
    'please_select' => 'कृपया चुनें',
    'your_details' => 'आपके विवरण',
    'thank_you' => 'धन्यवाद',
    'additional' => 'अपने कार्यस्थल के बारे में यहाँ अपने विचारों, भावनाओं और विचारों को साझा करें',
];
