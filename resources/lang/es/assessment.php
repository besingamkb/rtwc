<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Assessment Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during the assessment for various
    | messages that we need to display to the user that fall outside the data
    | driven text
    |
    */

    'welcome' => 'Bienvenido a Responsible Trade Worldwide',
    'language' => 'Seleccione su idioma',
    'next' => 'Siguiente',
    'end' => 'Evaluación Final',
    'please_select' => 'Seleccione',
    'your_details' => 'Sus datos',
    'thank_you' => 'Gracias',
    'additional' => 'Por favor comparta sus pensamientos, sentimientos y puntos de vista acerca de su lugar de trabajo aquí',
];
