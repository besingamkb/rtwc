<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Assessment Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during the assessment for various
    | messages that we need to display to the user that fall outside the data
    | driven text
    |
    */

    'dir' => 'ltr',
    'welcome' => 'હેલો અને રિસ્પોન્સિબલ ટ્રેડ વર્લ્ડવાઇડમાં સ્વાગત છે',
    'language' => 'ભાષા',
    'next' => 'આગામી',
    'end' => 'મારા આકારણી અંત',
    'please_select' => 'કૃપા કરીને પસંદ કરો',
    'your_details' => 'તમારી વિગતો',
    'thank_you' => 'આપનો આભાર',
    'additional' => 'તમારા કાર્યસ્થળે અમારા વિશે અહીં તમારા વિચારો, લાગણીઓ અને મંતવ્યો શેર કરો',
];
