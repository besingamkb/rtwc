<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Assessment Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during the assessment for various
    | messages that we need to display to the user that fall outside the data
    | driven text
    |
    */

    'dir' => 'ltr',
    'welcome' => 'Herzlich Willkommen bei Responsible Trade Worldwide',
    'language' => 'Sprachex',
    'next' => 'Weiter',
    'end' => 'Ende Bewertung',
    'please_select' => 'Bitte wählen Sie aus',
    'your_details' => 'Ihre Angaben',
    'thank_you' => 'Vielen Dank',
    'additional' => 'Bitte teilen Sie Ihre Gedanken, Gefühle und Ansichten über Ihren Arbeitsplatz hier',
];
