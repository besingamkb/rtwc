<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Assessment Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during the assessment for various
    | messages that we need to display to the user that fall outside the data
    | driven text
    |
    */

    'dir' => 'ltr',
    'welcome' => '您好，欢迎负责任的全球贸易',
    'language' => '语言',
    'next' => '下一步',
    'end' => '結束我的評價',
    'please_select' => '请选择',
    'your_details' => '您的资料',
    'thank_you' => '谢谢',
    'additional' => '請分享您對這裡的工作場所的想法，感受和看法',
];
