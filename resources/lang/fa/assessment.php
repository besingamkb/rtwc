<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Assessment Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during the assessment for various
    | messages that we need to display to the user that fall outside the data
    | driven text
    |
    */

    'dir' => 'rtl',
    'welcome' => 'سلام و به مسئول تجارت جهانی خوش آمدید',
    'language' => 'زبان',
    'next' => 'بعدی',
    'end' => 'پایان دادن به ارزیابی من',
    'please_select' => 'لطفا انتخاب کنید',
    'your_details' => 'طلاعات شما',
    'thank_you' => 'با تشکر از شما',
    'additional' => 'لطفا افکار خود، احساسات و دیدگاه در مورد محل کار خود را اینجا به اشتراک بگذارید',
];
