<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Assessment Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during the assessment for various
    | messages that we need to display to the user that fall outside the data
    | driven text
    |
    */

    'dir' => 'ltr',
    'welcome' => 'Приветствуем Вас на сайте компании Responsible Trade Worldwide!',
    'language' => 'Язык',
    'next' => 'Далее',
    'end' => 'Конец Оценка',
    'please_select' => 'Выберите',
    'your_details' => 'Сведения о Вас',
    'thank_you' => 'Спасибо',
    'additional' => 'Пожалуйста, поделитесь своими мыслями, чувствами и мнениями о вашем рабочем месте здесь',
];
