<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Assessment Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during the assessment for various
    | messages that we need to display to the user that fall outside the data
    | driven text
    |
    */

    'welcome' => 'Olá e bem-vindo à Responsible Trade Worldwide',
    'language' => 'Seleccione o idioma',
    'next' => 'Seguinte',
    'end' => 'Terminar minha avaliação',
    'please_select' => 'Please select',
    'your_details' => 'Os seus dados',
    'thank_you' => 'Obrigado',
    'additional' => 'Por favor, compartilhe seus pensamentos, sentimentos e pontos de vista sobre o seu local de trabalho aqui',
];
