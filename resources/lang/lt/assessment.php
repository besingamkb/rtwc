<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Assessment Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during the assessment for various
    | messages that we need to display to the user that fall outside the data
    | driven text
    |
    */

    'welcome' => 'Sveiki, Jus sveikina „Responsible Trade Worldwide',
    'language' => 'Pasirinkite kalbą',
    'next' => 'Toliau',
    'end' => 'Pabaiga vertinimas',
    'please_select' => 'Pasirinkite',
    'your_details' => 'Jūsų duomenys',
    'thank_you' => 'Ačiū',
    'additional' => 'Prašome pasidalinti savo mintis, jausmus ir nuomonę apie savo darbo vietoje čia',
];
