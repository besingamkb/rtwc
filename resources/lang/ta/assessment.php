<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Assessment Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during the assessment for various
    | messages that we need to display to the user that fall outside the data
    | driven text
    |
    */

    'dir' => 'ltr',
    'welcome' => 'வணக்கம். ரெஸ்பான்ஸிபல் டிரேட் வோர்ல்டுவைடுக்கு உங்களை வரவேற்கிறோம்',
    'language' => 'மொழி',
    'next' => 'அடுத்து',
    'end' => 'என்னுடைய மதிப்பீட்டை முடிக்கவும்',
    'please_select' => 'தயவுசெய்து தேர்ந்தெடுக்கவும்',
    'your_details' => 'உங்களுடைய விவரங்கள்',
    'thank_you' => 'நன்றி',
    'additional' => 'இங்கே உங்கள் பணியிடத்தில் பற்றி உங்கள் எண்ணங்கள், உணர்வுகள், கருத்துக்களை பகிர்ந்து கொள்ளவும்',
];
