<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Assessment Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during the assessment for various
    | messages that we need to display to the user that fall outside the data
    | driven text
    |
    */

    'welcome' => 'Witamy w Responsible Trade Worldwide',
    'language' => 'Wybierz język',
    'next' => 'Dalej',
    'end' => 'Zakończyć moją ocenę',
    'please_select' => 'Proszę wybrać',
    'your_details' => 'Dane osobowe',
    'thank_you' => 'Dziękujemy',
    'additional' => 'Proszę podzielić się swoimi przemyśleniami , uczucia i poglądy na temat swojej pracy tutaj',
];
