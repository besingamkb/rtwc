<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Assessment Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during the assessment for various
    | messages that we need to display to the user that fall outside the data
    | driven text
    |
    */

    'welcome' => 'ਨਮਸਕਾਰ ਅਤੇ ਰਿਸਪੌਂਸੀਬਲ ਟ੍ਰੇਡ ਵਰਲਡਵਾਈਡ ਵਿਚ ਤੁਹਾਡਾ ਸੁਆਗਤ ਹੈ',
    'language' => 'ਕਿਰਪਾ ਕਰਕੇ ਭਾਸ਼ਾ ਚੁਣੋ',
    'next' => 'ਅਗਲਾ',
    'end' => 'ਅੰਤ ਿਨਰਧਾਰਨ',
    'please_select' => 'ਕਿਰਪਾ ਕਰਕੇ ਚੁਣੋ',
    'your_details' => 'ਤੁਹਾਡੇ ਵੇਰਵੇ',
    'thank_you' => 'ਸ਼ੁਕਰੀਆ',
    'additional' => 'ਆਪਣੇ ਕੰਮ ਵਾਲੀ ਦੇ ਬਾਰੇ ਇੱਥੇ ਆਪਣੇ ਵਿਚਾਰ, ਭਾਵਨਾ ਅਤੇ ਵਿਚਾਰ ਸ਼ੇਅਰ ਕਰੋ ਜੀ',
];
