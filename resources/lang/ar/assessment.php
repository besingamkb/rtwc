<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Assessment Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during the assessment for various
    | messages that we need to display to the user that fall outside the data
    | driven text
    |
    */

    'dir' => 'rtl',
    'welcome' => 'أهلاً ومرحبًا بك في التجارة المسؤولة عالميًا',
    'language' => 'يرجى اختيار اللغة',
    'next' => 'التالي',
    'end' => 'إنهاء التقييم الخاصة بي',
    'please_select' => 'يرجى الاختيار',
    'your_details' => 'التفاصيل الخاصة بك',
    'thank_you' => 'شكرًا لك',
    'additional' => 'يرجى حصة أفكارك، والمشاعر والآراء حول مكان عملك هنا',
];
