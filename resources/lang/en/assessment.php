<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Assessment Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during the assessment for various
    | messages that we need to display to the user that fall outside the data
    | driven text
    |
    */

    'dir' => 'ltr',
    'welcome' => 'Hello and Welcome to Responsible Trade Worldwide',
    'language' => 'Language Selection',
    'en' => 'English',
    'es' => 'español',
    'pt' => 'português',
	'pa' => 'ਪੰਜਾਬੀ',
	'pl' => 'polszczyzna',
	'lt' => 'lietuvių',
	'ar' => 'العربية',
    'ro' => 'Română',
    'de' => 'Deutsche',
    'fa' => 'فارسی',
    'gu' => 'ગુજરાતી',
    'hi' => 'हिंदी',
    'ru' => 'Pусский',
    'ta' => 'தமிழ்',
    'zh' => '中文',
    'next' => 'Next',
    'end' => 'End My Assessment',
    'please_select' => 'Please select',
    'your_details' => 'Your details',
    'thank_you' => 'Thank You',
    'additional' => 'Please share your thoughts, feelings and views about your workplace here',
];
