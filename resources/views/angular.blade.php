<!doctype html>
<html ng-app="app" ng-strict-di>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />

    <link rel="stylesheet" href="{!! elixir('css/vendor.css') !!}">
    <link rel="stylesheet" href="{!! elixir('css/app.css') !!}">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <title>Responsible Trade Worldwide</title>
    <link rel="shortcut icon" type="image/x-icon" href="/img/favicons/favicon.ico"/>

    <script src="https://use.typekit.net/qlo8dpo.js"></script>
    <script>try{Typekit.load({ async: false });}catch(e){}</script>

    <!--[if lte IE 10]>
    <script type="text/javascript">document.location.href = '/unsupported-browser'</script>
    <![endif]-->
</head>
<body ng-class="[{'show-banner':$root.showTestBanner}, $root.bodyClass(), {'nav-open':$root.navOpen}]">

<header class="header" ui-view="header" ng-if="$root.user && $root.hideHeader == false"></header>
<aside class="sidebar" ng-class="$root.sidebar" ui-view="sidebar" ng-if="$root.user && $root.hideSidebar == false"></aside>

<div ui-view="main" ng-class="{
    'no-sidebar': $root.hideSidebar == true,
    'no-header': $root.hideHeader == true
}"></div>

<div class="view-loading-cover" ng-class="{
        'hidden':$root.hideViewLoadingCover,
        'no-sidebar': $root.hideSidebar == true,
        'no-header': $root.hideHeader == true
    }">
    <div class="sk-folding-cube">
        <div class="sk-cube1 sk-cube"></div>
        <div class="sk-cube2 sk-cube"></div>
        <div class="sk-cube4 sk-cube"></div>
        <div class="sk-cube3 sk-cube"></div>
    </div>
</div>
<div class="site-loading-cover" ng-class="{
        'hidden':$root.hideSiteLoadingCover
    }">
    <div class="sk-wave">
        <div class="sk-rect sk-rect1"></div>
        <div class="sk-rect sk-rect2"></div>
        <div class="sk-rect sk-rect3"></div>
        <div class="sk-rect sk-rect4"></div>
        <div class="sk-rect sk-rect5"></div>
    </div>
</div>

<script src="{!! elixir('js/vendor.js') !!}"></script>
<script src="{!! elixir('js/partials.js') !!}"></script>
<script src="{!! elixir('js/app.js') !!}"></script>
</body>
</html>
