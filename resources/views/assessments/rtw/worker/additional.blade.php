@extends('assessments.app')

@section('content')

<section class="main-content additional">
	<div class="container">
    	<div class="row">
            <div class="col-xs-12 text-center">
            	<h1 class="brand-primary">{{ $assessment->questionSet->welcome_text }}</h1>
				{!! $assessment->questionSet->intro_text !!}
            </div>
        </div>
       	@if ($errors->any())
            <div class="row">
                <div class="col-md-12">
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }} </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
		
		{!! Form::open(array('route' => array('assessment-additional-store', $assessment))) !!}

			<div class="row question">
				<div class="col-md-12 text-center">
					<div class="form-group">
						<div class="additional-text">@lang('assessment.additional')</div>
					
						{!! Form::textarea('additional') !!}
					</div>
				</div>
			</div>


			<div class="row">
				<div class="col-md-12 text-center">
		    		{!! Form::submit(trans('assessment.next'), ['class' => 'next-btn btn btn-lg btn-brand-primary']) !!}

					<div class="loader">
						<img src="/img/loading.gif">
					</div>
				</div>
			</div>

		{!! Form::close() !!}
	</div>
</section>

@endsection