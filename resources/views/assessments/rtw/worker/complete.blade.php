@extends('assessments.app')


@section('content')

    <section class="main-content complete">
        <section class="main-content questions">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        {!! $assessment->questionSet->finished_text !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <a class="end-btn btn btn-lg btn-brand-primary"
                           href="{{ route('assessment-locales', $assessment) }}">@lang('assessment.end')</a>
                    </div>
                </div>
            </div>
        </section>

    </section>

@endsection
