@extends('assessments.app')

@section('content')

    <section class="main-content demographics">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h1 class="brand-primary">{{ $assessment->questionSet->welcome_text }}</h1>
                    <div class="intro-text">{!! nl2br(e($assessment->questionSet->intro_text)) !!}</div>
                    <hr />
                    <h2>@lang('assessment.your_details')</h2>
                </div>
            </div>
            @if ($errors->any())
                <div class="row">
                    <div class="col-md-12">
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }} </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif
            <div class="row">
                <div class="col-xs-12 text-center">

                    {!! Form::open(array('route' => array('assessment-demographics-store', $assessment, App::getLocale()), 'class' => 'form-horizontal')) !!}

                    @foreach ($questionSet->questions as $question)

                        <div class="form-group">

                            <div class="col-sm-6 text-right">
                                {!! Form::label(config('assessment.form_name_prefix') . $question->id, $question->text . '?') !!}

                                @if ($question->additional_text!="")
                                    <i class="fa fa-info-circle btn-popover" data-toggle="popover" tabindex="0"
                                       data-trigger="focus" title="{{ $question->text }}"
                                       data-content="{{ htmlentities(nl2br(e($question->additional_text))) }}"></i>
                                @endif
                            </div>


                            <div class="col-sm-6">

                                {!! Form::select(config('assessment.form_name_prefix') . $question->id, $question->options->lists('text', 'id'), null, ['placeholder' => trans('assessment.please_select') . "...", 'class' => 'form-control']); !!}

                            </div>
                        </div>

                    @endforeach

                    {!! Form::submit(trans('assessment.next'), ['class' => 'btn-submit btn btn-lg btn-brand-primary']) !!}

                    <div class="loader">
                        <img src="/img/loading.gif">
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </section>

@endsection
