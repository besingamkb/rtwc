@extends('assessments.app')

@section('content')

<section class="main-content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
				<h1 class="brand-primary">{{ $assessment->questionSet->welcome_text }}</h1>
				<h2>@lang('assessment.language')</h2>
            </div>
        </div>
        <div class="row text-center flags">
			@foreach ($assessment->getLocales() as $locale)
				<div class="col-xs-6 col-md-3 text-center">
					<a href="{{ route('assessment-demographics', [$assessment, $locale]) }}">
						<img src="{{ asset( 'img/flags/' . $locale . '.png' ) }}" />
						<div class="local-name">@lang('assessment.' . $locale)</div>
					</a>
				</div>
			@endforeach
        </div>
    </div>
</section>

@endsection