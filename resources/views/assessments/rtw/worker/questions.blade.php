@extends('assessments.app')

@section('content')

<section class="main-content questions">
	<div class="container">
    	<div class="row">
            <div class="col-xs-12 text-center">
            	<h1 class="brand-primary">{{ $assessment->questionSet->welcome_text }}</h1>
				<div class="intro-text">{!! nl2br(e($assessment->questionSet->intro_text)) !!}</div>
                <hr />
            </div>
        </div>
       	@if ($errors->any())
            <div class="row">
                <div class="col-md-12">
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }} </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
		
		{!! Form::open(array('route' => array('assessment-questions-store', $assessment))) !!}

			@foreach ($questions as $key => $question)

				<div class="row question">
					<div class="col-md-12 text-center">
						<div class="form-group">
							<div class="question-text">{{ $question->text }}</div>
						
							@foreach ($question->options as $option)
								{!! Form::radio(config('assessment.form_name_prefix') . $question->id, $option->id, false, array('id' => 'option-' . $option->id)) !!}
								{!! Form::label('option-' . $option->id, $option->text) !!}
							@endforeach
						</div>
					</div>
				</div>

			@endforeach

			<div class="row">
				<div class="col-md-12 text-center">
		    		{!! Form::button(trans('assessment.next'), ['class' => 'next-btn btn btn-lg btn-brand-primary']) !!}

					<div class="loader">
						<img src="/img/loading.gif">
					</div>
				</div>
			</div>

		{!! Form::close() !!}
	</div>
</section>

@endsection