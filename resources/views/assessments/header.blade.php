<header class="header">
    <div class="top-bar">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <div class="logo-container">
                        <img class="logo" src="{{ asset( 'img/logo.png') }}" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>