<!DOCTYPE html>
<html>
	<head>
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">

	    <title>Responsible Trade Worldwide</title>
	    <link rel="shortcut icon" type="image/x-icon" href="/img/favicons/favicon.ico" />

	    <link rel="stylesheet" href="{!! elixir('css/assessments.css') !!}">

	    <script src="https://use.typekit.net/qlo8dpo.js"></script>
		<script>try{Typekit.load({ async: false });}catch(e){}</script>

	</head>
    <body class="{{ $viewName }}" dir="@lang('assessment.dir')">
        
    	@include('assessments.header')
        @yield('content')
    	@include('assessments.footer')
    	
    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	    <script src="{!! elixir('js/assessments.js') !!}"></script>
    </body>
</html>
