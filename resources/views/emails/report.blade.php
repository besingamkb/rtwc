<p>Hiya {{ $user->name }}.</p>

<p>You’ve done it. Awesome. 🙏</p>

<p>In five short minutes you’ve set yourself up to make great decisions and take aligned actions your future self will high five you for.  And that’s Fully Expressed. 💃</p>

<p>You can access your personalised <a href="{{ route('pdf', $user->token) }}" download>report here</a>. It contains your overall scores for each of the 15 categories and  implementable suggestions to create and solidify your <i>Foundations</i>.  </p>

<p>There are two parts of the report remaining that support your <i>Inside</i> and <i>Outside Job</i>.  We’ll share these with you in a staged way over the next four months.</p>

<p>Why four months? Because we know change can be tricky and overwhelming and that we want to help you navigate it in a way that is manageable so you stick at it. </p>

<p>When it comes to self improvement experience and science prove that quick fixes don’t cut it. It takes upon average at least three months to replace a habit so it sticks. To re-program neural pathways in your brain to create more helpful thoughts can take six months or more depending on how fully you embrace it and your starting point.</p>

<p>Implementing and truly living this method to get Fully Expressed took me a year of conscious effort.  Considering most of us have 30+ years of history one year is pretty quick to make big ass changes that set you up for an awesome next 30+ years. </p>

<p>Remember you’re not alone. We’ll check in every couple of weeks with reminders, tips and hacks to support you.</p>

<p>Your life audit is better shared with a friend. So share this <a href="http://www.labellavita.co/">link</a> and invite a friend to take their own life audit so you can support one another. This is how it started for me three years ago with one of my besties Jacqui and amazingness has happened in our lives.🌱</p>

<p>Now hop to. Your future self is waiting for you.</p>

<p>With love</p>
<p>Bella</p>