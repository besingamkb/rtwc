var elixir = require('laravel-elixir');
require('./tasks/angular.task.js');
require('./tasks/bower.task.js');
require('./tasks/ngHtml2Js.task.js');
require('./tasks/protractor.task.js');
require('laravel-elixir-livereload');
require('laravel-elixir-karma');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

 elixir.config.css.autoprefix = {
    enabled: true, //default, this is only here so you know how to disable
    options: {
        cascade: true,
        browsers: ['last 2 versions', '> 1%']
    }
};

elixir(function(mix) {

    var assets = [
            'public/js/vendor.js',
            'public/js/partials.js',
            'public/js/app.js',
            'public/js/assessments.js',
            'public/css/vendor.css',
            'public/css/app.css',
            'public/css/assessments.css'
        ],

        karmaJsDir = [
            'public/js/vendor.js',
            'node_modules/angular-mocks/angular-mocks.js',
            'node_modules/ng-describe/dist/ng-describe.js',
            'public/js/partials.js',
            'public/js/app.js',
            'tests/angular/**/*.spec.js'
        ];

        // gulp.task("prefix", function() {
        //     return gulp.src(src)
        //         .pipe(plugins.if(config.sourcemaps, plugins.sourcemaps.init()))
        //         .pipe(plugins[options.pluginName](options.pluginOptions)).on('error', onError)
        //         .pipe(plugins.autoprefixer())
        //         .pipe(plugins.if(config.production, plugins.minifyCss()))
        //         .pipe(plugins.if(config.sourcemaps, plugins.sourcemaps.write('.')))
        //         .pipe(gulp.dest(options.output || config.cssOutput))
        //         .pipe(new Notification().message(options.compiler + ' Compiled with prefix!'));
        // });

    mix
        .bower()
        .angular('./angular/')
        .ngHtml2Js('./angular/**/*.html')
        .scripts('assessments.js', 'public/js/assessments.js')
        .less('./angular/**/*.less', 'public/css')
        .less('./resources/assets/less/*.less', 'public/css/assessments.css')
        .version(assets)
        .livereload('public/build/rev-manifest.json', {
            liveCSS: true
        })
        .karma({
            jsDir: karmaJsDir
        });
        // .task('prefix');
});
